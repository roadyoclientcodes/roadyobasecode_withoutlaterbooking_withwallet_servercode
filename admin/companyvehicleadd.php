<?php
if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
?>
<style>
    #regCompanyErr,#regVehicleErr,#regTitleErr,#regVehicleModelErr,#regSeatingErr,#regVehicleregErr,#regLicenseErr,#regColorErr,#regExpireErr,#regExpireInsurErr,#regExpirePermitErr
    {
        color:red;
    }
</style>
<!---Code for adding images-->		
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>

<div class="content">

    <?php
    if ($status == '5') {
        ?>

        <div style="font-size:20px;"> NEW VEHICLE</div>
        <?php
    }
    ?>



    <div style="float:right;">
        <?php
        if ($status == '1' || $status == '5') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" data="3" data-msg="active"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">ADD</a></button>    
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="2" data-msg="active">ACCEPTED</button>   
        <?php }
        ?>
        <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="EditButton" data="5" data-msg="edit"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">EDIT</a></button>


    </div>
    <div style="float:none;"></div>
    <div id="refresh_table"><?php require ('refreshvechiledetail.php'); ?></div>

</div> 

<div class="modal" id="modal_default_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal with form</h4>
            </div>
            <div class="modal-body clearfix">

                <div class="controls">
                    <form  action="submitvechiles.php" name="new_vehicle" autocomplete="on" style="color:#000;" method="post" id="addVehicleForm" enctype="multipart/form-data"> 
                        <h1>ADD A VEHICLE</h1> 




                         <p> 
                            <label for="text" class="youpasswd" >Company Name <span style="color:red">*</span></label>
				
                                <?php
                                $get_vechile_type = "select * from company_info where company_id='".$_SESSION['admin_id']."'";
                                $get_vechile_type_res = mysql_query($get_vechile_type, $db1->conn);
                                $typelist = mysql_fetch_assoc($get_vechile_type_res);
								$getcompanyname=$typelist['companyname'];
								
                                   
                                
                                ?>
                   
							 <input id="companyname" name="companyname" required="required" class="editcompany" type="text" value="<?php echo $getcompanyname;?>" disabled /> 
							
							<span id="regCompanyErr"></span>
							
                          
							
                        </p>


                        <p>
                            <label for="usernames" class="uname" >Choose Vehicle Type<span style="color:red">*</span></label>
                            <select id="getvechiletype" name="getvechiletype" class="editvehicletype">
                                <option value="NULL">select Vehicle Type </option>
                                <?php
                                $get_vechile_type = "select type_id,type_name from workplace_types";
                                $get_vechile_type_res = mysql_query($get_vechile_type, $db1->conn);
                                while ($typelist = mysql_fetch_array($get_vechile_type_res)) {
                                    echo "<option value='" . $typelist['type_id'] . "' id='" . $typelist['type_id'] . "'>" . $typelist['type_name'] . "</option>";
                                }
                                ?>
                            </select>
                            <span id="regVehicleErr"></span>
                        </p>


                        <p> 
                            <label for="username" class="uname" >Choose Vehicle Make<span style="color:red">*</span></label>
                            <select id="title" name="title" class="edit_title">

                                <option value="NULL">Select a vehicle:</option>
                                <?php
                                $adv_sql = "SELECT * FROM vehicleType";
                                $adv_sql_res = mysql_query($adv_sql, $db1->conn);
                                while ($adv_sql_row = mysql_fetch_array($adv_sql_res)) {
                                    echo "<option value='" . $adv_sql_row['id'] . "' id='" . $adv_sql_row['id'] . "'>" . $adv_sql_row['vehicletype'] . "</option>";
                                }
                                ?>


                            </select>
                            <span id="regTitleErr"></span>
                        </p>

                        <p>
                            <label for="username" class="uname" >Choose a Vehicle Model<span style="color:red">*</span></label>
                            <select id="vehiclemodel" name="vehiclemodel" class="editvehiclemodel">

                                <option value="NULL">Select a vehicle Model:</option>
                                <?php
                                ?>
                            </select>
                            <span id="regVehicleModelErr"></span>
                        </p>






                        <p> 
                            <label for="text" class="youpasswd" > Vehicle Reg. No<span style="color:red">*</span> </label>
                            <input id="vechileregno" name="vechileregno" required="required" class="editvechileregno" type="text" placeholder=" " /> 
                            <span id="regVehicleregErr"></span>
                        </p>
                        <p> 
                            <label for="text" class="youpasswd" >Licence Plate No.<span style="color:red">*</span></label>
                            <input id="licenceplaetno" name="licenceplaetno" required="required" class="editlicenceplaetno" type="text" placeholder="eg. KA-05/1800" /> 
                            <span id="regLicenseErr"></span>
                        </p>

                        <p> 
                            <label for="text" class="youpasswd" >Insurance Number<span style="color:red">*</span></label>
                            <input id="Vehicle_Insurance_No" name="Vehicle_Insurance_No" required="required" class="editinsuranceno" type="text" placeholder="eg. PL-23111441" /> 
                            <span id="regLicenseErr"></span>
                        </p>


                        <p> 
                            <label for="text" class="youpasswd" >vechile Color<span style="color:red">*</span></label>
                            <input id="vechilecolor" name="vechilecolor" required="required" class="editvechilecolor" type="text" placeholder="eg. Metallic blue" /> 
                            <span id="regColorErr"></span>
                        </p>

                        <p>


                        <div id="fileuploader">Upload Certificate of Registration<span style="color:red">*</span><input type="file" name="certificate" class="certificate" id="file_upload" /></div>
                        <div id="preview_img1"></div>
                        <span id="uploadrc"></span>

                        </p>
                        <p>

                            <label for="text" class="youpasswd" >Expiration Date<span style="color:red">*</span></label>
                            <input id="expirationrc" name="expirationrc" required="required" value=" " class="editexpire" type="date" placeholder=" " /> 
                            <span id="regExpireErr"></span>

                        </p>
                        <p>


                        <div id="fileuploader">Upload Motor Insurance Certificate<span style="color:red">*</span><input type="file" name="insurcertificate" id="file_upload" /></div>
                        <div id="preview_img2"></div>

                        </p>

                        <p>

                            <label for="text" class="youpasswd" >Expiration Date<span style="color:red">*</span></label>
                            <input id="expirationinsurance" name="expirationinsurance" value="" required="required" class="editexpire" type="date" placeholder=" " /> 
                            <span id="regExpireInsurErr"></span>

                        </p>


                        <div id="fileuploader">Upload Contract Carriage Permit<span style="color:red">*</span><input type="file" name="carriagecertificate" id="file_upload" /></div>
                        <div id="preview_img3"></div>

                        </p>

                        <p>
                            <label for="text" class="youpasswd" >Expiration Date<span style="color:red">*</span></label>
                            <input id="expirationpermit" name="expirationpermit" value="" required="required" class="editexpire" type="date" placeholder=" " /> 
                            <span id="regExpirePermitErr"></span>
                            <input type="hidden" name="editdata" id="editdataval"/>
                        </p>

                        <div id="preview_img"></div>
                    </form>     

                </div>

            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_modal" style="color:red;border:solid 1px #a2a2a2">Close</button>
                <button type="submit" class="btn btn-success btn-clean" id="datasubmit">Submit form</button>
            </div>
        </div>
    </div>
</div>    

<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('#title').change(function() {
            var adv_id = $('#title').val();
            if (adv_id != null || adv_id != '')
                $('#vehiclemodel').load('getvehicle.php', {adv: adv_id});
        });


        $('.resetPassword').click(function() {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });

        $('#change_pass_doc').click(function() {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {company_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function(result) {
//                        alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });

        $('#RejectButton,#ActiveButton').click(function() {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function() {
                return this.value;
            }).get();

            if (values == '') {
                //alert('Please select  atleast vechile type in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 8, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function(result) {
                        //  alert(result.message);
                        //alert(result.error);
                        //alert(result.test);
                        if (result.flag == 0) {
                            $('.custom_check').each(function() {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        }
                    }
                });
            }
        });


        $('#EditButton').click(function() {
            var dis = $(this);
            var values = $('input:checkbox:checked.custom_check').map(function() {
                return this.value;
            }).get();
//            alert("values" + values);
            $.ajax({
                url: "test_upload.php",
                type: "POST",
                //data: formData,
                dataType: "JSON",
                async: false,
                data: {item_type: 5, item_list: values, add_val: 2},
                dataType: "JSON",
                        success: function(result) {
                    if (result.flag == 0) {

                        $('.editvechilemodel').val(result.Vehicle_Model);
                        $('.editvechiletype').val(result.Vehicle_Type);
                        $('.editvechileregno').val(result.Vehicle_Reg_No);
                        $('.editseating').val(result.Vehicle_Seating);
                        $('.editlicenceplaetno').val(result.License_Plate_No);
                        $('.editvechilecolor').val(result.Vehicle_Color);
                        $('.edit_title').val(result.Title);
                        $('.editvehicletype').val(result.type_id);
                        $('#expirationinsurance').val(result.expirydate);

                        alert(result.msg);
                        //  $('.editgetvechiletype').val(result.type_id);
                    } else {
                        alert("Error in getting values");
//                        alert(result.qry);
//						alert(result.msg);
                    }
                },
                error: function()
                {
                    alert("sorry,Error occured");
                }
            });
        });


        $("#datasubmit").click(function() {
            var count = 0;
            if ($('#companyname').val() == "NULL")
            {
                $('#companyname').focus();
                $('#regCompanyErr').html("Please choose a company name");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#getvechiletype').val() == "NULL")
            {
                $('#getvechiletype').focus();
                $('#regVehicleErr').html("Please choose a vehicle Type");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#title').val() == "NULL")
            {
                $('#title').focus();
                $('#regTitleErr').html("Please choose a vehicle Make");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#vehiclemodel').val() == "NULL")
            {
                $('#vehiclemodel').focus();
                $('#regVehicleModelErr').html("Please choose a vehicle Model");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#seating').val() === "")
            {
                $('#seating').focus();
                $('#regSeatingErr').html("Seating capacity is required");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#vechileregno').val() === "")
            {
                $('#vechileregno').focus();
                $('#regVehicleregErr').html("Vehicle registration number is required");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#licenceplaetno').val() === "")
            {
                $('#licenceplaetno').focus();
                $('#regLicenseErr').html("Vehicle license number is required");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#vechilecolor').val() === "")
            {
                $('#vechilecolor').focus();
                $('#regColorErr').html("Vehicle color is required");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#expirationrc').val() === "2014-05-01")
            {
                $('#expirationrc').focus();
                $('#regExpireErr').html("Expiration Date is required");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#expirationinsurance').val() === "2014-05-01")
            {
                $('#expirationrc').focus();
                $('#regExpireInsurErr').html("Expiration Date is required");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#expirationpermit').val() === "2014-05-01")
            {
                $('#expirationpermit').focus();
                $('#regExpirePermitErr').html("Expiration Date is required");
                return false;
            }
            if (count > 0)
                return false;

            var formElement = document.getElementById("addVehicleForm");
            var formData = new FormData(formElement);

            $.ajax({
                url: "test_upload.php",
                type: "POST",
                data: formData,
                dataType: "JSON",
                async: false,
                success: function(result) {
                    //alert(result.msg);
                    if (result.flag == 0) {
                        $('#close_modal').trigger('click');
                        $('#refresh_table').load('refreshvechiledetail.php', {test: 1});
                    } else {
                        alert('Error occured');
//						 alert(result.msg);
                    }
                },
                error: function(result) {
//                    alert('test1');
//                    alert(result.msg);
                    alert('Error occured');
                },
                cache: false,
                contentType: false,
                processData: false
            });

            e.preventDefault();
        });


    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>




