<?php

session_start();
error_reporting(0);
include('../Models/ConDB.php');
$db = new ConDB();
$file_formats = array("jpg", "png", "gif", "jpeg", "PNG", "JPEG", "JPG", "GIF");

$filepath = "../pics/";


$name = $_FILES['agent_profile_pic']['name']; // filename to get file's extension
$size = $_FILES['agent_profile_pic']['size'];

function _serverUpload($args) {
    $local_directory = dirname(__FILE__);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_URL, "http://104.236.223.211/uploader.php");
    //most importent curl assues @filed as file field
//    echo $local_directory;
    $post_array = array(
        "my_file" => "@" . $local_directory . '/' . $args['file_name'],
        "upload" => "Upload",
        "dir_to_upload" => $args['file_name']
    );
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_array);
    $response = curl_exec($ch);
    return $response;
}

if (!strlen($name)) {
    echo "Please select image..!";
    return false;
}

$ext = substr($name, strrpos($name, '.') + 1);
if (!in_array($ext, $file_formats)) {
    echo json_encode(array('msg' => "Invalid file format.", 'flag' => 1));
    exit();
}// check it if it's a valid format or not
if ($size > (2048 * 1024)) { // check it if it's bigger than 2 mb or no
    echo "Your image size is bigger than 2MB.";
    exit();
}

$imagename = md5(uniqid() . time()) . "." . $ext;
$file_to_open = $filepath . $imagename;
$tmp1 = $_FILES['agent_profile_pic']['tmp_name'];
$move = move_uploaded_file($tmp1, $file_to_open);
if (!$move) {

    echo "Could not move the file.";
    exit();
}

$updateDoctorPicQry = "update master set profile_pic = '" . $imagename . "' where mas_id = '" . $_REQUEST['mas_id'] . "'";
mysql_query($updateDoctorPicQry, $db->conn);

if (mysql_affected_rows() <= 0) {
    echo "Pic not updated";
    exit();
}
$location = $db->mongo->selectCollection('location');

$newdata = array('$set' => array("image" => $imagename));
$location->update(array("user" => (int) $_REQUEST['mas_id']), $newdata);



list($width, $height) = getimagesize($file_to_open);

$ratio = $height / $width;



/* mdpi 36*36 */
$mdpi_nw = 36;
$mdpi_nh = $ratio * 36;

$mtmp = imagecreatetruecolor($mdpi_nw, $mdpi_nh);

if ($ext == "jpg" || $ext == "jpeg" || $ext == "JPG" || $ext == "JPEG") {
    $new_image = imagecreatefromjpeg($file_to_open);
} else if ($ext == "gif" || $ext == "GIF") {
    $new_image = imagecreatefromgif($file_to_open);
} else if ($ext == "png" || $ext == "PNG") {
    $new_image = imagecreatefrompng($file_to_open);
}
imagecopyresampled($mtmp, $new_image, 0, 0, 0, 0, $mdpi_nw, $mdpi_nh, $width, $height);

$mdpi_file = '../pics/mdpi/' . $imagename;

imagejpeg($mtmp, $mdpi_file, 100);

/* HDPI Image creation 55*55 */
$hdpi_nw = 55;
$hdpi_nh = $ratio * 55;

$tmp = imagecreatetruecolor($hdpi_nw, $hdpi_nh);

if ($ext == "jpg" || $ext == "jpeg" || $ext == "JPG" || $ext == "JPEG") {
    $new_image = imagecreatefromjpeg($file_to_open);
} else if ($ext == "gif" || $ext == "GIF") {
    $new_image = imagecreatefromgif($file_to_open);
} else if ($ext == "png" || $ext == "PNG") {
    $new_image = imagecreatefrompng($file_to_open);
}
imagecopyresampled($tmp, $new_image, 0, 0, 0, 0, $hdpi_nw, $hdpi_nh, $width, $height);

$hdpi_file = '../pics/hdpi/' . $imagename;

imagejpeg($tmp, $hdpi_file, 100);

/* XHDPI 84*84 */
$xhdpi_nw = 84;
$xhdpi_nh = $ratio * 84;

$xtmp = imagecreatetruecolor($xhdpi_nw, $xhdpi_nh);

if ($ext == "jpg" || $ext == "jpeg" || $ext == "JPG" || $ext == "JPEG") {
    $new_image = imagecreatefromjpeg($file_to_open);
} else if ($ext == "gif" || $ext == "GIF") {
    $new_image = imagecreatefromgif($file_to_open);
} else if ($ext == "png" || $ext == "PNG") {
    $new_image = imagecreatefrompng($file_to_open);
}
imagecopyresampled($xtmp, $new_image, 0, 0, 0, 0, $xhdpi_nw, $xhdpi_nh, $width, $height);

$xhdpi_file = '../pics/xhdpi/' . $imagename;

imagejpeg($xtmp, $xhdpi_file, 100);

/* xXHDPI 125*125 */
$xxhdpi_nw = 125;
$xxhdpi_nh = $ratio * 125;

$xxtmp = imagecreatetruecolor($xxhdpi_nw, $xxhdpi_nh);

if ($ext == "jpg" || $ext == "jpeg" || $ext == "JPG" || $ext == "JPEG") {
    $new_image = imagecreatefromjpeg($file_to_open);
} else if ($ext == "gif" || $ext == "GIF") {
    $new_image = imagecreatefromgif($file_to_open);
} else if ($ext == "png" || $ext == "PNG") {
    $new_image = imagecreatefrompng($file_to_open);
}
imagecopyresampled($xxtmp, $new_image, 0, 0, 0, 0, $xxhdpi_nw, $xxhdpi_nh, $width, $height);

$xxhdpi_file = '../pics/xxhdpi/' . $imagename;

imagejpeg($xxtmp, $xxhdpi_file, 100);
//
//$serverUpload[] = _serverUpload(array('file_name' => $file_to_open));
//$serverUpload[] = _serverUpload(array('file_name' => $mdpi_file));
//$serverUpload[] = _serverUpload(array('file_name' => $hdpi_file));
//$serverUpload[] = _serverUpload(array('file_name' => $xhdpi_file));
//$serverUpload[] = _serverUpload(array('file_name' => $xxhdpi_file));

echo "<img src='../pics/xxhdpi/" . $imagename . "' style='width: 200px;height:200px;' />";
exit();
?>