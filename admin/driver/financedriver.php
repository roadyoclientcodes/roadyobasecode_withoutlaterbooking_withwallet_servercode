<?php
session_start();
include('../../Models/ConDB.php');
$db1 = new ConDB();
if (isset($_GET['mas_id'])) {
    $_SESSION['admin_ids'] = $_GET['mas_id'];
}

?>
<script type='text/javascript' src='js/settings.js'></script>
<script type='text/javascript' src='js/actions.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 10, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null]});
    });
</script>
<!--<div class="page-content page-content-white" style="margin: 0;">-->
<!--alert = function() {};-->

<div style="text-align: center;color: white;"><h1>TRANSACTION HISTORY</h1></div>

<div class="content">
    <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
        <thead style="font-size: 12px;">
            <tr>
                <th>S NO</th>
                <th>DATE</th>
                <th>TIME</th>   
                <th>PASSENGER</th> 
                <th>DRIVER</th>
                <th>CHARGE ID</th>
                <th>TRANSACTION ID</th>
                <th>AMOUNT</th>
                <th>STATUS</th>
                <th>DETAILS</th>
            </tr>
        </thead>
        <tbody style="font-size: 12px;">

            <?php

            $accQry = "select ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.txn_id,ap.status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status > 4 and d.mas_id='".$_SESSION['admin_ids']."' order by ap.appointment_id DESC";
            $result1 = mysql_query($accQry, $db1->conn);
            $i = 1;
            while ($row = mysql_fetch_assoc($result1)) {
                ?>

                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo date('d-m-Y',  strtotime($row['appointment_dt'])); ?></td>
                    <td><?php echo date('h:i A',  strtotime($row['appointment_dt'])); ?></td>
                    <td><?php echo $row['pat_email'] ?></td>
                    <td><?php echo $row['doc_email']; ?></td>
                    
                    <td><?php echo $row['inv_id']; ?></td>
                    <td><?php echo $row['txn_id']; ?></td>
                    <td><?php echo round($row['amount'],2); ?></td>
                    <td><?php echo $row['status']; ?></td>
                   <td class="detail_view_finance" id="<?php echo $row['appointment_id']; ?>"><a href="#modal_default_14" data-toggle="modal" class="btn btn-default" style="background: #555;color: white;">Details</a></td>
                
                </tr>
                <?php
                $i++;
            }
            ?> 

        </tbody>
    </table>     
       
<div class="modal modal-white" id="modal_default_14" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 120%;">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Appointment Details</h4>
            </div>                
            <div class="modal-body clearfix">
                <div id="appt_details_finance"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>              
            </div>
        </div>
    </div>
</div> 
</div> 

<script>
     $(document).on('click', '.detail_view_finance', function() {
            var dis = $(this).attr('id');
//            alert(dis);
            $('#appt_details_finance').html('');
            $.ajax({
                type: "POST",
                url: "getBookingDetailsdriver.php",
                data: {booking: dis},
//                dataType: "JSON",
                success: function(result) {
//                    alert(result);
                    $('#appt_details_finance').html(result);
                }
            });

        });
    </script>
