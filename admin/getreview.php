<?php
session_start();
include('../Models/ConDB.php');
$db = new ConDB();


if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '2';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 26, "aLengthMenu": [26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null]});
    });
</script>

<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/Plugins_8.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>
<!--<script type='text/javascript' src='js/plugins/uniform/jquery.uniform.min.js'></script>-->

<div class="page-content page-content-white" style="margin-left: 1%;">


    <?php
    if ($status == '1') {
        ?>

        <div style="font-size:20px;">ACTIVE DRIVER REVIEW</div>
        <?php
    }
    ?>
    <?php
    if ($status == '2') {
        ?>

        <div style="font-size:20px;">INACTIVE DRIVER REVIEW</div>
        <?php
    }
    ?>


    <!--<button type="button" style="margin-left: 71%;" class="btn btn-success btn-clean" id="ActiveButton">ACTIVE</button> --> 
    <?php
    if ($status == '1') {
        ?>
        <button type="button" class="btn btn-danger btn-clean" id="RejectButton" data="2" style="margin-left: 80%;" data-msg="inactive">DEACTIVATE</button>
        <?php
    }
    if ($status == '2') {
        ?>
        <button type="button" class="btn btn-success btn-clean" id="ActiveButton" data="1" style="margin-left: 80%;" data-msg="active">ACTIVATE</button>  

        <?php
    }
    ?>
    <!-- <a href="#" class="widget-icon widget-icon-dark" id="buttonClass" style=" color: rgb(247, 0, 0); background-color: blue;float: right;
        margin-Right: 80px;"><span class="icon-trash"></span></a>-->

 <!--   <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
        <thead style="font-size: 12px;">
            <tr>

                <th width="5%">SL NO</th>
                <th width="5%">DRIVER ID</th>
                <th width="25%">REVIEW</th>

                <th width="10%">REVIEW DATE</th>  
                <th width="8%">DRIVER NAME</th> 
                <th width="8%">PASSENGER NAME</th> 

                <th width="5%">STATUS</th> 
                <th width="5%">SELECT</th>

            </tr>
        </thead>
        <tbody style="font-size: 12px;">

    <?php
    if ($cityid == '') {
        $accQry = "SELECT r.rating,r.review, r.status, r.review_dt, r.mas_id, d.first_name AS doctorname, p.first_name AS patientname FROM master_ratings r, master d, slave p WHERE r.slave_id = p.slave_id  AND r.mas_id = d.mas_id  AND r.status IN (" . $status . ") and r.review != '' order by r.review_dt desc";
    } else {

        $accQry = "SELECT r.rating,r.review, r.status, r.review_dt, r.mas_id, d.first_name AS doctorname, p.first_name AS patientname FROM master_ratings r, master d, slave p WHERE  r.slave_id = p.slave_id  and r.review != '' AND r.mas_id = d.mas_id and d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . ")) AND r.status IN (" . $status . ") order by r.review_dt desc";
    }
    $result1 = mysql_query($accQry, $db->conn);
    $i = 1;
    while ($row = mysql_fetch_assoc($result1)) {

        $st = "";
        if ($row['status'] == '1') {
            $st = "Active";
        } else {
            $st = "INACTIVE";
        }
        ?>



                    <tr id="doc_rows<?php echo $i; ?>">




                        <td><?Php echo $i; ?></td>
                        <td><?Php echo $row['mas_id'] ?></td>
                        <td><?Php echo $row['review'] ?></td>
                        <td><?Php echo $row['review_dt']; ?></td>
                        <td><?Php echo $row['doctorname']; ?></td>
                        <td><?Php echo $row['patientname']; ?></td>
                        <td><?Php echo $st; ?></td>

                        <td><input type="checkbox" dat="<?php echo $i; ?>" name="checkbox_advertiser" value="<?php echo $row['review_dt']; ?>" class="custom_check"  /></td>

                    </tr>
    <?php
    $i++;
}
?> 

        </tbody>
    </table>        -->                                
    <div id="refresh_table">

<?php require ('refreshgetreview.php'); ?>
    </div>
</div>



<script>
    $(document).ready(function () {
//            alert('1');


        $('#ActiveButton,#RejectButton').click(function () {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;


            }).get();
            // alert(values);
            if (values == '') {
                alert('Please select  atleast one review in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "active_inactivereview.php",
                    data: {item_type: 1, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function (result) {
                        alert(result.message);
                        if (result.flag == 0) {
                            $('.custom_check').each(function () {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        }
                    }
                });

            }
        });

    });
</script>
