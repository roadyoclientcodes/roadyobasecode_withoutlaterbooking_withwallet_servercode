<?php
//session_start();
require_once '../Models/ConDB.php';

$db1 = new ConDB();
?>


<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>

<script type="text/javascript">


    $(document).ready(function () {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 20, "aLengthMenu": [20], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null]});
    });
</script>

<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>
<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
    <thead style="font-size: 12px;">
        <tr>
            <th>S No</th>
            <th>User name</th>
            <th>Email</th>
            <th>Change Password</th>
            <th>Remove</th>
        </tr>
    </thead>
    <tbody style="font-size: 12px;">

        <?php
        $i = 1;
        $getUsers = "select * from superadmin where user_type = 1";
        $result1 = mysql_query($getUsers, $db1->conn);
        while ($row = mysql_fetch_assoc($result1)) {
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row['username'] ?></td>
                <td><?php echo $row['email'] ?></td>
                <td><a href="#modal_default_12" style="color:red;text-decoration: underline;" data="<?php echo $row['id']; ?>" class="resetPassword" data-toggle="modal"> <?php echo "ReSetpassword" ?></a></td>
                <td><input type="button" class="delete_user" data="<?php echo $row['id']; ?>" value="Delete"/></td>
            </tr>

            <?php
            $i++;
        }
        ?> 

    </tbody>    
</table>    

<script type="text/javascript">
    $(document).ready(function () {
        $('.resetPassword').on('click', function () {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });
        $('#change_pass_doc').click(function () {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "updatePass.php",
                    data: {mas_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function (result) {
                        alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });

    });
</script>