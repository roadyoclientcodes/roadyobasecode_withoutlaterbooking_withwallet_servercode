<?php
session_start();
include('../Models/ConDB.php');
$db1 = new ConDB();

if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_GET['mas_id'])) {
    $_SESSION['admin_ids'] = $_GET['mas_id'];
}
?>
<script type='text/javascript' src='js/settings.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 11, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null, null]});
    });
</script>
<script type='text/javascript' src='js/actions.js'></script>
<!--<div class="page-content page-content-white" style="margin: 0;">-->
<!--alert = function() {};-->
<div class="modal" id="modal_default_4"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style=" color: white;background: white;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <!--<h4 class="modal-title">ADD  the Program Details..</h4>-->
            </div>
            <div class="modal-body clearfix" style="position: relative;">
                <div id="spinner_image" style="display:none;position:absolute;top:40%;left:40%;"><img src="img/spinner.gif" /></div>
                <!--<form id="popup1" action="Report1.php" method="Post">-->
                <table class="table table-bordered table-striped table-hover">
                    <tbody>


                        NEW PASSWORD: <input type="text"   name="pass" style="width: 200px;margin-left: 130px;margin-top: -23px;" id="chng_pass">&nbsp;<span style="color:red;" id="errmsg"></span><br> <br>
                    CONFIRM PASSWORD <input type="text" name="conf_pass" style="width: 200px;margin-left: 130px;
                                            margin-top: -23px;" id="chng_conf_pass" ><br><br>
                    <input type="hidden" name="sendData" id="sendData"/>
                    </tbody>
                </table>        
                <!--</form>-->
                <?php
                if (isset($_REQUEST['title'])) {
                    $delete_admin_id1 = "Update doctor Set password='" . $_REQUEST['title'] . "' where doc_id= '<?php echo $i; ?>";
                    $delete_admin_val = mysql_query($delete_admin_id1);
                }
                ?>
            </div>
            <div class="modal-footer">                
                <button type="button"  style="margin-right: 26%; width: 90px;" class="btn btn-warning btn-clean" data-dismiss="modal" id="close_popup">Cancel</button>
                <button type="button" style="float: left;margin-left: 30%;" name="Add" id="change_pass" class="btn btn-warning btn-clean">Submit form</button>
            </div>
        </div>
    </div>
</div>  
<div class="content">
    <div style="float:right;">
        <?php
        if ($status == '1') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="1" data-msg="active">BOOKING DETAIL</button>    
            <?php
        }
        ?>
    </div>

    <div style="float:none;"></div>
    <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
        <thead style="font-size: 12px;">
            <tr>
                <th width="8%">SL ID</th>
                <th width="8%">BOOKING ID</th>
                <th width="8%">DRIVER ID</th>
                <th width="8%">DRIVER NAME</th>   
                <th width="8%">PASSENGER NAME</th> 
                <th width="8%">PICKUP ADDRESS</th>
                <th width="8%">DROP ADDRESS</th>
                <th width="8%">PICKUP TIME & DATE</th>
                <th width="8%">DISTANCE(IN METERS)</th>
                <th width="8%">STATUS</th>
                <th width="8%">SELECT</th>
            </tr>
        </thead>
        <tbody style="font-size: 12px;">
<?php
$accQry = "select a.appointment_id,a.distance_in_mts,a.appointment_dt,a.drop_addr1,a.drop_addr2,a.mas_id,a.slave_id,d.first_name as doc_firstname,d.last_name as doc_lastname,p.first_name as patient_firstname,p.last_name as patient_lastname,a.address_line1,a.address_line2,a.status from appointment a,master d,slave p where a.slave_id=p.slave_id and d.mas_id=a.mas_id and a.status IN (7) and a.slave_id='" . $_SESSION['admin_idsnew'] . "' order by a.appointment_id desc";
$result1 = mysql_query($accQry, $db1->conn);
$i = 1;
while ($row = mysql_fetch_assoc($result1)) {
    $st = "";
    if ($row['status'] == '1') {
        $st = "NEW";
    } else if ($row['status'] == '3' || $row['status'] == '4') {
        $st = "ACCEPTED";
    } else if ($row['status'] == '5' || $row['status'] == '6') {
        $st = "REJECTED";
    }

    if ($row['profile_pic'] == "") {

        $st1 = "aa_default_profile_pic.gif";
    } else {




        $st1 = $row['profile_pic'];
    }
    ?>




                <tr id="doc_rows<?php echo $i; ?>">
                    <td><?php echo $i; ?></td>
                    <td   id="<?Php echo "appointment_id" . $i; ?>"><?Php echo $row['appointment_id'] ?></td>
                    <td><?php echo $row['mas_id']; ?></td>
    <?php $fullname_doc = $row['doc_firstname'] . " " . $row['doc_lastname']; ?>
                    <td><a target="_blank" href="docProfile.php?doc_id=<?Php echo $row['mas_id'] ?>" data="<?php echo $i; ?>" data-toggle="modal"><?php echo $fullname_doc; ?> </a></td>

                    <?php $fullname_patient = $row['patient_firstname'] . " " . $row['patient_lastname']; ?>
                    <?php $address = $row['address_line1'] . " " . $row['address_line2']; ?>
    <?php $pickup = $row['drop_addr1'] . " " . $row['drop_addr2']; ?>
                    <td><?php echo $fullname_patient; ?></td>
                    <td id="<?php echo "pickup" . $i; ?>"><?php echo trim($pickup, "%20"); ?></td>
                    <td id="<?Php echo "address" . $i; ?>"><?php echo trim($address, "%20"); ?></td>
                    <td id="<?php echo $row['appointment_dt'] . $i; ?>"><?php echo $row['appointment_dt']; ?></td>
                    <td id="<?php echo $row['distance_in_mts'] . $i; ?>"><?php echo $row['distance_in_mts']; ?></td>

                    <td id="<?php echo "status" . $i; ?>">
    <?php
    if ($row['status'] == '1') {
        echo "request";
    } elseif ($row['status'] == '2') {
        echo "Driver accepted";
    } elseif ($row['status'] == '3') {
        echo "Driver rejected";
    } elseif ($row['status'] == '4') {
        echo "Passenger Cancelled";
    } elseif ($row['status'] == '5') {
        echo "Driver on way";
    } elseif ($row['status'] == '6') {
        echo "Booking start";
    } elseif ($row['status'] == "7") {
        echo "Booking Completed";
    } else {
        
    }
    ?>
                    </td>
                    <td><input dat="<?php echo $i; ?>" type="checkbox" name="checkbox_advertiser"  class="custom_check" value="<?php echo $row['appointment_id']; ?>" style="background: white;height: 20px;width: 12px;" /></td>

                </tr>
    <?php
    $i++;
}
?>

        </tbody>
    </table>                                        

</div> 

<script>
    $(document).ready(function() {
        $('.resetPassword').click(function() {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });
        $('#change_pass').click(function() {

            var pass = $('#chng_pass').val();
            var conf_pass = $('#chng_conf_pass').val();
            if (pass == '' || conf_pass == '') {
                alert('Passwords are mandatory.');
            } else if (pass != conf_pass) {
                alert('Passwords does not match, check once.');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {doc_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function(result) {
                        alert(result.message);
                        if (result.flag == 0) {


                            $('#chng_pass').val("");
                            $('#chng_conf_pass').val("");

                            $('#close_popup').trigger('click');
                            $('.modal-backdrop.in').css('display', 'none');


                        }
                    }
                });
            }
        });

        $('#ActiveButton,#RejectButton').click(function() {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function() {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast one doctor in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 1, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function(result) {
                        alert(result.message);
                        if (result.flag == 0) {
                            $('.custom_check').each(function() {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        }
                    }
                });

            }
        });

    });
</script>

<script>
    $(document).ready(function() {
//called when key is pressed in textbox
        $("#chng_pass").blur(function() {
            //if the letter is not digit then display error and don't type anything

            var zipcode_expression = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
            if (zipcode_expression.test($.trim($('#chng_pass').val())) == false) {
// $("#errmsg").html("invalid zipcode").show().fadeOut("slow")
                alert('Password must contain atleast one digit,one Uppercase, one Lower case character and atleast 8 digit ');
            }


            // $("#errmsg").html("Digits Only").show().fadeOut("slow");


        });
    });
</script>

<script>
    $(document).ready(function() {
//called when key is pressed in textbox
        $("#close_popup").click(function() {
            $('#chng_pass').val("");
            $('#chng_conf_pass').val("");

            // $('#close_popup').trigger('click');
            $('.modal-backdrop.in').css('display', 'none');


        });
    });
</script>

