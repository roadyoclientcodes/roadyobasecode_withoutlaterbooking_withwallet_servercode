<?php
include('../../Models/ConDB.php');
$db1 = new ConDB();
if (isset($_REQUEST['type'])) {
    $codeType = $_REQUEST['type'];
} else {
    $codeType = '1';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>
<script type='text/javascript' src='../js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='../js/actions.js'></script>
<script type="text/javascript">
    $(document).ready(function () {
<?php if ($codeType == 2) { ?>
            if ($("table.sortable").length > 0)
                $("table.sortable").dataTable({"iDisplayLength": 26, "aLengthMenu": [26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null]});
<?php } else { ?>
            if ($("table.sortable").length > 0)
                $("table.sortable").dataTable({"iDisplayLength": 26, "aLengthMenu": [26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null]});
    <?php
}
?>
    });</script>

<script type="text/javascript">
    $(document).ready(function () {

        $('.detail_view_finance').on('click', function () {
            var dis = $(this).attr('id');
//            alert(dis);
            $('#appt_details_finance').html('');
            $.ajax({
                type: "POST",
                url: "coupons/getCouponUsage.php",
                data: {booking: dis},
//                dataType: "JSON",
                success: function (result) {
//                    alert(result);
                    $('#appt_details_finance').html(result);
                }
            });

        });


    });
</script>
<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
    <thead style="font-size: 12px;">
        <?php
        if ($codeType == '1') {
            ?>
            <tr>
                <th>DISCOUNT</th>
                <th>REFERRAL DISCOUNT</th>
                <th>MESSAGE</th>   
                <th>CITY</th>                        
                <th>SELECT</th>
            </tr>
        <?php } else {
            ?>
            <tr>
                <th>CODE</th>
                <th>START DATE</th>
                <th>END DATE</th>
                <th>DISCOUNT</th>
                <th>MESSAGE</th>   
                <th>CITY</th>                        
                <th>SELECT</th>
            </tr>
        <?php }
        ?>
    </thead>
    <tbody style="font-size: 12px;">

        <?php
        $accQry = "select cp.*,c.city_name,c.Currency as currency from coupons cp, city c where cp.city_id = c.city_id and cp.coupon_type = '" . $codeType . "' and cp.status = '" . $_REQUEST['status'] . "' and user_type = 2";
        $result1 = mysql_query($accQry, $db1->conn);
//            echo $accQry;
        $i = 1;
        while ($row = mysql_fetch_assoc($result1)) {
            ?>
            <?php
            if ($codeType == '1') {
                ?>
                <tr>
                    <td>
                        <?php
                        $val = ($row['discount_type'] == '1') ? '%' : ' ' . $row['currency'];
                        echo $row['discount'] . $val;
                        ?>
                    </td>
                    <td>
                        <?php
                        $val1 = ($row['referral_discount_type'] == '1') ? '%' : ' ' . $row['currency'];
                        echo $row['referral_discount'] . $val1;
                        ?>
                    </td>
                    <td><?php echo $row['message']; ?></td>   
                    <td><?php echo $row['city_name']; ?></td>                        
                    <td><input type="button" value="Deactivate" class="deactivate" data="<?php echo $row['id']; ?>" dat="1"/></td>
                </tr>
            <?php } else {
                ?>
                <tr>
                    <td class="detail_view_finance" id="<?php echo $row['coupon_code']; ?>"><a href="#modal_default_14" data-toggle="modal"><?php echo $row['coupon_code']; ?></a></td>
                    <!--<td><?php // echo $row['coupon_code'];   ?></td>-->
                    <td><?php echo $row['start_date']; ?></td>
                    <td><?php echo $row['expiry_date']; ?></td>
                    <td>
                        <?php
                        $val = ($row['discount_type'] == '1') ? '%' : ' ' . $row['currency'];
                        echo $row['discount'] . $val;
                        ?>
                    </td>
                    <td><?php echo $row['message']; ?></td>   
                    <td><?php echo $row['city_name']; ?></td>                   
                    <td><input type="button" value="Deactivate" class="deactivate" data="<?php echo $row['id']; ?>" dat="2"/></td>
                </tr>
                <?php
            }
            $i++;
        }
        ?> 

    </tbody>
</table>    


<div class="modal modal-white" id="modal_default_14" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 120%;">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Usage details</h4>
            </div>                
            <div class="modal-body clearfix">
                <div id="appt_details_finance"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>              
            </div>
        </div>
    </div>
</div> 