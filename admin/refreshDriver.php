<?php
include('../Models/ConDB.php');
$db1 = new ConDB();
if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>


<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
    <thead style="font-size: 12px;">
        <tr>


            <th width="8%">DRIVER ID</th>
            <th width="8%">FIRST NAME</th>

            <th width="8%">LAST NAME</th>   
            <th width="8%">MOBILE</th> 
            <th width="8%">EMAIL</th>
            <th width="8%">REG DATE</th>

            <th width="8%">DEVICE TYPE</th>
            <th width="8%">RESET PASSWORD</th>

            <th width="8%">PROFILE PICTURE</th> 
            <?php
            if ($status == '1') {
                ?>
                <th width="8%">Company</th> 
                <?php
            }
            ?>
            <th width="8%">SELECT</th>
        </tr>
    </thead>
    <tbody style="font-size: 12px;">

        <?php
        $logedIN = ' and u.loggedIN = 1 ';

        $type = ',u.type as type';

        if ($status == '6') {
            $logedIN = $type = '';
        }

        //if ($status == '5') 
        // $logedIN = '';
        if ($status == '1') {
            $getCompQry = "select company_id,companyname from company_info where Status = 3";
            $getCompRes = mysql_query($getCompQry, $db1->conn);

            while ($type = mysql_fetch_assoc($getCompRes)) {
                $compList[] = $type;
            }
        }

        $typeQry = ",(select type from user_sessions where oid = mas.mas_id order by oid DESC limit 0,1) as dev_type ";
        /*
         * SELECT distinct mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt,(select type from user_sessions where oid = mas.mas_id order by oid DESC limit 0,1) as dev_type   FROM master mas  where  mas.status = 3  order by mas.mas_id DESC
         */
        if ($status == 1) {
            $accQry = "SELECT distinct mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt" . $typeQry . "  FROM master mas  where  mas.status IN (" . $status . ")  order by mas.mas_id DESC";
        } else {

            if ($cityid == '' && $companyids == '') {
                $accQry = "SELECT distinct mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt" . $typeQry . "  FROM master mas where  mas.status IN (" . $status . ")  order by mas.mas_id DESC";
            } else if ($cityid != '' && $companyids == '') {
                $accQry = "SELECT distinct mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt" . $typeQry . "  FROM master mas  where  mas.status IN (" . $status . ") and mas.company_id IN (SELECT company_id FROM company_info WHERE city = " . $cityid . ") order by mas.mas_id DESC";
            } else if ($cityid == '' && $companyids != '') {
                $accQry = "SELECT distinct mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt" . $typeQry . "  FROM master mas where  mas.status IN (" . $status . ") and mas.company_id IN (" . $companyids . ") order by mas.mas_id DESC";
            } else {
                $accQry = "SELECT distinct mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt" . $typeQry . "  FROM master mas where  mas.status IN (" . $status . ") and mas.company_id IN (SELECT company_id FROM company_info WHERE city = " . $cityid . ") and mas.company_id = " . $companyids . " order by mas.mas_id DESC";
            }
        }
        $result1 = mysql_query($accQry, $db1->conn);
//            echo $accQry;
        $i = 1;
        while ($row = mysql_fetch_assoc($result1)) {

            if ($row['profile_pic'] == "") {

                $st1 = "aa_default_profile_pic.gif";
            } else {
                $st1 = $row['profile_pic'];
            }
            ?>



            <tr id="doc_rows<?php echo $i; ?>">
                <td><?Php echo $row['mas_id'] ?></td>
                <td><a target="_blank" href="driverprofile.php?mas_id=<?Php echo $row['mas_id'] ?>" data="<?php echo $i; ?>" data-toggle="modal"> <?php echo $row['first_name'] ?></a></td>
                <td><?Php echo $row['last_name'] ?></td>
                <td><?Php echo $row['mobile']; ?></td>
                <td><?Php echo $row['email']; ?></td>
                <td><?Php echo date('d/m/Y', strtotime($row['created_dt'])); ?></td>
                <td><?Php
                    if ($row['dev_type'] == '1')
                        echo "<img src='assets/iphone-logo.png' style='width: 35px;' />";
                    else if ($row['dev_type'] == '2')
                        echo "<img src='assets/android_icon.png' style='width: 35px;' />";
                    else
                        echo "Unavailable";
                    ?></td>

                <td><a href="#modal_default_20" style="color:red;text-decoration: underline;" data="<?php echo $row['mas_id']; ?>" class="resetPassword" data-toggle="modal"> <?php echo "ReSetpassword" ?></a></td>
                <td><img src="../pics/<?Php echo $st1; ?>" height="54" width="55"></td>
                <?php
                if ($status == '1') {
                    echo "<td style='width: 200px;'><select id='company_id" . $row['mas_id'] . "'><option value=''>Select a company</option>";
                    foreach ($compList as $company) {
                        echo "<option value='" . $company['company_id'] . "'>" . $company['companyname'] . "</option>";
                    }
                    echo "</select></td>";
                }
                ?>
                <td><input dat="<?php echo $i; ?>" type="checkbox" name="checkbox_advertiser"  class="custom_check" value="<?php echo $row['mas_id']; ?>" style="background: white;height: 20px;width: 12px;" /></td>

            </tr>
            <?php
            $i++;
        }
        ?> 

    </tbody>
</table>    