<?php
$status = '1';

if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>

<style>
    #regVehicleErr,#regMaxErr,#regMinErr,#regBaseErr,#regKmErr,#regFareErr,#regDescErr,#cityErr
    {
        color:red;
    }
</style>
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>

<div class="content">

    <?php
    if ($status == '1') {
        ?>

        <div style="font-size:20px;"> VEHICLE TYPE</div>
        <?php
    }
    ?>
    <div style="float:right;">
        <?php
        if ($status == '1' || $status == '5') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" data="3" data-msg="active"><a id="addButton" href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">ADD</a></button>    
        <?php }
        ?>
        <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="EditButton" data="5" data-msg="edit"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">EDIT</a></button>
        <!--  <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="RejectButton" data="5" data-msg="vechiletype">DELETE</button>-->

    </div>
    <div style="float:none;"></div>
    <div id="refresh_table"><?php require ('refreshVehicleTypeList.php'); ?></div>

</div> 

<div class="modal" id="modal_default_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body clearfix">

                <div class="controls">
                    <form  action="" autocomplete="on" style="color:#000;" method="post" id="add_edit_form"> 
                        <div id="add_edit_heading"><h1>ADD VEHICLE TYPE</h1> </div>
                        <p> 
                            <label for="username" class="uname" > Vehicle Type Name </label>
                            <input id="vechiletype" name="vechiletype" required="required" class="editname" type="text" placeholder="eg. Uber Silver "/>
                            <span id="regVehicleErr"></span>
                        </p>
                        <p> 
                            <label for="text" class="youpasswd" > Seating Capacity </label>
                            <input id="max_size" name="max_size" required="required"  class="edit_max_size" type="text" placeholder="eg. 10" /> 
                            <span id="regMaxErr"></span>
                        </p>

                        <p> 
                            <label for="text" class="youpasswd" > Minimum Fare </label>
                            <input id="min_fare" name="min_fare" required="required" class="edit_min_fare" type="text" placeholder="eg. 20" /> 
                            <span id="regFareErr"></span>
                        </p>

                        <p> 
                            <label for="text" class="youpasswd" >Base Fare </label>
                            <input id="base_fare" name="base_fare" required="required" class="edit_base_fare" type="text" placeholder="eg. 10" /> 
                            <span id="regBaseErr"></span>
                        </p>

                        <p> 
                            <label for="text" class="youpasswd" > Price Per Minute </label>
                            <input id="price_per_min" name="price_per_min" required="required" class="edit_price_per_min" type="text" placeholder="eg 0.5" /> 
                            <span id="regMinErr"></span>
                        </p>

                        <p> 
                            <label for="text" class="youpasswd" > Price Per Mile </label>
                            <input id="price_per_km" name="price_per_km" required="required" class="edit_price_per_km" type="text" placeholder="eg. 2.0" /> 
                            <span id="regKmErr"></span>
                        </p>

                        <p> 
                            <label for="text" class="youpasswd" > Vehicle Type Description</label>
                            <input id="type_description" name="type_description" required="required" class="edit_type_description" type="text" placeholder="Uber Silver is the base version" /> 
                            <span id="regDescErr"></span>
                            <input type="hidden" name="editvehicle" id="editvehicle" value="1"/>
                        </p>

                        <p>
                            <label for="text" class="youpasswd" >CITY </label>
                        <div id="city">
               <!--<input type="text" style='width: 227px;' name="city"  id='citynames' />-->
                            <select name="city"  id='city'>
                                <option value="NULL">Select Country to select a city</option>
                                <?php
                                $get_country_type = "select * from city_available";
                                $get_country_type_res = mysql_query($get_country_type, $db1->conn);
                                while ($typelist = mysql_fetch_array($get_country_type_res)) {
                                    echo "<option value='" . $typelist['City_Id'] . "'>" . $typelist['City_Name'] . "</option>";
                                }
                                ?>
                            </select>
                            <span id="cityErr"></span>
                        </div>
                        </p>


                    </form>               
                </div>

            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_modal" style="color: red;
                        border: solid 1px #a2a2a2;">Close</button>
                <button type="button" class="btn btn-success btn-clean" id="datasubmit">Submit form</button>
            </div>
        </div>
    </div>
</div>    

<script type="text/javascript">
    $(document).ready(function () {
//            alert('1');
        $('.resetPassword').click(function () {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });
        $('#countryname').change(function () {
            var coun = $("#countryname option:selected").attr('id');
            $('#city').load('get_cities.php', {country: coun});
        });

        $('#change_pass_doc').click(function () {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {company_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function (result) {
                        //alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });

        $('#RejectButton').click(function () {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            if (values == '') {
                //alert('Please select  atleast vechile type in the list');
            } else if (confirm('Are you confirm to delete ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 4, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function (result) {
                        //alert(result.message);
                        //alert(result.test);
                        if (result.flag == 0) {
                            $('.custom_check').each(function () {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('data')).remove();
                                }
                            });
                        }
                    }
                });

            }
        });


        $('#EditButton').click(function () {
            $('#add_edit_heading').html("<h2>EDIT VEHICLE TYPE</h2>");
            var dis = $(this);
            var count = 0;
            var values = $('input:checkbox:checked.custom_check').map(function () {
                count++;
                return this.value;
            }).get();
            if (values == '')
            {
                alert("please select one vehicle type");
                return false;
                // $('#close_modal').trigger('click');
            } else if (count > 1) {

                alert("please select only one vehicle type");
                return false;
            }
            else
            {
                $.ajax({
                    type: "POST",
                    url: "submitvechiletype.php",
                    data: {item_type: 5, item_list: values, add_val: 1},
                    dataType: "JSON",
                    success: function (result) {

                        if (result.errFlag == 0) {
//                        alert($('.editname').val(result.type_name));
                            $('#vechiletype').val(result.type_name);
                            $('.edit_max_size').val(result.max_size);
                            $('.edit_min_fare').val(result.min_fare);
                            $('.edit_base_fare').val(result.basefare);
                            $('.edit_price_per_min').val(result.price_per_min);
                            $('.edit_price_per_km').val(result.price_per_km);
                            $('.edit_type_description').val(result.type_desc);
//                        $("#city option:selected").attr('value');
//                        $("#city").val(result.city_id);
                            $('#city option').removeAttr("selected");
                            $('#city option[value="' + result.city_id + '"]').attr("selected", "selected");
                            $('#editvehicle').val(result.errFlag);
                            $('body').append(result.city_id);

                        } else {
//                        alert(result.qry);
                        }
                    },
                    error: function ()
                    {
                        alert("sorry,Error occured");
                    }
                });
            }
        });
        $('#addButton').click(function () {
            $('#editvehicle').val('1');
            $('#add_edit_form').find('input').val('');
            $('#add_edit_heading').html("<h2>ADD A VEHICLE TYPE</h2>");

            $('#city option').removeAttr("selected");
        });
        $('#datasubmit').click(function () {
            var count = 0;

            if ($('#vechiletype').val() === "")
            {
                $('#vechiletype').focus();
                $('#regVehicleErr').html("Vehicle Type Name is required");
                return false;
            }

            if (count > 0)
                return false;

            if ($('#max_size').val() === "")
            {
                $('#max_size').focus();
                $('#regMaxErr').html("Maximum Passenger size is required");
                return false;
            }

            if (count > 0)
                return false;

            if ($('#min_fare').val() === "")
            {
                $('#min_fare').focus();
                $('#regFareErr').html("Minimum Fare is required");
                return false;
            }

            if (count > 0)
                return false;

            if ($('#base_fare').val() === "")
            {
                $('#base_fare').focus();
                $('#regBaseErr').html("Base Fare is required");
                return false;
            }

            if (count > 0)
                return false;

            if ($('#price_per_min').val() === "")
            {
                $('#price_per_min').focus();
                $('#regMinErr').html("Price per minute is required");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#price_per_km').val() === "")
            {
                $('#price_per_km').focus();
                $('#regKmErr').html("Price per mile is required");
                return false;
            }

            if (count > 0)
                return false;
            if ($('#type_description').val() === "")
            {
                $('#type_description').focus();
                $('#regDescErr').html("Please provide type description");
                return false;
            }
            if (count > 0)
                return false;
            if ($("#city option:selected").attr('value') == "")
            {
                $('#city').focus();
                $('#cityErr').html("Please select a city");
                return false;
            }
            if (count > 0)
                return false;



            var dis = $(this);
            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            var vechiletype = $('#vechiletype').val();
            var max_size = $('#max_size').val();
            var min_fare = $('#min_fare').val();
            var base_fare = $('#base_fare').val();
            var price_per_min = $('#price_per_min').val();
            var price_per_km = $('#price_per_km').val();
            var type_description = $('#type_description').val();
            var city_id = $("#city option:selected").attr('value');
            if ($('#editvehicle').val() == '0')
            {
                $.ajax({
                    type: "POST",
                    url: "submitvechiletype.php",
                    data: {item_type: 1, item_list: values, to_do: dis.attr('data'), vechiletypes: vechiletype, max_sizes: max_size, min_fares: min_fare, base_fares: base_fare, price_per_mins: price_per_min, price_per_kms: price_per_km, type_descriptions: type_description, city: city_id, add_val: 2},
                    dataType: "JSON",
                    success: function (result) {
                        $('body').append("<div style='hidden'>" + result.qrys + "</div>");
                        //alert('1');
                        if (result.flag == 0) {
                            $('#refresh_table').load('refreshVehicleTypeList.php', {test: 1, cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {

                            });
                            $('#close_modal').trigger('click');
                        } else {
                            alert(result.msg);
                        }
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });
            }
            //alert('Out');
            else
            {
                $.ajax({
                    type: "POST",
                    url: "submitvechiletype.php",
                    data: {item_type: 1, to_do: dis.attr('data'), vechiletypes: vechiletype, max_sizes: max_size, min_fares: min_fare, base_fares: base_fare, price_per_mins: price_per_min, price_per_kms: price_per_km, type_descriptions: type_description, city: city_id, add_val: 1},
                    dataType: "JSON",
                    success: function (result) {
                        //  alert(result.qrys);
                        //alert('1');
                        if (result.flag == 0) {
                            $('#refresh_table').load('refreshVehicleTypeList.php', {test: 1, cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {

                            });
                            $('#close_modal').trigger('click');
                        } else {
                            alert(result.msg);
                        }
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });
            }

        });


        $('#ActiveButton').click(function () {


        });



    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>


