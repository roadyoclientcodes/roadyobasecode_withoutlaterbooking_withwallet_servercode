<?php
if (isset($_REQUEST['type'])) {
    $status = '1';
} else {
    $status = '1';
}
?>

<style>
    #regVehicleErr,#regMaxErr,#regMinErr,#regBaseErr,#regKmErr,#regFareErr,#regDescErr
    {
        color:red;
    }
</style>
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>
<div class="content">

    <?php
    if ($status == '1') {
        ?>

        <div style="font-size:20px;">LIST OF CITIES </div>
        <?php
    }
    ?>
    <div style="float:right;">
        <?php
        if ($status == '1' || $status == '5') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" data="3" data-msg="active"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">ADD</a></button>  


        <?php }
        ?>
        <!--<button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="DeleteButton" data="5" data-msg="edit">DELETE</button>-->
        <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="EditButton" data="5" data-msg="edit">EDIT</button>
        <a href="#modal_default_3" data-toggle="modal" id="editModal" style="display:none;" class="btn btn-default btn-block btn-clean">EDIT</a>
        <!--<button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="RejectButton" data="5" data-msg="vechiletype">DELETE</button>-->

    </div>
    <div style="float:none;"></div>
    <div id="refresh_table"><?php require ('refreshcity.php'); ?></div>

</div> 

<div class="modal" id="modal_default_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body clearfix">

                <div class="controls">
                    <form  action="" autocomplete="on" style="color:#000;" method="post"> 
                        <h1>ADD CITY</h1> 
                        <p> 
                            <label for="text" class="youpasswd" >COUNTRY </label>
                            <select  name="countryname" id="countryname" class="editcompany" >
                                <option value="NULL">SELECT A COUNTRY</option>
                                <?php
                                $get_country_type = "select * from country";
                                $get_country_type_res = mysql_query($get_country_type, $db1->conn);
                                while ($typelist = mysql_fetch_array($get_country_type_res)) {
                                    echo "<option value='" . $typelist['Country_Id'] . "' id='" . $typelist['Country_Id'] . "'>" . $typelist['Country_Name'] . "</option>";
                                }
                                ?>
                                <!--<option value="Others" name="others">Others</option>-->
                            </select>
                            <span id="regcompany"></span>
                        </p>

                        <p>
                            <label for="text" class="youpasswd" >CITY </label>
                        <div>
               <!--<input type="text" style='width: 227px;' name="city"  id='citynames' />-->
                            <select name="city"  id='city'>
                                <option value="NULL">Select Country to select a city</option>
                            </select>
                        </div>
                        </p>
                        <p>
                            <label for="text" class="youpasswd" >Latitude </label>
                        <div>
                            <input id="city_latitude" name="city_latitude" required="required" class="editlat" type="text" placeholder="0.000000" /> 
                        </div>
                        </p>
                        <p>
                            <label for="text" class="youpasswd" >Longitude </label>
                        <div>
                            <input id="city_longitude" name="city_longitude" required="required" class="editlong" type="text" placeholder="0.000000" /> 
                        </div>
                        </p>



                    </form>               
                </div>

            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_modal" style="color: red;
                        border: solid 1px #a2a2a2;">Close</button>
                <button type="button" class="btn btn-success btn-clean" id="datasubmit">Submit form</button>
                <span id='error_msg'></span>
            </div>
        </div>
    </div>
</div>    

<div class="modal" id="modal_default_3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body clearfix">

                <div class="controls">
                    <form  action="" autocomplete="on" style="color:#000;" method="post"> 
                        <h1>EDIT CITY</h1>

                        <p>
                            <label for="text" class="youpasswd" >Latitude </label>
                        <div>
                            <input id="city_latitude_edit" name="city_latitude1" required="required" class="editlat1" type="text" placeholder="0.000000" /> 
                        </div>
                        </p>
                        <p>
                            <label for="text" class="youpasswd" >Longitude </label>
                        <div>
                            <input id="city_longitude_edit" name="city_longitude1" required="required" class="editlong1" type="text" placeholder="0.000000" /> 
                        </div>
                        </p>



                    </form>               
                </div>

            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_modal-edit" style="color: red;
                        border: solid 1px #a2a2a2;">Close</button>
                <button type="button" class="btn btn-success btn-clean" id="datasubmit-edit">Submit form</button>
                <span id='error_msg'></span>
            </div>
        </div>
    </div>
</div> 



<script type="text/javascript">
    $(document).ready(function () {
//            alert('testing');
        $('.resetPassword').click(function () {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });
//change the drop down text box on select of 'other' option to add missing country
        $('#countrynamemanual').hide();
        $('#countryname').live('change', function () {

            var coun = $("#countryname option:selected").attr('value');
            $('#city').load('get_cities.php', {country: coun});

        });


//        $('#countryname').change(function() {
//            alert('testing');
//            var coun = $("#countryname option:selected").attr('id');
//            $('#city').load('get_cities.php', {country: coun});
//        });

        $('#change_pass_doc').click(function () {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {company_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function (result) {
                        //alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });

        $('#DeleteButton').click(function () {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast one city in the list');
            } else if (confirm("Please note that all the data associated with selected citi's will get deleted. Are you confirm to delete city's selected ?")) {
                $.ajax({
                    type: "POST",
                    url: "deleteCities.php",
                    data: {item_list: values},
                    dataType: "JSON",
                    success: function (result) {
                        //alert(result.message);
//                        $('body').append("<div style='hidden'>"+result+"</div>");
                        if (result.flag == 0) {
                            $('.custom_check').each(function () {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        }
                    }
                });

            }
        });


        $('#datasubmit').click(function () {
            var count = 0;
            var dis = $(this);
            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            var countrynames = $('#countryname').val();
            var countrynamemanuals = $('#countrynamemanual').val();
            var city_lat = $('#city_latitude').val();
            var city_long = $('#city_longitude').val();
            var citys = $("#city option:selected").attr('value');

            if (countrynames == '' || city_lat == '' || city_long == '' || citys == '')
                alert('All fields are mandatory');
            else
                $.ajax({
                    type: "POST",
                    url: "submitcity.php",
                    data: {item_type: 1, to_do: dis.attr('data'), countryname: countrynames, countrynamemanual: countrynamemanuals, city: citys, lats: city_lat, longs: city_long},
                    dataType: "JSON",
                    success: function (result) {
                        // alert(result.qrys);
                        // alert('1');
                        if (result.flag == 0) {
                            $('#refresh_table').load('refreshcity.php', {test: 1});
                            $('#close_modal').trigger('click');
                        } else {
                            alert('Error occured in adding city');// + result.qrys);
//                            $('#error_msg').html(result.qrys);
                        }
                    },
                    error: function () {
                        alert('Error occured');
                    }
                });


        });


        $('#datasubmit-edit').click(function () {

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            $.ajax({
                type: "POST",
                url: "editcity.php",
                dataType: "JSON",
                data: {type: 2, item_list: values, city_latitude1: $('#city_latitude_edit').val(), city_longitude1: $('#city_longitude_edit').val()},
                success: function (result) {
                    if (result.errFlag == 0) {
//                         alert(result.City_Lat);
                        $('#refresh_table').load('refreshcity.php', {test: 1});
                        $('#close_modal-edit').trigger('click');
                    } else {
                        alert(result.qry);
                    }


                },
                error: function ()
                {
                    alert("sorry,Error occured");
                }
            });

        });


        $('#EditButton').click(function () {
            var dis = $(this);
            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();


            if (values == '')
            {
                alert("please select one company");
                //$('#close_modal').trigger('click');
            }
            else
            {
                $('#editModal').click();
                // alert(values);
                $.ajax({
                    type: "POST",
                    url: "editcity.php",
                    dataType: "JSON",
                    data: {type: 1, item_list: values},
                    success: function (result) {
                        if (result.errFlag == 0) {
                            // alert(result.City_Lat);
                            $('.editlat1').val(result.City_Lat);
                            $('.editlong1').val(result.City_Long);
                            $('#city_id').val(result.City_Id);

                        } else {
                            alert(result.qry);
                        }


                    },
                    error: function ()
                    {
                        alert("sorry,Error occured");
                    }
                });
            }
        });



    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>


