
<?php
error_reporting(0);
include('../Models/ConDB.php');
$db1 = new ConDB();

if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
} 
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>
<script type="text/javascript">
     $(document).ready(function() {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 11, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null]});
    });
</script>
     <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
        <thead style="font-size: 12px;">
            <tr>
                <th width="8%">VEHICLE ID</th>
                <th width="8%">TITLE</th>

                <th width="8%">VEHICLE MODEL</th>
                <th width="8%">VEHICLE TYPE</th> 
                <th width="8%">VEHICLE REG NO</th>
                <th width="8%">LICENCE PLAT NO</th> 
                <th width="8%">VEHICLE SEATING</th>
                <th width="8%">VEHICLE COLOR</th>   

                <th width="8%">COMPANY</th>  
                <!--<th width="8%">STATUS</th>-->
               <th width="8%">SELECT </th>
            </tr>
        </thead>
        <tbody style="font-size: 12px;">

            <?php
     
            
            
              if($cityid == '' && $companyids == '')
        {
            $logedIN = ' and u.loggedIN = 1 ';

            $type = ',u.type as type';

            if ($status == '6') {
                $logedIN = $type = '';
            }

            if ($status == '5')
                $logedIN = '';

//            if ($status == '2')
//                $status = "1,2";


            $mongo = $db1->mongo;
            $location = $mongo->selectCollection('location');

            if ($status == '8') {
                $user_data = $location->find(array('status' => array('$in' => array(4, 5)), 'carId' => array('$gt' => 0)));
                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.workplace_id IN (" . rtrim($car_query, ',') . ")  order by w.workplace_id DESC";
            } else if ($status == '6' || $status == '7') {

                if ($status == '6')
                    $user_data = $location->find(array('status' => 3, 'inBooking' => 1));
                else
                    $user_data = $location->find(array('inBooking' => 2));

                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 1 and w.workplace_id IN (" . rtrim($car_query, ',') . ")  order by w.workplace_id DESC";
            } else if ($status == '2') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (1,2)   order by w.workplace_id DESC";
            } else if ($status == '4') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (4)  order by w.workplace_id DESC";
            } else if ($status == '5') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (2) order by w.workplace_id DESC";
            } else {
                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 4  order by w.workplace_id DESC";
            }
        }
        
        else if($cityid != '' && $companyids == '')
        {
            $logedIN = ' and u.loggedIN = 1 ';

            $type = ',u.type as type';

            if ($status == '6') {
                $logedIN = $type = '';
            }

            if ($status == '5')
                $logedIN = '';

//            if ($status == '2')
//                $status = "1,2";


            $mongo = $db1->mongo;
            $location = $mongo->selectCollection('location');

            if ($status == '8') {
                $user_data = $location->find(array('status' => array('$in' => array(4, 5)), 'carId' => array('$gt' => 0)));
                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.workplace_id IN (" . rtrim($car_query, ',') . ")  and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            } else if ($status == '6' || $status == '7') {

                if ($status == '6')
                    $user_data = $location->find(array('status' => 3, 'inBooking' => 1));
                else
                    $user_data = $location->find(array('inBooking' => 2));

                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 1 and w.workplace_id IN (" . rtrim($car_query, ',') . ")  and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            } else if ($status == '2') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (1,2) and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";//and company IN(SELECT company_id FROM company_info WHERE city = ".$cityid.") 
            } else if ($status == '4') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (4)  and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            } else if ($status == '5') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (2)   and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            } else {
                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 4  and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            }
        }
        else if($cityid == '' && $companyids != '')
        {
            $logedIN = ' and u.loggedIN = 1 ';

            $type = ',u.type as type';

            if ($status == '6') {
                $logedIN = $type = '';
            }

            if ($status == '5')
                $logedIN = '';

//            if ($status == '2')
//                $status = "1,2";


            $mongo = $db1->mongo;
            $location = $mongo->selectCollection('location');

            if ($status == '8') {
                $user_data = $location->find(array('status' => array('$in' => array(4, 5)), 'carId' => array('$gt' => 0)));
                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.workplace_id IN (" . rtrim($car_query, ',') . ") and company IN( ".$companyids.")  order by w.workplace_id DESC";
            } else if ($status == '6' || $status == '7') {

                if ($status == '6')
                    $user_data = $location->find(array('status' => 3, 'inBooking' => 1));
                else
                    $user_data = $location->find(array('inBooking' => 2));

                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 1 and w.workplace_id IN (" . rtrim($car_query, ',') . ") and company IN( ".$companyids.")  order by w.workplace_id DESC";
            } else if ($status == '2') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (1,2) and company IN( ".$companyids.") order by w.workplace_id DESC";
            } else if ($status == '4') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (4) and company IN( ".$companyids.")  order by w.workplace_id DESC";
            } else if ($status == '5') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (2)  and company IN( ".$companyids.")  order by w.workplace_id DESC";
            } else {
                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 4 and company IN( ".$companyids.")  order by w.workplace_id DESC";
            }
        }
        else
        {
            
             $logedIN = ' and u.loggedIN = 1 ';

            $type = ',u.type as type';

            if ($status == '6') {
                $logedIN = $type = '';
            }

            if ($status == '5')
                $logedIN = '';

//            if ($status == '2')
//                $status = "1,2";


            $mongo = $db1->mongo;
            $location = $mongo->selectCollection('location');

            if ($status == '8') {
                $user_data = $location->find(array('status' => array('$in' => array(4, 5)), 'carId' => array('$gt' => 0)));
                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.workplace_id IN (" . rtrim($car_query, ',') . ") and company IN(SELECT company_id FROM company_info WHERE city = ".$cityid." and company_id = ".$companyids.") and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            } else if ($status == '6' || $status == '7') {

                if ($status == '6')
                    $user_data = $location->find(array('status' => 3, 'inBooking' => 1));
                else
                    $user_data = $location->find(array('inBooking' => 2));

                $car_query = '';

                foreach ($user_data as $car) {
                    $car_query .= $car['carId'] . ',';
                }

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 1 and w.workplace_id IN (" . rtrim($car_query, ',') . ") and company IN(SELECT company_id FROM company_info WHERE city = ".$cityid." and company_id = ".$companyids.") order by w.workplace_id DESC";
            } else if ($status == '2') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (1,2) and company IN(SELECT company_id FROM company_info WHERE city = ".$cityid." and company_id = ".$companyids.") and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            } else if ($status == '4') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (4) and company IN(SELECT company_id FROM company_info WHERE city = ".$cityid." and company_id = ".$companyids.") and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            } else if ($status == '5') {

                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status in (2)  and company IN(SELECT company_id FROM company_info WHERE city = ".$cityid." and company_id = ".$companyids.") and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            } else {
                $accQry = "SELECT w.workplace_id,w.Title,w.Vehicle_Model,w.type_id,w.company,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Color,(select vehicletype from vehicleType where id= w.Title) as vehicletype, (select vehiclemodel from vehiclemodel where id= w.Vehicle_Model) as vehiclemodel, (select type_name from workplace_types where type_id=w.type_id) as type_name, (select max_size from workplace_types where type_id=w.type_id) as Vehicle_seating, (select companyname from company_info where company_id=w.company) as companyname FROM workplace w where w.Status = 4 and company IN(SELECT company_id FROM company_info WHERE city = ".$cityid." and company_id = ".$companyids.") and w.type_id in(select type_id from workplace_types where city_id = ".$cityid.") order by w.workplace_id DESC";
            }
            
        }
            
            
            
            
            
            
            
            $result1 = mysql_query($accQry, $db1->conn);
        // echo $accQry;
            $i = 1;


            while ($row = mysql_fetch_assoc($result1)) {
                ?>



                <tr id="doc_rows<?php echo $i; ?>">
                    <td><?php echo $row['workplace_id'] ?></td>
                    <td><a target="_blank" href="" data="<?php echo $i; ?>" data-toggle="modal"> 
                            <?php
                            echo $row['vehicletype'];
                            ?>
                        </a></td>
                    <td>
                        <?php
                        echo $row['vehiclemodel'];
                        ?>
                    </td>
                    <td>

                        <?php
                        echo $row['type_name'];
                        ?>
                    </td>
                    <td><?Php echo $row['Vehicle_Reg_No']; ?></td>
                    <td><?Php echo $row['License_Plate_No']; ?></td>
                    <td><?Php echo $row['Vehicle_seating']; ?></td>
                    <td><?Php echo $row['Vehicle_Color']; ?></td>

                    <td><?php echo $row['companyname']; ?></td>
                    <td><input dat="<?php echo $i; ?>" type="checkbox" name="checkbox_advertiser"  class="custom_check" value="<?php echo $row['workplace_id']; ?>" style="background: white;height: 20px;width: 12px;" /></td>
                </tr>
                <?php
                $i++;
            }
            ?> 

        </tbody>
    </table>   


