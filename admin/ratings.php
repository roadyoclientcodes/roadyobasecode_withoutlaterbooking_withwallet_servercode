<?php
session_start();
include('../Models/ConDB.php');
$db = new ConDB();
if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
?>
<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>
<script type="text/javascript">
    $(document).ready(function () {

        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 20, "aLengthMenu": [20, 40, 60, 80, 100], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null]});

    });
</script>
<div style="font-size:20px;"> PASSENGER RATING</div>
<div class="page-content page-content-white" style="margin-left: 1%;">

    <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
        <thead style="font-size: 12px;">
            <tr>

                <th>SL NO</th>
                <th>PASSENGER ID</th> 
                <th>PASSENGER NAME</th> 
                <th>PASSENGER EMAIL</th> 
                <th>AVG RATING</th> 

            </tr>
        </thead>
        <tbody style="font-size: 12px;">

            <?php
//            if ($cityid == '') {
            $accQry = "SELECT distinct p.slave_id, p.first_name AS patientname,p.email,(select avg(rating) from passenger_rating where slave_id = p.slave_id) as rating FROM passenger_rating r, slave p WHERE r.slave_id = p.slave_id  AND r.status IN (" . $status . ") order by patientname ASC";
//            } else {
//                $accQry = "SELECT r.rating, r.status, r.rating_dt, r.mas_id, d.first_name AS doctorname, p.first_name AS patientname FROM passenger_rating r, master d, slave p WHERE r.slave_id = p.slave_id AND r.mas_id = d.mas_id and d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . ")) AND r.status IN (" . $status . ") order by patientname ASC";
//            }
            $result1 = mysql_query($accQry, $db->conn);
            $i = 1;
            while ($row = mysql_fetch_assoc($result1)) {
                ?>



                <tr id="doc_rows<?php echo $i; ?>">
                    <td><?Php echo $i; ?></td>
                    <td><?Php echo $row['slave_id']; ?></td>
                    <td><?Php echo $row['patientname']; ?></td>
                    <td><?Php echo $row['email']; ?></td>
                    <td><?Php echo round($row['rating'], 1); ?></td>

                </tr>
                <?php
                $i++;
            }
            ?> 

        </tbody>
    </table>                                        

</div>





