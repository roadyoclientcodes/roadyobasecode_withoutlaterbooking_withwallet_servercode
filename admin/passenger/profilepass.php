<?php
session_start();
error_reporting(1);

include('../../Models/ConDB.php');
$db1 = new ConDB();
if (isset($_GET['slave_id'])) {
    $_SESSION['admin_idsnew'] = $_GET['slave_id'];
}
$disable = '';
$curTime = time();
if (!isset($_SESSION['admin_idsnew']) || $_SESSION['validity'] <= $curTime) {
    header('location: logout.php');
}
if ($_SESSION['admin'] != 'super') {
    $disable = 'disabled';
}
if ($_SESSION['admin'] != 'super' && $_SESSION['admin_idsnew'] == '') {

    header('location: SuperAdminLogin.php');
}
$oneHourExp = (24 * 60) + time();
$_SESSION['validity'] = $oneHourExp;
?>

<?php
//print_r($_REQUEST);

?>
		<link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
        <style>
            #crop_wrapper{                
			margin:0 auto;               
			font-family:'MYRIAD PRO';       
			}

            .crop_wrapper_box {
                color: #000000;
                max-height: 700px;
                left:10%;
                padding: 20px;
                position: fixed;
                right: 10%;
                top: 100%;
                width: 700px;
                z-index: 11;
                border-radius:10px;
                border-bottom-right-radius: 0px;
                border-bottom-left-radius: 0px;
                -moz-border-radius-bottomleft: 0px;
                -moz-border-radius-bottomright: 0px;
                -moz-border-radius:10px;
                margin:0 auto;
            }

            .crop_overlay {
                background: #000000;
                bottom: 0;
                left: 0;
                position: fixed;
                right: 0;
                top: 0;
                z-index: 10;
                opacity:0;
                display: none;
            }
            .crop_content{width: 700px;margin: 0 auto;}


            .upload_div
			{
                position:absolute;
                width: 206px;
                height: 34px;
                overflow:hidden;
                float: left;
				margin-top:271px;
            }
			.upload_div1
			{
			    position:absolute;
                width: 206px;
                height: 34px;
                overflow:hidden;
                float: left;
				margin-top: -125px;
                margin-left: 137px;
				
			}
			 .upload_div1 input{font: 500px monospace;opacity:0;filter: alpha(opacity=0);z-index: 1;top:0;bottom: 0;left: 0;right:0;padding:0;margin: 0;}
			.upload_div1 button{margin-left: 10%;position: absolute;border: none;width: 90px;height: 30px;background: #ffffff;color: #000000;border:solid 1px #000000;font-family: arial;font-weight: bold;font-size: 9px;border-radius: 2px;}

            .upload_div button{margin-left: 37%;cursor:pointer;border: none;position: absolute;width: 90px;height: 30px;background: #ffffff;color: #000000;border:solid 1px #000000;font-family: arial;font-weight: bold;font-size: 9px;border-radius: 2px;}
            .upload_div input{font: 500px monospace;opacity:0;                    filter: alpha(opacity=0);                   z-index: 1;                    top:0;                    bottom: 0;                    left: 0;                    right:0;                    padding:0;                    margin: 0;                }
			 
        </style>
        <!--<script type="text/javascript" src="js/global.js"></script>-->
        
  

<style>
#popup
{
display: none;
overflow: auto;
width: 732px;
height:834px;
position: absolute;
z-index: 2000;
background: #f9f9f9;
padding-bottom: 20px;
box-shadow: 5px 5px 55px #888888;
border-radius: 5px;
top: 10px;
left: 100px;
}
#wrappers_pop 
{
position: absolute;
top: 220px;
}
#head_pop
{
width:100%;
background:#8B91AB;
height:80px;
}
.history_tr:nth-child(even) {background: #F0F0F2;}
.history_tr:nth-child(odd) {background: #ffffff;}
#headers
{
width:100%;
height:100%;
}
#qrscaner
{
background:url('./Loupon Assets/qr-scaner.png');
height:183px;
width:209px;
}
#advertiser_contact
{
height: auto;
background: #FFF;
padding-bottom: 50px;
}
</style>

        <style>
            input[title]:hover:after {
                content: attr(title);
                padding: 4px 8px;
                color: #333;
                position: absolute;
                left: 0;
                top: 100%;
                white-space: nowrap;
                z-index: 20;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border-radius: 5px;
                -moz-box-shadow: 0px 0px 4px #222;
                -webkit-box-shadow: 0px 0px 4px #222;
                box-shadow: 0px 0px 4px #222;
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #eeeeee),color-stop(1, #cccccc));
                background-image: -webkit-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -ms-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -o-linear-gradient(top, #eeeeee, #cccccc);
            }
            .ui-autocomplete{max-height: 240px;overflow: hidden;font-size: 0.6em;}
            .select_dropdown{-webkit-appearance: none;width: 150px;background: url(assets/newui/dropdown_bg.png) no-repeat;background-size: 25px 24px;background-position: 99% 1px;height: 28px;border: 1px solid #dcdcdc;background-color: white;font-size: 15px;outline: none;font-family: arial;color: #8e8c8c;}
            #popup_wrapper{
                color:#000000;
                font-family:tahoma;
                font-size:15px;
                margin:0 auto;
            }
            .popup_content{ position: relative;margin: 5px;border: 1px dotted black; }
            .popup_content_box{margin:15px;}
            .popup_box{
                background: white;
                /*box-shadow: 0 1px 2px;*/
                color: #000000;
                width: 40%;
                left: 0;
                position: fixed;
                right: 0;
                top: 110%;
                z-index: 101;
                border-radius: 2px;
                margin: 0 auto;
                border: 1px solid #5c82e1;
                /*       border-bottom-right-radius: 0px;
               border-bottom-left-radius: 0px;
               -moz-border-radius-bottomleft: 0px;
               -moz-border-radius-bottomright: 0px;
                       -moz-border-radius:10px;*/
                margin:0 auto;
            }

            .popup_overlay{
                background: #ccc;
                bottom: 0;
                left: 0;
                position: fixed;
                right: 0;
                top: 0;
                z-index: 100;
                opacity:0;
                cursor: pointer;
                display: none;
            }

            a.popup_boxclose{
                /*background: url("assets/cancel.png") repeat scroll left top transparent;*/
                cursor: pointer;
                float: right;
                height: 26px;
                position: relative;
                top: 35px;
                width: 26px;
                font-size: 18px;
                text-decoration: none;
            }
            .page_body_content 
            {
                height: 2000px!important;
                background: #ededed;
            }
			
#regFirstErr
{
color:#FF0000;
padding-top:12px;
}
#regMailErr
{
color:#FF0000;
padding-top:12px;
}
#regTelErr
{
color:#FF0000;
padding-top:12px;
}
#regPerErr
{
color:#FF0000;
padding-top:12px;
}
#regFaxErr
{
color:#FF0000;
padding-top:12px;
}

        </style>
<div id="advertiser_center_area">
<div id="passenger">
<form id="imageform" method="post" enctype="multipart/form-data" action='image_upload_ajax.php'>
				
                <div class="upload_div">
				<div style="    width: 95px;    height: 30px;    float: left;">
                    <button style="position:absolute">UPLOAD IMAGE</button>
                    <input name='agent_profile_pic' type="file" id="file_upload" />
					<input type="hidden" name="photo_number" value="0" />
					</div>
					
					
                </div>
   </form>
    
 <?php
 $get_passenger_profile="select * from slave where slave_id='".$_SESSION['admin_idsnew']."'";
 //echo $get_passenger_profile;
 $get_passenger_profile_res=mysql_query($get_passenger_profile,$db1->conn);
 $get_passenger_row=mysql_fetch_assoc($get_passenger_profile_res);
 ?>
<!--<div style="position: absolute;margin-top: 271px;margin-left: 120px;" action="delete_advertier_url.php">
<div style="width:130px;margin-left: 91px;"><button class="remove_thumb_image" load_ids='advertiser_photo' name="thumbimage" l_id="<?php echo $_SESSION['admin_idsnew'];?>">Remove</button></div>
</div>-->
<form  action="passengerprofile.php" method="post" id="advertiserform" style="width:450px;margin-left: auto;margin-right: auto;">
<div id="advertiser_profile">
<div id="advertiser_photo" style="width:200px;height:200px;right: 1065px;margin-top: 60px;position:absolute">
<img src="http://107.170.66.211/roadyo_live/pics/hdpi/<?php echo $get_passenger_row['profile_pic'];?>" height="202" width="202"/>
</div><!---advertiser_photo-->
<h1>PASSENGER PROFILE</h1> 
								<p> 
                                    <label for="text" class="youpasswd" >FIRST NAME </label>
                                    <input id="firstname" name="firstname" required="required" class="editfirstname" type="text" value="<?php echo $get_passenger_row['first_name'];?>" /> 
									<span id="regfirstname"></span>
                                </p>
								<p> 
                                    <label for="text" class="youpasswd" >LAST NAME </label>
                                    <input id="lastname" name="lastname" required="required" class="editlastname" type="text" value="<?php echo $get_passenger_row['last_name'];?>" /> 
									<span id="reglastname"></span>
                                </p>
								
								<p> 
                                    <label for="text" class="youpasswd" > MOBILE </label>
                                    <input id="mobile" name="mobile" required="required" class="editmobile" type="text" value="<?php echo $get_passenger_row['phone'];?>"/> 
									<span id="regmobile"></span>
                                </p>
								
								 <p> 
                                    <label for="text" class="youpasswd" > EMAIL </label>
                                    <input id="email" name="email" required="required" class="editemail" type="text" value="<?php echo $get_passenger_row['email'];?>" /> 
									<span id="regemail"></span>
                                </p>
								
								
								
								 <p> 
                                    <label for="text" class="youpasswd" >ZIPCODE</label>
                                    <input id="zipcode" name="zipcode" required="required" class="editzipcode" type="text" value="<?php echo $get_passenger_row['zipcode'];?>" /> 
									<span id="regzipcode"></span>
                                </p>
							 	
							
						<div>
						<!--<input type="button" name="Cancel" value="Cancel" style="width: 200px;float: left;margin-left: 21px;margin-right: 10px;"/>-->
						<input type="submit" name="Update" value="Update" style="width:200px;"/>
						</div>
						  
				    	  


                     
                        
                      

                       

</form>

</div>
<!--end of advertiser form-->
<!--check the contact detail-->
<!---Advertiser contact details--->
</div>

        <div id="crop_wrapper">
            <div id="crop_overlay" class="crop_overlay"></div>
            <div id="croppopup" class="crop_wrapper_box">
                <div class="crop_content">
                    <div style="float:left;">
                        <div id="crop_error"></div>
                        <form action="crop1.php" method="post">
                            <label style="color:white;">Drag on the image to crop it.</label>
                            <input type="hidden" id="x" name="x" />
                            <input type="hidden" id="y" name="y" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                            <input type="button" id="crop_image" value="Crop Image"/>
							<input type="button" id="use_original" value="Use Original" />
                            <input type="hidden" value="cancel" onclick="closeCropDialog('croppopup');" />
                        </form>
                    </div>
                    <div id="crop_image_div" style="float:left;width:100%;"></div>
                </div>
            </div>
        </div>
		
        <script type="text/javascript" src="js/jquery.form.js"></script>
        <script type="text/javascript" src="js/jquery.Jcrop.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
			
			$('.remove_image').click(function(){
				var dis = $(this);
				$('#'+dis.attr('load_id')).load('delete_advertiser_thumb.php',{l_id:dis.attr('l_id'),src:dis.attr('image_url')});
			});
			$('.remove_thumb_image').click(function(){
			    var dis = $(this);
				$('#'+dis.attr('load_ids')).load('delete_advertier_url.php',{l_id:dis.attr('l_id')});
				//alert("hello");
			});

                $('#file_upload').live('change', function() {
                    //$("#advertiser_photo").html('');
                  $("#advertiser_photo").html('<img src="assets/loading.gif" alt="Uploading...."/>');
                    $("#imageform").ajaxForm({
                        target: '#advertiser_photo'
                    }).submit();
                });
			   $('.click_to_crop').live('click', function() 
				{
                    var dis = $(this);
                    window.im_src = dis.attr('src');
                    window.im_w = dis.attr('width');
                    window.im_h = dis.attr('height');
                    window.im_alt = dis.attr('alt');
                    window.im_title = dis.attr('title');
					window.photo_number = dis.attr('photo_number');
                    window.parent_id = dis.parent().attr('id');
                    //alert(dis.attr('ag_id'));
                    $('#crop_image_div').html('');
                    $('#crop_image_div').load('crop_agent.php', {src_dis: window.im_src});
                    openCropDialog();
                });

                $('#crop_image').live('click', function() {
                    $('#crop_error').html('');
                    if (parseInt($('#w').val())) {
                        $('#' + window.parent_id).load('crop_agent.php', {
                            x: $('#x').val(),
                            y: $('#y').val(),
                            w: $('#w').val(),
                            h: $('#h').val(),
                            im_w: window.im_w,
                            im_h: window.im_h,
                            src: window.im_src,
                            alt: window.im_alt,
                            title: window.im_title,
							ph_num: window.photo_number,
                            page: 'agent_dash'
                        }, function() {
                            closeCropDialog('croppopup');
                            $('html, body').animate({scrollTop: '0px'}, 500);
                        });
                    } else {
                        $('#crop_error').html('Please select a crop region then press submit.');
                        closeCropDialog('croppopup');
                        return false;
                    }
                });
				
				$('#use_original').click(function(){
					closeCropDialog('croppopup');
				});
            });
        </script>
		
        <script>
            function closeCropDialog(prospectElementID) {
                $('#' + prospectElementID).css('position', 'absolute');
                $('#' + prospectElementID).animate({'tp': '-100%'}, 1, function() {
                    $('#' + prospectElementID).css('position', 'fixed');
                    $('#' + prospectElementID).css('top', '100%');
                    $('#crop_overlay').hide();
                });
            }
            function openCropDialog() {
                $('html, body').animate({scrollTop: '0px'}, 500);
                $('#crop_overlay').css('opacity', '0.8');
                $('#crop_overlay').fadeIn('fast', function() {
                    $('#croppopup').css('display', 'block');
                    $('#croppopup').animate({'top': '17%'}, 500, function() {
                        $('.crop_wrapper_box').css('position', 'absolute');
                        $('.crop_wrapper_box').css('margin', '0 auto');
                        $('.crop_wrapper_box').css('top', '100px');
                    });
                });
            }
            function updateCoords(c) {
                $('#x').val(c.x);
                $('#y').val(c.y);
                $('#w').val(c.w);
                $('#h').val(c.h);
            }
        </script>