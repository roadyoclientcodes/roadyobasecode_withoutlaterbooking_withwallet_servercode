<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../Models/ConDB.php';
$db = new ConDB();

if (isset($_REQUEST['country_name'])) {
    $insertDataQry1 = "select * from country ";
    $res1 = mysql_query($insertDataQry1, $db->conn);

    while ($row = mysql_fetch_array($res1)) {
        if (strtolower($_REQUEST['country_name']) == strtolower($row['Country_Name'])) {
            echo json_encode(array('flag' => 1, 'message' => 'Country is already exist'));
            return false;
        }
    }

    if ($_REQUEST['country_name'] == '') {
        echo json_encode(array('flag' => 1, 'message' => 'Country name missing'));
        return false;
    }

    $insertDataQry = "INSERT INTO country (Country_Name) VALUES ('" . $_REQUEST['country_name'] . "')";
    mysql_query($insertDataQry, $db->conn);
    $newCountryId = mysql_insert_id();
    echo json_encode(array('flag' => 0, 'message' => 'Country added successfully', 'cid' => $newCountryId, 'cname' => $_REQUEST['country_name']));
    return false;
}

if (isset($_REQUEST['city_name'])) {
    if ($_REQUEST['city_name'] == '') {
        echo json_encode(array('flag' => 1, 'message' => 'City name missing'));
        return false;
    } else if ($_REQUEST['country_id'] == '') {
        echo json_encode(array('flag' => 1, 'message' => 'Choose a country'));
        return false;
    } else if ($_REQUEST['currency'] == '' || strlen($_REQUEST['currency']) < 3 || strlen($_REQUEST['currency']) > 3) {
        echo json_encode(array('flag' => 1, 'message' => 'Please type in a 3 digit currency code'));
        return false;
    }

    $cityCheckQry = "select City_Id from city where City_Name = '" . $_REQUEST['city_name'] . "' and Country_Id = '" . $_REQUEST['country_id'] . "'";
    $cityCheckRes = mysql_query($cityCheckQry, $db->conn);

    if (mysql_num_rows($cityCheckRes)) {
        echo json_encode(array('flag' => 1, 'message' => 'City already added'));
        return false;
    }

    $insertDataQry = "INSERT INTO city (Country_Id,City_Name,Currency) VALUES ('" . $_REQUEST['country_id'] . "','" . $_REQUEST['city_name'] . "','" . strtoupper($_REQUEST['currency']) . "')";
    mysql_query($insertDataQry, $db->conn);
    echo json_encode(array('flag' => 0, 'message' => 'City added successfully'));
    return false;
}
?>

<div style="margin: 5%;">
    <div style="float: left;width: 45%;">
        <h2>Add a country</h2>
        <form action ="#" id="country_form">
            Country Name : <input type="text" id="country_name" name="country_name" style="width: 180px;" mandatory/><br>
            <input type="button" id="add_country" value="Add Country" style="width: 180px;"/><br>
        </form>
        <span style="color: red;" id="country_span"></span>
    </div>
    <div style="float: left;width: 45%;">
        <h2>Add a City</h2>
        <form action ="#" id="city_form">
            Country: 
            <select id="country_list" name="country_id" style="width: 180px;" >
                <option value="">SELECT A COUNTRY</option>
                <?php
                $get_country_type = "select * from country";
                $get_country_type_res = mysql_query($get_country_type, $db->conn);
                while ($typelist = mysql_fetch_array($get_country_type_res)) {
                    echo "<option value='" . $typelist['Country_Id'] . "'>" . $typelist['Country_Name'] . "</option>";
                }
                ?>
            </select><br>
            City Name : <input type="text" id="city_name" name="city_name" style="width: 180px;" mandatory /><br>
            Currency : <input type="text" id="currency" name="currency" style="width: 180px;" mandatory /><br>
            <input type="button" id="add_city" value="Add City" style="width: 180px;"/><br>
        </form>
        <span style="color: red;" id="city_span"></span>
    </div>
    <div style="float: none;"></div>
</div>
<script>
    $(document).ready(function() {

        $('#add_country').click(function() {

            if (!/^[a-z A-Z'-.]+$/.test($('#country_name').val())) {
                $('#country_span').text('Only alphabets are allowed for country name');
            } else {
                $.ajax({
                    type: "POST",
                    url: "manageCities.php",
                    data: $('#country_form').serialize(),
                    dataType: "JSON",
                    success: function(result) {
                        //alert(result.message);
                        $('#country_span').text(result.message);
                        if (result.flag == 0)
                            $('#country_list').append("<option value='" + result.cid + "'>" + result.cname + "</option>");
                    }
                });
            }
        });
        $('#add_city').click(function() {
            if (!/^[a-z A-Z'-.]+$/.test($('#city_name').val())) {
                $('#city_span').text('Only alphabets are allowed for city name');
            } else if (!/^[a-z A-Z'-.]+$/.test($('#currency').val())) {
                $('#city_span').text('Only alphabets are allowed for currency');
            } else {
                $.ajax({
                    type: "POST",
                    url: "manageCities.php",
                    data: $('#city_form').serialize(),
                    dataType: "JSON",
                    success: function(result) {
                        //alert(result.message);
                        $('#city_span').text(result.message);
                    }
                });
            }
        });
    });
</script>
