<?php
$status = '1';

if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}

require_once '../Models/ConDB.php';
$db = new ConDB();
?>

<style>
    #regVehicleErr,#regMaxErr,#regMinErr,#regBaseErr,#regKmErr,#regFareErr,#regDescErr,#cityErr
    {
        color:red;
    }
</style>

<div class="content">

    <div style="font-size:20px;">ADMIN USERS</div>
    <div style="float:right;">
        <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" data="3" data-msg="active"><a id="addButton" href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">ADD</a></button>    

    </div>
    <div style="float:none;"></div>
    <div id="refresh_table"><?php require ('refreshUsers.php'); ?></div>

</div> 

<div class="modal" id="modal_default_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body clearfix">

                <div class="controls">
                    <form  action="" autocomplete="on" style="color:#000;" method="post" id="add_edit_form"> 
                        <div id="add_edit_heading"><h1>ADD A USER</h1> </div>
                        <p> 
                            <label for="username" class="uname" > Username *</label>
                            <input id="vechiletype" name="username" required="required" class="editname" type="text" placeholder="eg. username "/>
                            <span id="regVehicleErr"></span>
                        </p>
                        <p> 
                            <label for="username" class="uname" > Email *</label>
                            <input id="vechiletype" name="email" required="required" class="editname" type="text" placeholder="eg. admin@paradisride.com "/>
                            <span id="regVehicleErr"></span>
                        </p>
                        <p> 
                            <label for="text" class="youpasswd" > Password *</label>
                            <input id="password" name="password" required="required"  class="edit_max_size" type="text" placeholder="eg. P@$5word" /> 
                            <span id="regMaxErr"></span>
                        </p>
                        <p> 
                            <label for="text" class="youpasswd" > Confirm Password *</label>
                            <input id="conf_password" name="conf_password" required="required"  class="edit_max_size" type="text" placeholder="eg. P@$5word" /> 
                            <span id="regMaxErr"></span>
                        </p>

                        <input type="hidden" name="type" value="0"/>

                    </form>               
                </div>

            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_modal" style="color: red;
                        border: solid 1px #a2a2a2;">Close</button>
                <button type="button" class="btn btn-success btn-clean" id="datasubmit">Add User</button>
            </div>
        </div>
    </div>
</div>    

<script type="text/javascript">
    $(document).ready(function () {
//            alert('1');
        $('.delete_user').click(function () {
            $.ajax({
                type: "POST",
                url: "updatePass.php",
                data: {mas_id: $(this).attr('data'), type: 1},
                dataType: "JSON",
                success: function (result) {
                    alert(result.message);

//                    $('#refresh_table').load('refreshUsers.php', {type: 1});
                }
            });
        });

        $('#datasubmit').click(function () {
            var pass = $('#password').val();
            var conf_pass = $('#conf_password').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {
                $.ajax({
                    type: "POST",
                    url: "updatePass.php",
                    data: $('#add_edit_form').serialize(),
                    dataType: "JSON",
                    success: function (result) {
                        alert(result.message);
                        if (result.flag == 0) {
                            $('#close_modal').trigger('click');
                            $('#refresh_table').load('refreshUsers.php', {type: 1});
                        }
                    }
                });
            }
        });
    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>


