<?php
session_start();
$curTime = time();
if ($_SESSION['admin'] != 'super' || $_SESSION['validity'] <= $curTime) {

    header('location: logout_super.php');
}

$addUserPage = "";

if ($_SESSION['admin_type'] == '2') {
    $addUserPage .= "<li class='has-sub'  data='1001' style='width:230px;'><a href='#' class='list_item001' style='background:none;height:40px;padding-top: 11px;padding-left: 30px;'><span><img src='images/calendar_off.png' onmouseover='bookhover(this);' onmouseout='bookunhover(this);' height='25' width='30'/> </span><span style='font-size: 14px;color: #A8A8A8;padding-left:8px;'>MANAGE USER</span></a>
                            <ul>

                            </ul>
                        </li>";
}

$_SESSION['type'] = '1';

$oneHourExp = (24 * 60) + time();
$_SESSION['validity'] = $oneHourExp;

include '../Models/ConDB.php';
$db = new ConDB();
?>
<!DOCTYPE html>
<html lang="en" style="color:#2a2a2a;">
    <head>        
        <title>SUPER ADMIN</title>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" type="image/ico" href="favicon.ico"/>
        <link href="css/stylesheets.css" rel="stylesheet" type="text/css" />        
        <script type='text/javascript' src='js/plugins/jquery/jquery.min.js'></script>
        <script type='text/javascript' src='js/plugins/jquery/jquery-ui.min.js'></script>   
        <script type='text/javascript' src='js/plugins/jquery/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>    
        <script type='text/javascript' src='js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='js/plugins/uniform/jquery.uniform.min.js'></script>
        <script type='text/javascript' src='js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='js/actions.js'></script>
        <script type='text/javascript' src='js/settings.js'></script>
        <script type='text/javascript' src='js/plugins.js'></script>
        <link rel='stylesheet' type='text/css' href='menu_source/styles.css' />
        <script type='text/javascript' src='menu_source/menu_jquery.js'></script>
        <style>
            #overlay_div{display: none;}
            .container{max-width: none !important;background: #fff;color: #000;}
            .page-content{background:#fff;}
            .page-container{box-shadow:none;}
            .btn.btn-success{border-color:none;background:#2a2a2a;}
        </style>

        <script>


            var type = '1';
            var pageName = 'getDrivers.php';
            var typeId = '1';

            $(document).on('change', '#citychange,#companychange', function () {
//                $('#city-filter-form').submit();

                $('#overlay_div').toggle();
                $('.container').load(pageName, {type: typeId, cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                    $('#overlay_div').toggle();
                });
                /*     
                 $('#refresh_table').load('cityfilterrefreshcompanydetail.php', {type:status,cityid: city_id}, function() {
                 
                 /********/
            });

            $(document).ready(function () {

                $('#overlay_div').toggle();

                $('.container').load('getDrivers.php', {type: '1'}, function () {
                    $('#overlay_div').toggle();
                });

                /****************************************/

                $('.list_item100').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'appvariable.php';
                        $('.container').load('appvariable.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item001').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'manageUser.php';
                        $('.container').load('manageUser.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });


                $('.list_item').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type)
                    {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'getDrivers.php';
                        $('.container').load('getDrivers.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item1').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'getPassengers.php';
                        $('.container').load('getPassengers.php', {type: dis.attr('type')}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item19').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = 1
                        pageName = 'ratings.php';
                        $('.container').load('ratings.php', {type: 1, cityid: $('#citychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item26').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'getvehilceapproval.php';
                        $('.container').load('getvehilceapproval.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item27').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'assignedVehicles.php';
                        $('.container').load('refreshAssignedVehicles.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item16').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'cancelbooking.php';
                        $('.container').load('cancelbooking.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item2').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'getreview.php';
                        $('.container').load('getreview.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item3').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'booking.php';
                        $('.container').load('booking.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item4').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'financeOverview.php';
                        $('.container').load('financeOverview.php', {type: dis.attr('type')}, function () {

                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item5').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'financeHistory.php';
                        $('.container').load('financeHistory.php', {cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {

                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item175').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'vehiclemodel.php';
                        $('.container').load('vehiclemodel.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item176').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'vehiclemodeltype.php';
                        $('.container').load('vehiclemodeltype.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item105').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'financeStatements.php';
                        $('.container').load('finance_Statements.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {

                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item106').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'refreshallbookings.php';
                        $('.container').load('refreshallbookings.php', {page: 1, noofentries: 20, type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {

                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item6').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'getCompanyinfo.php';
                        $('.container').load('getCompanyinfo.php', {type: dis.attr('type'), cityid: $('#citychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item7').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'getdriverlicence.php';
                        $('.container').load('getdriverlicence.php', {type: dis.attr('19'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item42').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'rcpaper.php';
                        $('.container').load('rcpaper.php', {type: dis.attr('52'), cityid: $('#citychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item44').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'insurancepaper.php';
                        $('.container').load('insurancepaper.php', {type: dis.attr('54'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item46').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'registrationpaper.php';
                        $('.container').load('registrationpaper.php', {type: dis.attr('56'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item8').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'getdriverbankdet.php';
                        $('.container').load('getdriverbankdet.php', {type: dis.attr('type'), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item9').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = 1;
                        pageName = 'vechiletypelist.php';
                        $('.container').load('vechiletypelist.php', {type: 1, cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item10').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'getvehilceapproval.php';
                        $('.container').load('getvehilceapproval.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item21').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'driverlog.php';
                        $('.container').load('driverlog.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item22').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');

                        $('.container').load('logoutdriver.php', {type: dis.attr('type')}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item11').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'vechileformadd.php';
                        $('.container').load('vechileformadd.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item70').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');

                        $('.container').load('addcity.php', {type: dis.attr('type')}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item71').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');

                        $('.container').load('citylist.php', {type: dis.attr('type')}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item72').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');

                        $('.container').load('manageCities.php', {type: dis.attr('type')}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item31').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'getDisputes.php';
                        $('.container').load('getDisputes.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item150').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'deletepage.php';
                        $('.container').load('deletepage.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item178').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'coupons_getCampaigns.php';
                        $('.container').load('coupons_getCampaigns.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                $('.list_item179').click(function () {
                    var dis = $(this);
                    if (dis.attr('data') != type) {
                        $('#overlay_div').toggle();
                        type = dis.attr('data');
                        typeId = dis.attr('type');
                        pageName = 'coupons_getCampaigns.php';
                        $('.container').load('coupons_getCampaigns.php', {type: dis.attr('type'), cityid: $('#citychange').val(), companyid: $('#companychange').val()}, function () {
                            $('#overlay_div').toggle();
                        });
                    }

                });
                /****************/

                $('#change_pass_superadmin').click(function () {
                    var pass = $('#chng_pass').val();
                    var conf_pass = $('#chng_conf_pass').val();
                    if (pass == '' || conf_pass == '') {
                        alert('Passwords are mandatory.');
                    } else if (pass != conf_pass) {
                        alert('Passwords does not match, check once.');
                    } else if (checkStrength(pass) == 1) {
                        alert('Password must contain atleast one digit,one Uppercase, one Lower case character and atleast 8 digit ');
                    } else {
                        // alert('coming');
                        $.ajax({
                            type: "POST",
                            url: "changePass.php",
                            data: {doc_id: $('#sendData_a').val(), pass: conf_pass, type: 1},
                            dataType: "JSON",
                            success: function (result) {
                                //  alert(result.flag);
                                alert(result.message);
                                if (result.flag == 0) {
//                                    alert(result.message);
                                    $('#chng_pass').val("");
                                    $('#chng_conf_pass').val("");
                                    $('#change_pass_cancel').trigger('click');
                                }
                            },
                            error: function (result) {
                                alert('error');
                            }
                        });
                    }
                });
            });
            function checkStrength(password)
            {
                //initial strength
                var strength = 0;
                //if the password length is less than 6, return message.
                if (password.length < 6) {
                    return 1;
                }

                //length is ok, lets continue.

                //if length is 8 characters or more, increase strength value
                if (password.length > 7)
                    strength += 1;
                //if password contains both lower and uppercase characters, increase strength value
                if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                    strength += 1;
                //if it has numbers and characters, increase strength value
                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                    strength += 1;
                //if it has one special character, increase strength value
                if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                    strength += 1;
                //if it has two special characters, increase strength value
                if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                    strength += 1;
                //now we have calculated strength value, we can return messages

                //if value is less than 2
                if (strength < 3)
                {
                    return 2;
                }
            }

//            function hover(element)
//            {
//                element.setAttribute('src', 'images/driver_on.png');
//                element.setAttribute('color', '#fff');
//            }
//
//            function unhover(element)
//            {
//                element.setAttribute('src', 'images/driver_off.png');
//            }
//            function passengerhover(element)
//            {
//                element.setAttribute('src', 'images/passenger_on.png');
//            }
//            function passengerunhover(element)
//            {
//                element.setAttribute('src', 'images/passenger_off.png');
//            }
//            function reviewhover(element)
//            {
//                element.setAttribute('src', 'images/ratings_on.png');
//            }
//            function reviewunhover(element)
//            {
//                element.setAttribute('src', 'images/ratings_off.png');
//            }
//            function bookhover(element)
//            {
//                element.setAttribute('src', 'images/calendar_on.png');
//            }
//            function bookunhover(element)
//            {
//                element.setAttribute('src', 'images/calendar_off.png');
//            }
//            function financehover(element)
//            {
//                element.setAttribute('src', 'images/finance_on.png');
//            }
//            function financeunhover(element)
//            {
//                element.setAttribute('src', 'images/finance_off.png');
//            }
//            function companyhover(element)
//            {
//                element.setAttribute('src', 'images/company_on.png');
//            }
//            function companyunhover(element)
//            {
//                element.setAttribute('src', 'images/company_off.png');
//            }
//            function bookhover(element)
//            {
//                element.setAttribute('src', 'images/book_on.png');
//            }
//            function bookunhover(element)
//            {
//                element.setAttribute('src', 'images/book_off.png');
//            }
//            function roadyohover(element)
//            {
//                element.setAttribute('src', 'images/roadyo_icon_on.png');
//            }
//            function roadyounhover(element)
//            {
//                element.setAttribute('src', 'images/roadyo_icon_off.png');
//            }


        </script>

    </head>
    <body class="bg-img-num1" style="color:#2a2a2a;"> 
        <div class="header">
            <div class='root'>
                <div style="width: 100%; height:55px;text-align: center; background-image: url(images/header_bar.png);">
                    <div class="modal modal-draggable" id="modal_default_13" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content" style="background:#2a2a2a;color:#fff;">                
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Change super admin password</h4>
                                </div>                
                                <div class="modal-body clearfix" style="text-align:center;">
                                    <label style="width:150px;color:#fff;">New Password:</label> <input type="password"   name="pass" style="width: 200px;display: inline;" id="chng_pass"><br><br>
                                    <label style="width:150px;color:#fff;">Confirm Password:</label> <input type="password" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass" ><br>
                                    <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                                    <span style="color:red;" id="errmsg"></span>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning btn-clean" id="change_pass_superadmin">Submit</button>              
                                    <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel">Cancel</button>              
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="width: 230px;height: 59px;float: left;background: #2a2a2a;padding-top:6px;" >
                        <img src="images/roadyo_logo.png" style="height: 50px;" />
                    </div>
                    <!--<div style="width: 230px;height: 59px;float: left;background: #2a2a2a;padding-top:6px;" >                        
                        <form action="index.php">
                            <select  name="countryname" id="cityname" class="editcompany" style="background: wheat;color: black;">
                                <option value="NULL">Select a City</option>
                    <?php
//                                $get_country_type = "select * from city_available";
//                                $get_country_type_res = mysql_query($get_country_type, $db->conn);
//                                while ($typelist = mysql_fetch_array($get_country_type_res)) {
//                                    echo "<option value='" . $typelist['City_Id'] . "'>" . $typelist['City_Name'] . "</option>";
//                                }
                    ?>
                                <option value="Others" name="others">Others</option>
                            </select>
                        </form>
                    </div>-->
                    <div style=" background: url(images/logo.png) no-repeat; background-size: 109px 18px; background-position: center 35%; height: 59px;background: #fff;background: #2a2a2a;">

                        <!--**************************-->

                        <div style="float: left; width: 650px !important;margin-top:20px;">
                            <?php
                            $accQry = "select City_name,City_id from city_available order by City_Name ASC";
                            $result1 = mysql_query($accQry, $db->conn);
                            ?>
                            <form id="city-filter-form" method="get">
                                <label style="color:#A8A8A8;font-size:14px;padding-right:7px;">CITY</label>
                                <select name="filter" class="btn btn-danger btn-clean" style="border-radius: 7px;width:150px;margin-right:80px;background:#fff;color:black;border-color: transparent;" id="citychange">
                                    <option value="">None</option>
                                    <?php
                                    while ($row = mysql_fetch_assoc($result1)) {
                                        $selected = "";
                                        if (isset($_GET['filter']) && $row['City_id'] == $_GET['filter'])
                                            $selected = "selected";
                                        // else if(!isset ($_GET['filter']) && $row['City_id'] == '18833')
                                        //    $selected = "selected";
                                        ?>
                                        <option value="<?php echo $row['City_id']; ?>" <?php echo $selected; ?>><?php echo $row['City_name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>


                                <?php
                                $accQry1 = "select company_id,companyname from company_info where status= 3 ";
                                $result11 = mysql_query($accQry1, $db->conn);
                                ?>
                                <label style="color:#A8A8A8;padding-right:7px;font-size:14px;">COMPANY</label>
                                <select name="companyfilter" class="btn btn-danger btn-clean" style="border-radius: 7px;width:150px;margin-right:80px;background:#fff;;color:black;border-color:transparent;" id="companychange">
                                    <option value="">None</option>
                                    <?php
                                    while ($row1 = mysql_fetch_assoc($result11)) {
                                        $selected1 = "";
                                        if (isset($_GET['companyfilter']) && $row1['company_id'] == $_GET['companyfilter'])
                                            $selected1 = "selected";
                                        // else if(!isset ($_GET['filter']) && $row['City_id'] == '18833')
                                        //    $selected = "selected";
                                        ?>
                                        <option value="<?php echo $row1['company_id']; ?>" <?php echo $selected1; ?>><?php echo $row1['companyname']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>


                            </form>


                        </div>

                        <!--***********************************--> 








                        <div class="top-bar-right" style="float: right;margin-right: 4%;">
                            <ul class="buttons" style="margin: 0%">

                                <li class="btn-group" style="padding-top: 13px;font-size: 15px;font-weight: bold;">                
                                    <span class="avatar"><img src="images/avatar.png" class="img-circle" width="32" height="32" alt=""></span>
                                    <a href="#" class="dropdown-toggle tip"  title="Dropdown" data-toggle="dropdown" style="color: #888888;">

                                        <?php echo "" . $_SESSION['admin_name']; ?>
                                    </a>
                                    <ul class="dropdown-menu" role="menu" style="background:#2a2a2a;margin-top:18px;">
                                        <li><a href="#modal_default_13" data-toggle="modal" class="btn btn-success btn-block">CHANGE PASSWORD</a></li>

                                        <li><a href="logout_super.php" id="logout" name="logout">LOG OUT</a></li>  
                                    </ul>                                                                            
                                </li>
                            </ul>
                            </a>

                        </div>
                        &nbsp;</div>

                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div style="position: fixed;z-index: 9999;margin: 0;width: 100%;height: 100%;background: #000000;opacity: 0.4;" id="overlay_div">
            <div style="position: relative;top:30%;left:45%;">
                <img src="img/spinner.gif" height="75" width="75">
            </div>
        </div>
        <div class="page-container">
            <div class="page-sidebar">




                <div id='cssmenu'>
                    <ul>
                        <li class='active' style="width:230px;height: 3px;"><a href='index.php' style="background: none;"><span style="visibility: hidden;">Home</span></a></li>


<!--                        <li class='has-sub' type="1" style='width:230px;'><a href='#' data="001" class="list_item100" style="background:none;height:40px;padding-top: 11px;padding-left: 30px;"><span><img src='images/calendar_off.png' onmouseover="bookhover(this);" onmouseout="bookunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">APP VARIABLES</span></a>
                            <ul>

                            </ul>
                        </li>	-->

                        <?php echo $addUserPage; ?>

                        <!--
                        
                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;padding-left: 30px;"><span><img src='images/passenger_off.png' onmouseover="passengerhover(this);" onmouseout="passengerunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">CITIES</span></a>
                            <ul>
                                <li class='last'><a href='#'class="list_item100" type="" data="73"><span style="padding-left: 50px;">APP VARIABLES</span></a></li>
                             </ul>
                        </li>
                        
                        -->

                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;"><span><img src='images/cities.png' onmouseover="passengerhover(this);" onmouseout="passengerunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Add Country & City</span></a>
                            <ul>
                                <li class='last'><a href='#'class="list_item72" type="2" data="72"><span style="padding-left: 50px;">Manage</span></a></li>
                                <li class='last'><a href='#'class="list_item71" type="1" data="6"><span style="padding-left: 50px;">List Of Cities</span></a></li>
                            </ul>
                        </li>

                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;"><span><img src='images/companies.png' onmouseover="companyhover(this);" onmouseout="companyunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Add New Company</span></a>
                            <ul>
                                <li><span></span><a href='#' class="list_item6" type="1" data="12"><span style="padding-left: 50px;">New</span></a></li>
                                <li><a href='#'class="list_item6" type="3,4" data="13"><span style="padding-left: 50px;">Accepted</span></a></li>
                                <li><a href='#' class="list_item6" type="5" data="14"><span style="padding-left: 50px;">Rejected</span></a></li>
                                <!--<li><a href='#' class="list_item6" type="6" data="15"><span style="padding-left: 50px;">SUSPENDED</span></a></li>-->
                            </ul>
                        </li>
                        <li class='has-sub' style='width:230px;'><a href='#' class="list_item9" type="1" style="background:none;height:40px;padding-top: 11px;"><span><img src='images/vehicle_types.png' onmouseover="roadyohover(this);" onmouseout="roadyounhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Vehicle Types & Rates</span></a>
                            <ul>
                            </ul>
                        </li>

                        <li class='has-sub' style='width:230px;'><a href='#'   style="background:none;height:40px;padding-top: 11px;width: 350px;"><span><img src='images/vehicle_types.png' onmouseover="roadyohover(this);" onmouseout="roadyounhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Driver Vehicle Information</span></a>
                            <ul>
                                <li><a href='#' class="list_item11" type="5" data="20"><span style="padding-left: 50px;">NEW</span></a></li>
                                <li><a href='#'class="list_item10" type="2" data="21"><span style="padding-left: 50px;">ACCEPTED</span></a></li>
                                <li><a href='#' class="list_item10" type="4" data="22"><span style="padding-left: 50px;">REJECTED</span></a></li>
                              <!--  <li><a href='#' class="list_item27" type="8" data="26"><span style="padding-left: 50px;">OFFLINE</span></a></li>
                                --> 
                                <li><a href='#' class="list_item26" type="5" data="24"><span style="padding-left: 50px;">FREE VEHICLE</span></a></li>
                                <li><a href='#' class="list_item27" type="6" data="25"><span style="padding-left: 50px;">ASSIGNED VEHICLE</span></a></li>

<!-- <li><a href='#' class="list_item10" type="6" data="23"><span style="padding-left: 50px;">LIVE BUT NOT BOOKED</span></a></li>

<li><a href='#' class="list_item27" type="7" data="25"><span style="padding-left: 50px;">LIVE AND BOOKED</span></a></li>
                                -->
                            </ul>
                        </li>

                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;"><span><img src='images/drivers.png' onmouseover="hover(this);" onmouseout="unhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Drivers</span></a>
                            <ul>
                                <li><a href='#' class="list_item" type="1" data="1"><span style="padding-left: 50px;">New</span></a></li>
                                <li><a href='#'class="list_item" type="3" data="2"><span style="padding-left: 50px;">Accepted</span></a></li>
                                <li><a href='#' class="list_item" type="4" data="3"><span style="padding-left: 50px;">Rejected</span></a></li>
                                <li><a href='#' class="list_item21" type="5" data="4"><span style="padding-left: 50px;">Offline</span></a></li>
                                <li><a href='#' class="list_item21" type="6" data="8"><span style="padding-left: 50px;">Online And Free</span></a></li>
                                <li><a href='#' class="list_item21" type="7" data="5"><span style="padding-left: 50px;">Online And Booked</span></a></li>
                            </ul>
                        </li>
                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;"><span><img src='images/passanger.png' onmouseover="passengerhover(this);" onmouseout="passengerunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Registered Passengers</span></a>
                            <ul>
                                <li><a href='#'class="list_item1" type="3" data="122"><span style="padding-left: 50px;">Active</span></a></li>
                                <li class='last'><a href='#'class="list_item1" type="4" data="121"><span style="padding-left: 50px;">Inactive</span></a></li>
                            </ul>
                        </li>

                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;"><span><img src='images/booking.png' onmouseover="bookhover(this);" onmouseout="bookunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Live Booking Information</span></a>
                            <ul>
                                <li><a href='#' class="list_item3" type="6" data="9"><span style="padding-left: 50px;">Driver On The Way</span></a></li>

                                <li><a href='#'class="list_item3" type="7" data="36"><span style="padding-left: 50px;">Driver Arrived </span></a></li>
                                <li><a href='#' class="list_item3" type="8" data="10"><span style="padding-left: 50px;">Booking Started</span></a></li>


                                <li><a href='#'class="list_item3" type="9" data="35"><span style="padding-left: 50px;">Booking Completed</span></a></li>


                            </ul>
                        </li>
                        <li class='has-sub' type="1" style='width:230px;'><a href='#' class="list_item16" style="background:none;height:40px;padding-top: 11px;"><span><img src='images/cancel_booking.png' onmouseover="bookhover(this);" onmouseout="bookunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Canceled  Bookings</span></a>
                            <ul>
                                <!--<li><a href='#' class="list_item16" type="1" data="59"><span style="">CANCELLED</span></a></li>
                                <!--<li><a href='#' class="list_item16" type="2" data="60"><span style="">AFTER CAB WAS ASSIGNED BUT BEFORE 5 Min.</span></a></li>
                                <li><a href='#' class="list_item16" type="3" data="61"><span style="">AFTER CAB WAS ASSIGNED AND AFTER 5 Min.</span></a></li>
                                <li><a href='#' class="list_item16" type="4" data="62"><span style="">AFTER CAB WAS ASSIGNED BUT PASSENGER NOT CAME</span></a></li>
                                <li><a href='#' class="list_item16" type="5" data="63"><span style="">AFTER CAB WAS ASSIGNED BUT PASSENGER ADDRESS WRONG</span></a></li>
                                <li><a href='#' class="list_item16" type="6" data="64"><span style="">AFTER CAB WAS ASSIGNED BUT PASSENGER ASKED TO CANCEL</span></a></li>
                                <li><a href='#' class="list_item16" type="7" data="65"><span style="">AFTER CAB WAS ASSIGNED BUT DON`T CHARGE THE CUSTOMER</span></a></li>
                                <li><a href='#' class="list_item16" type="8" data="66"><span style="">OTHERS</span></a></li>-->
                            </ul>
                        </li>	

                        <li class='has-sub' type="2" style='width:230px;'><a href='#' class="list_item106" data="310" style="background:none;height:40px;padding-top: 11px;"><span><img src='images/dispatcher.png' onmouseover="bookhover(this);" onmouseout="bookunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">All Dispatched Jobs</span></a>
                            <ul>
                            </ul>
                        </li>	
                        
                        
                        <li class='has-sub' style='width:230px;'><a href='../dashboard/index.php/admin/payroll' target="_blank" type="1" style="background:none;height:40px;padding-top: 11px;"><span><img src='images/payroll.png' onmouseover="roadyohover(this);" onmouseout="roadyounhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Payroll</span></a>
                            <ul>
                            </ul>
                        </li>
                        <li class='has-sub' style='width:230px;'><a href='../dashboard/index.php/admin/transection' target="_blank" type="1" style="background:none;height:40px;padding-top: 11px;"><span><img src='images/accounting.png' onmouseover="roadyohover(this);" onmouseout="roadyounhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Accounting</span></a>
                            <ul>
                            </ul>
                        </li>

<!--                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;padding-left: 30px;"><span><img src='images/finance_off.png' onmouseover="financehover(this);" onmouseout="financeunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">FINANCE</span></a>
                            <ul>
                                <li><a href='#' class="list_item4" type="1" data="10"><span>OVERVIEW</span></a></li>
                                <li><a href='#' class="list_item5" type="2" data="111"><span style="padding-left: 50px;">HISTORY</span></a></li>
                                <li><a href='#' class="list_item105" type="2" data="166"><span style="padding-left: 50px;">STATEMENTS</span></a></li>
                                 <li><a href='#' class="list_item106" type="2" data="167"><span style="padding-left: 50px;">ALL BOOKINGS</span></a></li>
                                
                            </ul>
                        </li>-->


                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;"><span><img src='images/document.png' onmouseover="bookhover(this);" onmouseout="bookunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Document</span></a>
                            <ul>
                                <li><a href='#' class="list_item7" type="" data="19"><span style="padding-left: 50px;">Driver`s Licence</span></a></li>
                                <li><a href='#'class="list_item8" type="2" data="17"><span style="padding-left: 50px;">Bank Passbook </span></a></li>
                                <li><a href='#'class="list_item42" type="2" data="52"><span style="padding-left: 50px;">Carriage Permit</span></a></li>
                                <li><a href='#'class="list_item44" type="2" data="54"><span style="padding-left: 30px;">Insurance Certificate </span></a></li>
                                <li><a href='#'class="list_item46" type="2" data="56"><span style="padding-left: 0px;">Certificate Of Registration </span></a></li>
                            </ul>
                        </li>

                        <li class='has-sub' style='width:230px;'><a href='#' class="list_item19" type="1" data="71" style="background:none;height:40px;padding-top: 11px;"><span><img src='images/passanger_rating.png' onmouseover="reviewhover(this);" onmouseout="reviewunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Passenger Rating</span></a>
                            <ul>
                            </ul>
                        </li>
                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;"><span><img src='images/driver_review.png' onmouseover="reviewhover(this);" onmouseout="reviewunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Driver Review</span></a>
                            <ul>
                                <li><a href='#' class="list_item2" type="1" data="107"><span style="padding-left: 50px;">Active</span></a></li>
                                <li class='last'><a href='#'class="list_item2" type="2" data="108"><span style="padding-left: 50px;">Inactive</span></a></li>

                            </ul>
                        </li>
                        <li class='has-sub' style='width:230px;'><a href='#' style="background:none;height:40px;padding-top: 11px;"><span><img src='images/dispuite.png' onmouseover="reviewhover(this);" onmouseout="reviewunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Disputes</span></a>
                            <ul>
                                <li><a href='#' class="list_item31" type="1" data="117"><span style="padding-left: 50px;">Reported</span></a></li>
                                <li class='last'><a href='#'class="list_item31" type="2" data="118"><span style="padding-left: 50px;">Resolved</span></a></li>

                            </ul>
                        </li>
                       
                        <li class='has-sub' type="1" style='width:230px;'><a href='#' class="list_item150" data="198" style="background:none;height:40px;padding-top: 11px;"><span><img src='images/delete.png' onmouseover="bookhover(this);" onmouseout="bookunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Delete</span></a>
                            <ul>
                            </ul>
                        </li>	
                        <li class='has-sub' type="1" style='width:230px;'><a href='#'data="300" style="background:none;height:40px;padding-top: 11px;"><span><img src='images/vehicele_model.png' onmouseover="bookhover(this);" onmouseout="bookunhover(this);" height='25' width='30'/> </span><span style="font-size: 14px;color: #A8A8A8;padding-left:8px;">Vehicle Models</span></a>
                            <ul>
                                <li class='last'><a href='#'class="list_item176" type="2" data="1181"><span style="padding-left: 50px;">Vehicle Make</span></a></li>

                                <li><a href='#' class="list_item175" type="1" data="1188"><span style="padding-left: 50px;">Vehicle Models</span></a></li>

                            </ul>
                        </li>	
                     

                    </ul>
                </div>
            </div>
            <div class="page-content">
                <div class="container">        
                </div>
            </div>
        </div>


    </body>
</html>
