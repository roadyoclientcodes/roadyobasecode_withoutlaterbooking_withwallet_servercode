<?php
include('../Models/ConDB.php');
$db1 = new ConDB();
if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '5';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>
<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>
<script type="text/javascript">
    $(document).ready(function () {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 10, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null, null]});
    });
</script>

<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
    <thead style="font-size: 12px;">
        <tr>
            <th width="8%">VEHICLE ID</th>
            <th width="8%">VEHICLE MAKE</th>
            <th width="8%">VEHICLE MODEL</th>
            <th width="8%">VEHICLE TYPE</th> 
            <th width="8%">VEHICLE REG NO</th>
            <th width="8%">LICENSE PLATE NO</th> 
            <th width="8%">INSURANCE NUMBER</th>
            <th width="8%">VEHICLE SEATING</th>
            <th width="8%">VEHICLE COLOR</th>   
            <th width="8%">COMPANY</th>   
            <th width="8%">SELECT </th>
        </tr>
    </thead>
    <tbody style="font-size: 12px;">
        <?php
        if ($cityid == '' && $companyids == '') {
            $accQry = "SELECT w.*,wt.max_size,wt.type_name FROM workplace w, workplace_types wt where w.type_id = wt.type_id and w.status IN ('" . $status . "')  order by w.workplace_id desc";
        } else if ($cityid != '' && $companyids == '') {
            $accQry = "SELECT w.*,wt.max_size,wt.type_name FROM workplace w, workplace_types wt where w.type_id = wt.type_id and w.status IN ('" . $status . "') and wt.type_id = '" . $cityid . "' order by w.workplace_id desc";
        } else if ($cityid == '' && $companyids != '') {
            $accQry = "SELECT w.*,wt.max_size,wt.type_name FROM workplace w, workplace_types wt where w.type_id = wt.type_id and w.status IN ('" . $status . "') and w.company IN(" . $companyids . ") order by w.workplace_id desc";
        } else {
            $accQry = "SELECT w.*,wt.max_size,wt.type_name FROM workplace w, workplace_types wt where w.type_id = wt.type_id and w.status IN ('" . $status . "') and w.company IN(SELECT company_id FROM company_info WHERE city = " . $cityid . " and company_id = " . $companyids . ") and wt.city_id = '" . $cityid . "' order by w.workplace_id desc";
        }
        // echo $accQry;
        $result1 = mysql_query($accQry, $db1->conn);
        $i = 1;
        while ($row = mysql_fetch_assoc($result1)) {
            ?>
            <tr id="doc_rows<?php echo $i; ?>">
                <td   id="<?Php echo "workplace_id" . $i; ?>"><?php echo $row['uniq_identity'] ?></td>
                <td><a target="_blank" href="" data="<?php echo $i; ?>" data-toggle="modal"> 
                        <?php
                        $get_title = "select * from vehicleType where id='" . $row['Title'] . "'";
                        $get_title_res = mysql_query($get_title, $db1->conn);
                        $get_title_row = mysql_fetch_assoc($get_title_res);

                        echo $get_title_row['vehicletype']
                        ?>
                    </a></td>
                <td  id="<?Php echo "Vechile_Model" . $i; ?>">
                    <?php
                    $get_title = "select * from vehiclemodel where id='" . $row['Vehicle_Model'] . "'";
                    $get_title_res = mysql_query($get_title, $db1->conn);
                    $get_title_row = mysql_fetch_assoc($get_title_res);
                    echo $get_title_row['vehiclemodel']
                    ?>
                </td>
                <td>
                    <?php
                    echo $row['type_name'];
                    ?>
                </td>
                <td><?Php echo $row['Vehicle_Reg_No'] ?></td>
                <td><?Php echo $row['License_Plate_No']; ?></td>
                <td><?Php echo $row['Vehicle_Insurance_No']; ?></td>
                <td><?Php echo $row['max_size']; ?></td>
                <td><?Php echo $row['Vehicle_Color']; ?></td>
                <td>
                    <?php
                    $get_company = "select * from company_info where company_id='" . $row['company'] . "'";
                    $get_company_res = mysql_query($get_company, $db1->conn);
                    $get_company_row = mysql_fetch_assoc($get_company_res);
                    echo $get_company_row['companyname']
                    ?>
                </td>
                <td><input dat="<?php echo $i; ?>" type="checkbox" name="checkbox_advertiser"  class="custom_check" value="<?php echo $row['workplace_id']; ?>" style="background: white;height: 20px;width: 12px;" /></td>
            </tr>
            <?php
            $i++;
        }
        ?> 

    </tbody>
</table> 