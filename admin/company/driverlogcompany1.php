<?php
session_start();
include('../../Models/ConDB.php');
$db1 = new ConDB();

if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_GET['mas_id'])) {
    $_SESSION['admin_id'] = $_GET['mas_id'];
}
?>

<script type='text/javascript' src='js/settings.js'></script>
<script type="text/javascript">
    $(document).ready(function () {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 9, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null]});
    });
</script>
<!--<script type='text/javascript' src='js/actions.js'></script>-->
<!--<div class="page-content page-content-white" style="margin: 0;">-->
<!--alert = function() {};-->
<div class="modal" id="modal_default_4"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style=" color: white;background: white;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <!--<h4 class="modal-title">ADD  the Program Details..</h4>-->
            </div>
            <div class="modal-body clearfix" style="position: relative;">
                <div id="spinner_image" style="display:none;position:absolute;top:40%;left:40%;"><img src="img/spinner.gif" /></div>
                <!--<form id="popup1" action="Report1.php" method="Post">-->
                <table class="table table-bordered table-striped table-hover">
                    <tbody>


                        NEW PASSWORD: <input type="text"   name="pass" style="width: 200px;margin-left: 130px;margin-top: -23px;" id="chng_pass">&nbsp;<span style="color:red;" id="errmsg"></span><br> <br>
                    CONFIRM PASSWORD <input type="text" name="conf_pass" style="width: 200px;margin-left: 130px;margin-top: -23px;" id="chng_conf_pass" ><br><br>
                    <input type="hidden" name="sendData" id="sendData"/>

                    </tbody>
                </table>       
            </div>
            <div class="modal-footer">                
                <button type="button"  style="margin-right: 26%; width: 90px;" class="btn btn-warning btn-clean" data-dismiss="modal" id="close_popup">Cancel</button>
                <button type="button" style="float: left;margin-left: 30%;" name="Add" id="change_pass" class="btn btn-warning btn-clean">Submit form</button>
            </div>
        </div>
    </div>
</div>  

<div class="content">
    <div style="text-align: center;">
        <h1>
            <?php
            if ($status == '5') {
                echo 'OFFLINE DRIVERS';
                
            } else if ($status == '6') {
                echo 'ONLINE AND FREE';
            } else if ($status == '7') {
                echo 'ONLINE AND BOOKED';
            }
            ?>
        </h1>
    </div>
    <div style="float:right;">
        <?php
        if ($status == '1') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="1" data-msg="Log Out">LOG OUT</button>    
            <?php
        }
        ?>
    </div>

    <div style="float:none;"></div>
    <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
        <thead style="font-size: 12px;">
            <tr>
                <th width="8%">SL ID</th>
                <th width="8%">DRIVER ID</th>
                <th width="8%">DRIVER NAME</th>
                <th width="8%">COMPANY ID</th>
                <th width="8%">COMPANY NAME</th>
                <th width="8%">LOGIN DATETIME</th>   
                <th width="8%">VEHICLE ID</th>
                <th width="8%">VEHICLE TYPE</th>
                <th width="8%">LAST UPDATED LOCATION</th>

            </tr>
        </thead>
        <tbody style="font-size: 12px;">
            <?php
            $location = $db1->mongo->selectCollection('location');

            if ($status == '5')
                $user_data = $location->find(array('status' => 4));
            else if ($status == '6')
                $user_data = $location->find(array('status' => 3));
            else if ($status == '7')
                $user_data = $location->find(array('status' => 5));

            $user_query = '';

            foreach ($user_data as $driver) {
                $user_query .= $driver['user'] . ',';
            }
            $accQry = "SELECT m.mas_id,m.first_name,m.last_name,m.company_id,m.last_active_dt, m.workplace_id,m.type_id,
                (select type_name from workplace_types where type_id = m.type_id) as vehicletype, 
                (select companyname from company_info where company_id=m.company_id) as companyname  
FROM master m WHERE m.mas_id IN (" . rtrim($user_query, ',') . ") and company_id='" . $_SESSION['admin_id'] . "'
ORDER BY m.last_active_dt DESC ";

       // echo $accQry;
            $result1 = mysql_query($accQry, $db1->conn);
            $i = 1;

            while ($row = mysql_fetch_array($result1)) {

                if ($row['profile_pic'] == "") {
                    $st1 = "aa_default_profile_pic.gif";
                } else {
                    $st1 = $row['profile_pic'];
                }
                ?>


                <?php
                $driverids = $row['mas_id'];
                $docLoc = $location->findOne(array('user' => (int) $driverids));
                ?>

                <tr id="doc_rows<?php echo $i; ?>">
                    <td><?php echo $i; ?></td>
                    <td   id="<?php echo "driverid" . $i; ?>"><?php echo $row['mas_id']; ?></td>

                    <?php
                    $get_name = $row['first_name'] . ' ' . $row['last_name'];
                    ?>
                    <td>
                        <?php echo $get_name; ?>
                    </td>
                    <td><?php echo $row['company_id']; ?></td>
                    <td>
                        <?php echo $row['companyname']; ?>	
                    </td>

                    <td><?php echo $row['last_active_dt']; ?> </a></td>
                    <td><?php echo $row['workplace_id']; ?></td>
                    <td><?php echo $row['vehicletype']; ?></td>

                    <td>


                        <?php
                        /* $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$docLoc['location']['latitude'].",".$docLoc['location']['longitude']."&sensor=false";
                          $ch = curl_init();
                          curl_setopt($ch, CURLOPT_URL, $url);
                          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                          curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                          $response = curl_exec($ch);
                          curl_close($ch);
                          print($response);
                          $response = json_decode($response);
                          $lat = $response->results[0]->formatted_address;
                          echo $lat; */
                        ?>
                        <?php
                        $latitude = $docLoc['location']['latitude'];
                        $longitude = $docLoc['location']['longitude'];
                        $latlong = $latitude . ',' . $longitude;
//if($docLoc['location']['latitude']!=NULL)
                        echo $latlong;
                        ?>
                    </td>

                                                                    <!--<td><input dat="<?php echo $i; ?>" type="checkbox" name="checkbox_advertiser"  class="custom_check" value="<?php echo $row['dvid']; ?>" style="background: white;height: 20px;width: 12px;" /></td>-->

                </tr>
                <?php
                $i++;
            }
            ?>

        </tbody>
    </table>                                        

</div> 

<script>
    $(document).ready(function () {
        $('.resetPassword').click(function () {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });
        $('#change_pass').click(function () {

            var pass = $('#chng_pass').val();
            var conf_pass = $('#chng_conf_pass').val();
            if (pass == '' || conf_pass == '') {
                alert('Passwords are mandatory.');
            } else if (pass != conf_pass) {
                alert('Passwords does not match, check once.');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {doc_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function (result) {
                        alert(result.message);
                        if (result.flag == 0) {


                            $('#chng_pass').val("");
                            $('#chng_conf_pass').val("");

                            $('#close_popup').trigger('click');
                            $('.modal-backdrop.in').css('display', 'none');


                        }
                    }
                });
            }
        });

        $('#ActiveButton,#RejectButton').click(function () {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast one driver in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 11, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function (result) {
                        alert(result.message);
                        if (result.flag == 0) {
                            $('.custom_check').each(function () {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        }
                    }
                });

            }
        });

    });
</script>

<script>
    $(document).ready(function () {
//called when key is pressed in textbox
        $("#chng_pass").blur(function () {
            //if the letter is not digit then display error and don't type anything

            var zipcode_expression = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
            if (zipcode_expression.test($.trim($('#chng_pass').val())) == false) {
// $("#errmsg").html("invalid zipcode").show().fadeOut("slow")
                alert('Password must contain atleast one digit,one Uppercase, one Lower case character and atleast 8 digit ');
            }


            // $("#errmsg").html("Digits Only").show().fadeOut("slow");


        });

        //called when key is pressed in textbox
        $("#close_popup").click(function () {
            $('#chng_pass').val("");
            $('#chng_conf_pass').val("");

            // $('#close_popup').trigger('click');
            $('.modal-backdrop.in').css('display', 'none');


        });
    });
</script>


