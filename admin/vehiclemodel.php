<?php
//include('../Models/ConDB.php');
//$db1 = new ConDB();
if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}

?>
<style>
    #regCompanyErr,#regVehicleErr,#regTitleErr,#regVehicleModelErr,#regSeatingErr,#regVehicleregErr,#regLicenseErr,#regColorErr,#regExpireErr,#regExpireInsurErr,#regExpirePermitErr
    {
        color:red;
    }
</style>
<!---Code for adding images-->		
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>

<div class="content">
  <div style="float:right;">
               

            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" data="3" data-msg="active" id="addButton"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">ADD</a></button>    
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="2" data-msg="delete">DELETE</button>   
       

    </div>
   
    <div calss="vehiclemodel">
       
        <div id="refresh_table2"><?php include('getvehiclemodel.php');?></div>
    </div>

</div> 

<div class="modal" id="modal_default_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal with form</h4>
            </div>
            <div class="modal-body clearfix">

                <div class="controls">
                    <form  action="submitvechiles.php" name="new_vehicle" autocomplete="on" style="color:#000;" method="post" id="addVehicleForm" enctype="multipart/form-data"> 
                        <h1 id="heading-main">ADD A type model</h1> 




                        <p> 
                            <label for="text" class="youpasswd" >type<span style="color:red">*</span></label>

                            <select id="typename" name="typename" class="typename" >
                                <option value="NULL">Select a type  </option>
                                <?php
                                $get_vechile_type = "select * from vehicleType";
                                $get_vechile_type_res = mysql_query($get_vechile_type, $db1->conn);
                                while ($typelist = mysql_fetch_array($get_vechile_type_res)) {
                                    echo "<option value='" . $typelist['id'] . "' id='" . $typelist['id'] . "'>" . $typelist['vehicletype'] . "</option>";
                                }
                                ?>
                            </select>

                            <span id="regCompanyErr"></span>



                        </p>


                        
                        <p> 
                            <label for="text" class="youpasswd" >MODEL<span style="color:red">*</span></label>
                            <input id="typemodel" name="typemodel" required="required" class="typemodel" type="text" placeholder="eg. KA-05/1800" /> 
                            <span id="regLicenseErr"></span>
                        </p>

                       
                        <div id="preview_img"></div>
                    </form>     

                </div>

            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_modal" style="color:red;border:solid 1px #a2a2a2">CLOSE</button>
                <button type="submit" class="btn btn-success btn-clean" id="datasubmit">SUBMIT</button>
            </div>
        </div>
    </div>
</div>    



<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('#addButton').click(function(){
            $('#heading-main').text('Add a vehicle');
        });

        $('#title').change(function() {
            var adv_id = $('#title').val();
            if (adv_id != null || adv_id != '')
                $('#vehiclemodel').load('getvehicle.php', {adv: adv_id});
        });


        $('.resetPassword').click(function() {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });

        $('#change_pass_doc').click(function() {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {company_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function(result) {
//                        alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });

        $('#ActiveButton').click(function() {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function() {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast vechile type in the list');
            } else if (confirm('Are you confirm to  ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "addvehicletypemodel.php",
                    data: {item_type: 2,item_list_res: values,add_val:5},
                    dataType: "JSON",
                    success: function(result) {
                        //  alert(result.message);
                        //alert(result.qrys);
                        //alert(result.flag);
                        if (result.flag == 0) {
                            $('.custom_check').each(function() {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                             $('#refresh_table2').load('getvehiclemodel.php', {}, function() {
//                                alert('data refreshed');
                            });
                        }
                    }
                });
            }
        });



        $("#datasubmit").click(function() {

           
            var count = 0;
            if ($('#companyname').val() == "NULL")
            {
                $('#companyname').focus();
                $('#regCompanyErr').html("<?php echo SA_choose_company;?>");
                return false;
            }
            if (count > 0)
                return false;
            
            if (count > 0)
                return false;
            var typemodel = $('#typemodel').val();
               var typeid = $('#typename').val();
            if (typemodel == '')
            {
                alert("enter type model");
                return false;
            }else if (typeid == ''){
                
                alert("select type id");
                return false;
            }
            // alert(typemodel+'-'+typeid)
                $.ajax({
                    type: "POST",
                    url: "addvehicletypemodel.php",
                    data: {item_type: 2, typemodel: typemodel,typeid: typeid, add_val:3 },
                    dataType: "JSON",
                    success: function(result)
                    {

                        // alert(result.qrys);
                        //alert(result.flag);
                        if (result.flag == 0)
                        {
                             $('#close_modal').trigger('click');
                            $('#refresh_table2').load('getvehiclemodel.php', {}, function() {
//                                alert('data refreshed');
                            });

                          
                        } else
                        {
                            alert('Error type');
                        }
                    },
                    error: function()
                    {
                        alert('Error occured1');
                    }
                });
           
        });


    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>




