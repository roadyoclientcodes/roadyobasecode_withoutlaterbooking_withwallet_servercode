var map;
var placelatlng;
var searchArea, searchAreaMarker, searchAreaRadius = 50000; // metres
var geocoder;
var navflag = 1;
var searchflag = 0;
var city = '', vehicleflag = '';
var infowindow;

var baseurl = 'http://107.170.65.252/roadyo1.0/NewGodsview/';
var serverurl = 'http://107.170.65.252:8080';

//gmap search for searching location pointing marker
function initAutocomplete() {
    //map size based on window size
    var elem = (document.compatMode === "CSS1Compat") ?
        document.documentElement :
        document.body;

    var height = elem.clientHeight;
    var restSpace = height - 90;

    document.getElementById("body").style.height = height;

    var y = document.getElementsByClassName("restSpace");
    for (var i = 0, len = y.length; i < len; i++)
        y[i].style.height = restSpace + 'px';

    document.getElementById("map").style.height = restSpace + 'px';

    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 12.84862, lng: 77.600384 },
        zoom: 13,
        streetViewControl: false, //hiding human icon for street view
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
    });

    //show alert msg to share you location google map
    geocoder = new google.maps.Geocoder();

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            //passing lat lng to finding drivers 50km distance
            findingmarkers(pos);

            placelatlng = pos;
            searchflag = 1;

            map.setCenter(pos);
            var marker = new google.maps.Marker({
                map: map,
            });
            marker.setPosition(pos);

            codeLatLng(pos.lat, pos.lng);
            //after 7 seconds hiding the marker
            setTimeout(function () {
                marker.setVisible(false);
            }, 7000);

        });
    }

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        console.log(city+"fhg"+vehicleflag);

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            //search place lat lng
            placelatlng = {
                'lat': place.geometry.location.lat(),
                'lng': place.geometry.location.lng()
            }
            city = '';
            vehicleflag = '';

            console.log(placelatlng);
            searchflag = 1;
            //based on location finding drivers 50km distance
            findingmarkers(placelatlng);

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map,
                icon: baseurl + 'icons/arrow2.png',
                title: place.name,
                animation: google.maps.Animation.DROP,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
        map.setZoom(14);//set zoom level while searching place
    });
    //offline();
    getcitydata();
    // getvehicledata();
getdriversfromdb();
}

google.maps.event.addDomListener(window, 'load', initAutocomplete);




function getdriversfromdb(){
   $.getJSON(serverurl + "/drivers", function (response) {
       console.log(response.dlist);
           if(response.dlist){
               for(var i=0;i<response.dlist.length;i++){
                    drawMarkers(response.dlist[i])
               }
           }
   })
}







//based on lat lan finding name of city when accept share location
function codeLatLng(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            console.log(results)
            console.log(results[0].address_components[3].long_name);
            // alert(results[0].address_components[3].long_name);
            if (results[1]) {
                //formatted address
                //   alert(results[0].formatted_address)
                //find country name
                for (var i = 0; i < results[0].address_components.length; i++) {
                    for (var b = 0; b < results[0].address_components[i].types.length; b++) {
                        //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                        if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                            //this is the object you are looking for
                            city1 = results[0].address_components[i];
                            break;
                        }
                    }
                }
                //city data
                alert(city1.short_name + " " + city1.long_name)
            } else {
                alert("No results found");
            }
        } else {
            alert("Geocoder failed due to: " + status);
        }
    });
}

//based on place search updating drivers
setInterval(function () {
    if (searchflag == 1) {
        findingmarkers(placelatlng);
    }
}, 2000);

//based on search place lat lng  finding drivers from 50km distance
function findingmarkers(placelatlng) {
    // console.log(placelatlng);
    searchArea = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0,
        strokeWeight: 0,
        fillColor: '#FF0000',
        fillOpacity: 0,
        map: map,
        center: new google.maps.LatLng(placelatlng.lat, placelatlng.lng),
        radius: searchAreaRadius
    });
    // document.getElementById("driverlist").innerHTML = "";

            city = '';
            vehicleflag = '';

    for (var item in markerStore) {
        if (google.maps.geometry.spherical.computeDistanceBetween(markerStore[item].getPosition(), searchArea.getCenter()) <= searchArea.getRadius()) {
            console.log('=> is in searchArea');
           // console.log(markerStore[item]);
            //markerStore[item].setVisible(true);
            markerStore[item].placeflag = true;

            var p1 = markerStore[item].getPosition();
            var p2 = new google.maps.LatLng(placelatlng.lat, placelatlng.lng)
            // var distance = (google.maps.geometry.spherical.computeDistanceBetween(markerStore[item].getPosition(), p2) / 1000).toFixed(2);
            // console.log(distance);
            // markerStore[item].distance = distance + 'km'
           // console.log("item",markerStore[item]);

            var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                 origins: [p1],
                 destinations: [p2],
                 travelMode: google.maps.TravelMode.DRIVING,
                 unitSystem: google.maps.UnitSystem.METRIC,
                 avoidHighways: false,
                 avoidTolls: false
             }, function (response, status) {
                    if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
                            var distance = response.rows[0].elements[0].distance.text;
                            var duration = response.rows[0].elements[0].duration.text;
                            //distance convering meters to miles
                            var distance1 = response.rows[0].elements[0].distance.value;
                              console.log(distance1);
                            distance1=(distance1/1609.34);
                            markerStore[item].distance = '';//distance1.toFixed(2)+' Miles';

                           // markerStore[item].distance = distance;
                            console.log(markerStore[item]);
                            console.log(distance);
                            console.log(duration);
                     }
               })
            
        } else {
            console.log('=> is NOT in searchArea');
            // markerStore[item].setVisible(false);
            markerStore[item].placeflag = false;
        }
    }
    //availabledrivers();
}



function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


//@markerStore to store the markers assigned to the users received from pubnub
var markerStore = {}, flag = 12;

/* configure pubnub with the keys to subscribe*/
pubnub = PUBNUB({
    subscribe_key: 'sub-c-9abf75a0-1ea2-11e6-8b91-02ee2ddab7fe'
    //subscribe_key: "demo"
});

/* Subscribe to pubnub using channel name
@envelope will contain the raw message or data along with timestamp
*/
pubnub.subscribe({
    //  channel: "roadyo1.0_channel",
    //channel: "my_channel",
    channel: ['roadyo1.0_channel', 'presenceChn_roadyo1.0_channel'],
    presence: function (msg) {
       console.log("presence ",msg);

        if (validateEmail(msg.uuid)) {
            switch (msg.action) {
                case "join": createMarker(msg.uuid);
                    break;
                case "leave": removeMarker(msg.uuid);
                    break;
                case "timeout": removeMarker(msg.uuid);
                    break;
            }
        } else {
            //console.log("not email");
        }
    },
    message: function (message, envelope, channelOrGroup, time, channel) {
        // console.log(
        //     "Message Received." + "\n" +
        //     "Channel or Group : " + JSON.stringify(channelOrGroup) + "\n" +
        //     "Channel : " + JSON.stringify(channel) + "\n" +
        //     "Message : " + JSON.stringify(message) + "\n" +
        //     "Time : " + time + "\n" +
        //     "Raw Envelope : " + JSON.stringify(envelope)
        // );

        // console.log(
        //     "Message : " + JSON.stringify(message) + "\n" +
        //     "Raw Envelope : " + JSON.stringify(envelope)
        // );

        // console.log(message);

        if (message.e_id && message.fname) {
            console.log(message);
            //if (message.e_id){

            var data = envelope[0][0];
            data.t = Math.floor(envelope[1] / 10000000);

            //console.log(envelope[0][0]);
            // console.log(Math.floor(envelope[1] / 10000000));
            // console.log(envelope);
            //console.log(data);
            //drawMarkers(message);

            drawMarkers(data);
        }
    }
});



/* create a marker and store it in the markerStore object */

function createMarker(uuid) {
    var marker = new google.maps.Marker({
        map: map
        //icon: iconimg 
    });
   
    //console.log("join id: ", uuid);
}

/* remove the marker from markerStore object as well from the map */
function removeMarker(uuid) {
    //console.log("remove id :", uuid);
    if (markerStore[uuid]) {
        markerStore[uuid].setVisible(false);
        delete markerStore[uuid];
        if (navflag == 1) {
            availabledrivers();
        }
        else {
            busy_driver_list();
        }
    }

}


var position;
/* function to draw markers on the map
@data is the data received from the pubnub
@e_id is the email id, which is unique
@data.t is the timestamp of the particular message received from pubnub
@flag specifies status
*/
function drawMarkers(data) {
    // console.log(data);
    if (data.lt && data.lg) {
        //Do we have this marker already?
        if (markerStore.hasOwnProperty(data.e_id)) {

            switch (flag) {
                case 4:
                    if (vehicleflag != '') {
                        if (Number(markerStore[data.e_id].typeid) == Number(vehicleflag)) {
                            statusFour(data.e_id, data.bid, data.lt, data.lg, data.a);
                        } else {
                            markerStore[data.e_id].setVisible(false);
                        }
                    }
                    else {
                        statusFour(data.e_id, data.bid, data.lt, data.lg, data.a);
                    }
                    break;
                case 6:
                    if (vehicleflag != '') {
                        if (Number(markerStore[data.e_id].typeid) == Number(vehicleflag)) {
                            statusSix(data.e_id, data.bid, data.lt, data.lg, data.a);
                        } else {
                            markerStore[data.e_id].setVisible(false);
                        }
                    } else {
                        statusSix(data.e_id, data.bid, data.lt, data.lg, data.a);
                    }
                    break;
                case 7:
                    if (vehicleflag != '') {
                        if (Number(markerStore[data.e_id].typeid) == Number(vehicleflag)) {
                            statusSeven(data.e_id, data.bid, data.lt, data.lg, data.a);
                        } else {
                            markerStore[data.e_id].setVisible(false);
                        }
                    }
                    else {
                        statusSeven(data.e_id, data.bid, data.lt, data.lg, data.a);
                    }
                    break;
                case 8:
                    if (vehicleflag != '') {
                        if (Number(markerStore[data.e_id].typeid) == Number(vehicleflag)) {
                            statusEight(data.e_id, data.bid, data.lt, data.lg, data.a);
                        } else {
                            markerStore[data.e_id].setVisible(false);
                        }
                    }
                    else {
                        statusEight(data.e_id, data.bid, data.lt, data.lg, data.a);
                    }
                    break;
                case 12:
                    showAllMarkers();
                    break;
            }

            // markerStore[data.e_id].setPosition(new google.maps.LatLng(data.lt, data.lg));
            //@position old lat lng 
            var z = new google.maps.LatLng(markerStore[data.e_id].lt, markerStore[data.e_id].lg);
            position = [z.lat(), z.lng()];
            var re = new google.maps.LatLng(data.lt, data.lg);
            re = [re.lat(), re.lng()];
           // transition(re, data.e_id);

            // update the timestamp of a particular user and lat,lng,bid,status of a,car type and carid
            markerStore[data.e_id].a = data.a;
            markerStore[data.e_id].bid = data.bid;
            markerStore[data.e_id].t = data.t;
            markerStore[data.e_id].cartype = data.carType;
            markerStore[data.e_id].typeid = data.tp;
            markerStore[data.e_id].lt = data.lt;
            markerStore[data.e_id].lg = data.lg;
            markerStore[data.e_id].appendflag = true;
            markerStore[data.e_id].phone=data.phone;
            markerStore[data.e_id].cityid = data.cityid;
if (data.profilePic) {
                 markerStore[data.e_id].image = "http://www.roadyo.in/roadyo1.0/pics/" + data.profilePic;
            } else {
                 markerStore[data.e_id].image = baseurl + 'icons/driver.png';
            }
            //markerStore[data.e_id].image=data.profilePic;

        } else {

            // t is the time stamp received from the pubnub message
            //placeflag is true means the pariticular user is under the radius

            if (data.profilePic) {
                data.profilePic = "http://www.roadyo.in/roadyo1.0/pics/" + data.profilePic;
            } else {
                data.profilePic = baseurl + 'icons/driver.png';
            }

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(data.lt, data.lg),
                title: data.fname,
                map: map,
                t: data.t,
                name: data.fname,
                image: data.profilePic,
                bid: data.bid,
                phone: data.phone,
                cartype: data.carType,
                typeid: data.tp,
                cityid: data.cityid,
                driverid: data.driverid,
                a: data.a,
                lt: data.lt,
                lg: data.lg,
                eid: data.e_id,
                placeflag: false,
                appendflag: true
            });

            //when click on marker its shows marker 
            infowindow = new google.maps.InfoWindow({
                content: '<p><img src=" ' + data.profilePic + ' " width="34" height="34"/> ' + data.fname + ' <br>Driver Id: ' + data.driverid + '<br> Phone no:' + data.phone + '<p>'
            });

            marker.addListener('click', function () {
                // infowindow.open(map, marker);
                get_driver_data(data.driverid);
                popupmodel();
            });

            markerStore[data.e_id] = marker;
            markerStore[data.e_id].setPosition(new google.maps.LatLng(data.lt, data.lg));

            if(Number(data.a) == 4 ){
                 markerStore[data.e_id].setIcon(baseurl + 'car/green.png');
                 markerStore[data.e_id].setVisible(true);
            }else if(Number(data.a) == 6){
                 markerStore[data.e_id].setIcon(baseurl + 'car/red.png');
                 markerStore[data.e_id].setVisible(true);
            }else if(Number(data.a) == 7){
                 markerStore[data.e_id].setIcon(baseurl + 'car/blue.png');
                 markerStore[data.e_id].setVisible(true);
            }else if(Number(data.a) == 8){
                 markerStore[data.e_id].setIcon(baseurl + 'car/yellow.png');
                 markerStore[data.e_id].setVisible(true);
            }

            //markerStore[data.e_id].setVisible(false);
            

        }
    }
}

/* function to hide the markers if data is not received for the particular user from pubnub
@currentEpoch is the current time of the system in epoch format
@timeDifference is the time difference in seconds between the system time and the lastUpdated marker time
*/
function offline() {
    setInterval(function () {
        for (var item in markerStore) {
            //console.log(markerStore[item].visible + ' ' + markerStore[item].t);
            // if (markerStore[item].visible === true) {
            // difference of currentEpoch with the lastUpdated epoch of the marker
            var currentEpoch = Math.floor((new Date().valueOf()) / 1000);
            var timeDifference = currentEpoch - markerStore[item].t;
            if (timeDifference >= 8) {  //600sec   //8sec
                markerStore[item].setVisible(false);
                markerStore[item].placeflag = false;
                clearCircle();
                // delete markerStore[item];
                markerStore[item].appendflag = false;
                if (navflag == 1) {
                    availabledrivers();
                }
                else {
                    busy_driver_list();
                }
            }
            // }
        }
    }, 3000);
}

function statusFour(e_id, bid, lt, lg, a) {
    if (searchflag == 1) {
        if (markerStore[e_id].placeflag === true) {
            if (Number(a) == 4) {
                if (bid) {
                    if (Number(bid) == 0) {
                        markerStore[e_id].setVisible(true);
                        markerStore[e_id].setIcon(baseurl + 'car/green.png');
                    } else {
                        markerStore[e_id].setVisible(false);
                    }
                } else {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(baseurl + 'car/green.png');
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            markerStore[e_id].setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(markerStore[e_id].cityid) == Number(city)) {
                if (Number(a) == 4) {
                    if (bid) {
                        if (Number(bid) == 0) {
                            markerStore[e_id].setVisible(true);
                            markerStore[e_id].setIcon(baseurl + 'car/green.png');
                        } else {
                            markerStore[e_id].setVisible(false);
                        }
                    } else {
                        markerStore[e_id].setVisible(true);
                        markerStore[e_id].setIcon(baseurl + 'car/green.png');
                    }
                } else {
                    markerStore[e_id].setVisible(false);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            if (Number(a) == 4) {
                if (bid) {
                    if (Number(bid) == 0) {
                        markerStore[e_id].setVisible(true);
                        markerStore[e_id].setIcon(baseurl + 'car/green.png');
                    } else {
                        markerStore[e_id].setVisible(false);
                    }
                } else {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(baseurl + 'car/green.png');
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        }
    }
}

function statusSix(e_id, bid, lt, lg, a) {
    if (searchflag == 1) {
        if (markerStore[e_id].placeflag === true) {
            if (Number(a) == 6) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(baseurl + 'car/red.png');
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            markerStore[e_id].setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(markerStore[e_id].cityid) == Number(city)) {
                if (Number(a) == 6) {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(baseurl + 'car/red.png');
                } else {
                    markerStore[e_id].setVisible(false);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            if (Number(a) == 6) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(baseurl + 'car/red.png');
            } else {
                markerStore[e_id].setVisible(false);
            }
        }
    }
}

function statusSeven(e_id, bid, lt, lg, a) {
    if (searchflag == 1) {
        if (markerStore[e_id].placeflag === true) {
            if (Number(a) == 7) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(baseurl + 'car/blue.png');
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            markerStore[e_id].setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(markerStore[e_id].cityid) == Number(city)) {
                if (Number(a) == 7) {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(baseurl + 'car/blue.png');
                } else {
                    markerStore[e_id].setVisible(false);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            if (Number(a) == 7) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(baseurl + 'car/blue.png');
            } else {
                markerStore[e_id].setVisible(false);
            }
        }
    }
}

function statusEight(e_id, bid, lt, lg, a) {
    if (searchflag == 1) {
        if (markerStore[e_id].placeflag === true) {
            if (Number(a) == 8) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(baseurl + 'car/yellow.png');
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            markerStore[e_id].setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(markerStore[e_id].cityid) == Number(city)) {
                if (Number(a) == 8) {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(baseurl + 'car/yellow.png');
                } else {
                    markerStore[e_id].setVisible(false);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            if (Number(a) == 8) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(baseurl + 'car/yellow.png');
            } else {
                markerStore[e_id].setVisible(false);
            }
        }
    }
}

/* drivers list markers of available,enroute,reached,journey and all
 @ available=4, @enroute=6,@reached pickup=7, @journey=8, @all=12  
 @flag specifies the status of driver*/
function availableList() {
    flag = 4;
    clearMarkers();
   // availabledrivers();
   // clearCircle();
}

function enroutePickup() {
    flag = 6;
    clearMarkers();
    clearCircle();
}

function reachedPickup() {
    flag = 7;
    clearMarkers();
    clearCircle();
}

function journeyStarted() {
    flag = 8;
    clearMarkers();
    clearCircle();
}

function showAllMarkers() {
    flag = 12;
    clearMarkers();
    // clearCircle();
    //placelatlng='';
    //city='';
    //vehicleflag='';
    for (var item in markerStore) {
        // console.log(statusStore[item].a);
        switch (Number(markerStore[item].a)) {
            case 6:
                if (vehicleflag != '') {
                    if (Number(markerStore[item].typeid) == vehicleflag) {
                        showAllSix(markerStore[item]);
                    } else {
                        markerStore[item].setVisible(false);
                    }
                }
                else {
                    showAllSix(markerStore[item]);
                }
                break;
            case 7:
                if (vehicleflag != '') {
                    if (Number(markerStore[item].typeid) == vehicleflag) {
                        showAllSeven(markerStore[item]);
                    } else {
                        markerStore[item].setVisible(false);
                    }
                }
                else {
                    showAllSeven(markerStore[item]);
                }
                break;
            case 8:
                if (vehicleflag != '') {
                    if (Number(markerStore[item].typeid) == vehicleflag) {
                        showAllEight(markerStore[item]);
                    } else {
                        markerStore[item].setVisible(false);
                    }
                }
                else {
                    showAllEight(markerStore[item]);
                }
                break;
            case 4:
                if (vehicleflag != '') {
                    if (Number(markerStore[item].typeid) == vehicleflag) {
                        showAllFour(markerStore[item]);
                    } else {
                        markerStore[item].setVisible(false);
                    }
                }
                else {
                    showAllFour(markerStore[item]);
                }
                break;
        }
    }
}

function showAllSix(res) {
    if (searchflag == 1) {
        if (res.visible === true) {
            res.setIcon(baseurl + 'car/red.png');
            res.setVisible(true);
        }
    } else {
        if (city != '') {
            if (Number(res.cityid) == Number(city)) {
                res.setIcon(baseurl + 'car/red.png');
                res.setVisible(true);
            } else {
                res.setVisible(false);
            }
        } else {
            res.setIcon(baseurl + 'car/red.png');
            res.setVisible(true);
        }
    }
}

function showAllSeven(res) {
    if (searchflag == 1) {
        if (res.placeflag === true) {
            res.setIcon(baseurl + 'car/blue.png');
            res.setVisible(true);
        }
    } else {
        if (city != '') {
            if (Number(res.cityid) == Number(city)) {
                res.setIcon(baseurl + 'car/blue.png');
                res.setVisible(true);
            } else {
                res.setVisible(false);
            }
        } else {
            res.setIcon(baseurl + 'car/blue.png');
            res.setVisible(true);
        }
    }
}

function showAllEight(res) {
    if (searchflag == 1) {
        if (res.placeflag === true) {
            res.setIcon(baseurl + 'car/yellow.png');
            res.setVisible(true);
        }
    } else {
        if (city != '') {
            if (Number(res.cityid) == Number(city)) {
                res.setIcon(baseurl + 'car/yellow.png');
                res.setVisible(true);
            } else {
                res.setVisible(false);
            }
        } else {
            res.setIcon(baseurl + 'car/yellow.png');
            res.setVisible(true);
        }
    }
}

function showAllFour(res) {
    if (searchflag == 1) {
        if (res.placeflag === true) {
            if (res.bid) {
                if (Number(res.bid) == 0) {
                    res.setIcon(baseurl + 'car/green.png');
                    res.setVisible(true);
                } else {
                    res.setVisible(false);
                }
            } else {
                res.setIcon(baseurl + 'car/green.png');
                res.setVisible(true);
            }
        }
    } else {
        if (city != '') {
            if (Number(res.cityid) == Number(city)) {
                if (res.bid) {
                    if (Number(res.bid) == 0) {
                        res.setIcon(baseurl + 'car/green.png');
                        res.setVisible(true);
                    } else {
                        res.setVisible(false);
                    }
                } else {
                    res.setIcon(baseurl + 'car/green.png');
                    res.setVisible(true);
                }
            } else {
                res.setVisible(false);
            }
        } else {
            if (res.bid) {
                if (Number(res.bid) == 0) {
                    res.setIcon(baseurl + 'car/green.png');
                    res.setVisible(true);
                } else {
                    res.setVisible(false);
                }
            } else {
                res.setIcon(baseurl + 'car/green.png');
                res.setVisible(true);
            }
        }
    }
}

//clear all markers on the  map
function clearMarkers() {
    //console.log("clear");
    for (var item in markerStore) {
        markerStore[item].setVisible(false);
        // delete markerStore[item];
    }
}

var citydata = [], vehiclearr = [];
//@citydata storing all city's object data
//@vehiclearr storing all vehicle's type object data 
// var  circle, infowindow;

//soring ascending order of city name
jQuery.fn.sort = function () {
    return this.pushStack([].sort.apply(this, arguments), []);
};
function sortCityName(a, b) {
    if (a.City_Name.toUpperCase() == b.City_Name.toUpperCase()) {
        return 0;
    }
    return a.City_Name.toUpperCase() > b.City_Name.toUpperCase() ? 1 : -1;
};



//getting city's data from mysql 
function getcitydata() {
    $.getJSON(serverurl + "/citydata", function (response) {
        // console.log("citydata", response);
        response = $(response).sort(sortCityName);
        citydata = response;
        for (var i = 0; i < response.length; i++) {
            $('#city').append('<option value="' + response[i].City_Id + '">' + camelize(response[i].City_Name) + '</option>');
        }
    });
}


//soring ascending order of vehicletype
jQuery.fn.sort = function () {
    return this.pushStack([].sort.apply(this, arguments), []);
};
function sortVehicleName(a, b) {
    if (a.type_name.toUpperCase() == b.type_name.toUpperCase()) {
        return 0;
    }
    return a.type_name.toUpperCase() > b.type_name.toUpperCase() ? 1 : -1;
};

//converting camelcase
function camelize(inStr) {
    return inStr.replace(/\w\S*/g, function (tStr) {
        return tStr.charAt(0).toUpperCase() + tStr.substr(1).toLowerCase();
    });
}

//getting vehicle type and id from mysql
function getvehicledata() {
    $.getJSON(serverurl + "/vehicletype", function (response) {
        response = $(response).sort(sortVehicleName);
        vehiclearr = response;
        for (var i = 0; i < response.length; i++) {
            $('#vehicle').append('<option value="' + response[i].type_id + '">' + camelize(response[i].type_name) + '</option>');
        }
    });
}

//while loading page after 4 seconds execute function
// setTimeout(function () {
//     if (navflag == 1) {
//         availabledrivers();
//     }
//     else {
//         busy_driver_list();
//     }
// }, 1000);
 availabledrivers();
//every 7 seconds repeating loop
//@navflag 1 means selected available tab
//@navflag 0 means selected busy tab
setInterval(function () {
    if (navflag == 1) {
       // console.log("hai..available...");
        availabledrivers();
    }
    else {
       //  console.log("hai..busy..");
        busy_driver_list();
    }
}, 1000);

//appending drivers data to table
function appenddata(response) {
    var img, status;
    status = Number(response.a);
    switch (status) {
        case 4:
            img = baseurl + 'icons/green.png';
            break;
        case 6:
            img = baseurl + 'icons/red.png';
            break;
        case 7:
            img = baseurl + 'icons/blue.png';
            break;
        case 8:
            img = baseurl + 'icons/yellow.png';
            break;
    }
   // console.log(response.distance);
    var ds;
    if (response.distance) {

        ds = response.distance;
    } else {
        ds = '';
    }

    if (response.name != undefined && response.name != '' && response.appendflag === true) {
        $(".driverlist").append('<tr><td> <div onclick="get_driver_data(' + response.driverid + ');popupmodel();"><p> <span class="col-xs-height col-middle"><span class="thumbnail-wrapper d32 circular bg-success"> <img src="' + response.image + '"></span><img width="12" height="12" class="position" src="' + img + '"/> </span></p><p class="p-l-10 col-xs-height col-middle" style="width: 80%"><span class="text-master" style="padding-right: 30px;"> ' + response.name + '&nbsp;(ID:' + response.driverid + ')<br/>' + response.phone + '<font style="float:right;">' + ds + '</font></span></p></div></td></tr>');
    }
}

//right side bar ,its displays driver and jobs details
function popupmodel() {
    $('#myModal2').modal('show');
}

//appending available drivers based on city or vehicle type or place search to table
function availabledrivers() {
    navflag = 1;
   // console.log("available");
    $('.driverlist').html('');
    for (var item in markerStore) {
        if (Number(markerStore[item].a) == 4) {
            if (vehicleflag != '') {
                if (Number(markerStore[item].typeid) == Number(vehicleflag)) {
                    if (markerStore[item].bid) {
                        if (Number(markerStore[item].bid) == 0) {
                            if (city != '') {
                                if (Number(markerStore[item].cityid) == Number(city)) {
                                    appenddata(markerStore[item]);
                                }
                            } else {
                                if (searchflag == 1) {
                                    if (markerStore[item].placeflag === true) {
                                        appenddata(markerStore[item]);
                                    }
                                } else {
                                    appenddata(markerStore[item]);
                                }
                            }
                        }
                    } else {
                        if (city != '') {
                            if (Number(markerStore[item].cityid) == Number(city)) {
                                appenddata(markerStore[item]);
                            }
                        } else {
                            if (searchflag == 1) {
                                if (markerStore[item].placeflag === true) {
                                    appenddata(markerStore[item]);
                                }
                            } else {
                                appenddata(markerStore[item]);
                            }
                        }
                    }
                }
            } else {
                if (markerStore[item].bid) {
                    if (Number(markerStore[item].bid) == 0) {
                        if (city != '') {
                            if (Number(markerStore[item].cityid) == Number(city)) {
                                appenddata(markerStore[item]);
                            }
                        } else {
                            if (searchflag == 1) {
                                if (markerStore[item].placeflag === true) {
                                    appenddata(markerStore[item]);
                                }
                            } else {
                                appenddata(markerStore[item]);
                            }
                        }
                    }
                } else {
                    if (city != '') {
                        if (Number(markerStore[item].cityid) == Number(city)) {
                            appenddata(markerStore[item]);
                        }
                    } else {

                        if (searchflag == 1) {
                            if (markerStore[item].placeflag === true) {
                                appenddata(markerStore[item]);
                            }
                        } else {
                            appenddata(markerStore[item]);
                        }
                    }
                }
            }
        }
    }
}

//busy drivers based on city or vehicle type or radius appending drivers to the table
//@vehicleflag for checking vehicle type selected or not and storing vehicle type id
//@searchflag  for checking while search place from search bar
//@city for if select city from select box enable city and storing city id 
function busy_driver_list() {
    navflag = 0;
   // console.log("busy");
    $('.driverlist').html('');
    for (var item in markerStore) {
        if (markerStore[item].bid) {
            if (Number(markerStore[item].bid) != 0) {
                if (vehicleflag != '') {
                    if (Number(markerStore[item].typeid) == Number(vehicleflag)) {
                        if (city != '') {
                            if (Number(markerStore[item].cityid) == Number(city)) {
                                appenddata(markerStore[item]);
                            }
                        } else {
                            if (searchflag == 1) {
                                if (markerStore[item].placeflag === true) {
                                    appenddata(markerStore[item]);
                                }
                            } else {
                                appenddata(markerStore[item]);
                            }
                        }
                    }
                } else {
                    if (city != '') {
                        if (Number(markerStore[item].cityid) == Number(city)) {
                            appenddata(markerStore[item]);
                        }
                    } else {
                        if (searchflag == 1) {
                            if (markerStore[item].placeflag === true) {
                                appenddata(markerStore[item]);
                            }
                        } else {
                            appenddata(markerStore[item]);
                        }
                    }
                }
            }
        }
    }
}


var citymarker, cityobj;
//@citymarker storing particular city marker
//@cityobj storing city object based on city id

//based on city id appending available drivers
$('#city').on('change', function () {
    searchflag = 0;
    clearCircle();
    clearMarkers();
    vehicleflag='';
    $('.driverlist').html('');
    document.getElementById("vehicle").innerHTML = "";
    $("#vehicle").append("<option value=''>VEHICLE TYPE</option>");
    var value = $(this).val();
    console.log("value ",typeof(value));

    if (value != '') {
        city = value;


        //based on city lat long focusing map to particular location        
        cityobj = $.grep(citydata, function (item) {
            if (item.City_Id == value) {
                return item;
            }
        });
        

 console.log("cityobj ",cityobj);
        if (citymarker != undefined) {
            citymarker.setMap(null);
        }

        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(cityobj[0].City_Lat, cityobj[0].City_Long),
            icon: baseurl + 'icons/arrow2.png',
            animation: google.maps.Animation.DROP,
        });
        citymarker = marker;
        map.setCenter(new google.maps.LatLng(cityobj[0].City_Lat, cityobj[0].City_Long));
        map.setZoom(14);

        for (var item in markerStore) {
            if (Number(markerStore[item].a) == 4) {
                if (markerStore[item].bid) {
                    if (Number(markerStore[item].bid) == 0) {
                        if (Number(markerStore[item].cityid) == Number(value)) {

                             console.log("inbooking ",cityobj);
                             console.log(markerStore[item]);
                            // markerStore[item].placeflag=true;
                            appenddata(markerStore[item]);
                        }
                    }
                } else {
                     console.log("markerstoreCity ",markerStore[item]);
                    if (Number(markerStore[item].cityid) == Number(value)) {
                         console.log("out booking ",cityobj);
                        // markerStore[item].placeflag=true;
                          appenddata(markerStore[item]);
                    }
                }
            }
        }
//         if (navflag == 1) {
//            availabledrivers();
//        } else {
//            busy_driver_list();
//        }

        $.getJSON(serverurl + "/vehicletype/" + value, function (response) {
            response = $(response).sort(sortVehicleName);
            for (var i = 0; i < response.length; i++) {
                $('#vehicle').append('<option value="' + response[i].type_id + '">' + camelize(response[i].type_name) + '</option>');
            }
        });
    } else {
        city = '';
        document.getElementById("vehicle").innerHTML = "";
        $("#vehicle").append("<option value=''>VEHICLE TYPE</option>");
        //getvehicledata();
    }
});

//based on vehicle type getting busy and available drivers
$('#vehicle').on('change', function () {
    searchflag = 0;
    clearCircle();
    clearMarkers();
    var value = $(this).val();
    console.log(value);
    //vehicleflag=value;
    $('.driverlist').html('');

    // document.getElementsByClassName("driverlist").innerHTML = "";
    if (value != '' && value != undefined) {
        vehicleflag = value;
 console.log(typeof(value));
        // if (city == '') {
        //     //it's work only without select city
        //     //based on vehicle id showing regarding city lat long showing map focus 
        //     vehicle = $.grep(vehiclearr, function (item) {
        //         if (item.type_id == value) {
        //             return item;
        //         }
        //     });

        //     //console.log(vehicle);

        //     cityobj = $.grep(citydata, function (item) {
        //         if (item.City_Id == vehicle[0].city_id) {
        //             return item;
        //         }
        //     });
        //     //console.log(cityobj);

        //     if (citymarker != undefined) {
        //         citymarker.setMap(null);
        //     }

        //     var marker = new google.maps.Marker({
        //         map: map,
        //         position: new google.maps.LatLng(cityobj[0].City_Lat, cityobj[0].City_Long),
        //         icon: baseurl + 'icons/arrow2.png'
        //     });
        //     citymarker = marker;
        //     map.setCenter(new google.maps.LatLng(cityobj[0].City_Lat, cityobj[0].City_Long));
        //     map.setZoom(14);

        // }

        if (navflag == 1) {
            availabledrivers();
        } else {
            busy_driver_list();
        }

    } else {
        console.log("no vehicle selected");
        vehicleflag = '';
    }
});


//getting driver data and appointment data based on driverid 
function get_driver_data(id) {
    clearCircle();
    var id = Number(id);
    var apid;
    document.getElementById("driver").innerHTML = "";
    document.getElementById("jobid").innerHTML = "";
    document.getElementById("slave").innerHTML = "";
    document.getElementById("route").innerHTML = "";

    for (var item in markerStore) {
        if (Number(markerStore[item].driverid) == id) {
            infowindows(markerStore[item]);
            apid = markerStore[item].bid;
            // console.log("bid",apid);
            $("#driver").append('<h5 style="padding-left: 10px;color: white;">Driver Details</h5>');
            $("#driver").append('<h5><p style="margin-bottom: 0px;padding: 10px;">Driver Name: &nbsp;' + markerStore[item].name + '<br>Driver Id: &nbsp;' + markerStore[item].driverid + '<br>Phone No: &nbsp;' + markerStore[item].phone + '</p></h5>');
        }
    }

    if (id && Number(apid) != 0) {
        //  console.log("hai");
        // console.log(id +''+apid);
        $.getJSON(serverurl + "/appointmentdata/" + id + '/' + apid, function (response) {
            //  console.log(response);
            if (response.appointment[0]) {

                if (response.appointment[0].appt_type == 1) {
                    response.appointment[0].appt_type = 'Now';
                } else {
                    response.appointment[0].appt_type = 'Later';
                }

                $("#jobid").append('<h5 style="padding-left: 10px;color: white;"> JOB Details</h5>');
                $('#jobid').append('<h5 style="padding-left: 10px;color: white;">JOB ID: ' + response.appointment[0].appointment_id + '</h5>');
                $("#slave").append('<h5><p style="padding: 10px;">Customer Name: ' + response.slave[0].first_name + '<br>Phone No: ' + response.slave[0].phone + '<br>Appointment Type:' + response.appointment[0].appt_type + '</p></h5>');
                $("#route").append('<hr><p style="margin-bottom: 0px;"><i class="fa fa-map-marker" style="color: green;font-size: 20px;"></i><font size="3" style="padding-left: 20px;">Pickup</font><br/>');
                $("#route").append('<span class="col-xs-height col-middle" style="padding-left: 30px;">' + response.appointment[0].address_line1 + '</p>');
                $("#route").append(' <img src="http://107.170.65.252/roadyo1.0/NewGodsview/icons/verticaldots.png" width="20" height="20" style="position: absolute; margin-top: -26px;"/>');
                $("#route").append(' <p style="margin-bottom: 0px;"><i class="fa fa-map-marker" style="color:red;font-size: 20px;"></i><font size="3" style="padding-left: 20px;">Dropoff</font><br/>');
                $("#route").append('<span style="padding-left: 30px;">' + response.appointment[0].drop_addr1 + '</span></p><hr></div>');
            }
        });
    }

}

//closing circle and infowindow
function clearCircle() {
    // if (circle) {
    //     circle.setMap(null);
    // }
    if (infowindow) {
        infowindow.close();
    }
}

//infowindow and circle for while click the driver on table list
function infowindows(response) {
    // console.log(response.lt, response.lg);
    // circle = new google.maps.Circle({
    //     map: map,
    //     center: new google.maps.LatLng(response.lt, response.lg),
    //     radius: 10 * 70,//meters
    //     visible: true,
    //     fillColor: '#80b3ff',
    //     strokeColor: '#80b3ff',
    //     strokeOpacity: 0.2,
    // });
    // circle.bindTo('center', markerStore[response.eid], 'position');
    // markerStore[response.eid]._myCircle = circle;

    // if (response[0].image || response[0].image == '') {
    //     response[0].image = 'icons/driver.png';
    // }

    // map.setZoom(14);
    map.setCenter(new google.maps.LatLng(response.lt, response.lg));

    infowindow = new google.maps.InfoWindow({
        content: '<p><img src=" ' + response.image + ' " width="34" height="34"/> ' + response.name + ' <br>Driver Id: ' + response.driverid + '<br> Phone no:' + response.phone + '<p>'

    });
    //infowindow.open(map, markerStore[response.eid]);
}


//based on click  adding css class to statusbar enroute,pickup,journey,all 
$(".btn").click(function () {
    $(".btn-default").removeClass("raised");
    $(this).addClass("raised");
});

// to resize the map window based on browser or device size
function changeSize() {
    var elem = (document.compatMode === "CSS1Compat") ?
        document.documentElement :
        document.body;
    var height = elem.clientHeight;
    var restSpace = height - 90;
    document.getElementById("body").style.height = height;
    var y = document.getElementsByClassName("restSpace");
    for (var i = 0, len = y.length; i < len; i++)
        y[i].style.height = restSpace + 'px';
    document.getElementById("map").style.height = restSpace + 'px';
}
window.onresize = changeSize;

//when enable presence using these functions
// /* create a marker and store it in the markerStore object */
// function createMarker(uuid) {
//     var marker = new google.maps.Marker({
//         map: map,
//         //icon: iconimg 
//     });
//     markerStore[uuid] = marker;
//     store.push(uuid);
//     //console.log(uuid);
// }

// /* remove the marker from markerStore object as well from the map */
// function removeMarker(uuid) {
//     markerStore[uuid].setVisible(false);
//     delete markerStore[uuid];
// }

//marker moving smoothly  
var x;
var numDeltas = 100;
var delay = 60; //milliseconds
var i = 0;
var deltaLat;
var deltaLng;
function transition(result, ids) {
    x = ids;
    i = 0;
    deltaLat = (result[0] - position[0]) / numDeltas;
    deltaLng = (result[1] - position[1]) / numDeltas;
    moveMarker();
}

function moveMarker() {
    position[0] += deltaLat;
    position[1] += deltaLng;
    var latlng1 = new google.maps.LatLng(position[0], position[1]);
    if (markerStore[x]) {
        markerStore[x].setPosition(latlng1);

    }

    if (i != numDeltas) {
        i++;
        setTimeout(moveMarker, delay);
    }
}

