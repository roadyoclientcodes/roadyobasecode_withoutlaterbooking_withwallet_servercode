(function(d){var h=[];d.loadImages=function(a,e){"string"==typeof a&&(a=[a]);for(var f=a.length,g=0,b=0;b<f;b++){var c=document.createElement("img");c.onload=function(){g++;g==f&&d.isFunction(e)&&e()};c.src=a[b];h.push(c)}}})(window.jQuery);
$.fn.hasAttr = function(name) { var attr = $(this).attr(name); return typeof attr !== typeof undefined && attr !== false; };

var lwi=-1;function thresholdPassed(){var w=$(window).width();var p=false;var cw=0;if(w>=480){cw++;}if(w>=768){cw++;}if(w>=960){cw++;}if(w>=1200){cw++;}if(lwi!=cw){p=true;}lwi=cw;return p;}

$(document).ready(function() {
r=function(){dpi=window.devicePixelRatio;if(thresholdPassed()){if($(window).width()>=1200){$('.js-31').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-286.png' : 'images/rms-logo-wave-ps-1-143.png');
$('.js-32').attr('src', 'images/fotolia_69591919_s-1200.jpg');
$('.js-33').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-1200.png' : 'images/rms-logo-wave-ps-1-600.png');}else if($(window).width()>=960){$('.js-31').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-228.png' : 'images/rms-logo-wave-ps-1-114.png');
$('.js-32').attr('src', 'images/fotolia_69591919_s-960.jpg');
$('.js-33').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-960.png' : 'images/rms-logo-wave-ps-1-480.png');}else if($(window).width()>=768){$('.js-31').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-182.png' : 'images/rms-logo-wave-ps-1-91.png');
$('.js-32').attr('src', 'images/fotolia_69591919_s-768.jpg');
$('.js-33').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-766.png' : 'images/rms-logo-wave-ps-1-383.png');}else if($(window).width()>=480){$('.js-31').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-114.png' : 'images/rms-logo-wave-ps-1-57.png');
$('.js-32').attr('src', 'images/fotolia_69591919_s-480.jpg');
$('.js-33').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-477.png' : 'images/rms-logo-wave-ps-1-238.png');}else{$('.js-31').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-76.png' : 'images/rms-logo-wave-ps-1-38.png');
$('.js-32').attr('src', (dpi>1) ? 'images/fotolia_69591919_s-640.jpg' : 'images/fotolia_69591919_s-320.jpg');
$('.js-33').attr('src', (dpi>1) ? 'images/rms-logo-wave-ps-1-318.png' : 'images/rms-logo-wave-ps-1-159.png');}}};
if(!window.HTMLPictureElement){$(window).resize(r);r();}

});