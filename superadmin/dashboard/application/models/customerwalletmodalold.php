<?php

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

require_once 'StripeModule.php';
require 'aws.phar';
require_once 'AwsPush.php';

class customerwalletmodal extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
    }

    function GetRechargedata_ajax() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $cityId = $this->session->userdata('city_id');

        if ($cityId != '0') {



            $citycoordinate = $this->db->query("select City_Lat,(select Currency from city where City_Id='" . $cityId . "') as CurruncySmb,City_Long from city_available where City_Id='" . $cityId . "'")->row_array();
            //- (select COALESCE(sum(app_owner_pl),0) from appointment where status = 9  and mas_id = m.mas_id)),2)
//            print_r($citycoordinate);
//            exit();
            $query = "s.slave_id in (select slave_id from slave where "
                    . "(
    '" . customerRadiusUnit . "' * acos (
      cos ( radians('" . $citycoordinate['City_Lat'] . "') )
      * cos( radians( latitude ) )
      * cos( radians( longitude ) - radians('" . $citycoordinate['City_Long'] . "') )
      + sin ( radians('" . $citycoordinate['City_Lat'] . "') )
      * sin( radians( latitude ) )
    )
  ) < '" . customerRadius . "')";
            $this->datatables->select("s.last_name,s.slave_id,s.first_name,s.email,s.phone,s.lang,"
                            . "ROUND(( ifnull((select sum(CreditedAmount) from CustomerWallet where s.slave_id = slave_id),0)"
                            . " + ifnull((select sum(CreditedAmount) from CustomerWallet where CityId= '" . $cityId . "'),0)        ),2) ,"
                            . "(select CreditedDate from CustomerWallet where (s.slave_id = slave_id or slave_id = 0) and CityId = '" . $cityId . "' order by id desc limit 1)", false)
                    ->edit_column('s.last_name', 'counter/$1', 's.slave_id')
                    ->edit_column('s.lang', $citycoordinate['CurruncySmb'], 's.slave_id')
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 's.slave_id')
                    ->add_column('OPERATION', '<a href="' . base_url("index.php/customerwallet/UserWalletStatement/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">STATEMENT</button></a>
            ', 's.slave_id')
                    ->from('slave s')
                    ->where($query);
        } else {

            $query = "(
    '" . customerRadiusUnit . "' * acos (
      cos ( radians(ca.City_Lat) )
      * cos( radians( s.latitude ) )
      * cos( radians( s.longitude ) - radians(ca.City_Long) )
      + sin ( radians(ca.City_Lat) )
      * sin( radians( s.latitude ) )
    )
  ) < '" . customerRadius . "'";
            $this->datatables->select("s.last_name,s.slave_id,s.first_name,s.email,s.phone,(select Currency from city where City_Id=ca.City_Id),"
                            . "ROUND(( ifnull((select sum(CreditedAmount) from CustomerWallet where s.slave_id = slave_id),0)"
                            . " + ifnull((select sum(CreditedAmount) from CustomerWallet where CityId = ca.City_Id ),0)        ),2) ,"
                            . "(select CreditedDate from CustomerWallet where (s.slave_id = slave_id or slave_id = 0) and CityId = ca.City_Id order by id desc limit 1)", false)
                    ->edit_column('s.last_name', 'counter/$1', 's.slave_id')
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 's.slave_id')
                    ->add_column('OPERATION', '<a href="' . base_url("index.php/customerwallet/UserWalletStatement/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">STATEMENT</button></a>
            ', 's.slave_id')
                    ->from('slave s,city_available ca')
                    ->where($query);
        }

        echo $this->datatables->generate();
    }

    // 
    function CriditTowallet($args) {

        $amount = $this->input->post('amount');
        $cityId = $this->session->userdata('city_id');
        $user = $this->input->post('user');
        $pushmsg = $this->input->post('msg');
        $message = "Error While Processing Your request.";

        if ($user != '') {

            $query = "insert into CustomerWallet(CreditedDate,CreditedAmount,slave_id,creditedBy,TransactionType)values(now(),'" . $amount . "','" . $user[0] . "',2,1)";
            $flag = $this->db->query($query);
            if ($flag)
                $message = 'Added Amount to wallet.';
        } else {
            if ($cityId != '0') {
                $query = "insert into CustomerWallet(CreditedDate,CreditedAmount,CityId,creditedBy,TransactionType)values(now(),'" . $amount . "','" . $cityId . "',2,1)";
                $flag = $this->db->query($query);
                if ($flag)
                    $message = 'Added Amount to wallet.';
            }else {
                $flag = false;
                $message = 'Please select City First';
            }
        }
        echo json_encode(array('msg' => $message, 'flag' => $flag));
        return;
    }

    function UserWalletStatement($id) {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("ap.OpeningBal,ap.appointment_id,ROUND(ap.app_owner_pl,2),ap.ClosingBal", false)
                ->from('appointment ap')
                ->where('ap.mas_id', $id);
        echo $this->datatables->generate();
    }

    function GetuserDetails($id) {

        $mas = $this->db->query("select * from slave where slave_id = '" . $id . "'")->row();
        return $mas;
    }

    function getCurrencySymb($id) {
        $flag = false;
        if ($this->session->userdata('city_id') != "0") {
            $flag = true;
            $mas = $this->db->query("select Currency from city where City_Id = '" . $this->session->userdata('city_id') . "'")->row_array();
        }

        echo json_encode(array('flag' => $flag, 'smb' => $mas['Currency']));
        return;
    }

//    function CustomerStatement_ajax($slaveid) {
//        $condition = "('" . customerRadiusUnit . "' * acos ( cos ( radians(ca.City_Lat) ) * cos( radians( s.latitude ) ) * cos( radians( s.longitude ) - radians(ca.City_Long) ) + sin ( radians(ca.City_Lat) ) * sin( radians( s.latitude ) ) )) < '" . customerRadius . "'";
//        $query = "select cw.CreditedDate,cw.id,(case cw.TransactionType when 2 then 'DEBIT' when 1 then 'CREDIT' END) as transectionType,(case cw.creditedBy when 2 then 'ADMIN' when 1 then 'CUSTOMER' END) as creditedBy,cw.txn_id,cw.PaymentMethod,cw.pgcommission,cw.CreditedAmount from CustomerWallet cw,slave s,city_available ca where (cw.slave_id = s.slave_id OR cw.slave_id = 0 ) AND s.slave_id ='" . $slaveid . "' and " . $condition . " ";
//        return $this->db->query($query)->result();
//    }
    function CustomerStatement_ajax($slaveid) {
        $query = "select distinct cw.CreditedDate,cw.id,(case cw.TransactionType when 2 then 'DEBIT' when 1 then 'CREDIT' END) as transectionType,(case cw.creditedBy when 2 then 'ADMIN' when 1 then 'CUSTOMER' END) as creditedBy,cw.txn_id,cw.PaymentMethod,cw.pgcommission,cw.CreditedAmount from CustomerWallet cw, slave s where cw.slave_id ='" . $slaveid . "' or (cw.slave_id = 0 AND cw.CityId = s.city_id)";
    return $this->db->query($query)->result();
//    $data =  $this->db->query($query)->result();
    
//    echo $query;
//    print_r($data);
//    exit();
    }
    
}

?>
