var Config = require('config-js');
var config = new Config('./config-js.js');
var Utility = require("./UtilityFunc");
var moment = require('moment');
var async = require("async");
var util = require('util');
var _ = require('underscore');
var validator = require('validator');
var pubnub = require("pubnub")({
    ssl: config.get('define.bool_'), // <- enable TLS Tunneling over TCP
    publish_key: config.get('define.pulish_key'),
    subscribe_key: config.get('define.subscribe_key')
});


pubnub.subscribe({
    channel: config.get("define.presence_chn"),
    presence: function (m) {
        if (validator.isEmail(m.uuid)) {
            var status = 4;
            if (m.action === 'join') {
                status = 3;
            } else if (m.action === 'leave') {
                status = 4;
            } else if (m.action === 'timeout') {
                status = 4;
            }


            var cond = {email: m.uuid};
            var newdata = {status: parseInt(status)};


            Utility.Update('location', cond, newdata, function (err, result1) {
                if (err) {
                    console.log("Presence Status Error : ");
                } else if (result1) {
                    //console.log("success Status Error : " + JSON.stringify(m));
                }
            });
        }

    },
    message: function (m) {
    }
});
//
pubnub.subscribe({
    channel: config.get('define.Server_chn'),
    callback: function (mm) {
        console.log(mm);
        if (mm.a == 4) {

            var cond = {email: mm.e_id};
            var newdata = {location: {longitude: parseFloat(mm.lg),
                    latitude: parseFloat(mm.lt)}, lastTsPc: moment().unix()};
            Utility.Update('location', cond, newdata, function (err, result1) {
                if (err) {
                    console.log("Update Status Error : " + err);
                } else if (result1) {
                    if (typeof mm.bid != 'undefined' && mm.bid > 0) {
                        var data = {route: {longitude: parseFloat(mm.lg), latitude: parseFloat(mm.lt)}};
                        Utility.SelectOne('booking_route', {bid: mm.bid}, function (err, result1) {
                            if (err) {
                                console.log("Update Status Error : " + err);
                            } else if (result1) {
                                if (result1.lenght > 0) {

                                    Utility.UpdatePush('booking_route', {bid: mm.bid}, data, function (err, result1) {
                                        if (err) {
                                            console.log("booking_route Not Pushed : " + err);
                                        }
                                    });
                                } else {

                                    Utility.Insert('booking_route', {route: {bid: mm.bid, longitude: parseFloat(mm.lg), latitude: parseFloat(mm.lt)}}, function (err, result1) {
                                        if (err) {
                                            console.log("Booking Route Not inserted: " + err);
                                        }
                                    });
                                }

                            }
                        });
                    }
                    //  console.log("Updated ");
                }
            });
        } else if (mm.a == 11) {
            console.log(mm);
            if (typeof mm.bid != 'undefined' && mm.bid > 0 && typeof mm.bid != mm.receiveDt) {

                var data = {bid: parseInt(mm.bid), receiveDt: mm.receiveDt};
                var return_ = {ackn: 1};
                Utility.Insert('booking_route', data, function (err, result1) {
                    if (err) {
                        console.log("Booking receiveDt ERROR while inserting: " + err);
                    } else {
                        console.log("Booking receiveDt inserted: ");
                        return_ = {ackn: 0};
                    }
                });
                if (mm.chn)
                    pubnub.publish({
                        channel: mm.chn,
                        message: return_,
                        callback: function (e) {
                            console.log("SUCCESS!", return_);
                        },
                        error: function (e) {
                            console.log("FAILED! RETRY PUBLISH!", e);
                        }
                    });
            }

        } else if (mm.a === 1) {
            console.log(mm);

            var surgeTime = '';
            if (mm.dt != '') {
                var surgeHour = new Date(mm.dt).getHours();
                var surgeMin = new Date(mm.dt).getMinutes();
                //surgeTime = surgeHour + ":" + surgeMin;
                surgeTime = mm.dt + ":" + mm.dt1;

                //  var hh = d.getHours();
                // var m = d.getMinutes();
                // surgeTime = dateFormat(mm.dt, "h:MM");
            }

//console.log("select date---"+ surgeTime);
//console.log("select date2---"+moment(mm.dt*1000).format("HH:mm"));
            var types = [];
            var driversArr = [];
            var driverdata = {}; //new Array([]);
            var driverdataArr = [];
            var TypeArray = {};
            var surg_price = '';
            var ZoneCond = {
                "polygons": {
                    $geoIntersects: {
                        $geometry: {
                            type: "Point",
                            coordinates: [
                                parseFloat(mm.lg),
                                parseFloat(mm.lt)
                            ]
                        }
                    }
                }

            };
            async.waterfall(
                    [
                        function (callback) {

                            Utility.SelectOne('zones', ZoneCond, function (err, result1) {
                                if (err) {
                                    surg_price = '';
                                    console.log("zone resp" + err);
                                } else
                                if (result1) {
                                    if (_.isEqual(result1.charge_type, "SURGECHARGE") && _.isEqual(result1.surge_enabled, "1")) {
                                        if (result1.endTime > result1.startTime) {
                                            if (surgeTime > result1.startTime && surgeTime < result1.endTime)
                                                surg_price = result1.surge_price;
                                        } else {
                                            if (surgeTime > result1.startTime || surgeTime < result1.endTime)
                                                surg_price = result1.surge_price;
                                        }

                                    }
                                }
                                callback(null, callback);
                            });

                        },
                        function (arg1, callback) {

                            Utility.Cmd('vehicleTypes', {long: parseFloat(mm.lg),
                                lat: parseFloat(mm.lt)}, function (err, result1) {
                                if (err) {
                                    console.log("Error while getting vehicleTypes");
                                } else if (result1) {
                                    result1.results.forEach(function (item) {
                                        var data = item.obj;
                                        types.push(data);
                                        TypeArray[parseInt(data.type)] = {
                                            'type_name': data.type_name,
                                            'type_id': data.type,
                                            'max_size': parseInt(data.max_size),
                                            'basefare': parseFloat(data.basefare),
                                            'min_fare': parseFloat(data.min_fare),
                                            'price_per_min': parseFloat(data.price_per_min),
                                            'price_per_km': parseFloat(data.price_per_km),
                                            'type_desc': data.type_desc,
                                            'MapIcon': data.type_map_image,
                                            'vehicle_img': data.type_on_image,
                                            'vehicle_img_off': data.type_off_image,
                                            'surg_price': surg_price,
                                            'order': data.vehicle_order

                                        };

                                    });

                                    callback(null, callback);
                                }

                            });
                        },
                        function (arg1, callback) {

                            Utility.Cmd('location', {long: parseFloat(mm.lg),
                                lat: parseFloat(mm.lt)}, function (err, result1) {
                                if (err) {
                                    console.log("Update Status Error : " + err);
                                } else if (result1) {


                                    result1.results.forEach(function (item) {
                                        var doc = item.obj;
                                        var dis = item.dis;
                                        var typeid = parseInt(doc.type);
                                        if (typeid in driverdata) {
                                            driverdata[typeid].push({lt: doc.location.latitude, lg: doc.location.longitude, e: doc.email, chn: doc.chn, d: dis});
                                        } else
                                            driverdata[typeid] = [{lt: doc.location.latitude, lg: doc.location.longitude, e: doc.email, chn: doc.chn, d: dis}];//[words[i]];

                                    });

//                                        console.log(driverdata);
                                    callback(null, callback);
                                }
                            });
                        }
                    ],
                    // the bonus final callback function
                            function (err, status) {




                                var masarray = [];


                                for (var key in TypeArray) {
                                    var ArraryTopush = [];
                                    masarray.push({
                                        tid: key,
                                        mas: (typeof driverdata[parseInt(key)] != 'undefined') ?
                                                driverdata[parseInt(key)]
                                                : ArraryTopush

                                    });
                                }
                                // console.log(masarray);


                                var return_ = {};
                                if (types.length > 0)
                                    return_ = {a: 2,
                                        masArr: masarray,
                                        tp: mm.tp,
                                        st: mm.st,
                                        flag: 0,
                                        types: array_values(TypeArray)
                                    };
                                else
                                    return_ = {a: 2,
                                        flag: 1,
                                        types: []
                                    };
                                pubnub.publish({
                                    channel: mm.chn,
                                    message: return_,
                                    callback: function (e) {
//                                            console.log("SUCCESS!", return_);
                                    },
                                    error: function (e) {
                                        console.log("FAILED! RETRY PUBLISH!", e);
                                    }
                                });
                            }
                    );

                }
    }
});
function array_values(input) {

    var tmpArr = []
    var key = ''

    for (key in input) {
        tmpArr[tmpArr.length] = input[key]
    }

    return tmpArr
}


