
CREATE DATABASE  IF NOT EXISTS `database` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `database`;


-- MySQL dump 10.13  Distrib 5.7.15, for Linux (x86_64)
--
-- Host: 188.166.213.95    Database: delicab
-- ------------------------------------------------------
-- Server version	5.5.52-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CustomerWallet`
--

DROP TABLE IF EXISTS `CustomerWallet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CustomerWallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slave_id` int(11) DEFAULT '0',
  `CreditedDate` date DEFAULT NULL,
  `CreditedAmount` double DEFAULT NULL,
  `creditedBy` int(1) DEFAULT '1' COMMENT '1- individual,2-admin',
  `CityId` int(11) DEFAULT NULL,
  `TransactionType` int(1) DEFAULT NULL COMMENT '( 1-Credit / 2-Debit )',
  `txn_id` varchar(500) DEFAULT NULL COMMENT ', Payment Gateway Transaction Id',
  `PaymentMethod` varchar(500) DEFAULT NULL COMMENT 'payment getway like stripe,paypal,hyperpay etc.',
  `OpeningBalance` double DEFAULT '0',
  `ClosingBalance` double DEFAULT '0',
  `pgcommission` double DEFAULT NULL,
  `satteled` int(1) DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DriverNotification`
--

DROP TABLE IF EXISTS `DriverNotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DriverNotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` int(11) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `NumOfDriver` int(11) DEFAULT NULL,
  `user_type` int(2) DEFAULT '1' COMMENT '1- driver 2- passenger',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DriverRecharge`
--

DROP TABLE IF EXISTS `DriverRecharge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DriverRecharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mas_id` int(11) DEFAULT NULL,
  `RechargeDate` date DEFAULT NULL,
  `RechargeAmount` double DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=521 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_variables`
--

DROP TABLE IF EXISTS `app_variables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_variables` (
  `variable_id` int(11) NOT NULL AUTO_INCREMENT,
  `variable_name` varchar(100) NOT NULL,
  `variable_value` varchar(100) NOT NULL,
  PRIMARY KEY (`variable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointment` (
  `appointment_id` int(20) NOT NULL AUTO_INCREMENT,
  `slave_id` int(20) NOT NULL,
  `mas_id` int(20) NOT NULL,
  `created_dt` datetime NOT NULL,
  `accepted_dt` datetime DEFAULT NULL,
  `arrive_dt` datetime DEFAULT NULL COMMENT 'Arrived data time of driver',
  `start_dt` datetime DEFAULT NULL COMMENT 'Journey start dt',
  `complete_dt` datetime DEFAULT NULL COMMENT 'Complete date time',
  `last_modified_dt` datetime NOT NULL,
  `status` tinyint(2) NOT NULL COMMENT '1 - request, 2 - driver acpt, 3 - driver rjct, 4 - pat cncld, 5- driver cancelled, 6 - driver on way, 7 - driver arrived, 8 - jrney start, 9 - Jrney cmplt, 10 - Expired',
  `cancel_status` tinyint(2) DEFAULT NULL COMMENT '1-> before assign, 2-> after assign with in 5 min, 3-> after assign and 5 min, 4-> Passenger not came, 5-> Address wrong, 6-> Passenger asked to cancel, 7-> Dont charge customer, 8->Others',
  `cancel_dt` datetime DEFAULT NULL COMMENT 'cancel datetime',
  `appointment_dt` datetime NOT NULL,
  `appt_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-> Now, 2-> Later',
  `duration` float DEFAULT NULL COMMENT 'duration in mts',
  `waiting_mts` int(10) NOT NULL DEFAULT '0' COMMENT 'waiting time in minutes',
  `distance_in_mts` bigint(25) DEFAULT NULL COMMENT 'journey distance in meters',
  `google_distance` int(11) NOT NULL DEFAULT '0' COMMENT 'google calculated distance',
  `server_distance` int(11) NOT NULL DEFAULT '0' COMMENT 'server calculated distance from booking route',
  `app_distance` float NOT NULL DEFAULT '0' COMMENT 'App calculated distance',
  `car_id` int(15) NOT NULL COMMENT 'Car id for booking',
  `type_id` int(5) DEFAULT NULL COMMENT 'type of workplace',
  `address_line1` varchar(500) NOT NULL,
  `address_line2` varchar(300) DEFAULT NULL,
  `user_device` varchar(200) DEFAULT NULL COMMENT 'Device that booking is done',
  `appt_lat` double DEFAULT NULL COMMENT 'latitude of appointment address',
  `appt_long` double DEFAULT NULL COMMENT 'longitude of appointment address',
  `drop_addr1` varchar(500) DEFAULT NULL,
  `drop_addr2` varchar(150) DEFAULT NULL,
  `drop_lat` double DEFAULT NULL,
  `drop_long` double DEFAULT NULL,
  `city` int(10) DEFAULT NULL,
  `country` int(10) DEFAULT NULL,
  `zipcode` varchar(30) DEFAULT NULL,
  `payment_status` tinyint(2) DEFAULT NULL COMMENT '1->Paid,2->reported,3->Closed',
  `payment_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 -> Card, 2 -> Cash',
  `apprxAmt` float DEFAULT NULL COMMENT 'Approximate amount calculated for server',
  `amount` float NOT NULL,
  `master_tip` float NOT NULL DEFAULT '0' COMMENT 'Tip amount for master',
  `meter_fee` float NOT NULL DEFAULT '0',
  `toll_fee` float NOT NULL DEFAULT '0',
  `airport_fee` float NOT NULL DEFAULT '0',
  `parking_fee` float NOT NULL DEFAULT '0',
  `tip_amount` float NOT NULL DEFAULT '0',
  `tip_percent` int(3) NOT NULL DEFAULT '0' COMMENT 'Tip percentage',
  `coupon_code` varchar(30) DEFAULT NULL,
  `discount` float NOT NULL DEFAULT '0',
  `extra_notes` varchar(500) DEFAULT NULL COMMENT 'extra notes or details',
  `remarks` varchar(500) DEFAULT NULL COMMENT 'any other remarks',
  `inv_id` varchar(60) DEFAULT NULL COMMENT 'Invoice id',
  `B_type` tinyint(2) DEFAULT '0' COMMENT 'Booking type for dispatcher',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Transaction id from stripe to master',
  `cc_fee` double DEFAULT '0' COMMENT 'Charge fee',
  `cancel_amt` double DEFAULT '0' COMMENT 'Cancel Amount',
  `app_commission` double NOT NULL DEFAULT '0',
  `pg_commission` double NOT NULL DEFAULT '0',
  `mas_earning` double NOT NULL DEFAULT '0',
  `app_owner_pl` double NOT NULL DEFAULT '0' COMMENT 'app owner profit or loss',
  `expire_ts` int(11) NOT NULL DEFAULT '0',
  `settled_flag` tinyint(2) NOT NULL DEFAULT '0',
  `additional_info` varchar(1000) DEFAULT NULL,
  `OpeningBal` double DEFAULT '0' COMMENT 'opening balance for driver in wallet',
  `ClosingBal` double DEFAULT '0' COMMENT 'closing balance on wallet',
  `surge` int(1) DEFAULT '1' COMMENT '1 - default, > 1  actual surg price',
  `deviceType` int(1) DEFAULT '0' COMMENT '1- android 2- ios',
  `pricePerMin` float NOT NULL,
  `PricePerKm` float NOT NULL,
  `baseFare` float NOT NULL,
  `minmunfare` float NOT NULL,
  `wallet_settled` int(1) DEFAULT '0',
  `MininumAmtToCollect` float DEFAULT NULL,
  `walletDeductedAmt` float DEFAULT '0',
  `tripamount` float DEFAULT NULL,
  PRIMARY KEY (`appointment_id`),
  KEY `patient_id` (`slave_id`,`mas_id`,`status`),
  KEY `status` (`status`),
  KEY `mas_id` (`mas_id`),
  KEY `slave_id` (`slave_id`),
  KEY `status_2` (`status`),
  KEY `appointment_dt` (`appointment_dt`)
) ENGINE=InnoDB AUTO_INCREMENT=2799 DEFAULT CHARSET=utf8 COMMENT='appointment data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `City_Id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'City Id',
  `Country_Id` int(6) NOT NULL COMMENT 'Country Id',
  `City_Name` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'City Name',
  `Currency` varchar(15) NOT NULL DEFAULT 'USD' COMMENT 'Currency in 3 char',
  PRIMARY KEY (`City_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COMMENT='Cities in all states around all the countries';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `city_available`
--

DROP TABLE IF EXISTS `city_available`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_available` (
  `City_Id` int(8) NOT NULL COMMENT 'City Id',
  `Country_Id` int(6) NOT NULL COMMENT 'Country Id',
  `City_Name` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'City Name',
  `City_Lat` varchar(50) DEFAULT NULL,
  `City_Long` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`City_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Cities in all states around all the countries';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company_info`
--

DROP TABLE IF EXISTS `company_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_info` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `companyname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `addressline1` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `state` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `postcode` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `vat_number` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `firstname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `lastname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `mobile` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `landline` int(15) DEFAULT NULL,
  `userame` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Status` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `Country_Id` int(6) NOT NULL AUTO_INCREMENT COMMENT 'Country Id',
  `Country_Name` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Country name',
  PRIMARY KEY (`Country_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Country table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coupon_usage`
--

DROP TABLE IF EXISTS `coupon_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_usage` (
  `coupon_code` varchar(25) NOT NULL COMMENT 'coupon code from coupons',
  `appointment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupons_id` int(11) NOT NULL COMMENT 'campaign id or coupon id',
  KEY `coupon_code` (`coupon_code`),
  KEY `appointment_id` (`appointment_id`),
  KEY `user_id` (`user_id`),
  KEY `coupons_id` (`coupons_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `coupon_code` varchar(30) NOT NULL,
  `start_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `coupon_type` tinyint(2) DEFAULT '1' COMMENT '1 - referral, 2 - Promotional, 3 - Discount',
  `discount_type` tinyint(2) DEFAULT '1' COMMENT '1 - Percent, 2 - Amount',
  `discount` float DEFAULT NULL,
  `referral_discount_type` tinyint(2) DEFAULT '1' COMMENT '1 - Percent, 2 - Amount',
  `referral_discount` float DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `max_redemptions` int(11) DEFAULT '1',
  `status` tinyint(2) DEFAULT '0' COMMENT '0 - active, 1 - inactive',
  `city_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT 'In case of coupon type 1,2',
  `user_type` tinyint(2) DEFAULT '1' COMMENT '1 - User, 2 - Admin',
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incremented id',
  `referral_campaign_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Referral id that user code created from',
  `referred_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '0 - no user, else slave_id',
  `create_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 - no use, 1 - signup promo, 2 - referred promo',
  `title` varchar(120) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coupon_type` (`coupon_type`),
  KEY `start_date` (`start_date`),
  KEY `expiry_date` (`expiry_date`),
  KEY `status` (`status`),
  KEY `user_id` (`user_id`),
  KEY `user_type` (`user_type`),
  KEY `coupon_code` (`coupon_code`),
  KEY `create_type` (`create_type`),
  KEY `referred_user_id` (`referred_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dev_type`
--

DROP TABLE IF EXISTS `dev_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_type` (
  `dev_id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Device type id',
  `name` varchar(100) NOT NULL COMMENT 'Name of the device',
  PRIMARY KEY (`dev_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Stores device types which the appl can allow';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dispatcher`
--

DROP TABLE IF EXISTS `dispatcher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispatcher` (
  `dis_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_name` varchar(150) NOT NULL,
  `dis_email` varchar(150) NOT NULL,
  `dis_pass` varchar(150) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `city` int(12) NOT NULL,
  PRIMARY KEY (`dis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docdetail`
--

DROP TABLE IF EXISTS `docdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docdetail` (
  `doc_ids` int(11) NOT NULL AUTO_INCREMENT,
  `driverid` int(11) DEFAULT NULL,
  `url` varchar(300) DEFAULT NULL,
  `expirydate` date DEFAULT NULL,
  `doctype` int(11) DEFAULT NULL,
  PRIMARY KEY (`doc_ids`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `image_id` int(22) NOT NULL AUTO_INCREMENT COMMENT 'image id',
  `mas_id` int(20) NOT NULL COMMENT 'Doctor id',
  `image` varchar(500) NOT NULL COMMENT 'image name',
  PRIMARY KEY (`image_id`),
  KEY `doc_id` (`mas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Doctor images other than profile images';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `master`
--

DROP TABLE IF EXISTS `master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master` (
  `mas_id` int(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(150) NOT NULL,
  `type_id` int(3) DEFAULT NULL COMMENT 'type of speciality',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - signed up, 2 - under verification, 3 - active, 4 - rejected, 5 - inactive',
  `mobile` varchar(30) NOT NULL,
  `about` varchar(600) DEFAULT NULL COMMENT 'about doctor',
  `expertise` varchar(1000) DEFAULT NULL COMMENT 'Doc expertise',
  `zipcode` varchar(20) DEFAULT NULL,
  `created_dt` datetime NOT NULL,
  `last_active_dt` datetime NOT NULL,
  `profile_pic` varchar(500) DEFAULT NULL,
  `stripe_id` varchar(40) DEFAULT NULL COMMENT 'Stripe id for transactions',
  `stoken` varchar(50) DEFAULT NULL COMMENT 'Stripe token',
  `license_type` tinyint(1) DEFAULT NULL,
  `license_num` varchar(100) DEFAULT NULL,
  `license_exp` date DEFAULT NULL,
  `license_pic` varchar(500) DEFAULT NULL,
  `tax_num` varchar(30) DEFAULT NULL COMMENT 'tax number',
  `personal_description` varchar(1000) DEFAULT NULL,
  `insurance_carrier` varchar(250) DEFAULT NULL,
  `certification_board` varchar(250) DEFAULT NULL,
  `board_certification_expiry_dt` datetime DEFAULT NULL,
  `subscription_type` tinyint(1) DEFAULT NULL,
  `subscription_expiry` datetime DEFAULT NULL,
  `workplace_id` int(20) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `resetData` varchar(50) DEFAULT NULL COMMENT 'reset password link',
  `resetFlag` tinyint(1) DEFAULT NULL COMMENT '1-> reset link not used',
  `company_id` int(11) DEFAULT NULL,
  `lang` bigint(1) DEFAULT NULL,
  `vehicle_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`mas_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2038 DEFAULT CHARSET=utf8 COMMENT='Master data table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `master_bank`
--

DROP TABLE IF EXISTS `master_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_bank` (
  `bank_id` int(20) NOT NULL,
  `mas_id` int(20) NOT NULL,
  `stripe_id` varchar(40) NOT NULL,
  `default_stripe` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `master_ratings`
--

DROP TABLE IF EXISTS `master_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_ratings` (
  `mas_id` int(20) NOT NULL,
  `slave_id` int(20) NOT NULL COMMENT 'Patient who given review',
  `review_dt` datetime NOT NULL,
  `star_rating` float NOT NULL,
  `review` varchar(2000) CHARACTER SET ucs2 DEFAULT NULL,
  `appointment_id` int(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive',
  KEY `doc_id` (`mas_id`,`star_rating`,`appointment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='doctor ratings';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_types`
--

DROP TABLE IF EXISTS `notification_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_types` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `passenger_rating`
--

DROP TABLE IF EXISTS `passenger_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passenger_rating` (
  `mas_id` int(11) NOT NULL,
  `slave_id` int(11) NOT NULL,
  `appointment_id` int(15) DEFAULT NULL COMMENT 'Appointment id',
  `rating` float NOT NULL,
  `status` int(11) NOT NULL COMMENT '1-activate,2-Deactivate',
  `rating_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payroll`
--

DROP TABLE IF EXISTS `payroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payroll` (
  `payroll_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Payroll id',
  `trasaction_id` text NOT NULL COMMENT 'Transaction id',
  `mas_id` int(11) NOT NULL COMMENT 'Driver id',
  `opening_balance` double NOT NULL COMMENT 'Remaining balance in the account',
  `pay_date` datetime NOT NULL COMMENT 'Date of payment or transfer',
  `pay_amount` double NOT NULL COMMENT 'Amount transfered',
  `closing_balance` double NOT NULL COMMENT 'Remaining balance after last transaction',
  `due_amount` double NOT NULL,
  PRIMARY KEY (`payroll_id`),
  KEY `mas_id` (`mas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `report_id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'report id',
  `mas_id` int(20) NOT NULL COMMENT 'master id',
  `slave_id` int(20) NOT NULL COMMENT 'customer id',
  `appointment_id` int(20) NOT NULL COMMENT 'appointment id',
  `report_msg` varchar(350) CHARACTER SET utf8 NOT NULL COMMENT 'detailed message',
  `admin_note` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `report_dt` datetime NOT NULL COMMENT 'reporting date time',
  `report_status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-> Reported, 2-> Verified',
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf16 COMMENT='Reports from Customers';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slave`
--

DROP TABLE IF EXISTS `slave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slave` (
  `slave_id` int(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `email` varchar(500) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `password` varchar(150) NOT NULL,
  `stripe_id` varchar(40) DEFAULT NULL COMMENT 'Stripe id for money transferes',
  `paypal_token` varchar(750) DEFAULT NULL COMMENT 'Paypal refresh token for future payments',
  `status` tinyint(1) NOT NULL COMMENT '1 - sign up, 3 - active, 4 - inactive',
  `booking_status` tinyint(2) DEFAULT NULL,
  `created_dt` datetime NOT NULL,
  `last_active_dt` datetime NOT NULL,
  `profile_pic` varchar(600) DEFAULT NULL,
  `zipcode` varchar(15) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `resetData` varchar(50) DEFAULT NULL COMMENT 'Reset Data for password',
  `resetFlag` tinyint(1) DEFAULT NULL COMMENT 'Reset flag 1-> unused',
  `coupon` varchar(50) DEFAULT NULL,
  `lang` int(2) DEFAULT '1',
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`slave_id`),
  KEY `email` (`email`(255))
) ENGINE=InnoDB AUTO_INCREMENT=1813 DEFAULT CHARSET=utf8 COMMENT='patient information table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statusmessages`
--

DROP TABLE IF EXISTS `statusmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statusmessages` (
  `sid` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Status id',
  `statusNumber` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - success, 1 - error',
  `statusMessage` varchar(400) CHARACTER SET utf8 NOT NULL COMMENT 'brief status message',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=405 DEFAULT CHARSET=latin1 COMMENT='status messages for the appl response';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `superadmin`
--

DROP TABLE IF EXISTS `superadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `superadmin` (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(100) NOT NULL COMMENT 'super admin username',
  `password` varchar(100) NOT NULL COMMENT 'super admin pass',
  `email` varchar(150) DEFAULT NULL,
  `user_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 - Superadmin, 2 - manager',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='super admin credencials';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_sessions` (
  `sid` int(20) NOT NULL AUTO_INCREMENT COMMENT 'Session id',
  `oid` int(20) NOT NULL COMMENT 'Object Id',
  `token` varchar(500) NOT NULL COMMENT 'Session token',
  `expiry` datetime NOT NULL COMMENT 'Session expiry date and time in GMT',
  `user_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - driver, 2 - passenger',
  `device` varchar(500) NOT NULL COMMENT 'Device on which session is generated',
  `type` int(4) NOT NULL COMMENT 'Type of device or platform',
  `push_token` varchar(700) DEFAULT NULL COMMENT 'Token for push notification',
  `create_date` datetime NOT NULL COMMENT 'Current date and time in GMT',
  `loggedIn` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - logged in, 2 - logged out',
  PRIMARY KEY (`sid`),
  KEY `user_type` (`user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=6124 DEFAULT CHARSET=utf8 COMMENT='stores multiple session token for all the users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vechiledoc`
--

DROP TABLE IF EXISTS `vechiledoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vechiledoc` (
  `docid` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) DEFAULT NULL,
  `expirydate` date DEFAULT NULL,
  `doctype` int(11) DEFAULT NULL COMMENT '1-rc,2-insurance,3-carriage',
  `vechileid` int(11) DEFAULT NULL,
  PRIMARY KEY (`docid`)
) ENGINE=InnoDB AUTO_INCREMENT=3958 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vehicleType`
--

DROP TABLE IF EXISTS `vehicleType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicleType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicletype` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vehiclemodel`
--

DROP TABLE IF EXISTS `vehiclemodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiclemodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicletypeid` int(11) DEFAULT NULL,
  `vehiclemodel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workplace`
--

DROP TABLE IF EXISTS `workplace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workplace` (
  `workplace_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Workplace/vehicle id',
  `uniq_identity` varchar(20) NOT NULL,
  `type_id` int(3) NOT NULL COMMENT 'Workplace type id',
  `Title` varchar(150) DEFAULT NULL COMMENT 'workplace title',
  `Status` tinyint(1) DEFAULT NULL COMMENT '1 - assign, 2 - accept , 4- reject, 5 - created',
  `unique_id` varchar(250) DEFAULT NULL COMMENT 'Unique id for the vehicle',
  `Vehicle_Make` varchar(100) DEFAULT NULL COMMENT 'vehicle manufacturer',
  `Vehicle_Model` varchar(100) DEFAULT NULL COMMENT 'vehicle model',
  `Vehicle_Type` varchar(100) DEFAULT NULL COMMENT 'type of vehicle',
  `Vehicle_Reg_No` varchar(40) DEFAULT NULL COMMENT 'registration no',
  `License_Plate_No` varchar(40) DEFAULT NULL COMMENT 'plate no',
  `Vehicle_Seating` int(3) DEFAULT NULL COMMENT 'no of seating',
  `Vehicle_Color` varchar(15) DEFAULT NULL COMMENT 'color of vehicle',
  `Accept_Credit_Card` tinyint(1) DEFAULT NULL COMMENT '1 - yes, 2 - no',
  `Accept_Debit_Card` tinyint(1) DEFAULT NULL COMMENT '1 - yes, 2 - no',
  `Accept_Cash` tinyint(1) DEFAULT NULL COMMENT '1 - yes, 2 - no',
  `Radio_Taxi_Rate` float DEFAULT NULL COMMENT 'radio taxi rate',
  `Fixed_Minimum_Rate` float DEFAULT NULL COMMENT 'minimum rate',
  `Charge_Type` tinyint(1) DEFAULT NULL COMMENT 'charge type',
  `Rate_per_kilometer` float DEFAULT NULL COMMENT 'rate per km',
  `Rate_per_min` float DEFAULT NULL COMMENT 'rate per min',
  `Vehicle_Insurance_No` varchar(40) DEFAULT NULL COMMENT 'vehicle insurance number',
  `Vehicle_Insurance_Dt` datetime DEFAULT NULL COMMENT 'vehicle insurance datetime',
  `vehicle_insurance_expiry_dt` datetime DEFAULT NULL COMMENT 'expiration date for vehicle insurance',
  `Cancellation_Charges_Fixed` float DEFAULT NULL COMMENT 'cancellation charges per minute',
  `Waiting_Charges_per_minute` float DEFAULT NULL COMMENT 'waiting charges per minute',
  `last_login_lat` double DEFAULT NULL COMMENT 'last login latitude',
  `last_login_long` double DEFAULT NULL COMMENT 'last login longitude',
  `last_logout_lat` double DEFAULT NULL COMMENT 'last logout latitude',
  `last_logout_long` double DEFAULT NULL COMMENT 'last logout longitude',
  `company` int(11) DEFAULT NULL,
  `Vehicle_Image` varchar(250) NOT NULL,
  `vehicle_img` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`workplace_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1320 DEFAULT CHARSET=utf8 COMMENT='workplace/vehicle details';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workplace_types`
--

DROP TABLE IF EXISTS `workplace_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workplace_types` (
  `type_id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'Type id for the workplace',
  `type_name` varchar(100) NOT NULL COMMENT 'Name for workplace',
  `max_size` int(2) NOT NULL COMMENT 'Maximum size of people',
  `basefare` float NOT NULL COMMENT 'basefare for the workplace',
  `min_fare` float NOT NULL COMMENT 'Minimum fare for workplace',
  `price_per_min` float NOT NULL COMMENT 'Price per minute',
  `price_per_km` float NOT NULL COMMENT 'Price per km',
  `waiting_charge_per_min` float DEFAULT NULL COMMENT 'waiting charge per minute',
  `type_desc` varchar(300) DEFAULT NULL COMMENT 'Description for type',
  `city_id` int(11) NOT NULL DEFAULT '18833' COMMENT 'City id for the vehicle',
  `type_icon` varchar(250) DEFAULT NULL COMMENT 'Type icon',
  `cancilation_fee` double NOT NULL DEFAULT '0',
  `vehicle_img` varchar(300) DEFAULT NULL,
  `MapIcon` varchar(300) DEFAULT NULL,
  `vehicle_img_off` varchar(300) DEFAULT NULL,
  `vehicle_order` int(20) NOT NULL,
  `Comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='Types of workplaces';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-11 15:33:31
