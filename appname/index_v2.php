<?php

require_once 'Models/config.php';
require_once 'Models/ConDBServices.php';
$db = new ConDB();
$wpTypes = $typesArr = array();

$getDocTypesQry = "select type_id,type_name from workplace_types";
$getDocTypesRes = mysql_query($getDocTypesQry, $db->conn);

while ($type = mysql_fetch_assoc($getDocTypesRes)) {
    $wpTypes[] = $type;
}

$getCompQry = "select company_id,companyname from company_info where Status = 3";
$getCompRes = mysql_query($getCompQry, $db->conn);

while ($type = mysql_fetch_assoc($getCompRes)) {
    $compList[] = $type;
}

//print_r($wpTypes);

$getTypesQry = "select * from dev_type";
$getTypesRes = mysql_query($getTypesQry, $db->conn);

while ($type = mysql_fetch_assoc($getTypesRes)) {
    $typesArr[] = $type;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title></title>
        <style>
            .form_row{height: 40px;}
            .form_label{display: inline-block;width: 250px;text-align: right;}
            .form_field{display: inline;}
            .div_service{margin:3% 0;}
            .list_1 li{padding: 10px 0;}
            .list_1 li a{color: #1383e4;font-weight: bold;font-family: verdana;}
        </style>
    </head>
    <body>
        <div id="top">&nbsp;</div>
        <div style="position:fixed;right: 5%;top:40%;">
            <a href="#top"><input type="button" value="Top"/></a>
        </div>
        <h2>Taxi services</h2>
        <div style="width:35%;text-align: left;float: left;">
            <h3>Master app (D)</h3>
            <ol type="1" class="list_1">
                <!--<li><a href="#get_types_service">Get car types</a> (getTypes)</li>-->
                <li><a href="#Master_signup1_service">Master Signup step 1</a> (masterSignup1)</li>
                <!--<li><a href="#Master_signup2_service">Master Signup step 2</a> (masterSignup2)</li>-->
                <li><a href="#Master_login_service">Master Login</a> (masterLogin)<br><br></li>
                <!--<li><a href="#update_location_service">Update location</a> (updateMasterLocation)<br><br></li>-->                
                <li><a href="#get_appointments_service">Get appointments</a> (getMasterAppointments)</li>      
                <!--<li><a href="#update_rating_service">Give Rating for passenger</a> (updateMasterRating)</li>-->
                <li><a href="#get_apnt_det_service">Get appointment details</a> (getAppointmentDetails)</li>
                <li><a href="#get_pending_appointments_service">Get Pending Appointments</a> (getPendingAppointments)</li>                
                <!--<li><a href="#get_apnt_hist_service">Get appointment history with a Passenger</a> (getHistoryWith)</li>-->                
                <li><a href="#respond_to_request_service">Respond to appointment request</a> (respondToAppointment)</li>                
                <li><a href="#update_details_service">Update appointment details</a> (updateApptDetails)</li>                
                <li><a href="#update_status_service">Update appointment status to Passenger</a> (updateApptStatus)<br><br></li>                
                <li><a href="#abort_appointment_service">Abort Journey</a> (abortJourney)<br><br></li>                
                <!--<li><a href="#getmy_slots_service">Get my slots</a> (getMySlots)<br><br></li>-->                
                <li><a href="#get_master_profile_service">Get master profile</a> (getMasterProfile)</li>                   
                <li><a href="#reser_pass_service">Reset password</a> (resetPassword)</li>
                <li><a href="#update_presence_service">Update Master as active or inactive</a> (updateMasterStatus)<br><br></li>   
                <li><a href="#get_pending_request_service">Get Pending requests</a> (getPendingRequests)</li>  

            </ol>
        </div>
        <div style="width:35%;text-align: left;float: left;">
            <h3>Passenger app (P)</h3>
            <ol type="1" class="list_1">
                <li><a href="#Passenger_signup_service">Passenger Signup</a> (slaveSignup)</li>
                <li><a href="#Passenger_login_service">Passenger Login</a> (slaveLogin)<br><br></li>
<!--                <li><a href="#get_workplaces_service">Get workplace types</a> (getWorkplaces)</li>-->
                <li><a href="#get_Masters_service">Get Masters</a> (getMasters)</li>
                <!--<li><a href="#get_Master_det_service">Get Master details</a> (getMasterDetails)</li>-->
                <!--<li><a href="#get_Master_reviews_service">Get Master reviews</a> (getMasterReviews)<br><br></li>-->
                <li><a href="#fare_calculator_service">Fare Calculator</a> (fareCalculator)</li>
                <li><a href="#look_booking_service">Live booking</a> (liveBooking)</li>
                <li><a href="#get_apnt_det_service">Get appointment details</a> (getAppointmentDetails)</li>
                <li><a href="#update_review_service">Give Appointment review</a> (updateSlaveReview)</li>
                <li><a href="#get_slave_apnt_service">Get Passenger appointments</a> (getSlaveAppointments)</li>
                <li><a href="#cancel_appointment_service">Cancel appointment</a> (cancelAppointment)<br><br></li>
                <li><a href="#cancel_appointment_request_service">Cancel appointment request</a> (cancelAppointmentRequest)<br><br></li>
                <!--<li><a href="#pay_for_booking_service">Pay for booking</a> (payForBooking)<br><br></li>-->
                <li><a href="#report_dispute_service">Report dispute</a> (reportDispute)<br><br></li>
                <li><a href="#get_profile_service">Get profile data</a> (getProfile)</li>
                <li><a href="#update_profile_service">Update profile data</a> (updateProfile)<br><br></li>
                <li><a href="#get_master_workplace_service">Get master car details</a> (getMasterCarDetails)<br><br></li>
                <!--<li><a href="#add_paypal_service">Add paypal</a> (addPaypal)</li>-->
                <li><a href="#add_card_service">Add a credit card</a> (addCard)</li>
                <li><a href="#get_cards_service">Get cards</a> (getCards)</li>
                <li><a href="#remove_card_service">Remove a credit cards</a> (removeCard)</li>
                <li><a href="#make_card_def_service">Make a credit card default</a> (makeCardDefault)</li>
                <li><a href="#update_tip_service">Update tip for the appointment</a> (updateTip)</li>
                <li><a href="#check_push_android">Test Android Push</a> (android_push)</li>
            </ol>
        </div>
        <div>
            <h3></h3>
            <ol type="1" class="list_1">
                <li><a href="#upload_image_service">Upload image</a> (uploadImage)</li>
                <li><a href="#get_appt_status_service">Get Appointment status</a> (getApptStatus)</li>
                <li><a href="#check_email_service">Validate email and zipcode</a> (validateEmailZip)</li>
                <!--<li><a href="#check_zip_service">Validate zipcode</a> (checkZip)</li>-->
                <li><a href="#forgot_pass_service">Forgot password</a> (forgotPassword)</li>
                <li><a href="#check_sess_service">Check session expired or not</a> (checkSession)</li>
                <li><a href="#check_service_city">Check city we serving</a> (checkCity)</li>
                <li><a href="#update_session_service">Update session</a>(updateSession)</li>
                <li><a href="#logout_service">Logout user</a> (logout)<br><br></li>
                <li><a href="#check_coupon_service">Check coupon available or not</a> (checkCoupon)<br><br></li>
                <li><a href="#verify_coupon_service">Verify coupon available or not</a> (verifyCode)<br><br></li>
                <li><a href="#verify_mobile_service">Verify mobile available or not</a> (checkMobile)<br><br></li>
                <li><a href="#get_mobile_service">Get verification code to mobile</a> (getVerificationCode)<br><br></li>
                <li><a href="#verify_code_service">Verify mobile code</a> (verifyPhone)<br><br></li>
                <li><a href="#remove_user_service">Remove a passenger</a></li>
                <li><a href="#remove_driver_service">Remove a driver</a></li>
            </ol>
        </div>
        <div style="clear:both;"></div>
        <div id="Master_signup1_service" class="div_service">
            <form action="services.php/masterSignup1" method="post">
                <h3>(D) Master Signup step1</h3>
                <div class="form_row">
                    <div class="form_label">First Name *: </div>
                    <div class="form_field"><input type="text" name="ent_first_name" />name= "ent_first_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Last Name : </div>
                    <div class="form_field"><input type="text" name="ent_last_name" />name="ent_last_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Password *: </div>
                    <div class="form_field"><input type="text" name="ent_password" />name="ent_password",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_mobile" />name="ent_mobile",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Postcode : </div>
                    <div class="form_field"><input type="text" name="ent_zipcode" />name="ent_zipcode",type="string"</div>
                </div>     
                <!--                <div class="form_row">
                                    <div class="form_label">Company id *: </div>
                                    <div class="form_field">
                                        <select name="ent_comp_id">
                
                <?php foreach ($compList as $type) { ?>
                                                                    <option value="<?php echo $type['company_id']; ?>"><?php echo $type['companyname']; ?></option>
                <?php } ?>
                                            <option value="0">Other</option>
                                        </select>                     
                                        name="ent_comp_id",type="int",company id and for other send 0
                                    </div>
                                </div>         -->
                <!--                <div class="form_row">
                                    <div class="form_label">Company name(* if company id is other): </div>
                                    <div class="form_field"><input type="text" name="ent_comp_name" />name="ent_comp_name",type="string"</div>
                                </div>                    -->
                <div class="form_row">
                    <div class="form_label">Tax number *: </div>
                    <div class="form_field"><input type="text" name="ent_tax_num" />name="ent_tax_num",type="string"</div>
                </div>           
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Push Token *: </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device type *: </div>
                    <div class="form_field">
                        <select name="ent_device_type">
                            <?php foreach ($typesArr as $dev) { ?>
                                <option value="<?php echo $dev['dev_id']; ?>"><?php echo $dev['name'] . '(' . $dev['dev_id'] . ')'; ?></option>
                            <?php } ?>
                        </select>name="ent_device_type",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="Master_login_service" class="div_service">
            <form action="services.php/masterLogin" method="post">
                <h3>(D) Master Login</h3>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Password *: </div>
                    <div class="form_field"><input type="text" name="ent_password" />name="ent_password",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Car id *: </div>
                    <div class="form_field"><input type="text" name="ent_car_id" />name="ent_car_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Push Token *: </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_lat" />name="ent_lat",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_long" />name="ent_long",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device type *: </div>
                    <div class="form_field">
                        <select name="ent_device_type">
                            <?php foreach ($typesArr as $dev) { ?>
                                <option value="<?php echo $dev['dev_id']; ?>"><?php echo $dev['name'] . '(' . $dev['dev_id'] . ')'; ?></option>
                            <?php } ?>
                        </select>name="ent_device_type"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


<!--        <div id="get_types_service" class="div_service">
            <form action="services.php/getTypes" method="post">
                <h3>(D) Get Company list</h3>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>   -->



        <div id="Passenger_signup_service" class="div_service">
            <form action="services.php/slaveSignup" method="post">
                <h3>(P) Passenger Signup</h3>
                <div class="form_row">
                    <div class="form_label">First Name *: </div>
                    <div class="form_field"><input type="text" name="ent_first_name" />name= "ent_first_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Last Name : </div>
                    <div class="form_field"><input type="text" name="ent_last_name" />name="ent_last_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Password *: </div>
                    <div class="form_field"><input type="text" name="ent_password" />name="ent_password",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_mobile" />name="ent_mobile",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">City Name *: </div>
                    <div class="form_field"><input type="text" name="ent_city" />name="ent_city",type="string",ex: "Bangalore"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude: </div>
                    <div class="form_field"><input type="text" name="ent_latitude" />name="ent_latitude",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude: </div>
                    <div class="form_field"><input type="text" name="ent_longitude" />name="ent_longitude",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Access token : </div>
                    <div class="form_field"><input type="text" name="ent_token" />name="ent_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Terms and conditions *: </div>
                    <div class="form_field"><input type="text" name="ent_terms_cond" />name="ent_terms_cond",type="boolean"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Pricing conditions *: </div>
                    <div class="form_field"><input type="text" name="ent_pricing_cond" />name="ent_pricing_cond",type="boolean"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Referral code: </div>
                    <div class="form_field"><input type="text" name="ent_referral_code" />name="ent_referral_code",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Push Token *: </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device type *: </div>
                    <div class="form_field">
                        <select name="ent_device_type">
                            <?php foreach ($typesArr as $dev) { ?>
                                <option value="<?php echo $dev['dev_id']; ?>"><?php echo $dev['name'] . '(' . $dev['dev_id'] . ')'; ?></option>
                            <?php } ?>
                        </select>name="ent_device_type",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="Passenger_login_service" class="div_service">
            <form action="services.php/slaveLogin" method="post">
                <h3>(P) Passenger Login</h3>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Password *: </div>
                    <div class="form_field"><input type="text" name="ent_password" />name="ent_password",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">City Name *: </div>
                    <div class="form_field"><input type="text" name="ent_city" />name="ent_city",type="string",ex: "Bangalore"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Push Token *: </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude: </div>
                    <div class="form_field"><input type="text" name="ent_latitude" />name="ent_latitude",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude: </div>
                    <div class="form_field"><input type="text" name="ent_longitude" />name="ent_longitude",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device type *: </div>
                    <div class="form_field">
                        <select name="ent_device_type">
                            <?php foreach ($typesArr as $dev) { ?>
                                <option value="<?php echo $dev['dev_id']; ?>"><?php echo $dev['name'] . '(' . $dev['dev_id'] . ')'; ?></option>
                            <?php } ?>
                        </select>name="ent_device_type"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


<!--        <div id="get_workplaces_service" class="div_service">
            <form action="services.php/getWorkplaces" method="post">
                <h3>(P) Get workplace types</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>        -->

        <div id="upload_image_service" class="div_service">
            <form action="services.php/uploadImage" method="post">
                <h3>(D,P) Upload Image</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Image name *: </div>
                    <div class="form_field"><input type="text" name="ent_snap_name" />name="ent_snap_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Image Chunk *: </div>
                    <div class="form_field"><input type="text" name="ent_snap_chunk" />name="ent_snap_chunk",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Upload from *: </div>
                    <div class="form_field"><input type="text" name="ent_upld_from" />name="ent_upld_from",type="int",1 - Master, 2- Passenger</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Upload type *: </div>
                    <div class="form_field"><input type="text" name="ent_snap_type" />name="ent_snap_type",type="int",1 - profile pic, 2- others</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Offset *: </div>
                    <div class="form_field"><input type="text" name="ent_offset" />name="ent_offset",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="get_Masters_service" class="div_service">
            <form action="services.php/getMasters" method="post">
                <h3>(P) Get Masters around you</h3>
                <div class="form_row">
                    <div class="form_label">Api Key *: </div>
                    <div class="form_field"><input type="text" name="ent_api_key" />name="ent_api_key",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Search type *: </div>
                    <div class="form_field"><input type="text" name="ent_search_type" />name="ent_search_type",type="int",type of car</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_latitude" />name="ent_latitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_longitude" />name="ent_longitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



<!--        <div id="get_Master_det_service" class="div_service">
            <form action="services.php/getMasterDetails" method="post">
                <h3>(P) Get Master details</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Driver email id *: </div>
                    <div class="form_field"><input type="text" name="ent_dri_email" />name="ent_dri_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>        -->


        <div id="update_location_service" class="div_service">
            <form action="services.php/updateMasterLocation" method="post">
                <h3>(D) Update location</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_latitude" />name="ent_latitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_longitude" />name="ent_longitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


<!--        <div id="get_Master_reviews_service" class="div_service">
            <form action="services.php/getMasterReviews" method="post">
                <h3>(P) Get Master reviews</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Driver email *: </div>
                    <div class="form_field"><input type="text" name="ent_dri_email" />name="ent_dri_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Page number : </div>
                    <div class="form_field"><input type="text" name="ent_page" />name="ent_page",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>-->


        <div id="get_appointments_service" class="div_service">
            <form action="services.php/getMasterAppointments" method="post">
                <h3>(D) Get Appointments</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment date *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="date",format="YYYY-MM-DD" or "YYYY-MM"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

<!--        <div id="update_rating_service" class="div_service">
            <form action="services.php/updateMasterRating" method="post">
                <h3>(D) Give Rating for passenger</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Rating *: </div>
                    <div class="form_field"><input type="text" name="ent_rating" />name="ent_rating",type="int",1 to 5</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment dt *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Slave Email *: </div>
                    <div class="form_field"><input type="text" name="ent_slv_email" />name="ent_slv_email",type="text"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>-->




        <div id="get_pending_appointments_service" class="div_service">
            <form action="services.php/getPendingAppointments" method="post">
                <h3>(D) Get Pending Appointments</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="get_pending_request_service" class="div_service">
            <form action="services.php/getPendingRequests" method="post">
                <h3>(D) Get Pending requests</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


<!--        <div id="get_apnt_hist_service" class="div_service">
            <form action="services.php/getHistoryWith" method="post">
                <h3>(D) Get appointment history with a Passenger</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Passenger Email *: </div>
                    <div class="form_field"><input type="text" name="ent_pas_email" />name="ent_pas_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Page number (default - 1): </div>
                    <div class="form_field"><input type="text" name="ent_page" />name="ent_page",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>-->



        <div id="fare_calculator_service" class="div_service">
            <form action="services.php/fareCalculator" method="post">
                <h3>(P) Fare calculator</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Type Id *: </div>
                    <div class="form_field"><input type="text" name="ent_type_id" />name="ent_type_id",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Current latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_curr_lat" />name="ent_curr_lat",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Current longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_curr_long" />name="ent_curr_long",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Pickup latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_from_lat" />name="ent_from_lat",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Pickup longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_from_long" />name="ent_from_long",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Drop off latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_to_lat" />name="ent_to_lat",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Drop off longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_to_long" />name="ent_to_long",type="float"</div>
                </div>                
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="look_booking_service" class="div_service">
            <form action="services.php/liveBooking" method="post">
                <h3>(P) Live booking:</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" value="3931313332383835303039333831397V0Ks7V0KskOjm9CRWLbuAa8ikOjm9CRWLbuAa8i" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" value="911328850093819" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Workplace type *: </div>
                    <div class="form_field">
                        <select name="ent_wrk_type">
                            <?php foreach ($wpTypes as $type) { ?>                            
                                <option value="<?php echo $type['type_id']; ?>"><?php echo $type['type_name']; ?></option>
                            <?php } ?>
                        </select>                     
                        name="ent_wrk_type",type="int"
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Pickup Address line 1 *: </div>
                    <div class="form_field"><input type="text" name="ent_addr_line1" value="49, Bellary Rd, Lakshmayya Layout, Vishveshvaraiah Nagar, Hebbal"/>name="ent_addr_line1",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Pickup Address line 2 : </div>
                    <div class="form_field"><input type="text" name="ent_addr_line2" value="Ganga Nagar"/>name="ent_addr_line2",type="string"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Pickup Latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_lat" value="13.028834912793"/>name="ent_lat",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Pickup Longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_long" value="77.58957939122"/>name="ent_long",type="float"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Drop Address line 1 : </div>
                    <div class="form_field"><input type="text" name="ent_drop_addr_line1" value="730, 6th B Cross Rd, Koramangala 3 Block, Koramangala" />name="ent_drop_addr_line1",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Drop Address line 2 : </div>
                    <div class="form_field"><input type="text" name="ent_drop_addr_line2" value="Koramangala"/>name="ent_drop_addr_line2",type="string"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Drop Latitude : </div>
                    <div class="form_field"><input type="text" name="ent_drop_lat" value="12.930986"/>name="ent_drop_lat",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Drop Longitude : </div>
                    <div class="form_field"><input type="text" name="ent_drop_long" value="77.622696"/>name="ent_drop_long",type="float"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Payment type * : </div>
                    <div class="form_field"><input type="text" name="ent_payment_type" value=""/>name="ent_payment_type",type="int",1->Card 2-> Cash</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Zipcode *: </div>
                    <div class="form_field"><input type="text" name="ent_zipcode" value="560024" />name="ent_zipcode",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Notes : </div>
                    <div class="form_field"><input type="text" name="ent_extra_notes" value="test test test"/>name="ent_extra_notes",type="string"</div>
                </div>            
                <div class="form_row">
                    <div class="form_label">Card id : </div>
                    <div class="form_field"><input type="text" name="ent_card_id" />name="ent_card_id",type="string"</div>
                </div>   

                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" value="2014-07-15 17:50:00"/>name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Later Date time (for later booking only): </div>
                    <div class="form_field"><input type="text" name="ent_later_dt" value=""/>name="ent_later_dt",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>



                <div class="form_row">
                    <div class="form_label">eid  testing: </div>
                    <div class="form_field"><input type="text" name="ent_dri_email" />name="ent_dri_email",type="string"</div>
                </div>

            </form>
        </div>

        <div id="get_apnt_det_service" class="div_service">
            <form action="services.php/getAppointmentDetails" method="post">
                <h3>(P) Get appointment details</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment datetime *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:mm:SS"</div>
                </div>           
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type",type="int",1->Driver 2->Passenger</div>
                </div>          
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:mm:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="update_review_service" class="div_service">
            <form action="services.php/updateSlaveReview" method="post">
                <h3>(P) Give Appointment review</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Driver email *: </div>
                    <div class="form_field"><input type="text" name="ent_doc_email" />name="ent_dri_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment datetime *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:II:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Rating (1 to 5) *: </div>
                    <div class="form_field"><input type="text" name="ent_rating_num" />name="ent_rating_num",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Review message : </div>
                    <div class="form_field"><input type="text" name="ent_review_msg" />name="ent_review_msg",type="string"</div>
                </div>                   
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="rate_customer_service" class="div_service">
            <form action="services.php/rateCustomer" method="post">
                <h3>(P) Rate Customer</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment month *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="date",format="YYYY-MM"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="get_slave_apnt_service" class="div_service">
            <form action="services.php/getSlaveAppointments" method="post">
                <h3>(P) Get Passenger appointments</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment month *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="date",format="YYYY-MM"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="cancel_appointment_service" class="div_service">
            <form action="services.php/cancelAppointment" method="post">
                <h3>(P) Cancel appointment</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment datetime *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:mm:ss"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Driver email *: </div>
                    <div class="form_field"><input type="text" name="ent_dri_email" />name="ent_dri_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="abort_appointment_service" class="div_service">
            <form action="services.php/abortJourney" method="post">
                <h3>(D) Abort journey</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment datetime *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:mm:ss"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Passenger email *: </div>
                    <div class="form_field"><input type="text" name="ent_pas_email" />name="ent_pas_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Cancel Reason Type *: </div>
                    <div class="form_field"><input type="text" name="ent_cancel_type" />name="ent_cancel_type",type="int",4-> Passenger not came, 5-> Address wrong, 6-> Passenger asked to cancel, 7-> Do not charge ,8-> Others</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="cancel_appointment_request_service" class="div_service">
            <form action="services.php/cancelAppointmentRequest" method="post">
                <h3>(P) Cancel appointment request</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="get_profile_service" class="div_service">
            <form action="services.php/getProfile" method="post">
                <h3>(P) Get profile data</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="update_profile_service" class="div_service">
            <form action="services.php/updateProfile" method="post">
                <h3>(P) Update profile data</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">First name 1*: </div>
                    <div class="form_field"><input type="text" name="ent_first_name" />name="ent_first_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Last name 1*: </div>
                    <div class="form_field"><input type="text" name="ent_last_name" />name="ent_last_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Email 1*: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Phone 1*: </div>
                    <div class="form_field"><input type="text" name="ent_phone" />name="ent_phone",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="get_master_workplace_service" class="div_service">
            <form action="services.php/getMasterCarDetails" method="post">
                <h3>(P) Get master car details</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Master email : </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="add_card_service" class="div_service">
            <form action="services.php/addCard" method="post">
                <h3>(P) Add a credit card</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Access token : </div>
                    <div class="form_field"><input type="text" name="ent_token" />name="ent_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
<!--        <div id="add_paypal_service" class="div_service">
            <form action="services.php/addPaypal" method="post">
                <h3>(P) Add Paypal</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Code : </div>
                    <div class="form_field"><input type="text" name="ent_code" />name="ent_code",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>-->

        <div id="get_cards_service" class="div_service">
            <form action="services.php/getCards" method="post">
                <h3>(P) Get cards</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="update_card_service" class="div_service">
            <form action="services.php/updateCard" method="post">
                <h3>(P) Update a credit card</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Card Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cc_id" />name="ent_cc_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Card exp month *: </div>
                    <div class="form_field"><input type="text" name="ent_exp_month" />name="ent_exp_month",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Card exp year *: </div>
                    <div class="form_field"><input type="text" name="ent_exp_year" />name="ent_exp_year",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="remove_card_service" class="div_service">
            <form action="services.php/removeCard" method="post">
                <h3>(P) Remove a credit card</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Card Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cc_id" />name="ent_cc_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="make_card_def_service" class="div_service">
            <form action="services.php/makeCardDefault" method="post">
                <h3>(P) Make a credit card default</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Card Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cc_id" />name="ent_cc_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="update_tip_service" class="div_service">
            <form action="services.php/updateTip" method="post">
                <h3>(P) Update tip for a appointment </h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Tip percent *: </div>
                    <div class="form_field"><input type="text" name="ent_tip" />name="ent_tip",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment id *: </div>
                    <div class="form_field"><input type="text" name="ent_booking_id" />name="ent_booking_id",type="date time"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        
        <div id="check_push_android" class="div_service">
            <form action="services.php/android_push" method="post">
                <h3>(z) Android push </h3>
                <div class="form_row">
                    <div class="form_label">server Key </div>
                    <div class="form_field"><input type="text" name="ent_severkey" />name="ent_severkey",type="string"</div>
                </div>
                
                <div class="form_row">
                    <div class="form_label">Push token  </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">message </div>
                    <div class="form_field"><input type="text" name="ent_msg" />name="ent_msg",type="string"</div>
                </div>
             
               
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="respond_to_request_service" class="div_service">
            <form action="services.php/respondToAppointment" method="post">
                <h3>(D) Respond to appointment request</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" value="38324142364231462DwDE2rls3Pv0SF7Aw7UVO313030342D344531322D424531382D373336444631414142313346wDE2rls3Pv0SF7Aw7UVO"/>name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" value="82AB6B1F-1004-4E12-BE18-736DF1AAB13F"/>name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Passenger email *: </div>
                    <div class="form_field"><input type="text" name="ent_pas_email" />name="ent_pas_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment datetime *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:mm:ss"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Response *: </div>
                    <div class="form_field"><input type="text" name="ent_response" value="2"/>name="ent_response",type="int",2 -> Accept & 3 -> reject</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking type *: </div>
                    <div class="form_field"><input type="text" name="ent_book_type" value="1"/>name="ent_book_type",type="int",1 -> Now & 2 -> Later</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="update_details_service" class="div_service">
            <form action="services.php/updateApptDetails" method="post">
                <h3>(D) Update appointment details</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" value="38324142364231462DwDE2rls3Pv0SF7Aw7UVO313030342D344531322D424531382D373336444631414142313346wDE2rls3Pv0SF7Aw7UVO"/>name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" value="82AB6B1F-1004-4E12-BE18-736DF1AAB13F"/>name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment id *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_id" />name="ent_appnt_id",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Drop Address line 1 *: </div>
                    <div class="form_field"><input type="text" name="ent_drop_addr_line1" value="730, 6th B Cross Rd, Koramangala 3 Block, Koramangala" />name="ent_drop_addr_line1",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Drop Address line 2 : </div>
                    <div class="form_field"><input type="text" name="ent_drop_addr_line2" value="Koramangala"/>name="ent_drop_addr_line2",type="string"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Drop Latitude : </div>
                    <div class="form_field"><input type="text" name="ent_drop_lat" value="12.930986"/>name="ent_drop_lat",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Drop Longitude : </div>
                    <div class="form_field"><input type="text" name="ent_drop_long" value="77.622696"/>name="ent_drop_long",type="float"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Distance covered in mts *: </div>
                    <div class="form_field"><input type="text" name="ent_distance" value=""/>name="ent_distance",type="int"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="update_status_service" class="div_service">
            <form action="services.php/updateApptStatus" method="post">
                <h3>(D) Update appointment status to Passenger</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" value="38324142364231462DwDE2rls3Pv0SF7Aw7UVO313030342D344531322D424531382D373336444631414142313346wDE2rls3Pv0SF7Aw7UVO"/>name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" value="82AB6B1F-1004-4E12-BE18-736DF1AAB13F"/>name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Passenger email *: </div>
                    <div class="form_field"><input type="text" name="ent_pas_email" />name="ent_pas_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment datetime *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:mm:ss"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Response *: </div>
                    <div class="form_field"><input type="text" name="ent_response" value="5"/>name="ent_response",type="int",6 -> On the way, 7 -> Arrived, 8-> appointment start, 9 -> Appointment completed </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Amount *: </div>
                    <div class="form_field"><input type="text" name="ent_meter" value=""/>name="ent_meter",type="float",Meter fee in app</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Toll fee : </div>
                    <div class="form_field"><input type="text" name="ent_toll" value=""/>name="ent_toll",type="float"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Airport fee : </div>
                    <div class="form_field"><input type="text" name="ent_airport" value=""/>name="ent_airport",type="float"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Parking fee : </div>
                    <div class="form_field"><input type="text" name="ent_parking" value=""/>name="ent_parking",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Rating: </div>
                    <div class="form_field"><input type="text" name="ent_rating" value=""/>name="ent_rating"</div>
                </div>
                <!--                <div class="form_row">
                                    <div class="form_label">Drop Address line 1 : </div>
                                    <div class="form_field"><input type="text" name="ent_drop_addr_line1" value="730, 6th B Cross Rd, Koramangala 3 Block, Koramangala" />name="ent_drop_addr_line1",type="string"</div>
                                </div>
                                <div class="form_row">
                                    <div class="form_label">Drop Address line 2 : </div>
                                    <div class="form_field"><input type="text" name="ent_drop_addr_line2" value="Koramangala"/>name="ent_drop_addr_line2",type="string"</div>
                                </div>  
                                <div class="form_row">
                                    <div class="form_label">Drop Latitude : </div>
                                    <div class="form_field"><input type="text" name="ent_drop_lat" value="12.930986"/>name="ent_drop_lat",type="float"</div>
                                </div>
                                <div class="form_row">
                                    <div class="form_label">Drop Longitude : </div>
                                    <div class="form_field"><input type="text" name="ent_drop_long" value="77.622696"/>name="ent_drop_long",type="float"</div>
                                </div>  
                                <div class="form_row">
                                    <div class="form_label">Distance covered in mts : </div>
                                    <div class="form_field"><input type="text" name="ent_distance" value=""/>name="ent_distance",type="int"</div>
                                </div>  -->
                <div class="form_row">
                    <div class="form_label">Doc Notes : </div>
                    <div class="form_field"><input type="text" name="ent_doc_remarks" value="testing"/>name="ent_doc_remarks",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
<!--        <div id="pay_for_booking_service" class="div_service">
            <form action="services.php/payForBooking" method="post">
                <h3>(P) Pay for booking</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment date time *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Transaction id (For Payment through card)*: </div>
                    <div class="form_field"><input type="text" name="ent_transaction_id" />name="ent_transaction_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Transaction flag (For Payment through card) *: </div>
                    <div class="form_field"><input type="text" name="ent_flag" />name="ent_flag",type="int", 1 - Success, 2 - Failed</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>-->
        <div id="report_dispute_service" class="div_service">
            <form action="services.php/reportDispute" method="post">
                <h3>(P) Report dispute</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment date time *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Report message *: </div>
                    <div class="form_field"><input type="text" name="ent_report_msg" />name="ent_report_msg",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
<!--        <div id="getmy_slots_service" class="div_service">
            <form action="services.php/getMySlots" method="post">
                <h3>(D) Get my slots</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Month *: </div>
                    <div class="form_field"><input type="text" name="ent_slots_for" />name="ent_slots_for",type="date",format="YYYY-MM"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>-->

        <div id="get_master_profile_service" class="div_service">
            <form action="services.php/getMasterProfile" method="post">
                <h3>(D) Get master profile</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="reser_pass_service" class="div_service">
            <form action="services.php/resetPassword" method="post">
                <h3>(D) Reset password</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="reser_pass_service" class="div_service">
            <form action="services.php/resetPassword" method="post">
                <h3>(D) Reset password</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="update_presence_service" class="div_service">
            <form action="services.php/updateMasterStatus" method="post">
                <h3>(D) Update Master as active or inactive</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Master status *: </div>
                    <div class="form_field"><input type="text" name="ent_status" />name="ent_status",type="int",3->active,4->inactive</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="check_email_service" class="div_service">
            <form action="services.php/validateEmailZip" method="post">
                <h3>Validate email and zipcode :</h3>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Zipcode *: </div>
                    <div class="form_field"><input type="text" name="zip_code" />name="zip_code",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type", 1- Master, 2- Passenger</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div id="forgot_pass_service" class="div_service">
            <form action="services.php/forgotPassword" method="post">
                <h3>Forgot password :</h3>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type", 1- Master, 2- Passenger</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div id="get_appt_status_service" class="div_service">
            <form action="services.php/getApptStatus" method="post">
                <h3>Get Appointment status</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment date time : </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type : </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type",type="int",1->Driver,2->Passenger</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="check_sess_service" class="div_service">
            <form action="services.php/checkSession" method="post">
                <h3>Check session expired or not :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type", 1- Driver, 2- Passenger</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <div id="check_coupon_service" class="div_service">
            <form action="services.php/checkCoupon" method="post">
                <h3>Check coupon available or not :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Coupon *: </div>
                    <div class="form_field"><input type="text" name="ent_coupon" />name="ent_coupon"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Lat *: </div>
                    <div class="form_field"><input type="text" name="ent_lat" />name="ent_lat"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Long *: </div>
                    <div class="form_field"><input type="text" name="ent_long" />name="ent_long"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <div id="verify_coupon_service" class="div_service">
            <form action="services.php/verifyCode" method="post">
                <h3>Check coupon available or not :</h3>
                <div class="form_row">
                    <div class="form_label">Coupon *: </div>
                    <div class="form_field"><input type="text" name="ent_coupon" />name="ent_coupon"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Lat *: </div>
                    <div class="form_field"><input type="text" name="ent_lat" />name="ent_lat"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Long *: </div>
                    <div class="form_field"><input type="text" name="ent_long" />name="ent_long"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div id="verify_mobile_service" class="div_service">
            <form action="services.php/checkMobile" method="post">
                <h3>Check mobile available or not :</h3>
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_mobile" />name="ent_mobile"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <div id="get_mobile_service" class="div_service">
            <form action="services.php/getVerificationCode" method="post">
                <h3>Get code :</h3>
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_mobile" />name="ent_mobile"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <div id="verify_code_service" class="div_service">
            <form action="services.php/verifyPhone" method="post">
                <h3>Verify Mobile code :</h3>
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_phone" />name="ent_mobile"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Code *: </div>
                    <div class="form_field"><input type="text" name="ent_code" />name="ent_code"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <div id="check_service_city" class="div_service">
            <form action="services.php/checkCity" method="post">
                <h3>Check city we serving :</h3>
                <div class="form_row">
                    <div class="form_label">City Name *: </div>
                    <div class="form_field"><input type="text" name="ent_city" />name="ent_city",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <div id="update_session_service" class="div_service">
            <form action="services.php/updateSession" method="post">
                <h3>Update session if expires :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type", 1- Master, 2- Passenger</div>
                </div>           
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <div id="logout_service" class="div_service">
            <form action="services.php/logout" method="post">
                <h3>Logout the user :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type", 1- Master, 2- Passenger</div>
                </div>                
                <div class="form_row">
                    <div class="form_label">Latitude (* for driver) : </div>
                    <div class="form_field"><input type="text" name="ent_lat" />name="ent_lat",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude (* for driver) : </div>
                    <div class="form_field"><input type="text" name="ent_long" />name="ent_long",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <!--        <div id="truncate_db_service" class="div_service">
                    <form action="services.php/truncateDB" method="post">
                        <h3>(D,P) Delete all data, and start fresh</h3>
                        <div class="form_row">
                            <div class="form_label">*-marked are mandatory</div>
                            <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                        </div>
                    </form>
                </div>-->




        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>

        <div id="Master_signup_testdata_service" class="div_service">
            <form action="services.php/masterSignupTestData" method="post">
                <h3>(T) Master Signup with test data</h3>
                <div class="form_row">
                    <div class="form_label">First Name *: </div>
                    <div class="form_field"><input type="text" name="ent_first_name" />name= "ent_first_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Last Name *: </div>
                    <div class="form_field"><input type="text" name="ent_last_name" />name="ent_last_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Password *: </div>
                    <div class="form_field"><input type="text" name="ent_password" />name="ent_password",type="string"</div>
                </div>                
                <div class="form_row">
                    <div class="form_label">Profile pic link *: </div>
                    <div class="form_field"><input type="text" name="ent_pic_link" />name="ent_mobile",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_mobile" />name="ent_mobile",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Zipcode *: </div>
                    <div class="form_field"><input type="text" name="ent_zipcode" />name="ent_zipcode",type="string"</div>
                </div>                
                <div class="form_row">
                    <div class="form_label">Latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_latitude" />name="ent_latitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_longitude" />name="ent_longitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Master Type *: </div>
                    <div class="form_field">
                        <select name="ent_service_type">
                            <?php foreach ($wpTypes as $type) { ?>
                                <option value="<?php echo $type['type_id']; ?>"><?php echo $type['type_name']; ?></option>
                            <?php } ?>
                        </select>                     
                        name="ent_service_type",type="int"
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking type *: </div>
                    <div class="form_field"><input type="text" name="ent_booking_type" />name="ent_booking_type",type="int",1->Now,2->later</div>
                </div>
                <!--                <div class="form_row">
                                    <div class="form_label">Device Id *: </div>
                                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                                </div>
                                <div class="form_row">
                                    <div class="form_label">Push Token *: </div>
                                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                                </div>-->
                <div class="form_row">
                    <div class="form_label">Device type *: </div>
                    <div class="form_field">
                        <select name="ent_device_type">
                            <?php foreach ($typesArr as $dev) { ?>
                                <option value="<?php echo $dev['dev_id']; ?>"><?php echo $dev['name'] . '(' . $dev['dev_id'] . ')'; ?></option>
                            <?php } ?>
                        </select>name="ent_device_type",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">About *: </div>
                    <div class="form_field"><input type="text" name="ent_about" />name="ent_about",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Rating *: </div>
                    <div class="form_field"><input type="text" name="ent_rating" />name="ent_rating",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div>
            <form action="services.php/testUpdateLoc" method="post">
                <h3>(T) Update Master latlong</h3>
                <div class="form_row">
                    <div class="form_label">Doc *: </div>
                    <div class="form_field"><input type="text" name='ent_doc' />name="ent_doc",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Lat : </div>
                    <div class="form_field"><input type="text" name='ent_lat' />name="ent_lat",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Long : </div>
                    <div class="form_field"><input type="text" name='ent_long' />name="ent_token",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Now/later : </div>
                    <div class="form_field"><input type="text" name='ent_status' />name="ent_status",3->now, 4-> later</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div>
            <form action="services.php/acceptTestAppt" method="post">
                <h3>(T) Accept appointment</h3>
                <div class="form_row">
                    <div class="form_label">Appt *: </div>
                    <div class="form_field"><input type="text" name='ent_apt' />name="ent_apt",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="push_test_aple_service" class="div_service">
            <form action="services.php/testIosPush" method="post" enctype="multipart/form-data">
                <h3>Test the push notifications for ios:</h3>
                <div class="form_row">
                    <div class="form_label">Ios Certificate *: </div>
                    <div class="form_field"><input type="file" name="ent_ios_cer" />name= "ent_ios_cer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Certificate Pass *: </div>
                    <div class="form_field"><input type="text" name="ent_cer_pass" />name="ent_cer_pass"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Certificate Type *: </div>
                    <div class="form_field">
                        <input type="radio" name="ent_cer_type" value="1" />Production&nbsp;&nbsp;
                        <input type="radio" name="ent_cer_type" value="2" />Distribution&nbsp;&nbsp;name="ent_cer_type"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Push Token *: </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Message *: </div>
                    <div class="form_field"><input type="text" name="ent_message" />name="ent_message"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div>
            <form action="services.php/testUpdateLoc" method="post">
                <h3>(T) Update doctor latlong</h3>
                <div class="form_row">
                    <div class="form_label">Doc *: </div>
                    <div class="form_field"><input type="text" name='ent_doc' />name="ent_doc",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Lat : </div>
                    <div class="form_field"><input type="text" name='ent_lat' />name="ent_lat",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Long : </div>
                    <div class="form_field"><input type="text" name='ent_long' />name="ent_token",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Now/later : </div>
                    <div class="form_field"><input type="text" name='ent_status' />name="ent_status",3->now, 4-> later</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div>
            <form action="services.php/testPushwoosh" method="post">
                <h3>(T) Test pushwoosh push for ios</h3>
                <div class="form_row">
                    <div class="form_label">Token : </div>
                    <div class="form_field"><input type="text" name='ent_token' /></div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="remove_user_service">
            <form action="services.php/removePax" method="post">
                <h3>(T) remove a pax from server</h3>
                <div class="form_row">
                    <div class="form_label">Email : </div>
                    <div class="form_field"><input type="text" name='ent_token' /></div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div id="remove_driver_service">
            <form action="services.php/removeDriver" method="post">
                <h3>(T) remove a driver from server</h3>
                <div class="form_row">
                    <div class="form_label">Email : </div>
                    <div class="form_field"><input type="text" name='ent_token' /></div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div>
            <form action="services.php/logoutAllDrivers" method="post">
                <h3>(T) Offline drivers</h3>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div>
            <form action="services.php/testBooking" method="post">
                <h3>(T) Test booking</h3>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>
        <div>
            <form action="services.php/checkAvlblCarsInCompany" method="post">
                <h3>(T) Check available cars in a company</h3>
                <div class="form_row">
                    <div class="form_label">Company id *: </div>
                    <div class="form_field">
                        <select name="ent_comp_id">
                            <?php foreach ($compList as $type) { ?>
                                <option value="<?php echo $type['company_id']; ?>"><?php echo $type['companyname']; ?></option>
                            <?php } ?>
                        </select>                     
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div>
            <form action="services.php/testPushWoosh" method="post">
                <h3>(T) test push woosh</h3>
                <div class="form_row">
                    <div class="form_label">type *: </div>
                    <div class="form_field">
                        <select name="type">
                            <option value="1">Driver</option>
                            <option value="2">Passenger</option>
                        </select>                     
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label">msg *: </div>
                    <div class="form_field">
                        <input type="text" name="message" />
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label">token *: </div>
                    <div class="form_field">
                        <input type="text" name="token" />
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

    </body>
</html>