<?php
session_start();
//$_SESSION['']
?>

<!--start of map script-->
<style>
    html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
    }
    #map-canvas {
        margin: 0;
        padding: 0;
        height: 600px;
        border: 1px solid #ccc;
        /*display: none;*/

    }
    .nav-tabs-fillup li {
        margin-top: 0px !important;
    }
    .datepicker{z-index:1151 !important;}
    #map_canvas {display:none;}
    /*#map img {*/
        /*max-width: none;*/
    /*}*/
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<!---->
<script>
    // Note: This example requires that you consent to location sharing when
    // prompted by your browser. If you see a blank space instead of the map, this
    // is probably because you have denied permission for location sharing.


//
//        var map;
//
//        function initialize() {
//            var mapOptions = {
//                zoom: 6
//            };
//
//            map = new google.maps.Map(document.getElementById('map-canvas'),
//                mapOptions);
//
//            // Try HTML5 geolocation
//            if (navigator.geolocation) {
//                navigator.geolocation.getCurrentPosition(function (position) {
//                    var pos = new google.maps.LatLng(position.coords.latitude,
//                        position.coords.longitude);
//
//                    var infowindow = new google.maps.InfoWindow({
//                        map: map,
//                        position: pos,
//                        content: 'Location found using HTML5.'
//                    });
//
//                    map.setCenter(pos);
//                }, function () {
//                    handleNoGeolocation(true);
//                });
//            } else {
//                // Browser doesn't support Geolocation
//                handleNoGeolocation(false);
//            }
//        }
//
//        function handleNoGeolocation(errorFlag) {
//            if (errorFlag) {
//                var content = 'Error: The Geolocation service failed.';
//            } else {
//                var content = 'Error: Your browser doesn\'t support geolocation.';
//            }
//
//            var options = {
//                map: map,
//                position: new google.maps.LatLng(60, 105),
//                content: content
//            };
//
//            var infowindow = new google.maps.InfoWindow(options);
//            map.setCenter(options.position);
//
//        }
//
//        $('#map-canvas').on('shown', function (e) {
//
//            google.maps.event.trigger(map, 'resize');
//
//        });
////    google.maps.event.trigger(map, 'resize');
//        google.maps.event.addDomListener(window, 'load', initialize);

//    google.maps.event.addDomListener(window, 'load', initialize);
    function displayMap() {

        var lat = $('#lat').val();
        var long = $('#long').val();
        if(lat||long)
         initialize(lat,long);
        else
            initialize('13.0288555','77.58961360000001');
    }

    function initialize(lat,long) {
        var myLatlng = new google.maps.LatLng(lat,long);
        var mapOptions = {
            zoom:14,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Hello World!'
        });

    }




</script>


<!--/** end of map script-->



<script type="text/javascript">
    $(document).ready(function(){

        $("#resize").click(function() {
//            alert('testing');
        google.maps.event.trigger(map, 'resize');
        });

        $('#userstatus').hide();

         $('.nav-tabs-fillup').click(function(){


             if($('#apbkg').hasClass('active')){
                 $('#circle').hide();

             }
             else{
                 $('#circle').show();

             }

//                 $('#circle').show();


         });




            var table = $('#tableWithSearchDriver');

            var settings = {
                "sDom": "<'table-responsive't><'row'<p i>>",
                "sPaginationType": "bootstrap",
                "destroy": true,
                "scrollCollapse": true,
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
                },
                "iDisplayLength": 11
            };

            table.dataTable(settings);


            $('#search-tableDriver').keyup(function() {
                table.fnFilter($(this).val());
            });



//        var jobstable= $('#tableWithSearchforjobs');
//        jobstable.dataTable(settings);

//        var reviewtable = $('#tableWithSearchforreview');
//
//        reviewtable.dataTable(settings);

        $('#search-tableclient').keyup(function() {
            tableclient.fnFilter($(this).val());
        });

        var tableclient = $('#tableWithSearchClient');
        tableclient.dataTable(settings);

        var tablewithdriverlist = $('#tableWithSearchDriverList');

        tablewithdriverlist.dataTable(settings);

        $('.tableWithSearchDriverListsearch').keyup(function() {
            tablewithdriverlist.fnFilter($(this).val());
        });





    });

    if(typeof(EventSource) !== "undefined") {

        var source = new EventSource("<?php echo base_url()?>serverdata/demo_sse.php");
        source.onmessage = function(event) {



                var Returneddata = $.parseJSON(event.data);

                document.getElementById("circle").innerHTML = '<div id="newbkng" style="padding: 3px 9px;color: white;">' + Returneddata.booking + '</div>';


        };
    } else {
        document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
    }




    function get_driver_data(userid){
//        var source1;
        $.ajax({
            type: "POST",
            url: 'get_driver_Data',
            data: { did: userid },
            dataType: "json",
            success: function(data) {
//                $('#query').html(data.test);
                $('#userjob').html(data.test);
                if(data.rest == 2){
                    $('#nodata').hide();
                    $('#tab1data').show();
                    $('#userjob').html(data.test);
                }

                else if(data.rest == 1)
                {
                    $('#tab1data').hide();
                    $('#nodata').show();
                    $('#nodata').html(data.test);

                }

                $('#tab2profile').html(data.profile);
                $('#userreview').html(data.review);
                $('#userstatus').show();
                $('#lat').val(data.lat);
                $('#long').val(data.long);
                displayMap();


                var settings = {
                    "sDom": "<'table-responsive't><'row'<p i>>",
                    "sPaginationType": "bootstrap",
                    "destroy": true,
                    "scrollCollapse": true,
                    "oLanguage": {
                        "sLengthMenu": "_MENU_ ",
                        "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
                    },
                    "iDisplayLength": 11
                };

                var reviewtable = $('#tableWithSearchforreview');

                reviewtable.dataTable(settings);
                var jobstable= $('#tableWithSearchforjobs');
                jobstable.dataTable(settings);



                source.close();
                var source1 = new EventSource("<?php echo base_url()?>serverdata/userstatus.php?uid="+userid);
                source1.onmessage = function(event) {



                    var Returneddata = $.parseJSON(event.data);

                     if(Returneddata.status == 1)
                     document.getElementById("userstatus").innerHTML = '<span style="color: #008000;">Online</div>';
                    else if(Returneddata.status == 2)
                         document.getElementById("userstatus").innerHTML = '<span style="color:red;">Offline</div>';



                };

//                source.close();
            }


         });



       }

    function call_modal(){
        $('#myModal').modal('show');
    }

</script>
<style>
    .panel-controls{
        display: none;
    }

    .mapplic-map{
        position: relative !important;
    }
    #circle {
        width: 28px;
        height: 28px;
        background: #9BCA3E;
        -moz-border-radius: 50px;
        -webkit-border-radius: 50px;
        border-radius: 50px;
        position: absolute;
        margin-left: 15%;
        z-index: 100;
display: none;
    }
    .nav-tabs-fillup li{
        margin-top: 17px;
    }

</style>


<div id="query"></div>


<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content" style="padding-top: 2px !important;">
        <!-- START JUMBOTRON -->
        <div class="jumbotron bg-white" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">



                <div class="row">

                    <div class=" pull-left sm-table" style="margin-top: 11px;width: 11%;">
                        <div class="header-inner">
                            <div class="brand inline">
                                <img src="<?php echo base_url(); ?>theme/assets/img/logo.png" alt="logo" class="brand" data-src="<?php echo base_url(); ?>theme/assets/img/logo.png" data-src-retina="<?php echo base_url(); ?>theme/assets/img/logo.png" style="width: 46px;height: 30px">             <img src="<?php echo base_url(); ?>theme/assets/img/Rlogo.png" alt="logo" data-src="<?php echo base_url(); ?>theme/assets/img/Rlogo.png" data-src-retina="<?php echo base_url(); ?>theme/assets/img/logo_2x.png" style="width: 58px;height: 30px">


                            </div>




                        </div>
                    </div>
                    <div class=" pull-right">
                        <!-- START User Info-->
                        <div class="visible-lg visible-md m-t-10" id="caldw">
                            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
                                <span class="semi-bold"><?php echo $this->session->userdata("first_name"); ?></span>
                                <span class="text-master"><?php echo $this->session->userdata("last_name"); ?></span>
                            </div>

                            <div class="btn-group">
                                <img data-toggle="dropdown" style="border-radius: 28px;margin-top: 4px;margin-right: 7px;cursor: pointer;" data-hover="dropdown" src="http://107.170.66.211/roadyo_live/pics/hdpi/<?php echo $this->session->userdata("profile_pic"); ?>" alt="" data-src="http://107.170.66.211/roadyo_live/pics/hdpi/<?php echo $this->session->userdata("profile_pic"); ?>" data-src-retina="http://107.170.66.211/roadyo_live/pics/hdpi/<?php echo $this->session->userdata("profile_pic"); ?>" width="32" height="32">
                                <ul class="dropdown-menu" style="margin-left: -135px;margin-top: 14px;background: #ffffff;width: 171px;">
                                    <li>
                                        <div class="row center-margin m-b-10">
                                            <div class="col-xs-2 text-center">
                                                <i class="fs-14 sl-user-follow"></i>
                                            </div>
                                            <div class="col-xs-8 text-center">
                                                <a tabindex="-1" href="<?php echo base_url(); ?>index.php/admin/loadDashbord">My Profile</a>
                                            </div>
                                        </div>

                                    </li>
                                    <li class="divider"></li>

                                    <li>

                                        <center><a tabindex="-1" href="<?php echo base_url(); ?>index.php/admin/Logout">Logout</a></center>
                                    </li>

                                </ul>
                            </div>

                        </div>
                        <!-- END User Info-->
                    </div>


                    <div class="panel panel-transparent ">
                        <!-- Nav tabs -->
<!--                        <div id="circle"></div>-->
                        <ul class="nav nav-tabs nav-tabs-fillup">
                            <li class="active" id="apbkg">

                                <a data-toggle="tab" href="#slide1" ><span>APP BOOKINGS</span></a>


                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#slide2"><span>PHONE BOOKINGS</span></a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#slide3"><span>DRIVERS</span></a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#slide4" style="min-width: 172px !important;"><span>BOOKING HISTORY</span></a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#slide5"><span>CUSTOMERS</span></a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">

<!--                            // app bookings-->
                            <div class="tab-pane slide-left active" id="slide1">
                                <div class="row column-seperation">
                                    <div class="col-md-12">



                                        <div class="container-fluid container-fixed-lg bg-white">
                                            <!-- START PANEL -->
                                            <div class="panel panel-transparent">
                                                <div class="panel-heading">
                                                    <div class="panel-title">TODAY'S BOOKINGS
                                                    </div>
                                                    <div class="pull-right">
                                                        <div class="col-xs-12">
                                                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive">
                                                            <table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                                                <thead>
                                                                <tr role="row">
                                                                    <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;">SLNO</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Places: activate to sort column ascending" style="width: 275px;">BOOKING ID</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 304px;">PASSENGER NAME</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 175px;">PHONE</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">PICKUP D & T</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">PICKUP ADDRESS</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">DROP ADDRESS</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">VEHICLE TYPE</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">STATUS</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>






<?php
$slno = 1;

foreach ($aap_booking as $result) {
?>



    <tr role="row"  class="gradeA odd">
        <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $slno; ?></p></td>
        <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result->appointment_id; ?></p></td>
        <td class="v-align-middle"><?php echo $result->pessanger_fname.$result->pessanger_lname; ?></td>
        <td class="v-align-middle"><?php echo  $result->phone; ?></td>
        <td class="v-align-middle"><?php echo date("M d Y g:i A", strtotime($result->appointment_dt)); ?></td>
        <td class="v-align-middle"><?php echo  $result->address_line1.$result->address_line2; ?></td>
        <td class="v-align-middle"><?php echo  $result->drop_addr1.$result->drop_addr2; ?></td>
        <td class="v-align-middle">BMW</td>
            <?php
        if ($result->status == '1')
            $status = 'Appointment requested';
        else if ($result->status == '2')
            $status = $result->driver_fname.' accepted.';
        else if ($result->status == '3')
            $status = $result->driver_fname.' rejected.';
        else if ($result->status == '4')
            $status = 'Passenger has cancelled.';
        else if ($result->status == '5')
            $status = $result->driver_fname.' '.$result->driver_lname.' has canceled';
        else if ($result->status == '6')
            $status = $result->driver_fname.'on the way.';
        else if ($result->status == '7')
            $status = 'Appointment started.';
        else if ($result->status == '8')
            $status = $result->driver_fname.' Arrived';
        else if ($result->status == '9')
            $status = 'Appointment completed.';
        else if ($result->status == '10')
            $status = 'Appointment Timed out.';
        else
            $status = 'Status unavailable.';
        ?>
        <td class="v-align-middle"><?php echo $status; ?></td>

    </tr>

    <?php
    $slno++;
}
//                                            ?>

                                                                </tbody>
                                                            </table>

                                                        </div><div class="row"><div></div></div></div>
                                                </div>
                                            </div>
                                            <!-- END PANEL -->
                                        </div>






                                    </div>

                                </div>
                            </div>


                            <!-- end of app bookings -->


<!--                            startung of map-->
                            <div class="tab-pane slide-left" id="slide2">
                                <div class="row">
                                    <div class="col-md-12">



                                        <div class="map-container relative">
                                            <div class="map-controls" style="left: 0px;top: 0px;">
                                                <div class="pull-right">
                                                    <div class="btn-group btn-group-vertical" data-toggle="buttons-radio">

                                                        <div id="quickview" class="quickview-wrapper open" data-pages="quickview" style="max-height: 487px;">
                                                            <!-- Nav tabs -->
                                                            <ul class="nav nav-tabs" style="padding: 0 14px;">
                                                                <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                                                                 <span class="col-xs-height col-middle">
                                                                <span class="thumbnail-wrapper d32 circular bg-success">
                                                                    <img width="34" height="34" alt="" data-src-retina="<?php echo base_url() ?>theme/assets/img/profiles/1x.jpg" data-src="<?php echo base_url() ?>theme/assets/img/profiles/1.jpg" src="<?php echo base_url() ?>theme/assets/img/profiles/1.jpg" class="col-top">
                                                                </span>
                                                                </span>
                                                                                                              <p class="p-l-20 col-xs-height col-middle col-xs-12">
                                                                        <span class="text-master" style="color: #ffffff !important;">ava flores</span>
                                                                        <span class="block text-master hint-text fs-12" style="color: #ffffff !important;">+91 8865256326</span>
                                                                    </p>
                                                                </a>


                                                            </ul>
                                                            <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close" style="color: #ffffff ! important;"></i></a>
                                                            <!-- Tab panes -->
                                                            <div class="tab-content" style="top: 21px !important;">


                                                                <div class="list-view-group-container" >

                                                                    <ul>
                                                                        <!-- BEGIN Chat User List Item  !-->
                                                                        <li class="chat-user-list clearfix">

                                                                            <div class="">

                                                                                <input type="text" class="form-control" id="name" placeholder="Car Type" required="required">
                                                                                </div>


                                                                        </li>
                                                                        <li class="chat-user-list clearfix">
                                                                            <div class="">
                                                                                <div class="col-sm-6" style="padding-left: 0px;">
                                                                                    <input type="text" class="form-control" id="name" placeholder="Model" required="required">
                                                                                </div>
                                                                                <div class="col-sm-6" style="padding-right: 0px;">
                                                                                    <input type="text" class="form-control" id="name" placeholder="Color" required="required">
                                                                                </div>
                                                                            </div>

                                                                        </li>
                                                                        <li class="chat-user-list clearfix">

                                                                            <div class="">

                                                                                <input type="text" class="form-control" id="name" placeholder="License no" required="required">
                                                                            </div>

                                                                        </li>


                                                                    </ul>


                                                                    <div class="list-view-group-container" >
                                                                        <div class="list-view-group-header text-uppercase" style="background-color: #f0f0f0;padding: 10px;">
                                                                           ASSIGNED JOBS</div>
                                                                        <div style="height: 297px;overflow: auto;background: #fff;">
                                                                        <ul style="margin-top: 15px;">
                                                                            <!-- BEGIN Chat User List Item  !-->
                                                                            <li class="chat-user-list clearfix">


                                                                                <div class="item share share-self col1" data-social="item" style="border: 2px solid #e5e8e9;">
                                                                                    <div class="pull-right" style="margin: 4px 10px 0px 2px;">
                                                                                        10:00 am

                                                                                    </div>
                                                                                    <div class="item-header clearfix" style="margin: 5px 8px 11px 12px;">

                                                                                        JOB ID

                                                                                    </div>
                                                                                    <div class="item-description" style="">

                                                                                        <ul>
                                                                                            <!-- BEGIN Chat User List Item  !-->
                                                                                            <li class="chat-user-list clearfix">


                                                                                                <div class="">

                                                                                                    <input type="text" class="form-control" id="name" placeholder="Pickup" required="required">
                                                                                                </div>


                                                                                            </li>
                                                                                            <li class="chat-user-list clearfix">


                                                                                                <div class="">

                                                                                                    <input type="text" class="form-control" id="name" placeholder="Drop" required="required">
                                                                                                </div>

                                                                                            </li>
                                                                                            <!-- END Chat User List Item  !-->
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>



                                                                            </li>

                                                                            <!-- END Chat User List Item  !-->
                                                                        </ul>

                                                                        <ul style="margin-top: 15px;">
                                                                            <!-- BEGIN Chat User List Item  !-->
                                                                            <li class="chat-user-list clearfix">


                                                                                <div class="item share share-self col1" data-social="item" style="border: 2px solid #e5e8e9;">
                                                                                    <div class="pull-right" style="margin: 4px 10px 0px 2px;">
                                                                                        10:00 am

                                                                                    </div>
                                                                                    <div class="item-header clearfix" style="margin: 5px 8px 11px 12px;">

                                                                                        JOB ID

                                                                                    </div>
                                                                                    <div class="item-description" style="">

                                                                                        <ul>
                                                                                            <!-- BEGIN Chat User List Item  !-->
                                                                                            <li class="chat-user-list clearfix">


                                                                                                <div class="">

                                                                                                    <input type="text" class="form-control" id="name" placeholder="Pickup" required="required">
                                                                                                </div>


                                                                                            </li>
                                                                                            <li class="chat-user-list clearfix">


                                                                                                <div class="">

                                                                                                    <input type="text" class="form-control" id="name" placeholder="Drop" required="required">
                                                                                                </div>

                                                                                            </li>
                                                                                            <!-- END Chat User List Item  !-->
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>



                                                                            </li>

                                                                            <!-- END Chat User List Item  !-->
                                                                        </ul>
                                                                        <ul style="margin-top: 15px;">
                                                                            <!-- BEGIN Chat User List Item  !-->
                                                                            <li class="chat-user-list clearfix">


                                                                                <div class="item share share-self col1" data-social="item" style="border: 2px solid #e5e8e9;">
                                                                                    <div class="pull-right" style="margin: 4px 10px 0px 2px;">
                                                                                        10:00 am

                                                                                    </div>
                                                                                    <div class="item-header clearfix" style="margin: 5px 8px 11px 12px;">

                                                                                        JOB ID

                                                                                    </div>
                                                                                    <div class="item-description" style="">

                                                                                        <ul>
                                                                                            <!-- BEGIN Chat User List Item  !-->
                                                                                            <li class="chat-user-list clearfix">


                                                                                                <div class="">

                                                                                                    <input type="text" class="form-control" id="name" placeholder="Pickup" required="required">
                                                                                                </div>


                                                                                            </li>
                                                                                            <li class="chat-user-list clearfix">


                                                                                                <div class="">

                                                                                                    <input type="text" class="form-control" id="name" placeholder="Drop" required="required">
                                                                                                </div>

                                                                                            </li>
                                                                                            <!-- END Chat User List Item  !-->
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>



                                                                            </li>

                                                                            <!-- END Chat User List Item  !-->
                                                                        </ul>
                                                                        <ul style="margin-top: 15px;">
                                                                            <!-- BEGIN Chat User List Item  !-->
                                                                            <li class="chat-user-list clearfix">


                                                                                <div class="item share share-self col1" data-social="item" style="border: 2px solid #e5e8e9;">
                                                                                    <div class="pull-right" style="margin: 4px 10px 0px 2px;">
                                                                                        11:50 am

                                                                                    </div>
                                                                                    <div class="item-header clearfix" style="margin: 5px 8px 11px 12px;">

                                                                                        JOB ID

                                                                                    </div>
                                                                                    <div class="item-description" style="">

                                                                                        <ul>
                                                                                            <!-- BEGIN Chat User List Item  !-->
                                                                                            <li class="chat-user-list clearfix">


                                                                                                <div class="">

                                                                                                    <input type="text" class="form-control" id="name" placeholder="Pickup" required="required">
                                                                                                </div>


                                                                                            </li>
                                                                                            <li class="chat-user-list clearfix">


                                                                                                <div class="">

                                                                                                    <input type="text" class="form-control" id="name" placeholder="Drop" required="required">
                                                                                                </div>

                                                                                            </li>
                                                                                            <!-- END Chat User List Item  !-->
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>



                                                                            </li>

                                                                            <!-- END Chat User List Item  !-->
                                                                        </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>


                                                </div>




                                                <div class="col-md-10">
                                                <div class="pull-left">
<!--                                                    <form role="form">-->
<!--                                                        <div class="form-group form-group-default input-group">-->
<!--                                                            <label>Country</label>-->
<!--                                                            <div class="controls">-->
<!--                                                                <input type="hidden" id="country-list" data-placeholder="Search by locationg, tag, ID">-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </form>-->


                                                        <!-- START PANEL -->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                  New
                                                                </div>
                                                            </div>
                                                            <div class="panel-body">

                                                                <form class="" role="form">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <input type="text" class="form-control" id="name" placeholder="First name" required="required">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" class="form-control" id="name" placeholder="Last name" required="required">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <input type="text" class="form-control" id="name" placeholder="Email" required="required">
                                                                    </div>
                                                                    <div class="row">
                                                                        <input type="text" class="form-control" id="name" placeholder="phone" required="required">
                                                                    </div>
                                                                    <div class="row">
                                                                        <input type="text" class="form-control" id="name" placeholder="Pickup" required="required">
                                                                    </div>
                                                                    <div class="row">
                                                                        <input type="text" class="form-control" id="name" placeholder="Drop" required="required">
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group ">
                                                                                <label>Date</label>
                                                                                <input type="date" class="form-control" id="name" placeholder="Date" required="required">
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-sm-6">

                                                                            <div class="form-group ">
                                                                                <label>Time</label>
                                                                                <input type="time" class="form-control" id="name" placeholder="Time" required="required">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="row">
                                                                        <input type="text" class="form-control" id="name" placeholder="Care type" required="required">
                                                                    </div>
                                                                     <div class="row">
                                                                        <input type="text" class="form-control" id="name" placeholder="Care Model" required="required">
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-sm-6">

                                                                                <input type="text" class="form-control" id="name" placeholder="Distance" required="required">


                                                                        </div>
                                                                        <div class="col-sm-6">

                                                                                <input type="text" class="form-control" id="name" placeholder="Approx Fare" required="required">

                                                                        </div>
                                                                    </div>


                                                                    <div class="row" style="margin-top: 10px;">
                                                                        <div class="col-sm-4">
                                                                            <button class="btn btn-complete btn-cons" style="min-width: 20px ">Clear</button>
                                                                        </div>

                                                                        <div class="col-sm-4">
                                                                            <button class="btn btn-primary btn-cons" style="min-width: 20px ">Locate</button>
                                                                        </div>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!-- END PANEL -->

                                                </div>




                                                </div>

                                            </div>

<!--                                            <div id="mapplic">-->
<!---->
<!--                                            </div>-->



                                        </div>


























                                    </div>
                                </div>
                                <div class="row">
<!--                                    <div id="map-canvas"></div>-->
                                </div>
                            </div>

<!--                             end of map-->



















<!--

             Driver data


-->




                            <div class="tab-pane slide-left " id="slide3">
                                <div class="row column-seperation">
                                    <div class="col-md-12">



                                            <div class="col-md-2" style="border: 1px solid rgba(0, 0, 0, 0.07);">

                                            <div class="tab-pane fade in active no-padding" id="quickview-chat">
                                            <div class="view-port clearfix" id="chat">
                                            <div class="view bg-white">
                                            <!-- BEGIN View Header !-->
                                            <div class="navbar navbar-default">
                                                <div class="navbar-inner">
                                                    <!-- BEGIN Header Controler !-->
                                                    <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">

                                                    </a>
                                                    <!-- END Header Controler !-->
                                                    <div class="view-heading">
                                                        Drivers
                                                        <div class="input-group transparent col-md-11 center-margin" style="padding: 7px 0px 14px;margin-left: 0px;">
                                                            <input type="text" class="form-control tableWithSearchDriverListsearch" placeholder="Search Driver" id="icon-filter " name="icon-filter">
                                                      <span class="input-group-addon ">
                                                                    <i class="pg-search"></i>
                                                                </span>

                                                         </div>
                                                    </div>
                                                    <!-- BEGIN Header Controler !-->

                                                    <!-- END Header Controler !-->
                                                </div>
                                            </div>
                                            <!-- END View Header !-->
                                            <table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearchDriverList" role="grid" aria-describedby="tableWithSearch_info">
                                                <thead>
                                                <tr role="row">
                                                    <th  class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;display: none">name</th>
                                                </tr>
                                                </thead>
                                                <tbody>






                                                <?php
                                                $slno = 1;

                                                foreach ($drivers as $result) {
                                                    ?>



                                                    <tr role="row"  class="gradeA odd">
                                                        <td id = "d_no" class="v-align-middle sorting_1" style="padding: 8px !important;"> <p style="float: left;">


                                                                <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#" onclick="get_driver_data(<?php echo $result->mas_id; ?>)" >
                                                                                            <span class="col-xs-height col-middle">
                                                                                            <span class="thumbnail-wrapper d32 circular bg-success" >
                                                                                                <img width="34" height="34" alt="" data-src-retina="http://107.170.66.211/roadyo_live/pics/<?php echo $result->profile_pic?>" data-src="http://107.170.66.211/roadyo_live/pics/<?php echo $result->profile_pic?>" src="http://107.170.66.211/roadyo_live/pics/<?php echo $result->profile_pic?>" class="col-top">
                                                                                            </span>
                                                                                            </span>
                                                                                                                                <p class="p-l-10 col-xs-height col-middle" style="">
                                                                                                                                    <span class="text-master"><?php echo $result->first_name.$result->last_name;;?></span>
                                                                                                                                    <span class="block text-master hint-text fs-12"><?php echo $result->mobile;?></span>
                                                                                                                                </p>
                                                                                                                            </a>



                                                            </p>
                                                        </td>


                                                    </tr>

                                                    <?php
                                                    $slno++;
                                                }
                                                //                                            ?>

                                                </tbody>
                                            </table>



                                            </div>

                                            </div>
                                            </div>











                                            </div>
                                         <div class="col-md-10" style="border: 1px solid rgba(0, 0, 0, 0.07);">

                                             <div class="panel">
                                                 <ul class="nav nav-tabs nav-tabs-simple" role="tablist" id="tablist">
                                                     <li class="active"><a href="#tab2hellowWorld" data-toggle="tab" role="tab">Jobs</a>
                                                     </li>
                                                     <li class=""><a onclick="displayMap()" href="#tab2FollowUs" data-toggle="tab" role="tab" >Locate</a>
                                                     </li>
                                                     <li class=""><a href="#tab2profile" data-toggle="tab" role="tab">Profile</a>
                                                     </li>
                                                     <li class=""><a href="#tab2Inspire" data-toggle="tab" role="tab">Reviews</a>
                                                     </li>
                                                     <div class="pull-right" style="margin-top: 13px;color: red" id="userstatus">Offline</div>
                                                 </ul>

                                                 <div class="tab-content">
                                                     <div class="tab-pane active" id="tab2hellowWorld">
                                                         <div class="row column-seperation">
                                                             <div id="nodata"></div>
                                                             <div class="col-md-12" id="tab1data">


                                                                 <table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearchforjobs" role="grid" aria-describedby="tableWithSearch_info">
                                                                     <thead>
                                                                     <tr role="row">
                                                                         <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;">SLNO</th>
                                                                         <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Places: activate to sort column ascending" style="width: 275px;">BOOKING ID</th>
                                                                         <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 304px;">PASSENGER NAME</th>
                                                                         <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 175px;">PHONE</th>
                                                                         <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">PICKUP D & T</th>
                                                                         <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">PICKUP ADDRESS</th>
                                                                         <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">DROP ADDRESS</th>
                                                                         <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">VEHICLE TYPE</th>
                                                                         <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">STATUS</th>
                                                                     </tr>
                                                                     </thead>
                                                                     <tbody id="userjob">






                                                                     <?php
                                                                     $slno = 1;

                                                                     foreach ($aap_booking as $result) {
                                                                         ?>



                                                                         <tr role="row"  class="gradeA odd">
                                                                             <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $slno; ?></p></td>
                                                                             <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result->appointment_id; ?></p></td>
                                                                             <td class="v-align-middle"><?php echo $result->pessanger_fname.$result->pessanger_lname; ?></td>
                                                                             <td class="v-align-middle"><?php echo  $result->phone; ?></td>
                                                                             <td class="v-align-middle"><?php echo date("M d Y g:i A", strtotime($result->appointment_dt)); ?></td>
                                                                             <td class="v-align-middle"><?php echo  $result->address_line1.$result->address_line2; ?></td>
                                                                             <td class="v-align-middle"><?php echo  $result->drop_addr1.$result->drop_addr2; ?></td>
                                                                             <td class="v-align-middle">BMW</td>
                                                                             <?php
                                                                             if ($result->status == '1')
                                                                                 $status = 'Appointment requested';
                                                                             else if ($result->status == '2')
                                                                                 $status = $result->driver_fname.' accepted.';
                                                                             else if ($result->status == '3')
                                                                                 $status = $result->driver_fname.' rejected.';
                                                                             else if ($result->status == '4')
                                                                                 $status = 'Passenger has cancelled.';
                                                                             else if ($result->status == '5')
                                                                                 $status = $result->driver_fname.' '.$result->driver_lname.' has canceled';
                                                                             else if ($result->status == '6')
                                                                                 $status = $result->driver_fname.'on the way.';
                                                                             else if ($result->status == '7')
                                                                                 $status = 'Appointment started.';
                                                                             else if ($result->status == '8')
                                                                                 $status = $result->driver_fname.' Arrived';
                                                                             else if ($result->status == '9')
                                                                                 $status = 'Appointment completed.';
                                                                             else if ($result->status == '10')
                                                                                 $status = 'Appointment Timed out.';
                                                                             else
                                                                                 $status = 'Status unavailable.';
                                                                             ?>
                                                                             <td class="v-align-middle"><?php echo $status; ?></td>

                                                                         </tr>

                                                                         <?php
                                                                         $slno++;
                                                                     }
                                                                     //                                            ?>

                                                                     </tbody>
                                                                 </table>


                                                                                                                                  </div>

                                                                                                                              </div>
                                                                                                                          </div>





<!--                                                     // locate  driver-->


                                                                                                                          <div class="tab-pane" id="tab2FollowUs">
                                                                                                                              <div class="row">
                                                                                                                                  <div class="col-md-12">
                                                                                                                                      <input type="hidden" id="lat">
                                                                                                                                      <input type="hidden" id="long">
                                                                                                                                      <div style="height: 65px;">
                                                                                                                                      <p class="pull-right">
                                                                                                                                          <button type="button" class="btn btn-success btn-cons" onclick="call_modal()">Add Order</button>

                                                                                                                                      </p>

                                                                                                                                      </div>
                                                                                                                                                <div id="mapouter">
                                                                                                                                                    <div id="map-canvas"></div>

                                                                                                                                                </div>


                                                                                                                                  </div>
                                                                                                                              </div>
                                                                                                                          </div>





<!--                                                     //end of locate driver-->


                                                                                                                          <div class="tab-pane" id="tab2profile">
                                                                                                                              <div class="row">
                                                                                                                                  <div class="col-md-12">
                                                                                                                                      <div class="panel-body">
                                                                                                                                          <div class="row">
                                                                                                                                              <div class="col-sm-12">
                                                                                                                                                  <form id="form-work" class="form-horizontal" method="post" role="form" autocomplete="off" novalidate="novalidate" action="udpadedataProfile">
                                                                                                                                                      <input type="hidden" id="profile_pic_to_save" name="fdata[profile_pic]" value="https://s3.amazonaws.com/tutree/profile/master/file201531754710.png">
                                                                                                                                                      <div class="col-sm-4">
                                                                                                                                                          <center>
                                                                                                                                                              <div class="col_2">
                                                                                                                                                                  <div class="img" id="prof_img">
                                                                                                                                                                      <img style="-webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);width: 126px;height: 127px;" class="img-circle" id="uimg" src="https://s3.amazonaws.com/tutree/profile/master/file201531754710.png" enctype="multipart/form-data">
                                                                                                                                                                  </div>
                                                                                                                                                                  <div class="img" id="prof_img" style="margin-top: 28px;">
                                                                                                                                                                      </button>
                                                                                                                                                                  </div>
                                                                                                                                                              </div>
                                                                                                                                                          </center>
                                                                                                                                                      </div>
                                                                                                                                                      <div class="col-sm-8">
                                                                                                                                                          <div class="form-group">
                                                                                                                                                              <label for="fname" class="col-sm-3 control-label">First Name</label>
                                                                                                                                                              <div class="col-sm-9">
                                                                                                                                                                  <input type="text" class="form-control" id="fname" placeholder="Full name" value="Carl" name="fdata[first_name]" required="" aria-required="true">
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                          <div class="form-group">
                                                                                                                                                              <label for="fname" class="col-sm-3 control-label">Last Name</label>
                                                                                                                                                              <div class="col-sm-9">
                                                                                                                                                                  <input type="text" class="form-control" id="fname" placeholder="Last name" value="Jones" name="fdata[last_name]" required="" aria-required="true">
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                      </div>
                                                                                                                                                      <div class="form-group">
                                                                                                                                                      </div>
                                                                                                                                                      <div class="col-sm-12">
                                                                                                                                                          <div class="col-sm-4">
                                                                                                                                                              <div class="form-group">
                                                                                                                                                                  <label for="position" class="col-sm-3 control-label">Mobile</label>
                                                                                                                                                                  <div class="col-sm-9">
                                                                                                                                                                      <input type="text" class="form-control " id="position" value="8880256479" placeholder="Mobile" name="fdata[mobile]" aria-required="true" aria-invalid="true">
                                                                                                                                                                      <!--<label id="position-error" class="error" for="position">This field is required.</label>-->
                                                                                                                                                                  </div>
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                          <div class="col-sm-4">
                                                                                                                                                              <div class="form-group">
                                                                                                                                                                  <label for="name" class="col-sm-3 control-label">Email</label>
                                                                                                                                                                  <div class="col-sm-9">
                                                                                                                                                                      <input type="text" class="form-control" id="position" value="subir@mobifyi.com" placeholder="Email" name="fdata[email]" aria-required="true" aria-invalid="true" disabled="">
                                                                                                                                                                  </div>
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                          <div class="col-sm-4">
                                                                                                                                                              <div class="form-group">
                                                                                                                                                                  <label for="name" class="col-sm-3 control-label">Zip </label>
                                                                                                                                                                  <div class="col-sm-9">
                                                                                                                                                                      <input type="text" class="form-control " id="position" value="560097" placeholder="Zipcode" name="fdata[zipcode]" aria-required="true" aria-invalid="true">
                                                                                                                                                                  </div>
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                      </div>
                                                                                                                                                      <div class="form-group">
                                                                                                                                                      </div>
                                                                                                                                                      <div class="col-sm-12">
                                                                                                                                                          <div class="col-sm-4">
                                                                                                                                                              <div class="form-group">
                                                                                                                                                                  <label for="name" class="col-sm-3 control-label">About</label>
                                                                                                                                                                  <div class="col-sm-9">
                                                                                                                                                                      <textarea type="text" class="form-control " id="position" placeholder="About" name="fdata[about]" aria-required="true" aria-invalid="true"></textarea>
                                                                                                                                                                  </div>
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                          <div class="col-sm-4">
                                                                                                                                                              <div class="form-group">
                                                                                                                                                                  <label for="name" class="col-sm-3 control-label">Address</label>
                                                                                                                                                                  <div class="col-sm-9">
                                                                                                                                                                      <textarea type="text" class="form-control " id="position" placeholder="address" name="fdata[address_line1]" aria-required="true" aria-invalid="true"></textarea>
                                                                                                                                                                  </div>
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                          <div class="col-sm-4">
                                                                                                                                                              <div class="form-group">
                                                                                                                                                                  <label for="name" class="col-sm-3 control-label">Other Address </label>
                                                                                                                                                                  <div class="col-sm-9">
                                                                                                                                                                      <textarea type="text" class="form-control " id="position" placeholder="address" name="fdata[address_line2]" aria-required="true" aria-invalid="true"></textarea>
                                                                                                                                                                  </div>
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                      </div>
                                                                                                                                                      <div class="form-group">
                                                                                                                                                      </div>
                                                                                                                                                      <div class="col-sm-12">
                                                                                                                                                          <div class="col-sm-6">
                                                                                                                                                              <div class="form-group">
                                                                                                                                                                  <label for="name" class="col-sm-3 control-label">TAX NUMBER</label>
                                                                                                                                                                  <div class="col-sm-9">
                                                                                                                                                                      <input type="text" class="form-control " id="position" value="" placeholder="License number" name="fdata[license_num]" aria-required="true" aria-invalid="true">
                                                                                                                                                                  </div>
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                          <div class="col-sm-6">
                                                                                                                                                              <div class="form-group">
                                                                                                                                                                  <label for="name" class="col-sm-3 control-label">Expiration Date</label>
                                                                                                                                                                  <div class="col-sm-9">
                                                                                                                                                                      <input type="text" class="form-control " id="position" value="" placeholder="Designation" name="fdata[expirydate]" aria-required="true" aria-invalid="true" disabled="">
                                                                                                                                                                  </div>
                                                                                                                                                              </div>
                                                                                                                                                          </div>
                                                                                                                                                      </div>
                                                                                                                                                      <div class="form-group">
                                                                                                                                                      </div>
                                                                                                                                                  </form>
                                                                                                                                              </div>
                                                                                                                                          </div>
                                                                                                                                      </div>
                                                                                                                                  </div>
                                                                                                                              </div>
                                                                                                                          </div>

                                                                                                                             <div class="tab-pane" id="tab2Inspire">
                                                                                                                                 <div class="row">
                                                                                                                                     <div class="col-md-12">




                                                                                                                                         <table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearchforreview" role="grid" aria-describedby="tableWithSearch_info">
                                                                                                                                             <thead>
                                                                                                                                             <tr role="row">
                                                                                                                                                 <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 50px !important;padding-left: 0px !important;">SLNO</th>
                                                                                                                                                 <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Places: activate to sort column ascending" style="width: 106px;">BOOKING ID</th>
                                                                                                                                                 <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 140px;">PASSENGER NAME</th>
                                                                                                                                                 <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 333px;">Review</th>
                                                                                                                                                 <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 182px;">Review Date</th>

                                                                                                                                                 <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 50px;">Rating</th>
                                                                                                                                             </tr>
                                                                                                                                             </thead>
                                                                                                                                             <tbody id="userreview">






                                                                                                                                             <?php
                                                                                                                                             $slno = 1;

                                                                                                                                             foreach ($aap_booking as $result) {
                                                                                                                                                 ?>



                                                                                                                                                 <tr role="row"  class="gradeA odd">
                                                                                                                                                     <td id = "d_no" class="v-align-middle sorting_1" style="padding: 7px!important;"> <p><?php echo $slno; ?></p></td>
                                                                                                                                                     <td id = "d_no" class="v-align-middle sorting_1" style="padding: 7px!important;"> <p><?php echo $result->appointment_id; ?></p></td>
                                                                                                                                                     <td class="v-align-middle" style="padding: 7px!important;"><?php echo $result->pessanger_fname.$result->pessanger_lname; ?></td>
                                                                                                                                                     <td class="v-align-middle" style="padding: 7px!important;"><?php echo date("M d Y g:i A", strtotime($result->appointment_dt)); ?></td>
                                                                                                                                                     <td class="v-align-middle" style="padding: 7px!important;"><?php echo date("M d Y g:i A", strtotime($result->appointment_dt)); ?></td>
                                                                                                                                                     <td class="v-align-middle" style="padding: 7px!important;">4</td>

                                                                                                                                                 </tr>

                                                                                                                                                 <?php
                                                                                                                                                 $slno++;
                                                                                                                                             }
                                                                                                                                             //                                            ?>

                                                                                                                                             </tbody>
                                                                                                                                         </table>






                                                                                                                                     </div>
                                                                                                                                 </div>
                                                                                                                             </div>

                                                                                                                      </div>
                                                                                                                  </div>


                                                                                                             </div>



                                                                                                         </div>

                                                                                                     </div>
                                                                                                 </div>

                                                                                                 <div class="tab-pane slide-left" id="slide4">
                                                                                                     <div class="row column-seperation">
                                                                                                         <div class="col-md-12">



                                                                                                             <div class="container-fluid container-fixed-lg bg-white">
                                                                                                                 <!-- START PANEL -->
                                            <div class="panel panel-transparent">
                                                <div class="panel-heading">
                                                    <div class="panel-title">TODAY'S BOOKINGS
                                                    </div>
                                                    <div class="pull-right">
                                                        <div class="col-xs-12">
                                                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive"><table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                                                <thead>
                                                                <tr role="row">
                                                                    <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;">SLNO</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Places: activate to sort column ascending" style="width: 275px;">BOOKING ID</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 304px;">PASSENGER NAME</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 175px;">PHONE</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">PICKUP D & T</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">PICKUP ADDRESS</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">DROP ADDRESS</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">VEHICLE TYPE</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">STATUS</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>






                                                                <?php
                                                                $slno = 1;

                                                                foreach ($aap_booking as $result) {
                                                                    ?>



                                                                    <tr role="row"  class="gradeA odd">
                                                                        <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $slno; ?></p></td>
                                                                        <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result->appointment_id; ?></p></td>
                                                                        <td class="v-align-middle"><?php echo $result->pessanger_fname.$result->pessanger_lname; ?></td>
                                                                        <td class="v-align-middle"><?php echo  $result->phone; ?></td>
                                                                        <td class="v-align-middle"><?php echo date("M d Y g:i A", strtotime($result->appointment_dt)); ?></td>
                                                                        <td class="v-align-middle"><?php echo  $result->address_line1.$result->address_line2; ?></td>
                                                                        <td class="v-align-middle"><?php echo  $result->drop_addr1.$result->drop_addr2; ?></td>
                                                                        <td class="v-align-middle">BMW</td>
                                                                        <?php
                                                                        if ($result->status == '1')
                                                                            $status = 'Appointment requested';
                                                                        else if ($result->status == '2')
                                                                            $status = $result->driver_fname.' accepted.';
                                                                        else if ($result->status == '3')
                                                                            $status = $result->driver_fname.' rejected.';
                                                                        else if ($result->status == '4')
                                                                            $status = 'Passenger has cancelled.';
                                                                        else if ($result->status == '5')
                                                                            $status = $result->driver_fname.' '.$result->driver_lname.' has canceled';
                                                                        else if ($result->status == '6')
                                                                            $status = $result->driver_fname.'on the way.';
                                                                        else if ($result->status == '7')
                                                                            $status = 'Appointment started.';
                                                                        else if ($result->status == '8')
                                                                            $status = $result->driver_fname.' Arrived';
                                                                        else if ($result->status == '9')
                                                                            $status = 'Appointment completed.';
                                                                        else if ($result->status == '10')
                                                                            $status = 'Appointment Timed out.';
                                                                        else
                                                                            $status = 'Status unavailable.';
                                                                        ?>
                                                                        <td class="v-align-middle"><?php echo $status; ?></td>

                                                                    </tr>

                                                                    <?php
                                                                    $slno++;
                                                                }
                                                                //                                            ?>

                                                                </tbody>
                                                            </table>

                                                        </div><div class="row"><div></div></div></div>
                                                </div>
                                            </div>
                                            <!-- END PANEL -->
                                        </div>






                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane slide-left" id="slide5">
                                <div class="row column-seperation">
                                    <div class="col-md-12">



                                        <div class="container-fluid container-fixed-lg bg-white">
                                            <!-- START PANEL -->
                                            <div class="panel panel-transparent">
                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                    </div>
                                                    <div class="pull-right">
                                                        <div class="col-xs-12">
                                                            <input type="text" id="search-tableclient" class="form-control pull-right" placeholder="Search">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive">
                                                            <table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearchClient" role="grid" aria-describedby="tableWithSearch_info">
                                                                <thead>
                                                                <tr role="row">
                                                                    <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;">SLNO</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Places: activate to sort column ascending" style="width: 275px;">CUSTOMER ID</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 304px;">CUSTOMER NAME</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 175px;">PHONE</th>
                                                                    <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">EMAIL</th>

                                                                </tr>
                                                                </thead>
                                                                <tbody>






                                                                <?php
                                                                $slno = 1;

                                                                foreach ($customers as $result) {
                                                                    ?>



                                                                    <tr role="row"  class="gradeA odd">
                                                                        <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $slno; ?></p></td>
                                                                        <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result->slave_id; ?></p></td>
                                                                        <td class="v-align-middle"><?php echo $result->first_name.$result->last_name; ?></td>
                                                                        <td class="v-align-middle"><?php echo  $result->phone; ?></td>
                                                                        <td class="v-align-middle"><?php echo $result->email; ?></td>

                                                                    </tr>

                                                                    <?php
                                                                    $slno++;
                                                                }
                                                                //                                            ?>

                                                                </tbody>
                                                            </table>

                                                        </div><div class="row"><div></div></div></div>
                                                </div>
                                            </div>
                                            <!-- END PANEL -->
                                        </div>






                                    </div>

                                </div>
                            </div>

                        </div>





                    </div>








                </div>




            </div>









        </div>








        <!-- END JUMBOTRON -->

        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->

    </div>
    <!-- END PAGE CONTENT -->
    <!-- START FOOTER -->
    <div class="container-fluid container-fixed-lg footer">
        <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
                <span class="hint-text">Copyright © 2014</span>
                <span class="font-montserrat">REVOX</span>.
                <span class="hint-text">All rights reserved.</span>
                <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a>
                </span>
            </p>
            <p class="small no-margin pull-right sm-pull-reset">
                <a href="#">Hand-crafted</a>
                <span class="hint-text">&amp; Made with Love ®</span>
            </p>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- END FOOTER -->
</div>

















<div class="modal fade stick-up in" id="myModal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Create New Order </h5>

            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group-attached">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Order Title</label>
                                    <input type="email" class="form-control">
                                </div>
                            </div>
                        </div>


                    </div>
                     <div class="form-group"></div>
                    <br>
                    <div class="form-group-attached">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>To Whom ?</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Phone</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group form-group-default">
                                    <label>Where To ?</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>Time</label>
                                    <div id="datepicker-component" class="input-group date col-sm-8">
                                        <input type="text" class="form-control"><span class="input-group-addon" style="background: white;border: 0px solid rgba(0, 0, 0, -4.93);"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-8">
                            <div class="p-t-20 clearfix p-l-10 p-r-10">
                                <div class="pull-left">
                                    <p class="bold font-montserrat text-uppercase"></p>
                                </div>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-default btn-cons no-margin pull-left inline" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 m-t-10 sm-m-t-10" style="margin-top: 16px">
                            <button type="button" class="btn btn-primary btn-block m-t-5">Add Order</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

















