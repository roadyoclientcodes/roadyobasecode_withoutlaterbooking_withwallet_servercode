<?php
error_reporting(0);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");



define("APP_NAME", "TAXI");

/*
 * Navigation items
 */
define("LIST_COMPANY_MOBIFY", "Only Numbers Are Allowed");


define("SELECT_BY_DRIVER", "Select By Driver");

define("NAV_DASHBOARD", "DASHBOARD");
define("NAV_CITIES", "CITIES");
define("NAV_COMPANYS", "COMPANIES");
define("NAV_VEHICLETYPES", "VEHICLE TYPES");
define("NAV_VEHICLES", "VEHICLES");
define("NAV_DRIVERS", "DRIVERS");
define("NAV_PASSENGERS", "PASSENGERS");
define("NAV_BOOKINGS", "ALL BOOKINGS");
define("NAV_CANCLEDBOOKINGS", "CANCLED BOOKINGS");
define("NAV_DISPATCHERS", "DISPATCHERS");
define("NAV_PAYROLL", "PAYROLL");
define("NAV_ACCOUNTING", "ACCOUNTING");
define("NAV_DOCUMENT", "DOCUMENTS");
define("NAV_DRIVERREVIEW", "DRIVER REVIEWS");
define("NAV_PASSENGERRATING", "PASSENGER RATINGS");
define("NAV_DISPUTES", "DISPUTES");
define("NAV_VEHICLEMODELS", "VEHICLE MODELS");
define("NAV_FINANCE", "MARKETING");
define("NAV_DELETE", "DELETE");
define("NAV_GODSVIEW", "GODSVIEW");
define("NAV_COMPAIGNS", "CAMPAIGNS");

/*
 * headings
 */
define("HEAD_DASHBOARD", "Dashboard ");
define("HEAD_CITIES", "Cities ");
define("HEAD_COMPANYS", "Company's ");
define("HEAD_VEHICLETYPE", "Vehicle Type ");
define("HEAD_VEHICLES", "Vehicles ");
define("HEAD_DRIVERS", "Drivers ");
define("HEAD_PASSENGERS", "Passengers ");
define("HEAD_BOOKINGS", "Bookings ");
define("HEAD_CANCLEDBOOKINGS", "Cancelled Bookings ");
define("HEAD_DISPATCHERS", "Dispatchers ");
define("HEAD_PAYROLL", "Payroll ");
define("HEAD_ACCOUNTING", "Accounting");

define("HEAD_DOCUMENT", "Document ");
define("HEAD_DRIVERREVIEW", "Driver Review ");
define("HEAD_PASSENGERRATING", "Passenger Rating");
define("HEAD_DISPUTES", "Disputes ");
define("HEAD_VEHICLEMODEL", "Vehicle Model ");
define("HEAD_COMPAIGNS", "Campaigns ");
define("HEAD_GODSVIEW", "GodzView ");
define("HEAD_FINANCE", "Finance ");
define("HEAD_DELETE", "Delete ");

define("SELECT_COUNTRY_ANDCITY", "Select City And Country");

/*
 * LIST ELEMENTS
 */
define("LIST_RESETPASSWORD_HEAD", "Change Password");
define("FIELD_COMPAIGNS_TITLE", "Title");


//define("VEHICLE_TYPE", "New");



define("LIST_NEW", "New");
define("LIST_ACCEPTED", "Accepted ");
define("LIST_REJECTED", "Rejected ");
define("LIST_SUSPENDED", "Suspended ");

define("LIST_ACCEPT", "ACCEPT");
define("LIST_REJECT", "REJECT");

define("COMPAIGNS_TABLE_CURRENCY", "CURRENCY ");

define("LIST_UNDERREVIEW", "Under Review");
define("LIST_ACCEPTEDFREE", "Accepted & Free");
define("LIST_REJECTED", "Rejected");
define("LIST_ACTIVE", "Active");
define("LIST_INACTIVE", "Inactive");

define("LIST_COMPAIGNS", "CAMPAIGNS");
define("LIST_REFFERED_PROMOS", "Reffered Promos");
define("LIST_OF_PROMOTION_ANALYTICS", "PROMO ANALYTICS");
define("LIST_OF_REFERRAL_ANALYTICS", "REFERRAL ANALYTICS");
define("LIST_REFFERAL_HEAD", "Add Referral");
define("LIST_PRAMOTION_HEAD", "Add Promotion");
define("POPUP_COMPAIGN_ONETOEDIT", "Select any one campaign to edit");
define("POPUP_COMPAIGN_ONLYONE", "Select only one campaign to edit");
define("POPUP_COMPAIGNS_TEXT_PROMOTIONS_S", "Great! Your promotion has been added sucessfully for this city");


define("LIST_FREE", "Free");
define("LIST_FREEONLINE", "Online & Free");
define("LIST_OFFLINE", "Offline");
define("LIST_BOOKED", "Booked");



define("LIST_DRIVERSLICENSE", "Drivers Licence");
define("LIST_BANKPASSBOOK", "Bank Passbook");
define("LIST_CARRIAGEPERMIT", "Carriage Permit");
define("LIST_CR", "Certificate Of Registration");
define("LIST_INSURENCECERTIFICATE", "Insurance Certificate");

define("LIST_PASSENGERRATING", "Passenger Rating");

define("LIST_REPORTED", "Reported");

define("LIST_VEHICLEMAKE", "Vehicle Make");
define("LIST_VEHICLEMODELS", "Vehicle Models");

define("LIST_REFERRALS", "Referrals");
define("LIST_PROMOTIONS", "Promotions");

define("LIST_CITIES", "Cities");
define("LIST_OF_CITIES", "List of Cities");
define("LIST_ADDCITIES", "Add City");
define("LIST_ADD_COUNTRY_DETAILS", "Add Country Details");
define("LIST_ADD_CITY_DETAILS", "Add City Details");

define("LIST_COMPANY", "COMPANY");
define("LIST_ADD_COMPANY_DETAILS", "Add Company Details");
define("LIST_EDIT_COMPANY_DETAILS", "Edit Company Details");

define("LIST_VEHICLETYPE", "Vehicle Type");
define("LIST_ADD_VEHICLETYPE_DETAILS", "Add Vehicle Type Details");
define("LIST_EDIT_VEHICLETYPE_DETAILS", "Edit Vehicle Details");

define("LIST_VEHICLE", "Vehicle");
define("LIST_VEHICLE_ADD", "Add New");
define("LIST_VEHICLE_VEHICLESETUP", "Vehicle Setup");
define("LIST_VEHICLE_DETAILS", "Details");
define("LIST_VEHICLE_DOCUMETS", "Documents");

define("LIST_DRIVER", "DRIVER");
define("LIST_DRIVER_ADDNEW", "ADD NEW");
define("LIST_DRIVER_PESIONALDETAILS", "PERSONAL DETAILS");
define("LIST_DRIVER_LOGINDETAILS", "LOGIN DETAILS");
define("LIST_DRIVER_DRIVINGLICENCE", "DRIVING LICENSE");

define("LIST_DISPATCHERS_ADDMODEL", "Select Country And City");
define("LIST_DISPATCHERS_EDITMODEL", "Select City");

define("LIST_PASSENGERRATING_EDITMODEL", "PASSENGER RATING");

define("LIST_DISPUTES_REPORTED", "REPORTED");
define("LIST_DISPUTES_RESOLVED", "RESOLVED");
define("LIST_DISPUTES_EDITDISPUTE", "Edit Dispute");


define("LIST_DISPUTES", "ADD VEHICLE TYPE");

define("LIST_COMPAIGNS_HEAD", "Add Referral");

define("VEHICLEMAKE_ADDVEHICLE", "Add Vehicle make");
define("VEHICLEMODEL_ADDVEHICLE", "Add Vehicle model");
define("SELECT_COMPANY", "Select Company");




/*
 *BUTTONS
 */
define("COMPAIGNS_DISPLAY", "Are You Sure To Deactivate");
define("VEHICLEMODEL_DELETE", "Are You Sure To Delete");
define("BUTTON_YES", "Yes ");
define("BUTTON_NO", "No ");
define("BUTTON_OK", "OK ");

define("SEARCH", "Search ");

define("SELECT", "Select ");

define("LIST_RESETPASSWORD_DRIVERDOCUMENTS", "Driver Documents");
//define("LIST_RESETPASSWORD_VEHICLEDOCUMENTS", "Vehicle Documents");
define("VEHICLEDOCUMENTS", "Vehicle Documents");
define("DRIVERS_TABLE_DRIVER_DOCUMENT", "DOCUMENT TYPE");
define("DRIVERS_TABLE_DRIVER_EXPIREDATE", "EXPIRY DATE");
define("DRIVERS_TABLE_DRIVER_VIEW", "VIEW/DOWNLOAD");



define("BUTTON_ADD", "Add ");
define("BUTTON_ADD_NEW", "Configure & Activate City ");
define("POPUP_VEHICLE_MAKE", "Are You Sure To Delete VehicleMake ");
define("BUTTON_ACTIVATE", "Activate ");
define("BUTTON_ACCEPT", "Accept ");
define("BUTTON_ACTIVE", "Active ");
define("BUTTON_REJECT", "Reject");
define("BUTTON_DEACTIVATE", "Deactivate ");
define("BUTTON_INACTIVE", "Inactivate ");
define("BUTTON_DEACTIVE", "Deactive ");
define("BUTTON_EDIT", "Edit ");
define("BUTTON_SUSPEND", "Suspend ");
define("BUTTON_DELETE", "Delete ");
define("BUTTON_CITIES", "Create Country or City");

define("BUTTON_RESOLVE", "Resolve");

define("BUTTON_RESETPASSWORD", "Reset Password");

define("BUTTON_ADDCOUNTRY", "Add Country");
define("BUTTON_ADDCITY", "Add City");
define("BUTTON_CANCEL", "Cancel");
define("BUTTON_ADD_COMPANY", "Add Company");
define("BUTTON_EDIT_COMPANY", "Edit Company");
define("BUTTON_CHANGES_COMPANY", "Save Changes");

define("BUTTON_ADD_VEHICLETYPE", "Add Vehicle Type");

define("BUTTON_EDIT_VEHICLETYPE", "Edit Vehicle Type");

define("BUTTON_NEXT", "NEXT");
define("BUTTON_PREVIOUS", "PREVIOUS");
define("BUTTON_FINISH", "FINISH");

define("BUTTON_SUBMIT", "Submit");
define("BUTTON_AUTOMATIC", "AUTOMATIC");
define("BUTTON_MANUAL", "MANUAL");
define("LIST_ADD_DISPATCHER", "Add Dispatcher");

/*
 * Popups
 */
define("POPUP_RESETPASSWORD_ANYONE", "Select Any One To Reset The Password ");

define("POPUP_SELECT_CITY", "Please Select The City");
define("POPUP_SELECT_COUNTRY", "Please Select The Country");

define("POPUP_DRIVER_ALERTTAB", "Complete Personal Details Tab Properly");
define("POPUP_DRIVER_ALERTLOGINTAB", "Complete Login Details Tab Properly");
define("POPUP_DRIVER_ALERTLOGINTABDRIVING", "Complete Driving Licence Tab Properly");

define("POPUP_SELECT_COUNTRY", "Please Select The Country");


define("POPUP_CITIES_ENTER_COUNTRY_NAME", "Please Enter the Country Name ");
define("POPUP_CITIES_COUNTRY_ADDED", "Country Added Successfully ");

define("POPUP_CITIES_CITY_ENTER", "Please Enter The City Name");
define("POPUP_CITIES_CITY_ENTERALPHA", "Please Enter The City Name As Alphabets Only");
define("POPUP_CITIES_CITY_CURENCY", " Enter The Currency With 3 Characters Only");
define("POPUP_CITIES_CITY_ADDED", "City Added Successfully ");
define("POPUP_CITIES_CITY_EXIST", "City Already Exist ");
define("POPUP_LAT_LONG_UPDATED", "Your Lat Long Updated Successfully");
define("POPUP_CITYTT_UPDATED", "Your City Updated Successfully");
define("POPUP_LAT_LONG_ADDED", "Your City Added Successfully");
define("POPUP_LAT_LONG_DELETED", "Your City Deleted Successfully");
define("POPUP_COUNTRY_ADDED", "Your Country Added Successfully");
define("POPUP_CITY_ADDED", "Your City Added Successfully");
define("POPUP_LAT_LONG_UPDATE_FAILED", "Your Lat Long Update Failed");
define("POPUP_ALPHABET", "Please Enter Alphabet");


define("POPUP_ENTER", "Please Enter The Data");
define("POPUP_COMPANY_NAME", "Please Enter The Company Name");
define("POPUP_COMPANY_NAMEVALID", "Enter The Company Name as Text/Number Or Both But Not A Special Characters");
define("POPUP_COMPANY_PASSWORD", "Enter The Password");
define("POPUP_COMPANY_EMAIL", "Please Enter Your Email Number");







define("POPUP_DRIVER_FIRSTNAME", "Please Enter The Driver Name");
define("POPUP_DRIVER_LASTNAME", "Please Enter The Last Name");
define("POPUP_DRIVER_MOBILE", "Please Enter The Mobile Number");
define("POPUP_DRIVER_DRIVERPHOTO", "Please Upload A Driver Photo");

define("POPUP_DRIVER_DRIVER_EMAIL", "Please Enter The Email ");
define("POPUP_DRIVER_DRIVER_YOUREMAIL", "Please Enter A Valid Email ");
define("POPUP_DRIVER_DRIVER_ALLOCATED", "Email Is Already Allocated !");
define("POPUP_DRIVER_DRIVER_PASSWORD", "Please Enter The Password ");
define("POPUP_DRIVER_DRIVER_PASSWORD_VALID", "The Password  Length Must Be 6 And Atleast One Number Followed By Character Only(No Special Characters)");
define("POPUP_DRIVER_DRIVER_ZIPCODE", "Please Enter The Zipcode ");



define("POPUP_ADDCOMPANY_NAME", "Please Select The Company");
define("POPUP_ADDCITY__NAME", "Please Select  The City");
define("POPUP_SELECT_TYPE", "Please Select The Vehicle Type");
define("POPUP_SELECT_VEHICLEID", "Please enter  The Vehicle id ");
define("POPUP_SELECT_VEHICLEMAKE", "Please Select  The Vehicle  Make");
define("POPUP_SELECT_VEHICLEMODAL", "Please Select  The Vehicle  Modal");
define("POPUP_SELECT_VEHICLEIMAGE", "Please Select  Vehicle Image");

define("POPUP_SELECT_VEHICLEREGNO", "Please Enter The Vehicle Reg Number");
define("POPUP_SELECT_VEHICLEPLATENO", "Please Enter The Vehicle Plate Number");
define("POPUP_SELECT_VINSURENCENUMBER", "Please Enter The Vehicle  Insurence Number");
define("POPUP_SELECT_VEHICLECOLOR", "Please Enter The Vehicle Color");
define("POPUP_SELECT_VEHICLEIMAGE", "Please Select  Vehicle Image");


define("POPUP_SELECT_VEHICLEUPLOADREGNO", "Please Upload The Registration Certificate");
define("POPUP_SELECT_VEHICLE_DATE", "Please Select The Expire Date ");
define("POPUP_SELECT_VINSURENCENUMBER_INSURENCE", "Please Upload  The  Motor Insurence Certificate");
define("POPUP_SELECT_VEHICLECOLOR_CARRIAGE_PERMIT", "Please Upload The Carriage Permit Certificate");


define("POPUP_COMPANY_EMAILVALID", "Please Enter A Valid Email");
define("POPUP_COMPANY_ADDRESS", "Enter The Address");
define("POPUP_COMPANY_ADDED_D", " Great ! The Company has been Added successfully and is Ready for Admin Approval");
define("POPUP_COMPANY_EXIST", " Company Already Register With This Email Id ");
define("POPUP_COMPANY_EDITED_D", "Are you sure you wish to save these changes , changes once made cannot be recovered ?");
define("POPUP_COMPANY_SELECT", "Select The City");
define("POPUP_COMPANY_VATNUMBER", "Enter The Vat Number");
define("POPUP_COMPANY_PPCODE", "Enter Valid Pincode Number");
define("POPUP_COMPANY_VATNUMBERNUM", "Enter The Valid Vat Number");
define("POPUP_COMPANY_ADDED", "Your Company Added Successfully");


define("POPUP_COMPANY_DELETEVEHICLE", "Please Select Vehicle Type To Delete");
define("POPUP_COMPANY_DELETECOMPANY", "Please Select Company To Delete");
define("POPUP_COMPANY_DELETECOUNTRY", "Please Select Country To Delete");
define("POPUP_COMPANY_DELETEDRIVER", "Please Select Driver To Delete");
define("POPUP_COMPANY_LOGOUTDRIVER", "Please Select Driver To Logout");

define("POPUP_COMPANY_SURELOGOUTDRIVER", "Are You Sure To Logout This Driver");

//define("POPUP_COMPANY_VATNUMBER", "Enter the vat number");
//define("POPUP_COMPANY_VATNUMBERNUM", "Enter the valid vat number");
//define("POPUP_COMPANY_ADDED", "Your company added successfully");


define("POPUP_VEHICLE_TAB_D", "Complete Vehicle Setup Tab Properly");
define("POPUP_VEHICLE_DETAILTAB_D", "Complete Details Tab Properly");




define("POPUP_COMPANY_ATLEASTONENAME", "Please Select  Atleast One Company Name");
define("POPUP_ACCEPTED", "Are You Sure You Want To Activate This Company And have Validated All Of Its Documents ,etc ? ");
define("POPUP_REJECTED", "Are You Sure You Want To Reject This Company And have Validated All Of Its Documents ,etc ? ");
define("POPUP_SUSPENDED", "Are You Sure You Want To Suspend This Company And have Validated All Of Its Documents ,etc ? ");
define("POPUP_DELETE", "Are You Sure You Want To Delete This Company And Have Validated All Of Its Documents ,etc ? ");



define("POPUP_DELETEVEHICLETYPE", "Are You Sure To Delete This Vehicle Type? ");
define("POPUP_DELETECOMPANY", "Are You Sure To Delete This Company? ");
define("POPUP_DELETECOUNTRY", "Are You Sure To Delete This Country ? ");
define("POPUP_DELETEDRIVER", "Are You Sure To Delete This Driver ? ");
define("POPUP_DELETEDVEHICLEMODAL", "Are You Sure To Delete This Vehiclemodal ? ");
define("POPUP_DELETEVEHICLEMODAL", "Please Select Vehicle Modal To Delete ? ");




define("POPUP_COMPANY_FIRST_NAME", "Please Enter The First Name");
define("POPUP_COMPANY_LAST_NAME", "Please Enter The Last Name");
define("POPUP_COMPANY_MOBILE", "Please Enter Your Mobile Number");
define("POPUP_COMPANY_CTATE_NAME", "Please Enter The State Name");
define("POPUP_COMPANY_PINCODE_NAME", "Please Enter The Pincode");
define("POPUP_COMPANY_ADDED", "Your Company Updated Successfully");

define("POPUP_CANCEL", "Are You Sure To Cancel The Data ");

define("POPUP_COMPANY_MOBILE", "Please Enter The Mobile Number ");

define("POPUP_CONFIRM", "Are You Sure To Delete Companies");
define("POPUP_VEHICLETYPE", "Are You Sure To Delete Vehicle Model");
define("POPUP_VEHICLES", "Are You Sure To Delete Vehicles");
define("POPUP_DRIVERS", "Are You Sure To Delete Drivers");
define("POPUP_PASSENGERS", "Are You Sure To Inactive Passengers");
define("POPUP_DISPATCHERS", "Are You Sure To Inactive Dispatchers");
define("POPUP_DRIVERREVIEW_INACTIVE", "Are You Sure To Inactive Driver Reviews");
define("POPUP_DRIVERREVIEW_ACTIVE", "Are You Sure To Active Driver Reviews");
define("POPUP_VEHICLEMODEL", "are You Sure To Delete Vehicletypes");

define("POPUP_COMPANY_ATLEAST", "Please Select Atleast One Company");
define("POPUP_CITY_ATLEAST", "Please Select Atleast One City");


define("POPUP_VEHICLETYPE_ATLEAST", "please Select Atleast One Vehicle Type");

define("POPUP_COMPANY_ACTIVATED", "Your Selected Company/Companys Activated Successfully");
define("POPUP_COMPANY_DEACTIVATED", "Your Selected Company/Companys Deactivated Successfully");
define("POPUP_COMPANY_SUSPENDED", "Your Selected Company/Companys Suspended Successfully");
define("POPUP_COMPANY_DELETED", "Your Selected Company/Companys Deleted Successfully");

define("POPUP_COMPANY_ANYONE", "Please Select Any One Company");
define("POPUP_COMPANY_ONLYONE", "Please Select Only One Company To Edit");

define("POPUP_CITY_ANYONE", "Please Select Any One City");
define("POPUP_CITY_ONLYONE", "Please Select Only One City To Edit");
define("POPUP_CITY_LAT", "Please Enter The Latitude");
define("POPUP_CITY_LATVAL", "Please Enter Valid Data At Latitude");
define("POPUP_CITY_LOT", "Please Enter The Longitude");
define("POPUP_CITY_LONVAL", "Please Enter Valid Data At Longitude");



define("POPUP_COMPANY_ONLYONE", "Please Select Only One Company To Edit");

define("POPUP_VEHICLETYPE_ADDED", "Your Vehicletype Added Successfully");
define("POPUP_VEHICLETYPE_DELETED", "Vehicletype Deleted Successfully");
define("POPUP_VEHICLETYPE_UPDATED", "Your Vehicletype Updated Successfully");

define("POPUP_VEHICLETYPE_ENTER", "Please Enter The Vehicle Type");
define("POPUP_VEHICLETYPE_ENTERTEXT", "Please Enter The Vehicle Type As Text");
define("POPUP_VEHICLETYPE_NOSEATINGS", "Please Enter The Number Of Seatings");
define("POPUP_VEHICLETYPE_NUMSEATINGS", "Please Enter The Seating As Number");
define("POPUP_VEHICLETYPE_MINMUM", "Please enter the minimum fare");
define("POPUP_VEHICLETYPE_MINMUM_NUMBER", "Please enter the minimum as number only");



define("POPUP_VEHICLETYPE_BASEFARE", "Enter The Base Fare");
define("POPUP_VEHICLETYPE_BASEFARENUM", "Enter The Base Fare As Number Only");
define("POPUP_VEHICLETYPE_MINUTE", "Enter The Cost Per Minute");
define("POPUP_VEHICLETYPE_NUMMINUTE", "Enter The Price Per Minute As Number Only");
define("POPUP_VEHICLETYPE_KM", "Enter The Cost Per Kilometer");

define("POPUP_VEHICLETYPE_ANYONE", "Please Select Any One Vehicle Type");
define("POPUP_VEHICLETYPE_ONLYONE", "Please Select Only One Vehicle Type To Edit");
define("POPUP_VEHICLETYPE_ATLEASTONE", "Please Select Atleast One Vehicle Type");
define("POPUP_VEHICLETYPE_SUREDELETE", "Areyou Sure You Want To Delete Vehicletypes");



define("POPUP_VEHICLETYPE_KMNUM", "Enter The Cost Per Kilometer As Number Only");
define("POPUP_VEHICLETYPE_SELECTCITY", "Please Select The City");

define("POPUP_VEHICLES_ATLEAST", "Please Mark Any One Option");

define("POPUP_PASSENGERS_ATLEAST", "Select Atleast One Passenger");
define("POPUP_DRIVERS_ATLEAST", "Select Atleast One Driver");
define("POPUP_PASSENGERS_DEACTIVATE", "Are You Sure You Want To Deactivate Driver Reviews");
define("POPUP_PASSENGERS_ACTIVATE", "Are You Sure You Want To Activate Driver Reviews");

define("POPUP_DRIVERS_ACTIVAT", "Are You Sure You Want To Activate Drivers");
define("POPUP_DRIVERS_DELETE", "Are You Sure You Want To Delete Driver/Drivers");
define("POPUP_DRIVERS_DEACTIVAT", "Are You Sure You Want To Deactivate Drivers");
define("POPUP_DRIVERS_NEWPASSWORD", "Your New Password Updated Successfully");
define("POPUP_DRIVERS_ERRPASSWORD", "Your Password Already Exist! Enter New Password");



define("POPUP_PASSENGERS_DEACTIVAT", "Are You Sure You Want To Deactivate Passengers");
define("POPUP_PASSENGERS_ACTIVAT", "Are You Sure You Want To Activate Passengers");

define("POPUP_PASSENGERS_ANYONEPASS", "Please Select Any One To Reset The Password");
define("POPUP_PASSENGERS_ONLYONEPASS", "Please Select Only One To Reset The Password");

define("POPUP_PASSENGERS_PASSVALID", "The Password  Length Must Be 6 And Atleast One Number Followed By Character Only(No Special Characters)");
define("POPUP_PASSENGERS_PASSCONFIRM", "Please Confirm The Password ");
define("POPUP_PASSENGERS_PASSENTER", "Please Enter The Password ");
define("POPUP_PASSENGERS_SAMEPASSCONFIRM", "Please Confirm The Same Password ");
define("POPUP_PASSENGERS_PASSNEW", "Please Enter The New Password ");

define("POPUP_PASSENGERS_PASSUPDATED", "Password Updated Successfully ");

define("POPUP_DISPATCHERS_CITY", "Please Select City");

define("POPUP_DISPATCHERS_NAME", "Please Enter The Name");
define("POPUP_DISPATCHERS_NAMETEXT", "Please Enter The Name As Text");
define("POPUP_DISPATCHERS_EMAIL", "Please Enter The Email");
define("POPUP_DISPATCHERS_VALIDEMAIL", "Please Enter Valid Email");
define("POPUP_DISPATCHERS_PASSWORD", "Please Enter The Password");

define("POPUP_DISPATCHER_DELETE", "Please Select Aatleast One Dispatcher");
define("POPUP_DISPATCHER_EDIT", "Please Select Any One Dispatcher");
define("POPUP_DRIVER_EDIT", "Please Select Any One Driver");
define("POPUP_VEHICLE_DOCUMENT", "Please Select Any One Vehicle");
define("POPUP_DISPATCHER_ONLYEDIT", "Please Select Only One Dispatcher To Edit");
define("POPUP_DRIVER_ONLYEDIT", "Please Select Only One Driver To Edit");
define("POPUP_DRIVER_ONLYEDIT_DOCUMENT", "Please Select Only One Vehicle To View Documents");
define("POPUP_DISPATCHER_INACTIVE", " Select Atleast One Dispatcher");
define("POPUP_DISPATCHER_SUREINACTIVE", " Are You Sure You Want To inactivate Dispatchers");
define("POPUP_DISPATCHER_SUREACTIVE", "Are You Sure You Want To Activate Dispatchers");

define("POPUP_DRIVERREVIEW_ATLEAST", "Please Select Atleast One Driver Review");

define("POPUP_VEHICLE_ATLEAST", "Please Select Atleast One Vehicle");
define("POPUP_VEHICLE_ONE", "Please Select Any  One Vehicle");
define("POPUP_VEHICLE_DELETE", "Are You Sure To Delete These Vehicles");
define("POPUP_VEHICLE_DEACTIVATE", "Are You Sure You Want To Reject These Vehicle/Vehicles");
define("POPUP_VEHICLE_ACTIVATE", "Are You Sure You Want To Activate These Vehicle/Vehicles");


define("POPUP_MESSAGE", "Please Enter The Message");

define("POPUP_DISPUTES_ANYONE", "Please Select Any One To Resolve");
define("POPUP_DISPUTES_ONLYONE", "Please Select Only One To Resolve");
define("POPUP_COMPAIGN_ATLEAST", "Select Atleast One Compaign");

define("POPUP_COMPAIGN_AREYOUSURE", "Are You Sure To Deactivate");
define("POPUP_COMPAIGN_REFERRAL", "Referral");

define("POPUP_MESSAGE", "Please Enter The Message");



define("POPUP_VEHICLEMODEL_TYPENAMES", "Please Enter The Type Name");
define("POPUP_VEHICLEMODEL_TEXT", "Please Enter The Type Name As Text");
define("POPUP_MESSAGE_COMPLETED", "Your Type Name Added Successfully");
define("POPUP_VEHICLEMODEL_ATLEAST", "Select Atleast One Vehicle Type");

define("POPUP_VEHICLEMODEL_TYPENAME", "Please Select The Vehicle Type");
define("POPUP_VEHICLEMODEL_MODELNAME", "Please Enter The Model  Name");
define("POPUP_VEHICLEMODEL_SUCCESSES", "Your  Model  Name Added Successfully");


define("POPUP_VEHICLEMODEL_CODE", "Please Enter The Code");
define("POPUP_VEHICLEMODEL_MODELNAME", "Please Select Model  Name");
define("POPUP_VEHICLEMODEL_STARTDATE", "Please Select The Start Date");
define("POPUP_VEHICLEMODEL_EXPIREDATE", "Please Select The Expire Date");

define("POPUP_COMPAIGNS_DISCOUNT", "Please Enter The New User Discount");
define("POPUP_COMPAIGNS_TITLE", "Please Enter The Title");
define("POPUP_COMPAIGNS_NUMBERS", "Please Enter The Numbers Only");
define("POPUP_COMPAIGNS_REFERALDISCOUNT", "Please Enter The Referral Bonus");
define("POPUP_COMPAIGNS_MESSAGE", "Please Enter The Message");
define("POPUP_COMPAIGNS_TEXT", "Please Enter Message As Text Only");

define("POPUP_COMPAIGNS_CODE", "Please Enter The Code");
define("POPUP_COMPAIGNS_TEXTNUMBER", "Please Enter Code As Text Or Number");
define("POPUP_COMPAIGNS_STARTDATE", "Please Select  The Start Date");
define("POPUP_COMPAIGNS_EXPIREDATE", "Please Select  The Expire Date");



/*
 * Fields
 */

define("FIELD_ENTER_COUNTRY_NAME", "ENTER COUNTRY NAME");


define("FIELD_CITIES_COUNTRY", "SELECT COUNTRY");
define("FIELD_CITIES_CITYNAME_NAME", "CITY NAME");
define("FIELD_CITIES_CURRENCY_NAME", "ENTER CURRENCY NAME");

define("FIELD_COMPANY_COMPANYNAME", "COMPANY NAME");
define("FIELD_COMPANY_FIRSTNAME", "FIRST NAME");
define("FIELD_COMPANY_LASTNAME", "LAST NAME");
define("FIELD_COMPANY_USERNAME", "USER NAME");
define("FIELD_COMPANY_PASSWORD", "PASSWORD");
define("FIELD_COMPANY_EMAIL", "EMAIL");
define("FIELD_COMPANY_ADDRESS", "ADDRESS");
define("FIELD_COMPANY_MOBILE", "MOBILE");
define("FIELD_COMPANY_CITY", "CITY");
define("FIELD_COMPANY_STATE", "STATE");
define("FIELD_COMPANY_POSTCODE", "POST CODE");
define("FIELD_COMPANY_VATNUMBER", "VAT NUMBER");

define("FIELD_COMPANY_SELECTCOUNTRY", "SELECT COUNTRY");

define("FIELD_VEHICLETYPE_NAME", "VEHICLE TYPE NAME");
define("FIELD_VEHICLETYPE_SEATINGCAPACITY", "SEATING CAPACITY");
define("FIELD_VEHICLETYPE_MINIMUMFARE", "MINIMUM FARE");
define("FIELD_VEHICLETYPE_BASEFARE", "BASE FARE   (INR)");
define("FIELD_VEHICLETYPE_PRICEMINUTE", "PRICE PER MINUTE ($)");
define("FIELD_VEHICLETYPE_PRICEKM", "PRICE PER KM/MILE ($)");
define("FIELD_VEHICLETYPE_DESCRIPTION", "VEHICLE TYPE DESCRIPTION");
define("FIELD_VEHICLETYPE_CITY", "CITY");

define("FIELD_VEHICLETYPE_LATITUDE", "LATITUDE");
define("FIELD_VEHICLETYPE_LONGITUDE", "LONGITUDE");


define("FIELD_VEHICLE_SELECTCITY", "SELECT CITY");
define("FIELD_VEHICLE_SELECTCOMPANY", "SELECT COMPANY");

define("FIELD_VEHICLE_VEHICLETYPE", "SELECT VEHICLE TYPE");
define("FIELD_VEHICLE_VEHICLEMAKE", "SELECT VEHICLE MAKE");
define("FIELD_VEHICLE_VEHICLEMODEL", "SELECT VEHICLE MODEL");
define("FIELD_VEHICLE_IMAGE", "UPLOAD PHOTO OF THE VEHICLE");

define("FIELD_VEHICLE_REGNO", "VEHICLE REG.NO");
define("FIELD_VEHICLE_PLATENO", "LICENSE PLATE NO");
define("FIELD_VEHICLE_INSURENCE", "INSURANCE NUMBER");
define("FIELD_VEHICLE_COLOR", "VEHICLE COLOR");

define("FIELD_VEHICLE_UPLOADCR", "UPLOAD CERTIFICATE OF REGISTRATION");
define("FIELD_VEHICLE_EXPIREDATE", "EXPIRATION DATE");
define("FIELD_VEHICLE_UPLOADMOTOR", "UPLOAD MOTOR INSURENCE CERTIFICATE");
define("FIELD_VEHICLE_UPLOADCP", "UPLOAD CONTRACT CARRIAGE PERMIT");
define("FIELD_VEHICLE_CHOOSE", "CHOOSE FILE");
define("FIELD_VEHICLE_NOCHOOSE", "NO FILE CHOOSEN");

define("FIELD_VEHICLE_VEHICLEID", "VEHICLE ID");

define("FIELD_DRIVERS_FIRSTNAME", "FIRST NAME");
define("FIELD_DRIVERS_LASTNAME", "LAST NAME");
define("FIELD_DRIVERS_MOBILE", "MOBILE ");
define("FIELD_DRIVERS_UPLOADPHOTO", "UPLOAD PHOTO");
define("FIELD_DRIVERS_EMAIL", "EMAIL");
define("FIELD_DRIVERS_PASSWORD", "PASSWORD");
define("FIELD_DRIVERS_ZIPCODE", "ZIP CODE");
define("FIELD_DRIVERS_UPLOADDRIVERLICENSE", "UPLOAD DRIVING LICENCE");
define("FIELD_DRIVERS_EXDATE", "EXPIRY DATE");
define("FIELD_DRIVERS_UPLOADPASSBOOK", "UPLOAD BANK PASSBOOK COPY");

define("FIELD_DISPUTES_MANAGEMENTNOTE", "MANAGEMENT NOTE");

define("FIELD_SELECTCITY", "SELECT CITY");

define("FIELD_DISPATCHERS_NAME", "NAME");
define("FIELD_DISPATCHERS_EMAIL", "EMAIL");
define("FIELD_DISPATCHERS_PASSWORD", "PASSWORD");

define("FIELD_VEHICLEMODEL_TYPENAME", "TYPE NAME");


define("FIELD_VEHICLEMODEL_SELECTTYPE", "SELECT TYPE");
define("FIELD_VEHICLEMODEL_MODAL", "MODEL");

define("FIELD_COMPAIGNS_DISCOUNTTYPE", "DISCOUNT TYPE");
define("FIELD_COMPAIGNS_PERCENTAGE", "PERCENTAGE");
define("FIELD_COMPAIGNS_FIXED", "FIXED");
define("FIELD_COMPAIGNS_DISCOUNT", "NEW USER DISCOUNT ");
define("FIELD_COMPAIGNS_REFERRALDISCOUNTTYPE", "REFERRAL DISCOUNT TYPE");
define("FIELD_COMPAIGNS_REFERRALDISCOUNT", "REFERRAL BONUS ");
define("FIELD_COMPAIGNS_MESSAGE", "MESSAGE");




define("FIELD_COMPAIGNS_CODE", "CODE");
define("FIELD_COMPAIGNS_STARTDATE", "STATRT DATE");
define("FIELD_COMPAIGNS_EXPIREDATE", "EXPIRATION DATE");
define("FIELD_COMPAIGNS_DISCOUNTTYPE", "DISCOUNT TYPE");
define("FIELD_PROMOTION_DISCOUNT", "PROMOTION DISCOUNT");

define("LIST_ADDCOMPANYS", "ADD COMPANY");









/* NEW PASSWORD && CONFIRM PASSWORD*/


define("FIELD_NEWPASSWORD", "NEW PASSWORD");
define("FIELD_CONFIRMPASWORD", "CONFIRM PASSWORD");







/*
 * Table headings
 */

define("LIST_CITY_TABLE_SLNO", "SL.NO");
define("LIST_CITY_TABLE_COUNTRY", "COUNTRY");
define("LIST_CITY_TABLE_CITY", "CITY");
define("LIST_CITY_TABLE_LATITUDE", "LATITUDE");
define("LIST_CITY_TABLE_LONGITUDE", "LONGITUDE");
define("LIST_CITY_TABLE_SELECT", "SELECT");


define("LIST_CITY_TABLE_EDITCITIESGEO", "Edit City's Geo Location");



define("COMPANY_ADDEDIT_PERSIONAL", "Personal Contact Details");
define("COMPANY_TABLE_COMPANYID", "COMPANY ID");
define("COMPANY_TABLE_COMPANYNAME", "COMPANY NAME");
define("COMPANY_TABLE_ADDRESSLINE", "ADDRESS LINE");
define("COMPANY_TABLE_CITY", "CITY");
define("COMPANY_TABLE_STATE", "STATE");
define("COMPANY_TABLE_POSTALCODE", "POSTAL CODE");
define("COMPANY_TABLE_FIRSTNAME", "FIRST NAME");
define("COMPANY_TABLE_LASTNAME", "LAST NAME");
define("COMPANY_TABLE_EMAIL", "EMAIL");
define("COMPANY_TABLE_MOBILE", "MOBILE");
define("COMPANY_TABLE_SELECT", "SELECT");

define("VTYPE_TABLE_TYPEID", "TYPE ID");
define("VTYPE_TABLE_TYPENAME", "TYPE NAME");
define("VTYPE_TABLE_MAXSIZE", "MAX SIZE");
define("VTYPE_TABLE_BASEFARE", "BASE FARE");
define("VTYPE_TABLE_MINFARE", "MIN FARE");
define("VTYPE_TABLE_PRICEMINUTE", "PRICE PER MINUTE");
define("VTYPE_TABLE_PRICRMILE", "PRICE PER MILE");
define("VTYPE_TABLE_TYPEDESCRIPTION", "TYPE DESCRIPTION");
define("VTYPE_TABLE_CITY", "CITY");
define("VTYPE_TABLE_SELECT", "SELECT");

define("VEHICLES_TABLE_VEHICLEID", "VEHICLE_ID");
define("VEHICLES_TABLE_TITLE", "TITLE");
define("VEHICLES_TABLE_VMODAL", "V MODEL");
define("VEHICLES_TABLE_VTYPE", "V TYPE");
define("VEHICLES_TABLE_VREGNO", "V REG NO");
define("VEHICLES_TABLE_LICENSEPLATNO", "LICENSE PLATE NO");
define("VEHICLES_TABLE_INSURENCENUMBER", "INSURENCE NUMBER");
define("VEHICLES_TABLE_VCOLOR", "V COLOR");
define("VEHICLES_TABLE_OPTION", "OPTION");

define("DRIVERS_TABLE_DRIVER_ID", "DRIVER_ID");
define("DRIVERS_TABLE_NAME", "NAME");
define("DRIVERS_TABLE_MOBILE", "MOBILE");
define("DRIVERS_TABLE_EMAIL", "EMAIL");
define("DRIVERS_TABLE_REGDATE", "REG DATE");
define("DRIVERS_TABLE_DEVICETYPE", "DEVICE TYPE");
define("DRIVERS_TABLE_RESETPASSWORD", "RESET PASSWORD");
define("DRIVERS_TABLE_PROFILEPIC", "PROFILE PIC");
define("DRIVERS_TABLE_OPTION", "OPTION");

define("PASSENGERS_TABLE_PASSENGERID", "PASSENGER ID");
define("PASSENGERS_TABLE_FIRSTNAME", "FIRST NAME");
define("PASSENGERS_TABLE_LASTNAME", "LAST NAME");
define("PASSENGERS_TABLE_MOBILE", "MOBILE");
define("PASSENGERS_TABLE_EMAIL", "EMAIL");
define("PASSENGERS_TABLE_REGDATE", "REG DATE");
define("PASSENGERS_TABLE_DEVICETYPE", "DEVICE TYPE");
define("PASSENGERS_TABLE_ZIPCODE", "ZIP CODE");
define("PASSENGERS_TABLE_PROFILEPIC", "PROFILE PIC");
define("PASSENGERS_TABLE_STATUS", "STATUS");
define("PASSENGERS_TABLE_SELECT", "SELECT");


define("DISPATCHERS_TABLE_DISPATCHERID", "DISPATCHER ID");
define("DISPATCHERS_TABLE_CITY", "CITY");
define("DISPATCHERS_TABLE_EMAIL", "EMAIL");
define("DISPATCHERS_TABLE_DISPATCHERNAME", "DISPATCHER NAME");
define("DISPATCHERS_TABLE_NOOFBOOKINGS", "NO OF BOOKINGS");
define("DISPATCHERS_TABLE_OPTION", "OPTION");

define("DOCUMENT_TABLE_DOCUMENTID", "DOCUMENT ID");
define("DOCUMENT_TABLE_FIRSTNAME", "FIRST NAME");
define("DOCUMENT_TABLE_LASTNAME", "LAST NAME");
define("DOCUMENT_TABLE_VIEW", "VIEW");
define("DOCUMENT_TABLE_EXPIRYDATE", "EXPIRY DATE");


define("DOCUMENT_TABLE_VEHICLEID", "VEHICLE ID");
define("DOCUMENT_TABLE_COMPANY", "COMPANY");


define("DRIVERREVIEW_TABLE_SLNO", "SL.NO");
define("DRIVERREVIEW_TABLE_DRIVERID", "BOOKING ID");
define("DRIVERREVIEW_TABLE_REVIEW", "REVIEW");
define("DRIVERREVIEW_TABLE_RATING", "RATING");
define("DRIVERREVIEW_TABLE_REVIEWDATE", "BOOKING DATE AND TIME");
define("DRIVERREVIEW_TABLE_DRIVERNAME", "DRIVER NAME");
define("DRIVERREVIEW_TABLE_PASSENGERNAME", "PASSENGER ID");
define("DRIVERREVIEW_TABLE_STATUS", "STATUS");
define("DRIVERREVIEW_TABLE_SELECT", "SELECT");

define("PASSENGRRATING_TABLE_SLNO", "SL.NO");
define("PASSENGRRATING_TABLE_PASSENGERID", "PASSENGER ID");
define("PASSENGRRATING_TABLE_PASSENGERNAME", "PASSENGER NAME");
define("PASSENGRRATING_TABLE_PASSENGEREMAIL", "PASSENGER EMAIL");
define("PASSENGRRATING_TABLE_AVGRATING", "AVG RATING");

define("DISPUTES_TABLE_DISPUTEID", "DISPUTE ID");
define("DISPUTES_TABLE_BOOKINGID", "BOOKING ID");
define("DISPUTES_TABLE_PASSENGERID", "PASSENGER ID");
define("DISPUTES_TABLE_PASSENGERNAME", "PASSENGER NAME");
define("DISPUTES_TABLE_DRIVERID", "DRIVER ID");
define("DISPUTES_TABLE_DRIVERNAME", "DRIVER NAME");
define("DISPUTES_TABLE_DISPUTEMESSAGE", "DISPUTE MESSAGE");
define("DISPUTES_TABLE_DISPUTEDATE", "DISPUTE DATE");
define("DISPUTES_TABLE_APPOINTMENTID", "APPOINTMENT ID");
define("DISPUTES_TABLE_SELECT", "SELECT");


define("VEHICLEMAKE_TABLE_ID", "ID");
define("VEHICLEMAKE_TABLE_TYPENAME", "TYPE NAME");
define("VEHICLEMAKE_TABLE_SELECT", "SELECT");

define("VEHICLEMODEL_TABLE_ID", "ID");
define("VEHICLEMODEL_TABLE_MODELID", "MODEL ID");
define("VEHICLEMODEL_TABLE_MAKE", "MAKE");

define("VEHICLEMODEL_TABLE_MODEL", "MODEL");
define("VEHICLEMODEL_TABLE_SELECT", "SELECT");

define("COMPAIGNS_TABLEL_DISCOUNT", "DISCOUNT");
define("COMPAIGNS_TABLE_REFERRALDISCOUNT", "REFERRAL DISCOUNT");
define("COMPAIGNS_TABLE_MESSAGE", "MESSAGE");
define("COMPAIGNS_TABLE_CITY", "CITY ");
define("COMPAIGNS_TABLE_SELECT", " SELECT");

define("COMPAIGNS_TABLE_CODE", "CODE");
define("COMPAIGNS_TABLE_STARTDATE", "START DATE");
define("COMPAIGNS_TABLE_ENDDATE", "END DATE");
define("COMPAIGNS_TABLE_DISCOUNT", "DISCOUNT");
define("COMPAIGNS_TABLE_MESSAGE", "MESSAGE");
define("COMPAIGNS_TABLE_PROMOT_CITY", "CITY");
define("COMPAIGNS_TABLE_PROMOT_SELECT", "SELECT");









/*
 * Generic
 */

define("GEN_DELETE", "Delete");
define("GEN_OPEN", "Open");
define("GEN_CREATE", "Create");
