<?php
//session_start();
if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>

<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>
<script type="text/javascript">
    $(document).ready(function () {
<?php if ($status == 1) { ?>
            if ($("table.sortable").length > 0)
                $("table.sortable").dataTable({"iDisplayLength": 12, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null, null]});
<?php } else { ?>
            if ($("table.sortable").length > 0)
                $("table.sortable").dataTable({"iDisplayLength": 12, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null]});
    <?php
}
?>
    });
    
     function isNumberKey(evt)
    {
        $("#mobify").text("");
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 45 || charCode > 57)) {
            //    alert("Only numbers are allowed");
            $("#mobify").text(<?php echo json_encode(LIST_COMPANY_MOBIFY); ?>);
            return false;
        }
        return true;
    }
    
    $("#firstname").keypress(function (event) {
            var inputValue = event.which;
            //if digits or not a space then don't let keypress work.
            if ((inputValue > 64 && inputValue < 91) // uppercase
                    || (inputValue > 96 && inputValue < 123) // lowercase
                    || inputValue == 32) { // space
                return;
            }
            event.preventDefault();
        });
    
    $("#lastname").keypress(function (event) {
            var inputValue = event.which;
            //if digits or not a space then don't let keypress work.
            if ((inputValue > 64 && inputValue < 91) // uppercase
                    || (inputValue > 96 && inputValue < 123) // lowercase
                    || inputValue == 32) { // space
                return;
            }
            event.preventDefault();
        });
    
    
</script>
<style>

    body
    {
        font-family:arial;
    }
    .preview
    {
        width:200px;
        border:solid 1px #dedede;
        padding:10px;
    }
    #preview
    {
        color:#cc0000;
        font-size:12px
    }
    #regpassword,#regfirstname,#reglastname,#regmobile,#regpassword,#regzipcode,#regcompany,#regexpirationrc
    {
        color:red;
    }
</style>
<!--<div class="page-content page-content-white" style="margin: 0;">-->
<!--alert = function() {};-->
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div style="text-align: center;"><h1>
            <?php
            if ($status == '1') {
                echo 'NEW DRIVERS';
                // echo 'hi---'.$cityid;
                // echo $cityid;
            } else if ($status == '3') {
                echo 'ACCEPTED DRIVERS';
            } else if ($status == '4') {
                echo 'REJECTED DRIVERS';
            }
            ?></h1></div>
    <div style="float:right;">
        <?php
        if ($status == '1' || $status == '5') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" data="3" data-msg="active"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean" id="AddButton">ADD</a></button>    

        <?php }
        ?>
        <?php
        if ($status == '1' || $status == '4') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="3" data-msg="active">ACTIVATE</button>    
            <?php
        }

        if ($status == '1' || $status == '3') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="RejectButton" data="4" data-msg="inactive">REJECT</button>
            <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="EditButton" data="5" data-msg="edit"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">EDIT</a></button>


            <?php
        }
        ?>

    </div>
    <div style="float:none;"></div>
    <!-- <a href="#" class="widget-icon widget-icon-dark" id="buttonClass" style=" color: rgb(247, 0, 0); background-color: blue;float: right;
        margin-Right: 80px;"><span class="icon-trash"></span></a>-->

    <div id="refresh_table">
        <?php require ('refreshDriver.php'); ?>
    </div>

</div> 
<div class="modal" id="modal_default_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body clearfix">

                <div class="controls">
                    <form  action="#" autocomplete="on" style="color:#000;" method="post" id="addDriverForm" enctype="multipart/form-data"> 
                        <h1>ADD DRIVER</h1> 
                        <p> 
                            <label for="text" class="youpasswd" >FIRST NAME </label>
                            <input id="firstname" name="firstname" required="required" class="editfirstname" type="text" placeholder="First Name" /> 
                            <span id="regfirstname"></span>
                        </p>
                        <p> 
                            <label for="text" class="youpasswd" >LAST NAME </label>
                            <input id="lastname" name="lastname" required="required" class="editlastname" type="text" placeholder="Last Name" /> 
                            <span id="reglastname"></span>
                        </p>

                        <p> 
                            <label for="text" class="youpasswd" > MOBILE </label>
                            <input id="mobile" name="mobile" required="required" class="editmobile" type="text" placeholder="Mobile" onkeypress="return isNumberKey(event)"/> 
                            <span id="regmobile"></span>
                        </p>

                        <p> 
                            <label for="text" class="youpasswd" > EMAIL </label>
                            <input id="email" name="email" required="required" class="editemail" type="text" placeholder="Email" /> 
                            <span id="regemail"></span>
                        </p>

                        <p id="par_password"> 
                            <label for="text" class="youpasswd" > PASSWORD </label>
                            <input id="password" name="password" required="required" class="editpassword" type="password" placeholder="password" /> 
                            <span id="regpassword"></span>
                        </p>

<!--                        <p> 
                            <label for="text" class="youpasswd" >ZIPCODE</label>
                            <input id="zipcode" name="zipcode" required="required" class="editzipcode" type="text" placeholder="Zipcode" /> 
                            <span id="regzipcode"></span>
                        </p>
                        -->
                        <p id="company_list" style="display: none;"> 
                            <label for="text" class="youpasswd" >COMPANY </label>
                            <select id="company" name="companyid" class="editcompany" >
                                <option value="NULL">SELECT A COMPANY</option>
                                <?php
                                $get_vechile_type = "select * from company_info ";
                                $get_vechile_type_res = mysql_query($get_vechile_type, $db1->conn);
                                while ($typelist = mysql_fetch_array($get_vechile_type_res)) {
                                    echo "<option value='" . $typelist['company_id'] . "' id='" . $typelist['company_id'] . "'>" . $typelist['companyname'] . "</option>";
                                }
                                ?>
                            </select>
                            <span id="regcompany"></span>
                        </p>


                        <p>
                        <div id="fileuploader">Upload Profile Pic<input type="file" name="photos" id="file_upload" /></div>
                        <div id="preview_img2"></div>
                        </p>

                        <p>
                        <div id="fileuploader">Upload Driving License<input type="file" name="certificate" class="certificate" id="file_upload" /></div>
                        <div id="preview_img1"></div>
                        </p>

                        <p>

                            <label for="text" class="youpasswd" >License Expiration Date</label>
                            <input id="expirationrc" name="expirationrc" required="required" min="<?php echo date('Y-m-d', time()); ?>" value="" class="editexpire" type="date" placeholder="eg. X8df!90EO" /> 
                            <span id="regexpirationrc"></span>

                        </p>

                        <p>
                        <div id="fileuploader">Upload Bank Passbook Copy<input type="file" name="passbook" class="passbook" id="file_upload" /></div>
                        <div id="preview_img2"></div>
                        <input type="hidden" name="editdata" id="editdataval" value="1"/>

                        </p>


                        <input type="hidden" name="item_type" value="1"/>
                        <input type="hidden" name="mas_id" value="" id="hidden_mas"/>

                    </form> 
                </div>

            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_modal" style="color:red;border:solid 1px #a2a2a2">Close</button>
                <button type="button" class="btn btn-success btn-clean" id="datasubmit">Submit form</button>
            </div>
        </div>
    </div>
</div>    


<div class="modal modal-draggable" id="modal_default_20" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="driverpass" style="width: 200px;display: inline;" id="driverchng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="driverconf_pass" style="width: 200px;display: inline;" id="driverchng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="driversendData" id="driversendData" value=""/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="driver_change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="driverchange_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    $('#refresh_table .resetPassword').click(function () {
//            alert($(this).attr('data'));
        var dis = $(this);
        $('#driversendData').val(dis.attr('data'));
    });
    $(document).ready(function () {
//            alert('1');


        $('#change_pass_doc').click(function () {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {mas_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function (result) {
                        alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });




        $('#driver_change_pass_doc').click(function () {

            $('#errmsgDoc').text(' ');
            var driverpass = $('#driverchng_pass_doc').val();
            var driverconf_pass = $('#driverchng_conf_pass_doc').val();
            if (driverpass == '' || driverconf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (driverpass != driverconf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } /*else if (checkStrengthDoc(driverpass) == 1) {
             $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
             } */ else {

                $.ajax({
                    type: "POST",
                    url: "changePassDriver.php",
                    data: {doc_id: $('#driversendData').val(), pass: driverconf_pass, type: 1},
                    dataType: "JSON",
                    success: function (result) {
                        alert(result.message);
                        if (result.flag == 0) {

                            $('#driverchng_pass_doc').val("");
                            $('#driverchng_conf_pass_doc').val("");

                            $('#driverchange_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });


        $('#AddButton').click(function () {
            $('#company_list').hide();
            $('#par_password').show();
        });


        $('#EditButton').click(function () {
            var dis = $(this);

            $('#company_list').show();

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();
            // alert("values" + values);

            if (values == '') {
                alert('Please select one driver first.');
            } else {

                $.ajax({
                    type: "POST",
                    url: "submitdriver.php",
                    data: {item_type: 5, item_list: values},
                    dataType: "JSON",
                    success: function (result) {
                        // alert(result.errFlag);
                        if (result.errFlag == 0) {
                            $('#par_password').hide();
                            $('.editfirstname').val(result.first_name);
                            $('.editlastname').val(result.last_name);
                            $('.editpassword').val(result.password);
                            $('.editemail').val(result.email);
                            $('.editzipcode').val(result.zipcode);
                            $('#editdataval').val(result.errFlag);
                            $('.editmobile').val(result.mobile);
                            $('#company').val(result.company_id);
                            $('#hidden_mas').val(result.mas_id);
                            $('#expirationrc').val(result.expirationrc);


                        } else {
                            alert(result.msg);
                            $('html').append(result.qry);
                        }


                    },
                    error: function ()
                    {
                        alert("sorry,Error occured");
                    }
                });
            }
        });


        $('#datasubmit').click(function () {
//alert('submit');

            if ($('#firstname').val() === "")
            {
                $('#firstname').focus();
                $('#regfirstname').html("First Name is required");
                return false;
            }

            if (!/^[a-zA-Z]+$/.test($('#firstname').val()))
            {
                $('#firstname').focus();
                $('#regfirstname').html("First Name should only contain characters");
                return false;
            }

            if ($('#lastname').val() === "")
            {
                $('#lastname').focus();
                $('#reglastname').html("Last Name is required");
                return false;
            }

            if (!/^[a-zA-Z]+$/.test($('#lastname').val()))
            {
                $('#lastname').focus();
                $('#reglastname').html("Last Name should only contain characters");
                return false;
            }

            if (!/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($('#email').val()))
            {
                $('#email').focus();
                $('#regemail').html("Email format is wrong.");
                return false;
            }
//alert('bfr mobile test');
            var filter = /^[0-9-+]+$/;
            if (!filter.test($('#mobile').val())) {
                $('#mobile').focus();
                $('#regmobile').html("Mobile Should be a number");
                return false;
            }
//alert('after mobile test');
//
//            if ($('#company').val() == "NULL")
//            {
//                $('#company').focus();
//                $('#regcompany').html("Last Name is required");
//                return false;
//            }
//            
//            if ($('#expirationrc').val() === "")
//            {
//                $('#expirationrc').focus();
//                $('#regexpirationrc').html("Expiration Date is required");
//                return false;
//            }


            var formElement = document.getElementById("addDriverForm");
            var formData = new FormData(formElement);

            if ($('#editdataval').val() != '0')
            {

                var pass = $('#password').val();
                if (pass == '') {
                    $('#regpassword').html('Password is required.');
                    $('#password').focus();
                    return false;
                } else if (checkStrengthDriver(pass) == 1) {
                    $('#regpassword').html('Password must contain atleast one digit,one Uppercase, one Lower case character and atleast 8 digit ');
                    $('#password').focus();
                    return false;
                }

                $.ajax({
                    url: "driverupload.php",
                    type: "POST",
                    data: formData,
                    dataType: "JSON",
                    async: false,
                    success: function (result) {
                        // alert(result.flag);
                        if (result.flag == 0) {
                            $('#close_modal').trigger('click');
                            $('#refresh_table').load('refreshDriver.php', {type: 1});
                        } else {
//                        alert('Error occured');
                            alert(result.msg);
//$('body').append("<div>"+result.msg+"</div>");
                        }
                    },
                    error: function (result) {
                        alert('test1');
                        console.log(result);
                        alert(result.flag);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                e.preventDefault();
            }
            else
            {
                $.ajax({
                    url: "submitdriver.php",
                    method: "POST",
                    data: formData,
                    dataType: "JSON",
                    async: false,
                    success: function (result) {
//                    alert('hi');
                        if (result.flag == 0) {
                            $('#close_modal').trigger('click');
                            $('#refresh_table').load('refreshDriver.php', {type: 3});
                        } else {
//                        alert('Error occured');
                            alert(result.msg);
                            $('html').append(result);
//$('body').append("<div>"+result.msg+"</div>");
                        }
                    },
                    error: function (result) {
                        // alert('test1');
                        alert('Error');

                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

        });
        $('#deletebutton').click(function () {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast one driver in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 19, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function (result) {
                        alert(result.message);
                        alert(result.test);
                        if (result.flag == 0) {
                            $('.custom_check').each(function () {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        }
                    }
                });

            }
        });


        $('#ActiveButton,#RejectButton').click(function () {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function () {

<?php
if ($status == 1) {
    ?>
                    if ($("#company_id" + this.value).val() == '') {
                        alert('Seems you havent selected company for the driver');
                        exit();
                    }
                    return this.value + '-' + $("#company_id" + this.value).val();
<?php } else { ?>

                    return this.value;
<?php } ?>
            }).get();
//            alert(values);
            if (values == '') {
                alert('Please select  atleast one driver in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                var status = <?php echo json_encode($status); ?>;
//                alert(status);
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 1, to_do: dis.attr('data'), item_list: values, status: status},
                    dataType: "JSON",
                    success: function (result) {
                        // alert(result.message);
                        //alert(result.test);
                        if (result.flag == 0) {
//                            alert('here');
                            $('.custom_check').each(function () {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        } else {
                            alert('error:' + result.message);
                        }
                    }
                });

            }
        });

    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }


    function checkStrengthDriver(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>


