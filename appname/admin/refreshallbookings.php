
<style>
    .pagination_num2
    {
        padding: 0px 5px;
        background-color: gray;
    }
</style>
<?php
//session_start();
require_once '../Models/ConDB.php';
require_once 'php/pagination.php';
$db1 = new ConDB();

if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
?>


<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>

<script type="text/javascript">


    $(document).ready(function () {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 20, "aLengthMenu": [20], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null]});
    });
</script>

<div style="text-align: center;color: black;">
    <div style="font-size:20px;">ALL BOOKINGS</div>
</div>
<!--<div id="DataTables_Table_1_length" class="dataTables_length">
  <label>Show <select size="1"  id="selectno" class="selectno2" name="DataTables_Table_1_length" aria-controls="DataTables_Table_1">
    <option value="#">select option</option>
    <option value="10">10</option>
    <option value="20">20</option>
    <option value="30">30</option>
    <option value="40">40</option>
    <option value="50">50</option>
      </select> entries</label>
  </div>-->
<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
    <thead style="font-size: 12px;">

        <tr>
            <th>BOOKING ID</th>
            <th>PICKUP ADDR</th>  
            <th>DROP ADDR</th>
            <th>BOOKING TYPE</th>
            <th>DRIVER NAME</th> 
            <th>DRIVER ID</th> 
            <th>STATUS</th>
            <th>PICKUP TIME</th>
            <th>DROP TIME</th>
        </tr>

    </thead>
    <tbody style="font-size: 12px;">

        <?php
        if (!isset($_REQUEST['page'])) {
            $page_num = 1;
        } else {
            $page_num = (int) $_REQUEST['page'];
        }

        if (!isset($_REQUEST['noofentries'])) {
            $no_of_data_per_page = 20;
        } else {
            $no_of_data_per_page = (int) $_REQUEST['noofentries'];
        }
//        $page_num = 2;
        // echo  'records'.$no_of_data_per_page;

        $from = ((int) ((int) $page_num - 1) * (int) $no_of_data_per_page);
        // echo 'tot'.$from;
        // $get_count = mysql_query("select count(*) as total from appointment ap,slave p,master d where d.mas_id=ap.mas_id and p.slave_id = ap.slave_id  and ap.status = 5 order by ap.appointment_id", $db1->conn);
        // $count_res = mysql_fetch_assoc($get_count);
        //  $accQry = "select ap.appointment_dt,ap.appointment_id,ap.slave_id,ap.status,ap.address_line1,ap.drop_addr1,p.first_name as pat_fname,p.last_name as pat_lname,p.phone as ph,d.first_name as dname,d.mas_id as driver_id from appointment ap,slave p,master d where d.mas_id=ap.mas_id and  p.slave_id = ap.slave_id  and ap.status = 5 order by ap.appointment_dt DESC limit ".$from.",".$no_of_data_per_page;



        if ($cityid == '' and $companyids == '') {
            $accQry = "select ap.mas_id,ap.appt_type,ap.type_id,(select type_name from workplace_types where type_id=ap.type_id ) as typename,ap.address_line1,ap.drop_addr1,ap.start_dt,ap.complete_dt,ap.appointment_dt,ap.payment_type,ap.appointment_id,ap.inv_id,ap.status,ap.cancel_status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where  ap.mas_id = d.mas_id and ap.slave_id = p.slave_id  order by ap.appointment_id DESC limit " . $from . "," . $no_of_data_per_page;
            $get_count = mysql_query("select count(*) as total from appointment ap,slave p,master d where  ap.mas_id = d.mas_id and ap.slave_id = p.slave_id  order by ap.appointment_id", $db1->conn);
        } else if ($cityid != '' && $companyids == '') {

            $accQry = "select ap.mas_id,ap.appt_type,ap.type_id,(select type_name from workplace_types where type_id=ap.type_id ) as typename,ap.address_line1,ap.drop_addr1,ap.start_dt,ap.complete_dt,ap.appointment_dt,ap.payment_type,ap.appointment_id,ap.inv_id,ap.status,ap.cancel_status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . ")) AND ap.mas_id = d.mas_id and ap.slave_id = p.slave_id  order by ap.appointment_id DESC limit " . $from . "," . $no_of_data_per_page;

            $get_count = mysql_query("select count(*) as total from appointment ap,slave p,master d where d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . ")) AND ap.mas_id = d.mas_id and ap.slave_id = p.slave_id  order by ap.appointment_id", $db1->conn);
        } else if ($cityid == '' && $companyids != '') {

            $accQry = "select ap.mas_id,ap.appt_type,ap.type_id,(select type_name from workplace_types where type_id=ap.type_id ) as typename,ap.address_line1,ap.drop_addr1,ap.start_dt,ap.complete_dt,ap.appointment_dt,ap.payment_type,ap.appointment_id,ap.inv_id,ap.status,ap.cancel_status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where d.company_id IN((" . $companyids . ")) AND ap.mas_id = d.mas_id and ap.slave_id = p.slave_id  order by ap.appointment_id DESC limit " . $from . "," . $no_of_data_per_page;

            $get_count = mysql_query("select count(*) as total from appointment ap,slave p,master d where d.company_id IN((" . $companyids . ")) AND ap.mas_id = d.mas_id and ap.slave_id = p.slave_id  order by ap.appointment_id", $db1->conn);
        } else {


            $accQry = "select ap.mas_id,ap.appt_type,ap.type_id,(select type_name from workplace_types where type_id=ap.type_id ) as typename,ap.address_line1,ap.drop_addr1,ap.start_dt,ap.complete_dt,ap.appointment_dt,ap.payment_type,ap.appointment_id,ap.inv_id,ap.status,ap.cancel_status,ap.amount,d.email as doc_email,d.first_name as doc_fname,d.last_name as doc_lname,p.email as pat_email,p.first_name as pat_fname,p.last_name as pat_lname from appointment ap,master d,slave p where d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . " and company_id = " . $companyids . "))  AND ap.mas_id = d.mas_id and ap.slave_id = p.slave_id  order by ap.appointment_id DESC limit " . $from . "," . $no_of_data_per_page;

            $get_count = mysql_query("select count(*) as total from appointment ap,slave p,master d where d.company_id IN((SELECT company_id FROM company_info WHERE city = " . $cityid . " and company_id = " . $companyids . "))  AND ap.mas_id = d.mas_id and ap.slave_id = p.slave_id  order by ap.appointment_id", $db1->conn);
        }

        $count_res = mysql_fetch_assoc($get_count);

        $result1 = mysql_query($accQry, $db1->conn);

        $i = $from + 1;

        $statusArr = array('test', "requested.", "driver accepted", 'driver rejected.',
            'Passenger cancelled.', 'Driver cancelled.', 'driver on the way.', 'driver arrived.', 'journey start.',
            'journey complete.', 'expired.');
        while ($row = mysql_fetch_assoc($result1)) {
            ?>



            <tr>
                <td><?php echo $row['appointment_id']; ?></td>
                <td><?php echo $row['address_line1'] ?></td>
                <td><?php echo $row['drop_addr1'] ?></td>

                <td><?php
                    if ($row['appt_type'] == 1)
                        echo 'Now';
                    else
                        echo 'Later';
                    ?></td>
                <td><?php echo $row['doc_fname']; ?></td>
                <td><?php echo $row['mas_id']; ?></td>
                <td><?php echo (empty($statusArr[$row['status']])) ? $statusArr[0] : $statusArr[$row['status']];
                    ?></td>
                <td><?php echo $row['start_dt']; ?></td>
                <td><?php echo $row['complete_dt'] ?></td>

            </tr>

            <?php
            $i++;
        }
        ?> 

    </tbody>    
</table>    
<div style="width: 250px; float: left;padding-top:20px; "><?php echo 'Showing ' . ($from + 1) . ' - ' . ($i - 1) . ' of ' . $count_res['total']; ?></div>
<?php
$num_of_pages = $count_res['total'] / $no_of_data_per_page;
$tot_num_of_pages = ceil($num_of_pages);
echo pagination(20, $page_num, '', (int) $tot_num_of_pages - 20);
?>

<div style="padding-right:50px;padding-top:20px;  text-align: right;float:right;">
    <?php
//    $num_of_pages = $count_res['total'] / $no_of_data_per_page;
//    $tot_num_of_pages = ceil($num_of_pages);
//    $page_num_str = '';
//    for ($j = 1; $j <= $tot_num_of_pages; $j++) {
//        // echo'<a href="#" name="tabs" class="pagination_num2" id="pageclick" datavalue="'.$j.'">'.$j.'</a>';
//        echo'<input type="button" name="tabs" class="pagination_num2" id="pageclick" datavalue="' . $j . '" value="' . $j . '" style="width:20px;float:left;">';
//
//        //$page_num_str .= ' '.$j.' ';
//    }
    //echo $page_num_str;
    ?>


</div>

<script type="text/javascript">

    $(document).on('click', '.pagination_num2', function () {
        var a = $(this).attr('datavalue');
        //alert(a);
        //$('.container').load('cancelled.php',{ page : $('#pageid').val() });
        $('.container').load('refreshallbookings.php', {page: a});

    });

</script>

























<?php /*


  <script type="text/javascript">
  $(document).ready(function() {



  });


  </script>
  <style>
  #tabledesign
  {
  background-color:white;
  color:black;
  }

  .dataTable{float: left; border-bottom: 1px solid #DAE1E8; margin-bottom: 5px;}
  .dataTable div.checker, .dataTable div.radio{display: inherit;}

  .dataTables_wrapper{float: left; width: 100%;}

  .dataTables_length{width: 160px; float: left; padding: 5px;}
  .dataTables_length label,.dataTables_filter label{padding: 0px; line-height: 16px; height: auto; margin: 0px; font-weight: normal;}
  .dataTables_length select{width: 50px; display: inline; margin: 0px 5px;}
  .dataTables_filter{width: 160px; float: right; padding-left: 5px; padding: 5px;}
  .dataTables_filter label input[type="text"]{width: 100px; display: inline; margin-left: 5px; border-width: 1px;}

  td.dataTables_empty{font-size: 11px; text-align: center; color: #333;}

  .dataTables_info{float: left; font-size: 11px; padding: 8px 10px; line-height: 16px;}
  .dataTables_paginate{padding: 5px 5px; text-align: right; float: right;}
  .dataTables_paginate a.paginate_disabled_previous,.dataTables_paginate a.paginate_disabled_next,.dataTables_paginate a.paginate_button,.paginate_enabled_next,.paginate_active,.paginate_enabled_previous
  {padding: 3px 5px; margin-left: 3px; font-size: 11px; background: rgba(0,0,0,0.2);}
  .paginate_active{background: rgba(0,0,0,0.5);}
  .dataTables_paginate .paginate_enabled_next:hover,.dataTables_paginate a.paginate_button:hover,.dataTables_paginate .paginate_enabled_previous:hover
  {background: rgba(0,0,0,0.4); cursor: pointer;}
  .dataTables_paginate a.paginate_disabled_previous,.dataTables_paginate a.paginate_button_disabled,.dataTables_paginate a.paginate_disabled_next
  {cursor: default; color: #ccc;}
  .dataTables_paginate a.paginate_button_disabled:hover{box-shadow: none; color: #ccc;}

  .sorting_desc,.sorting_asc,.sorting{position: relative; cursor: pointer;}
  .sorting:before,.sorting_desc:before,.sorting_asc:before
  {position: absolute; right: 5px; font-family: 'FontAwesome'; opacity: 0.9; filter: alpha(opacity=90);}
  .sorting:before{content: "\f0dc"; opacity: 0.3; filter: alpha(opacity=30);}
  .sorting_desc:before{content: "\f0d8";}
  .sorting_asc:before{content: "\f0d7";}

  .sorting_asc_disabled:before{opacity: 0.1; filter: alpha(opacity=10);}
  .sorting_desc_disabled:before{opacity: 0.1; filter: alpha(opacity=10);}


  </style>
  <!--<div class="page-content page-content-white" style="margin: 0;">-->
  <!--alert = function() {};-->

  <div style="text-align: center;color: black;"><h1>CANCELLED BOOKINGS</h1></div>

  <div class="content">
  <div id="DataTables_Table_1_length" class="dataTables_length">
  <label>Show <select size="1" name="DataTables_Table_1_length" aria-controls="DataTables_Table_1">
  <option value="13">13</option><option value="26">26</option><option value="39">39</option><option value="52">52</option><option value="65">65</option></select> entries</label>
  </div>
  <div class="dataTables_filter" id="DataTables_Table_1_filter"><label>Search: <input type="text" aria-controls="DataTables_Table_1"></label>
  </div>

  <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable" id="tabledesign">
  <thead style="font-size: 12px;">
  <tr>
  <th>S NO</th>
  <th>Booking Id</th>
  <th>NAME</th>
  <th>PHONE</th>
  <th>TIME</th>
  <th>PICK UP</th>
  <th>DROP</th>
  <th>STATUS</th>

  </tr>
  </thead>
  <tbody style="font-size: 12px;">

  <?php
  $accQry = "select ap.appointment_dt,ap.appointment_id,ap.slave_id,ap.status,ap.address_line1,ap.drop_addr1,p.first_name as pat_fname,p.last_name as pat_lname,p.phone as ph from appointment ap,slave p where p.slave_id = ap.slave_id  and ap.status = 5 order by ap.appointment_id DESC";
  $result1 = mysql_query($accQry, $db1->conn);
  //echo $accQry;
  $i = 1;
  while ($row = mysql_fetch_assoc($result1)) {
  ?>

  <tr>
  <td><?php echo $i; ?></td>
  <td><?php echo $row['appointment_id'] ?></td>
  <td><?php echo $row['pat_fname'] ?></td>
  <td><?php echo $row['ph'] ?></td>
  <!-- <td><?php echo date('d-m-Y', strtotime($row['appointment_dt'])); ?></td>-->
  <td><?php echo date('h:i A', strtotime($row['appointment_dt'])); ?></td>
  <td><?php echo $row['address_line1'] ?></td>
  <td><?php echo $row['drop_addr1']; ?></td>
  <td><?php echo $row['status']; ?></td>


  <!--<td><a href="dispatchmap.php?id=<?php echo $row['appointment_id']; ?>"  class="driver" ><button type="submit" value="view">ASSIGN</button></a></td>-->
  <!--<td><a href="dispattchlaterBookingDriver.php?id=<?php echo $row['appointment_id']; ?>"  class="driver" ><button type="submit" value="view">ASSIGN</button></a></td>-->
  <!--         <td><a href="laterBooking/index_1.php?id=<?php echo $row['appointment_id']; ?>"  class="driver" ><button type="submit" value="view">ASSIGN</button></a></td>
  -->
  </tr>
  <?php
  $i++;
  }
  ?>

  </tbody>
  </table>
  <div class="dataTables_info" id="DataTables_Table_1_info">Showing 1 to 8 of 16 entries</div>
  <div class="dataTables_paginate paging_full_numbers" id="DataTables_Table_1_paginate"><a tabindex="0" class="first paginate_button paginate_button_disabled" id="DataTables_Table_1_first">First</a><a tabindex="0" class="previous paginate_button paginate_button_disabled" id="DataTables_Table_1_previous">Previous</a><span><a tabindex="0" class="paginate_active">1</a><a tabindex="0" class="paginate_button">2</a></span><a tabindex="0" class="next paginate_button" id="DataTables_Table_1_next">Next</a><a tabindex="0" class="last paginate_button" id="DataTables_Table_1_last">Last</a>
  </div>
  </div>

 */ ?>