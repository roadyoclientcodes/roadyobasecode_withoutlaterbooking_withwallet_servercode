<?php
//session_start();
include('../Models/ConDB.php');
$db1 = new ConDB();

if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} 
?>

<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 12, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null, null, null]});
    });
</script>
<!--<div class="page-content page-content-white" style="margin: 0;">-->
<!--alert = function() {};-->
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>
<div style="text-align: center;color: white;"><h1>
        <?php
        if ($status == '12') {
            echo 'NEW DOCTORS';
        } else if ($status == '13,14') {
            echo 'ACTIVE DOCTORS';
        } else if ($status == '15') {
            echo 'REJECTED DOCTORS';
        } else if ($status == '16') {
            echo 'INACTIVE DOCTORS';
        }
        ?></h1></div>

<div class="content">
    <div style="float:right;">
        <?php
        if ($status == '1' || $status == '5') {
            ?>
			
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="3" data-msg="active">ACTIVATE</button>   
			
            <?php
        }
		 
        if ($status == '1' || $status == '3,4') {
            ?>
			
            <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="RejectButton" data="5" data-msg="inactive">DEACTIVATE</button>
			<button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="`" data="3" data-msg="active"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">ADD</a></button>    
	     	<button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="EditButton" data="5" data-msg="edit"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">EDIT</a></button>
            <?php
        }
        ?>
		 
		</div>
    <div style="float:none;"></div>
	
	<div id="refresh_table">
	<?php require ('refreshcompanydetail.php'); ?>
   </div>
	
	</div>
<div class="modal" id="modal_default_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal with form</h4>
                </div>
                <div class="modal-body clearfix">

                    <div class="controls">
                         <form  action="submitvechiles.php" autocomplete="on" style="color:#000;" method="post" id="addVehicleForm" enctype="multipart/form-data"> 
                                <h1>ADD COMPANY</h1> 
								
								
								
								<p> 
                                    <label for="text" class="youpasswd" >Company Name </label>
                                    <input id="companyname" name="companyname" required="required" class="editcompany" type="text" placeholder="Company Name" /> 
                                </p>
								<p> 
                                    <label for="text" class="youpasswd" >First Name </label>
                                    <input id="firstname" name="firstname" required="required" class="editfirstname" type="text" placeholder="First Name" /> 
                                </p>
								
							<p> 
                                    <label for="text" class="youpasswd" > Last Name </label>
                                    <input id="lastname" name="lastname" required="required" class="editlastname" type="text" placeholder="Last Name" /> 
                                </p>
								 <p> 
                                    <label for="text" class="youpasswd" > User Name </label>
                                    <input id="username" name="username" required="required" class="editusername" type="text" placeholder="User Name" /> 
                                </p>
								 <p> 
                                    <label for="text" class="youpasswd" >Password</label>
                                    <input id="password" name="password" required="required" class="editpassword" type="text" placeholder="Password" /> 
                                </p>
								 <p> 
                                    <label for="text" class="youpasswd" >Email</label>
                                    <input id="email" name="email" required="required" class="editemail" type="text" placeholder="Email" /> 
                                </p>
						
						   <p>
						      <label for="text" class="youpasswd" >Address</label>
                              <input id="address" name="address" required="required"  class="editaddress" type="text" placeholder="Address" /> 
                          </p>
				    	    <p>
						      <label for="text" class="youpasswd" >Mobile</label>
                              <input id="mobile" name="mobile"  required="required" class="editmobile" type="text" placeholder="Mobile" /> 
                          </p>
						  <p>
						  <label for="text" class="youpasswd" >City</label>
                          <input id="city" name="city"  required="required" class="editcity" type="text" placeholder="City" /> 
                          </p>
						  
						  <p>
						  <label for="text" class="youpasswd" >State</label>
                          <input id="state" name="state"  required="required" class="editstate" type="text" placeholder="State" /> 
                          </p>
						  
						  
						  <p>
						  <label for="text" class="youpasswd" >PostCode</label>
                          <input id="postcode" name="postcode"  required="required" class="editpostcode" type="text" placeholder="PostCode" /> 
                          </p>
						  
						  <p>
						  <label for="text" class="youpasswd" >Vat Number</label>
                          <input id="vatnumber" name="vatnumber" required="required" class="editvatnumber" type="text" placeholder="Vat Number" /> 
						  <input type="hidden" name="editdata" id="editdataval"/>
                          </p>
                           </form>     
                  </div>

                </div>
                <div class="modal-footer">                
                    <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_modal" style="color:red;border:solid 1px #a2a2a2">Close</button>
                    <button type="button" class="btn btn-success btn-clean" id="datasubmit">Submit form</button>
                </div>
            </div>
        </div>
    </div>    	
</div> 

<script type="text/javascript">
    $(document).ready(function() {
//            alert('1');
        $('.resetPassword').click(function() {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });


        $('#change_pass_doc').click(function() {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {company_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function(result) {
                        alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });
		
		$('#EditButton').click(function() {
            var dis = $(this);
			var values = $('input:checkbox:checked.custom_check').map(function() {
                return this.value;
            }).get();
			alert("values"+values);
		            $.ajax({
                    type: "POST",
                    url: "submitcompany.php",
                    data: {item_type:5,item_list: values},
                    dataType: "JSON",
                    success: function(result) {
					   if (result.errFlag == 0) {
					   $('.editcompany').val(result.companyname);
					   $('.editfirstname').val(result.firstname);
					   $('.editlastname').val(result.lastname);
					   $('.editusername').val(result.userame);
					   $('.editpassword').val(result.password);
					   $('.editaddress').val(result.addressline1);
					   $('.editemail').val(result.email);
					   $('.editpostcode').val(result.postcode);
					   $('.editstate').val(result.state);
					   $('.editcity').val(result.city);
					   $('#editdataval').val(result.errFlag);
					   $('.editmobile').val(result.mobile);
					   $('.editvatnumber').val(result.vat_number);
					    }else{
						alert(result.qry);
						}
                    },
					error:function()
					{
					alert("sorry,Error occured");
					}
                    });
        });
		
				
		$('#datasubmit').click(function() {
			
		            var dis = $(this);
			var values = $('input:checkbox:checked.custom_check').map(function() {
                return this.value;
            }).get();
					
					var companynames=$('#companyname').val();
					var firstnames=$('#firstname').val();
					var lastnames=$('#lastname').val();
					var passwords=$('#password').val();
					var usernames= $('#username').val();
					var addresses= $('#address').val();
					var emails = $('#email').val();
					var mobiles = $('#mobile').val();
					var citys = $('#city').val();
					var states = $('#state').val();
					var postcodes = $('#postcode').val();
					var vatnumbers = $('#vatnumber').val();
					if($('#editdataval').val()=='0')
					{
      		        $.ajax({
                    type: "POST",
                    url: "submitcompany.php",
                    data: {item_type:1,item_list: values,to_do: dis.attr('data'),companyname:companynames,firstname:firstnames,lastname:lastnames,password:passwords,username:usernames,address:addresses,email:emails,mobile:mobiles,city:citys,state:states,postcode:postcodes,vatnumber:vatnumbers,add_val:2},
                    dataType: "JSON",
				    success: function(result) 
					{
					
					   //alert(result.qrys);
	                    if(result.flag == 0)
						{
							$('#refresh_table').load('refreshcompanydetail.php',{test:1},function(){});
							$('#close_modal').trigger('click');
						}else
						{
							alert('Error occured in adding vehicle type');
						}
                    },
					error: function()
					{
						alert('Error occured1');
					}
                });
				}
				else
				{
				$.ajax({
                    type: "POST",
                    url: "submitcompany.php",
                    data: {item_type: 1, to_do: dis.attr('data'),companyname:companynames,firstname:firstnames,lastname:lastnames,password:passwords,username:usernames,address:addresses,email:emails,mobile:mobiles,city:citys,state:states,postcode:postcodes,vatnumber:vatnumbers,add_val:1},
                    dataType: "JSON",
				    success: function(result) 
					{
					
					  // alert(result.qry);
	                    if(result.flag == 0)
						{
							$('#refresh_table').load('refreshcompanydetail.php',{test:1},function(){});
							$('#close_modal').trigger('click');
						}else
						{
							alert('Error occured in adding vehicle type1');
						}
                    },
					error: function()
					{
						alert('Error occured23');
					}
                });
				}
				

});



        $('#ActiveButton,#RejectButton').click(function() {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function() {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast one doctor in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 3, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function(result) {
                        alert(result.message);
                        alert(result.test);
                        if (result.flag == 0) {
                            $('.custom_check').each(function() {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        }
                    }
                });

            }
        });

    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>


