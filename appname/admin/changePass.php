<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include('../Models/ConDB.php');
$db = new ConDB();
$doc_id = $_REQUEST['doc_id'];
$pass = $_REQUEST['pass'];
$type = $_REQUEST['type'];

//echo $doc_id.'---'.$pass;
//exit();


function checkPassword($pwd) {

    $errors = array();

    if (strlen($pwd) < 8) {
        $errors[] = "Password too short, 8 characters least!";
    }

    if (!preg_match("#[0-9]+#", $pwd)) {
        $errors[] = "Password must include at least one number!";
    }

    if (!preg_match("#[a-zA-Z]+#", $pwd)) {
        $errors[] = "Password must include at least one letter!";
    }

    return $errors;
}

$strength = checkPassword($pass);

$message = "";

if (count($strength) > 0) {
    foreach ($strength as $err) {
        $message .= $err . "\n";
    }
    $res = array('flag' => 1, 'message' => $message);
    echo json_encode($res);
} else {
    if ($type == 1) {
        $updateQry = "update superadmin set password = '" . md5($pass) . "' where id = " . $doc_id;
        $updateRes = mysql_query($updateQry, $db->conn);
    }


    if (mysql_affected_rows() >= 0) {

        $res = array('flag' => 0, 'message' => 'Password reset completed');
    } else {
        $res = array('flag' => 1, 'message' => $updateQry);
    }

    echo json_encode($res);
}
?>

