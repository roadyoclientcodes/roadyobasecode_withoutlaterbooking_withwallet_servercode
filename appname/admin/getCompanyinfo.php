<?php
session_start();

if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
}
if (isset($_REQUEST['cityid'])) {
    $cityid = $_REQUEST['cityid'];
}
if (isset($_REQUEST['companyid'])) {
    $companyids = $_REQUEST['companyid'];
}
//require('../Models/ConDB.php');
//$db1 = new ConDB();
?>

<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>

<script type="text/javascript">
    $(document).ready(function () {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 12, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null, null, null, null, null, null, null, null]});
    });
</script>
<style>
    #resultmail,#regCompanyErr,#regFirstErr,#regLastErr,#regUserErr,#regPassErr,#regEmailErr,#regPassErr,#regAddressErr,#regMobErr,#regCityErr,#regStateErr,#regPostErr
    {
        color:red;
    }

</style>
<!--<div class="page-content page-content-white" style="margin: 0;">-->
<!--alert = function() {};-->
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>
<div class="content">
    <?php
    if ($status == '1') {
        ?>

        <div style="font-size:20px;"> NEW COMPANY</div>
        <?php
    }
    ?>
    <?php
    if ($status == '3,4') {
        ?>

        <div style="font-size:20px;"> ACCEPTED COMPANY</div>
        <?php
    }
    ?>
    <?php
    if ($status == '5') {
        ?>

        <div style="font-size:20px;"> REJECTED COMPANY</div>
        <?php
    }
    ?>
    <?php
    if ($status == '6') {
        ?>

        <div style="font-size:20px;"> SUSPENDED COMPANY</div>
        <?php
    }
    ?>
    <div style="float:right;">
        <?php
        if ($status == '1' || $status == '5' || $status == '6') {
            ?>

            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="3" data-msg="active">ACTIVATED</button>   

            <?php
        }
        /* if ($status == '1' || $status == '3,4' || $status == '5') {
          ?>

          <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="SuspendButton" data="6" data-msg="suspend">SUSPENDED</button>

          <?php
          } */



        if ($status == '1' || $status == '3,4' || $status == '5') {
            ?>

            <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="RejectButton" data="5" data-msg="inactive">REJECTED</button>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" data="3" data-msg="active" id="addButton"><a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean">ADD</a></button>    
            <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="EditButton" data="5" data-msg="edit">EDIT</button>
            <a href="#modal_default_2" data-toggle="modal" class="btn btn-default btn-block btn-clean" style="display: none;" id="editModal">EDIT</a>

            <?php
        }
        ?>

    </div>
    <div style="float:none;"></div>

    <div id="refresh_table">

        <?php require ('refreshcompanydetail.php'); ?>
    </div>

</div>
<div class="modal" id="modal_default_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">COMPANY FORM</h4>
            </div>
            <div class="modal-body clearfix">

                <div class="controls">
                    <form  action="submitvechiles.php" autocomplete="on" style="color:#000;" method="post" id="add_edit_form" enctype="multipart/form-data"> 

                        <!-- <div id="add_edit_heading"><h1>ADD COMPANY</h1> </div>-->
                        <p> 
                            <label for="text" class="youpasswd" >Company Name <span style="color:red">*</span></label>
                            <input id="companyname" name="companyname" required="required" class="editcompany" type="text" placeholder="Company Name" /> 
                        <div id="regCompanyErr"></div>
                        </p>
                        <p> 
                            <label for="text" class="youpasswd" >First Name <span style="color:red">*</span></label>
                            <input id="firstname" name="firstname" required="required" class="editfirstname" type="text" placeholder="First Name" /> 
                            <span id="regFirstErr"></span>
                        </p>

                        <p> 
                            <label for="text" class="youpasswd" > Last Name <span style="color:red">*</span></label>
                            <input id="lastname" name="lastname" required="required" class="editlastname" type="text" placeholder="Last Name" /> 
                            <span id="regLastErr"></span>
                        </p>
                        <p> 
                            <label for="text" class="youpasswd" > User Name <span style="color:red">*</span></label>
                            <input id="username" name="username" required="required" class="editusername" type="text" placeholder="User Name" />
                            <span id="regUserErr"></span>	
                        </p>
                        <p> 
                            <label for="text" class="youpasswd" >Password<span style="color:red">*</span></label>
                            <input id="password" name="password" required="required" class="editpassword" type="password" placeholder="Password" /> 
                            <span id="regPassErr"></span>
                        </p>
                        <p> 
                            <label for="text" class="youpasswd" >Email<span style="color:red">*</span></label>
                            <input id="email" name="email" required="required" class="editemail" type="text" placeholder="Email" /> 
                            <span id="resultmail"></span>

                        </p>

                        <p>
                            <label for="text" class="youpasswd" >Address<span style="color:red">*</span></label>
                            <input id="address" name="address" required="required"  class="editaddress" type="text" placeholder="Address" /> 
                            <span id="regAddressErr"></span>
                        </p>
                        <p>
                            <label for="text" class="youpasswd" >Mobile<span style="color:red">*</span></label>
                            <input id="mobile" name="mobile"  required="required" class="editmobile" type="text" placeholder="Mobile" /> 
                            <span id="regMobErr"></span>
                        </p>
                        <p>
                            <label for="text" class="youpasswd" >City<span style="color:red">*</span></label>
                            <select name="city" id='city' required="required" class="editcity">
                                <option value="NULL">Select a city</option>
                                <?php
                                $get_country_type = "select * from city_available";
                                $get_country_type_res = mysql_query($get_country_type);
                                while ($typelist = mysql_fetch_array($get_country_type_res)) {
                                    echo "<option value='" . $typelist['City_Id'] . "'>" . $typelist['City_Name'] . "</option>";
                                }
                                ?>
                            </select>

                            <span id="regCityErr"></span>
                        </p>

                        <p>
                            <label for="text" class="youpasswd" >State<span style="color:red">*</span></label>
                            <input id="state" name="state"  required="required" class="editstate" type="text" placeholder="State" /> 
                            <span id="regStateErr"></span>
                        </p>


                        <p>
                            <label for="text" class="youpasswd" >PostCode<span style="color:red">*</span></label>
                            <input id="postcode" name="postcode"  required="required" class="editpostcode" type="text" placeholder="PostCode" /> 
                            <span id="regPostErr"></span>
                        </p>

                        <p>
                            <label for="text" class="youpasswd" >Vat Number</label>
                            <input id="vatnumber" name="vatnumber" required="required" class="editvatnumber" type="text" placeholder="Vat Number" /> 
                            <input type="hidden" name="editdata" id="editdataval" value="1"/>
                        </p>
                    </form>     
                </div>

            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_modal" style="color:red;border:solid 1px #a2a2a2">Close</button>
                <button type="button" class="btn btn-success btn-clean" id="datasubmit">Submit form</button>
            </div>
        </div>
    </div>
</div>    	
</div> 

<script type="text/javascript">
    $(document).ready(function () {
//            alert('1');

        var status = <?php echo json_encode($status); ?>;
        $('.resetPassword').click(function () {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });


        $('#change_pass_doc').click(function () {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {company_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function (result) {
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        } else {
                            alert(result.message);
                        }
                    }
                });
            }
        });

        $('#EditButton').click(function () {
            $('#add_edit_heading').html("<h2>EDIT COMPANY</h2>");
            var dis = $(this);
            var count = 0;
            var values = $('input:checkbox:checked.custom_check').map(function () {
                count++;
                return this.value;
            }).get();
            if (values == '')
            {
                alert("please select one company");
                $('#close_modal').trigger('click');
            } else if (count > 1) {

                alert("please select only one company");
                return false;
            }
            else
            {
                $('#editModal').click();
                $.ajax({
                    type: "POST",
                    url: "submitcompany.php",
                    data: {item_type: 5, item_list: values},
                    dataType: "JSON",
                    success: function (result) {
                        if (result.errFlag == 0) {
                            $('.editcompany').val(result.companyname);
                            $('.editfirstname').val(result.firstname);
                            $('.editlastname').val(result.lastname);
                            $('.editusername').val(result.userame);
                            $('.editpassword').val(result.password);
                            $('.editaddress').val(result.addressline1);
                            $('.editemail').val(result.email);
                            $('.editpostcode').val(result.postcode);
                            $('.editstate').val(result.state);
                            $('.editcity').val(result.city);
                            $('#editdataval').val(result.errFlag);
                            $('.editmobile').val(result.mobile);
                            $('#vatnumber').val(result.vat_number);
                        } else {
                            //alert(result.qry);
                        }


                    },
                    error: function ()
                    {
                        alert("sorry,Error occured");
                    }
                });
            }
        });

        $('#addButton').click(function () {
            $('#editdataval').val('1');
            $('#add_edit_form').find('input').val('');
            $('#add_edit_heading').html("<h2>ADD A VEHICLE TYPE</h2>");

            $('#city option').removeAttr("selected");
        });
        $('#datasubmit').click(function () {
            var status = <?php echo json_encode($status); ?>;
            var city_id = $('#citychange').val();
            var count = 0;

            //Code Ends
            if ($('#companyname').val() === "") {
                $('#companyname').focus();
                $('#regCompanyErr').html("Company name is required");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#firstname').val() === "") {
                $('#firstname').focus();
                $('#regFirstErr').html("First Name is required");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#lastname').val() === "") {
                $('#lastname').focus();
                $('#regLastErr').html("Last name is required");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#username').val() === "") {
                $('#username').focus();
                $('#regUserErr').html("Username is required");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#password').val() === "") {
                $('#password').focus();
                $('#regPassErr').html("Password is required");
                return false;
            }
            if (count > 0)
                return false;

            if ($('#email').val() === "")
            {
                $('#email').focus();
                $('#resultmail').html("email  is required");
                return false;
            }

            if (!/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($('#email').val()))
            {
                $('#email').focus();
                $('#resultmail').html("Email format is wrong.");
                return false;
            }
            if (count > 0)
                return false;


            if (!/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($('#email').val()))
            {
                $('#email').focus();
                $('#resultmail').html("Email format is wrong.");
                return false;
            }
            if (count > 0)
                return false;


            if ($('#address').val() === "")
            {
                $('#address').focus();
                $('#regAddressErr').html("Address is required");
                return false;
            }
            if (count > 0)
                return false;


            var filter = /^[0-9-+]+$/;
            if (!filter.test($('#mobile').val())) {
                $('#mobile').focus();
                $('#regMobErr').html("Mobile Should be a number");
                return false;
            }

            if (count > 0)
                return false;

            if ($('#city').val() === "")
            {
                $('#city').focus();
                $('#regCityErr').html("City is required");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#state').val() === "")
            {
                $('#state').focus();
                $('#regStateErr').html("State is required");
                return false;
            }
            if (count > 0)
                return false;
            if ($('#postcode').val() === "")
            {
                $('#postcode').focus();
                $('#regPostErr').html("Postcode is required");
                return false;
            }
            if (count > 0)
                return false;


            var dis = $(this);
            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            var companynames = $('#companyname').val();
            var firstnames = $('#firstname').val();
            var lastnames = $('#lastname').val();
            var passwords = $('#password').val();
            var usernames = $('#username').val();
            var addresses = $('#address').val();
            var emails = $('#email').val();
            var mobiles = $('#mobile').val();
            var citys = $('#city').val();
            var states = $('#state').val();
            var postcodes = $('#postcode').val();
            var vatnumbers = $('#vatnumber').val();
            if ($('#editdataval').val() != '0')
            {
                $.ajax({
                    type: "POST",
                    url: "submitcompany.php",
                    data: {item_type: 1, item_list: values, to_do: dis.attr('data'), companyname: companynames, firstname: firstnames, lastname: lastnames, password: passwords, username: usernames, address: addresses, email: emails, mobile: mobiles, city: citys, state: states, postcode: postcodes, vatnumber: vatnumbers, add_val: 1},
                    dataType: "JSON",
                    success: function (result)
                    {

//                         alert(result.qrys);
                        //alert(result.flag);
                        if (result.flag == 0)
                        {
                            $('#refresh_table').load('refreshcompanydetail.php', {type: status, test: 1, cityid: city_id}, function () {
//                                alert('data refreshed');
                            });

                            $('#close_modal').trigger('click');
                        } else
                        {
                            alert(result.msg);
                        }
                    },
                    error: function ()
                    {
                        alert('Error occured1');
                    }
                });
            }
            else
            {
                $.ajax({
                    type: "POST",
                    url: "submitcompany.php",
                    data: {item_type: 1, item_list: values, to_do: dis.attr('data'), companyname: companynames, firstname: firstnames, lastname: lastnames, password: passwords, username: usernames, address: addresses, email: emails, mobile: mobiles, city: citys, state: states, postcode: postcodes, vatnumber: vatnumbers, add_val: 2},
                    dataType: "JSON",
                    success: function (result)
                    {
                        // alert('test');
                        $('body').append("<div style='hidden'>" + result.qrys + "</div>");
                        //alert(result.flag);
                        if (result.flag == 0)
                        {
                            $('#refresh_table').load('refreshcompanydetail.php', {type: status, test: 1, cityid: city_id}, function () {
                            });
                            $('#close_modal').trigger('click');
                        } else
                        {
                            alert(result.msg);
                        }
                    },
                    error: function ()
                    {
                        alert('Error occured');
                    }
                });
            }


        });



        $('#ActiveButton,#RejectButton').click(function () {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast one Company in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 3, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function (result) {
//                        alert(result.message);
//                        alert(result.test);
                        if (result.flag == 0) {
                            $('.custom_check').each(function () {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        } else {
                            alert(result.message);
                        }
                    }
                });

            }
        });


        $('#SuspendButton').click(function () {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function () {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast one doctor in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "suspended.php",
                    data: {item_type: 3, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function (result) {
//                        alert(result.message);
//                        alert(result.test);
                        if (result.flag == 0) {
                            $('.custom_check').each(function () {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        } else {
                            alert(result.message);
                        }
                    }
                });

            }
        });

        /*   $(document).on('change', '#citychange', function() {
         
         //$('#citychange').onchange(function() {
         
         var  city_id= $('#citychange').val();
         alert(city_id);
         // ('.').load()
         //  $('#refresh_table').load('cityfilterrefreshcompanydetail.php', {type:<?php echo $status; ?>,cityid: city_id}, function() {
         
         //           });
         });*/

    });
    $(document).on('change', '#citychange', function () {
        var status = <?php echo json_encode($status); ?>;
        var city_id = $('#citychange').val();

        $('#refresh_table').load('cityfilterrefreshcompanydetail.php', {type: status, cityid: city_id}, function () {

//            $('#refresh_table').load('cityfilterrefreshcompanydetail.php', {type:<?php echo $status; ?>,cityid: city_id}, function() {
//                            
        });
    });
</script>
<script>

    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>

