<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include '../Models/ConMongo.php';

$db = new ConMongo();

$iconPath = "http://roadyo.in/roadyo/admin/icons/";

if ($_REQUEST['type'] != '1') {
    $location = $db->mongo->selectCollection('location');
    $userDet = $location->findOne(array('user' => (int) $_REQUEST['id']));

    $switch = ($userDet['status'] != 3) ? (int) $userDet['apptStatus'] : 3;
    $ico = $iconPath . $_REQUEST['ty'];
    switch ($switch) {
        case 6: $icon = $ico . "blue.png";
            break;
        case 7: $icon = $ico . "yellow.png";
            break;
        case 8: $icon = $ico . "red.png";
            break;
        default : $icon = $ico . "green.png";
            break;
    }


    echo json_encode(array('lat' => $userDet['location']['latitude'], 'lon' => $userDet['location']['longitude'], 'icon' => $icon, 'status' => (int) $userDet['status']));
    return TRUE;
}
