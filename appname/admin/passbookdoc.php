<?php
session_start();
include('../Models/ConDB.php');
$db1 = new ConDB();

if (isset($_REQUEST['type'])) {
    $status = $_REQUEST['type'];
} else {
    $status = '1';
}
if (isset($_GET['mas_id'])) {
    $_SESSION['admin_ids'] = $_GET['mas_id'];
}
?>

<script type='text/javascript' src='js/settings.js'></script>
<!--<script type='text/javascript' src='js/plugins_13.js'></script>-->
<script type='text/javascript' src='js/actions.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ($("table.sortable").length > 0)
            $("table.sortable").dataTable({"iDisplayLength": 5, "aLengthMenu": [13, 26, 39, 52, 65], "aaSorting": [], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, null]});
    });
</script>
<!--<div class="page-content page-content-white" style="margin: 0;">-->
<!--alert = function() {};-->
<div class="modal modal-draggable" id="modal_default_12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>                
            <div class="modal-body clearfix" style="text-align:center;">
                <label style="width:150px;">New Password:</label> <input type="text"   name="pass" style="width: 200px;display: inline;" id="chng_pass_doc">&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br><br>
                <label style="width:150px;">Confirm Password:</label> <input type="text" name="conf_pass" style="width: 200px;display: inline;" id="chng_conf_pass_doc" >&nbsp;&nbsp;&nbsp;<span class="icon-cogs" title="Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit"></span><br>
                <input type="hidden" name="sendData" id="sendData_a" value="<?php echo $_SESSION['admin_id']; ?>"/>
                <div style="    margin-top: 10px;    margin-left: 30px;"><span style="color:red;" id="errmsgDoc"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-clean" id="change_pass_doc">Submit</button>              
                <button type="button" class="btn btn-warning btn-clean" data-dismiss="modal" id="change_pass_cancel_doc">Cancel</button>              
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div style="float:right;">
        <?php
        if ($status == '1' || $status == '5') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-success btn-clean" id="ActiveButton" data="3" data-msg="active">ACTIVATE</button>    
            <?php
        }
        if ($status == '1' || $status == '3,4') {
            ?>
            <button type="button" style="margin-right: 80px;" class="btn btn-danger btn-clean" id="RejectButton" data="5" data-msg="inactive">REJECT</button>
            <?php
        }
        ?></div>
    <div style="float:none;"></div>
    <!-- <a href="#" class="widget-icon widget-icon-dark" id="buttonClass" style=" color: rgb(247, 0, 0); background-color: blue;float: right;
        margin-Right: 80px;"><span class="icon-trash"></span></a>-->

    <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable">
        <thead style="font-size: 12px;">
            <tr>


                <th width="8%">DOCUMENT ID</th>
                <th width="8%">DRIVER ID</th> 
                <th width="8%">FIRST NAME</th>   
                <th width="8%">LAST NAME</th> 
                <th width="8%">VIEW</th>



            </tr>
        </thead>
        <tbody style="font-size: 12px;">

            <?php
            $logedIN = ' and u.loggedIN = 1 ';

            $type = ',u.type as type';

            if ($status == '6') {
                $logedIN = $type = '';
            }

            if ($status == '5')
                $logedIN = '';

            $accQry = "select c.first_name,d.driverid,c.last_name,d.url,d.doc_ids,d.expirydate from master c,docdetail d where c.mas_id=d.driverid and doctype=2 and c.mas_id='" . $_SESSION['admin_ids'] . "'";
            $result1 = mysql_query($accQry, $db1->conn);
            //echo $accQry;
            $i = 1;
            while ($row = mysql_fetch_assoc($result1)) {
                $st = "";
                if ($row['status'] == '1') {
                    $st = "NEW";
                } else if ($row['status'] == '3' || $row['status'] == '4') {
                    $st = "ACCEPTED";
                } else if ($row['status'] == '5') {
                    $st = "REJECTED";
                }

                if ($row['profile_pic'] == "") {

                    $st1 = "aa_default_profile_pic.gif";
                } else {
                    $st1 = $row['profile_pic'];
                }
                ?>



                <tr id="doc_rows<?php echo $i; ?>">




                    <td   id="<?Php echo "doc_ids" . $i; ?>"><?Php echo $row['doc_ids'] ?></td>
                    <td   id="<?Php echo "driverid" . $i; ?>"><?Php echo $row['driverid'] ?></td>

                    <td  id="<?Php echo "firstname" . $i; ?>"><?Php echo $row['first_name'] ?></td>
                    <td id="<?Php echo "lastname" . $i; ?>"><?Php echo $row['last_name']; ?></td>
                    <td><button class="button" onClick="window.open('https://docs.google.com/gview?url=<?php echo $db1->host; ?>/admin/document/<?php echo $row['url']; ?>&embedded=true');"><span class="icon">Open</span></button></td>



                </tr>
                <?php
                $i++;
            }
            ?> 

        </tbody>
    </table>               
</div> 

<script type="text/javascript">
    $(document).ready(function() {
//            alert('1');
        $('.resetPassword').click(function() {
            var dis = $(this);
            $('#sendData').val($(dis).attr('data'));
        });


        $('#change_pass_doc').click(function() {

            $('#errmsgDoc').text(' ');
            var pass = $('#chng_pass_doc').val();
            var conf_pass = $('#chng_conf_pass_doc').val();
            if (pass == '' || conf_pass == '') {
                $('#errmsgDoc').text('Both fields are mandatory.');
            } else if (pass != conf_pass) {
                $('#errmsgDoc').text('Passwords does not match, check once.');
            } else if (checkStrengthDoc(pass) == 1) {
                $('#errmsgDoc').text('Password must contain atleast one digit, one Uppercase, one Lower case character and atleast 8 digit');
            } else {

                $.ajax({
                    type: "POST",
                    url: "changePass.php",
                    data: {mas_id: $('#sendData').val(), pass: conf_pass},
                    dataType: "JSON",
                    success: function(result) {
                        alert(result.message);
                        if (result.flag == 0) {

                            $('#chng_pass_doc').val("");
                            $('#chng_conf_pass_doc').val("");

                            $('#change_pass_cancel_doc').trigger('click');

                        }
                    }
                });
            }
        });

        $('#ActiveButton,#RejectButton').click(function() {

            var dis = $(this);

            var values = $('input:checkbox:checked.custom_check').map(function() {
                return this.value;
            }).get();

            if (values == '') {
                alert('Please select  atleast one doctor in the list');
            } else if (confirm('Are you confirm to make ' + dis.attr('data-msg') + '?')) {
                $.ajax({
                    type: "POST",
                    url: "activate_reject.php",
                    data: {item_type: 1, to_do: dis.attr('data'), item_list: values},
                    dataType: "JSON",
                    success: function(result) {
                        alert(result.message);
                        alert(result.test);
                        if (result.flag == 0) {
                            $('.custom_check').each(function() {

                                if ($(this).is(':checked') == true) {
                                    $('#doc_rows' + $(this).attr('dat')).remove();
                                }
                            });
                        }
                    }
                });

            }
        });

    });
    function checkStrengthDoc(password)
    {
        //initial strength
        var strength = 0;

        //if the password length is less than 6, return message.
        if (password.length < 6) {
            return 1;
        }

        //length is ok, lets continue.

        //if length is 8 characters or more, increase strength value
        if (password.length > 7)
            strength += 1;

        //if password contains both lower and uppercase characters, increase strength value
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;

        //if it has numbers and characters, increase strength value
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;

        //if it has one special character, increase strength value
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //if it has two special characters, increase strength value
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;

        //now we have calculated strength value, we can return messages

        //if value is less than 2
        if (strength < 3)
        {
            return 2;
        }
    }
</script>


