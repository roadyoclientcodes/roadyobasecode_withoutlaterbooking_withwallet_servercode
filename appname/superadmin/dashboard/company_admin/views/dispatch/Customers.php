<style>
    .new-container {
        padding-left: 4px;
        padding-right: 2px;
    }
    .new-12{
        padding-right: 0px;
        padding-left: 0px;
    }
    .table > thead > tr > th {
        text-align: -webkit-center;
    }
    .table > tbody > tr > td {
        text-align: -webkit-center;
        vertical-align: inherit;
    }
</style>

<script>
    $(document).ready(function(){
        $('.customer').addClass('active');
      });
</script>
<link rel="stylesheet" href="http://www.jacklmoore.com/colorbox/example3/colorbox.css" />
<script src="http://www.jacklmoore.com/colorbox/jquery.colorbox.js"></script>
<script>
    $(document).ready(function(){
        $(".iframe").colorbox({iframe:true, width:"100%", height:"100%"});
    });
</script>


<div class="tab-pane slide-left" id="slide5">
    <div class="row column-seperation">
        <div class="col-md-12 new-12">



            <div class="container-fluid container-fixed-lg bg-white new-container">
                <!-- START PANEL -->
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <div class="panel-title">
                        </div>
                        <div class="pull-right">
                            <div class="col-xs-12">
                                <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive">
                                <table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                    <thead>
                                    <tr role="row">

                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Places: activate to sort column ascending" style="width: 150px;text-align: ">CUSTOMER ID</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 100px;">PROFILE</th>
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 100px;">CUSTOMER NAME</th>

                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 175px;">PHONE</th>
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">EMAIL</th>
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">Joined</th>
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">Last active</th>

                                    </tr>
                                    </thead>
                                    <tbody>






                                    <?php


                                    foreach ($customers as $result) {
                                        ?>



                                        <tr role="row"  class="gradeA odd">
                                            <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result->slave_id; ?></p></td>

                                            <?php if($result->profile_pic){?>
                                            <td class="v-align-middle"><img src="<?php echo base_url()."../pics/xxhdpi/".$result->profile_pic;?>" style="width: 40px;height: 40px;border-radius: 30px;"></td>
                                            <?php }else {?>
                                            <td class="v-align-middle"><img src="http://104.236.41.101/tutree/pics/aa_default_profile_pic.gif" style="width: 40px;height: 40px;border-radius: 30px;"></td>
                                            <?php }?>
                                            <td class="v-align-middle"><?php echo $result->first_name.$result->last_name; ?></td>
                                            <td class="v-align-middle"><?php echo  $result->phone; ?></td>
                                            <td class="v-align-middle"><?php echo $result->email; ?></td>
                                            <td class="v-align-middle"><?php echo date("M d Y g:i A", strtotime($result->created_dt)); ?></td>
                                            <td class="v-align-middle"><?php echo date("M d Y g:i A", strtotime($result->last_active_dt)); ?></td>

                                        </tr>

                                        <?php

                                    }
                                    //                                            ?>

                                    </tbody>
                                </table>

                            </div><div class="row"><div></div></div></div>
                    </div>
                </div>
                <!-- END PANEL -->
            </div>






        </div>

    </div>
</div>


<!--this is the end of customers tab-->



<!--the div which we needs to close is it follows-->
</div>





</div>








</div>




</div>









</div>

