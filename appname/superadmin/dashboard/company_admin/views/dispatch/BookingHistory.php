
<style>
    .new-container {
        padding-left: 4px;
        padding-right: 2px;
    }
    .new-12{
        padding-right: 0px;
        padding-left: 0px;
    }
</style>
<script>
    $(document).ready(function(){
        $('.bookingH').addClass('active');
    });
</script>
<link rel="stylesheet" href="http://www.jacklmoore.com/colorbox/example3/colorbox.css" />
<script src="http://www.jacklmoore.com/colorbox/jquery.colorbox.js"></script>
<script>
    $(document).ready(function(){
        $(".iframe").colorbox({iframe:true, width:"100%", height:"100%"});

        $('#Sortby').change(function(){


            $.ajax({
                type: 'post',
                url: '<?php echo base_url('index.php/dispatch/BookingHistoryController_ajax')?>/'+$(this).val(),
                dataType:"JSON",
                success: function (result) {


                    var t = $('#tableWithSearch').DataTable();
                    t
                        .clear()
                        .draw();
//
                   var status = '';
                    $.each(result , function(index , row1) {

                        if (row1.status == '1')
                            status = 'request';
                        else if (row1.status == '2')
                            status = 'accepted.';
                        else if (row1.status == '3')
                            status = 'rejected.';
                        else if (row1.status == '4')
                            status = 'Passenger has cancelled.';
                        else if (row1.status == '5')
                            status ='Driver has canceled';
                        else if (row1.status == '6')
                            status = 'Driver on the way.';
                        else if (row1.status == '7')
                            status = 'Appointment started.';
                        else if (row1.status == '8')
                            status ='Driver Arrived';
                        else if (row1.status == '9')
                            status = 'Appointment completed.';
                        else if (row1.status == '10')
                            status = 'Appointment Timed out.';
                        else
                            status = 'Status unavailable.';


                         t.row.add([
                             row1.appointment_id,
                             row1.pessanger_fname+row1.pessanger_lname,
                             row1.phone,
                             row1.appointment_dt,
                             row1.address_line1,
                             row1.drop_addr1,
                             status

                        ]).draw();//.node(); //.order( [[ 0, 'desc' ]] )
                    });
//
                   }



            });

        });

    });
</script>


<div class="tab-pane slide-left" id="slide5">
    <div class="row column-seperation">
        <div class="col-md-12 new-12">



            <div class="container-fluid container-fixed-lg bg-white  new-container">
                <!-- START PANEL -->
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <div class="panel-title">
                        </div>
                        <div class="pull-left">
                            <div class="col-xs-12">

                                <select  class="full-width select2-offscreen" id="Sortby" data-init-plugin="select2" tabindex="-1" title="select" >
                                    <option value="1" >New Request</option>
                                    <option value="9" selected>Completed</option>
                                    <option value="3" >Canceled By Driver</option>
                                    <option value="4" >Canceled By Passenger</option>
                                    <option value="10" >Expired</option>


                                </select>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="col-xs-12">
                                <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive">
                                <table class="table table-hover  table-detailed dataTable no-footer " id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                    <thead>
                                    <tr role="row">
<!--                                        <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;">SLNO</th>-->
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Places: activate to sort column ascending" style="width: 275px;">BOOKING ID</th>
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 304px;">PASSENGER NAME</th>
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 175px;">PHONE</th>
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">PICKUP D & T</th>
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">PICKUP ADDRESS</th>
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">DROP ADDRESS</th>
<!--                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">VEHICLE TYPE</th>-->
                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">STATUS</th>

                                    </tr>
                                    </thead>
                                    <tbody>






                                    <?php
//                                    $slno = 1;

                                    foreach ($aap_booking as $result) {
                                        ?>



                                        <tr role="row"  class="gradeA odd">
<!--                                            <td id = "d_no" class="v-align-middle sorting_1"> <p>--><?php //echo $slno; ?><!--</p></td>-->
                                            <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result->appointment_id; ?></p></td>
                                            <td class="v-align-middle"><?php echo $result->pessanger_fname.' '.$result->pessanger_lname; ?></td>
                                            <td class="v-align-middle"><?php echo  $result->phone; ?></td>
                                            <td class="v-align-middle"><?php echo date("M d Y g:i A", strtotime($result->appointment_dt)); ?></td>
                                            <td class="v-align-middle"><?php echo  $result->address_line1; ?></td>
                                            <td class="v-align-middle"><?php echo  $result->drop_addr1; ?></td>
<!--                                            <td class="v-align-middle">BMW</td>-->
                                            <?php
                                            if ($result->status == '1')
                                                $status = 'request';
                                            else if ($result->status == '2')
                                                $status = $result->driver_fname.' accepted.';
                                            else if ($result->status == '3')
                                                $status = $result->driver_fname.' rejected.';
                                            else if ($result->status == '4')
                                                $status = 'Passenger has cancelled.';
                                            else if ($result->status == '5')
                                                $status = $result->driver_fname.' '.$result->driver_lname.' has canceled';
                                            else if ($result->status == '6')
                                                $status = $result->driver_fname.' on the way.';
                                            else if ($result->status == '7')
                                                $status = 'Appointment started.';
                                            else if ($result->status == '8')
                                                $status = $result->driver_fname.' Arrived';
                                            else if ($result->status == '9')
                                                $status = 'Appointment completed.';
                                            else if ($result->status == '10')
                                                $status = 'Appointment Timed out.';
                                            else
                                                $status = 'Status unavailable.';
                                            ?>
                                            <td class="v-align-middle"><?php echo $status; ?></td>

                                        </tr>

                                        <?php
                                        $slno++;
                                    }
                                    //                                            ?>

                                    </tbody>
                                </table>

                            </div><div class="row"><div></div></div></div>
                    </div>
                </div>
                <!-- END PANEL -->
            </div>






        </div>

    </div>
</div>


<!--this is the end of customers tab-->



<!--the div which we needs to close is it follows-->
</div>





</div>








</div>




</div>









</div>

