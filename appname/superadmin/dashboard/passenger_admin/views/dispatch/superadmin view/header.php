<?php require_once 'language.php'; ?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>Pages - Admin Dashboard UI Kit</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>theme/pages/ico/60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>theme/pages/ico/76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>theme/pages/ico/120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>theme/pages/ico/152.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN Vendor CSS-->
        <link href="<?php echo base_url(); ?>theme/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/cssextra/style.css" rel="stylesheet" type="text/css" media="screen"/>
        <!-- BEGIN Pages CSS-->
        <link rel="stylesheet" href="<?php echo base_url()?>theme/assets/css/jquery-ui.css" type="text/css" media="screen"/>
        <link href="<?php echo base_url(); ?>theme/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" />
        <link class="main-stylesheet" href="<?php echo base_url(); ?>theme/pages/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet" type="text/css" media="screen" />

        <script src="<?php echo base_url(); ?>theme/assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
        <!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->


        <link href="<?php echo base_url(); ?>theme/assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
        <link href="<?php echo base_url(); ?>theme/assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />

        <!--<link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">-->

        <!--[if lte IE 9]>
            <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
        <![endif]-->

        <script type="text/javascript">
            window.onload = function () {
                // fix for windows 8
                if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                    document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/pages/css/windows.chrome.fix.css" />'
            }
        </script>
        <style>
            span .title{

                width: 100% !important;
            }
        </style>

    </head>

    <body class="fixed-header">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar" data-pages="sidebar">
            <div id="appMenu" class="sidebar-overlay-slide from-top">
            </div>
            <!-- BEGIN SIDEBAR HEADER -->
            <div class="sidebar-header">

                <div class="sidebar-header-controls " class="pull-left" style="margin-left:-60px" >
<!--                    <button data-pages-toggle="#appMenu" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" type="button"><i class="fa fa-angle-down fs-16"></i></button>
                    <button data-toggle-pin="sidebar" class="btn btn-link visible-lg-inline" type="button"><i class="fa fs-12"></i></button>-->
                    <h3 style="color:white" ><?php echo APP_NAME; ?></h3>
                </div>
                <div class="pull-right m-l-20">
                    <img   src="<?php echo base_url(); ?>theme/assets/img/logo.png" alt="logo" class="brand" data-src="<?php echo base_url(); ?>theme/assets/img/logo.png" data-src-retina="<?php echo base_url(); ?>theme/assets/img/logo.png" width="93" height="25" >
                </div>
            </div>
            <!-- END SIDEBAR HEADER -->
            <!-- BEGIN SIDEBAR MENU -->
            <div class="sidebar-menu">
                <ul class="menu-items">
                    <?php $request_uri = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>
                    <!--                    <li class="m-t-30">
                                            <a href="<?php echo base_url(); ?>index.php/admin/loadDashbord" class="detailed">
                                                <span class="title">PROFILE</span>
                                                <span class="details">234 notifications</span>
                                            </a>
                                            <span class="icon-thumbnail bg-success"><i class="pg-home"></i></span>
                                        </li>-->
                    <li class="dashboard">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/Dashboard">
                            <span class="title"><?php echo NAV_DASHBOARD; ?></span>
                        </a>
                        <span class="icon-thumbnail <?php echo (base_url() . "index.php/superadmin/Dashboard" == $request_uri ? "bg-success" : ""); ?>"><i class="pg-home"></i></span>
                    </li>


                    <!--                    <li class="">-->
                    <!--                        <a href="--><?php //echo base_url();     ?><!--index.php/superadmin/services">-->
                    <!--                            <span class="title">Services</span>-->
                    <!--                            <!--<span class="details">Details</span>-->
                    <!--                        </a>-->
                    <!--                        <span class="icon-thumbnail"><i class="pg-bag"></i></span>-->
                    <!--                    </li>-->
                    <!--                    <li class="">-->
                    <!--                        <a href="--><?php //echo base_url();     ?><!--index.php/superadmin/Banking">-->
                    <!--                            <span class="title">Banking</span>-->
                    <!--                            <!--<span class="details">Details</span>-->
                    <!--                        </a>-->
                    <!--                        <span class="icon-thumbnail"><i class="pg-calender"></i></span>-->
                    <!--                    </li>-->


                    <li class="cities">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/cities">
                            <span class="title"><?php echo NAV_CITIES; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail cities_thumb" ><i class="pg-charts"  class="cities"></i></span>
                    </li>


                    <li class="company_s">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/company_s/1">
                            <span class="title"><?php echo NAV_COMPANYS; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail company_sthumb"><i class="pg-charts" class="company_s"></i></span>
                    </li>



                    <li class="vehicle_type">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/vehicle_type">
                            <span class="title"><?php echo NAV_VEHICLETYPES; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail vehicletype_thumb"><i class="pg-charts" class="vehicle_type"></i></span>
                    </li>

                    <li class="Vehicles">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/Vehicles/5">
                            <span class="title"><?php echo NAV_VEHICLES; ?></span>
                        </a>
                        <span class="icon-thumbnail vehicles_thumb"><i class="pg-form" class="Vehicles" ></i></span>
                    </li>




                    <li class="Drivers">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/Drivers/my/1">
                            <span class="title"><?php echo NAV_DRIVERS; ?></span>
                        </a>
                        <span class="icon-thumbnail  driver_thumb"><i class="pg-map" class="Drivers"></i></span>
                    </li>

                    <li class="passengers">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/passengers/3">
                            <span class="title"><?php echo NAV_PASSENGERS; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail passengers_thumb"><i class="pg-charts" class="passengers"></i></span>
                    </li>



                    <li class="bookings">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/bookings/11">
                            <span class="title"><?php echo NAV_BOOKINGS; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail booking_thumb"><i class="pg-charts" class="bookings"></i></span>
                    </li>

<!--                    <li class="cancled_booking">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/cancled_booking">
                            <span class="title "><?php echo NAV_CANCLEDBOOKINGS; ?></span>
                            <span class="details">Details</span>
                        </a>
                        <span class="icon-thumbnail cancledbooking_thumb"><i class="pg-charts" class="cancled_booking"></i></span>
                    </li>-->

                    <li class="dispatches">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/dispatched/1">
                            <span class="title"><?php echo NAV_DISPATCHERS; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail dispatches_thumb"><i class="pg-charts" class="dispatches"></i></span>
                    </li>




                    <li class="payroll">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/payroll">
                            <span class="title"><?php echo NAV_PAYROLL; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail payroll_thumb"><i class="pg-grid" class="payroll"></i></span>
                    </li>


                    <li class="transection">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/transection">
                            <span class="title"><?php echo NAV_ACCOUNTING; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail"><i class="pg-charts"></i></span>
                    </li>



<!--                    <li class="document">-->
<!--                        <a href="--><?php //echo base_url(); ?><!--index.php/superadmin/document/1">-->
<!--                            <span class="title">--><?php //echo NAV_DOCUMENT; ?><!--</span>-->
<!--                            <!--<span class="details">Details</span>-->
<!--                        </a>-->
<!--                        <span class="icon-thumbnail document_thumb"><i class="pg-charts" class="document"></i></span>-->
<!--                    </li>-->

                    <li class="driver_review">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/driver_review/1">
                            <span class="title"><?php echo NAV_DRIVERREVIEW; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail driver_review_thumb"><i class="pg-charts" class="driver_review"></i></span>
                    </li>


                    <li class="passenger_rating">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/passenger_rating">
                            <span class="title"><?php echo NAV_PASSENGERRATING; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail passenger_rating_thumb"><i class="pg-charts" class="passenger_rating"></i></span>
                    </li>


                    <li class="disputes">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/disputes/1">
                            <span class="title"><?php echo NAV_DISPUTES; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail disputes_thumb"><i class="pg-charts" class="disputes"></i></span>
                    </li>

                    <li class="vehicle_models">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/vehicle_models/1">
                            <span class="title"><?php echo NAV_VEHICLEMODELS; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail vehicle_models_thumb"><i class="pg-charts" class="vehicle_models"></i></span>
                    </li>






<!--                    <li class="finance">-->
<!--                        <a href="--><?php //echo base_url(); ?><!--index.php/superadmin/finance/1">-->
<!--                            <span class="title">--><?php //echo NAV_FINANCE; ?><!--</span>-->
<!--                            <!--<span class="details">Details</span>-->
<!--                        </a>-->
<!--                        <span class="icon-thumbnail finance_thumb"><i class="pg-charts" class="finance"></i></span>-->
<!--                    </li>-->






                    <li class="delete">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/delete">
                            <span class="title"><?php echo NAV_DELETE; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail delete_thumb"><i class="pg-charts"  class="delete"></i></span>
                    </li>

                    <li class="godsview">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/godsview">
                            <span class="title"><?php echo NAV_GODSVIEW; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail godsview_thumb"><i class="pg-charts" class="godsview"></i></span>
                    </li>



                    <li class="compaigns">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/compaigns/1">
                            <span class="title"><?php echo NAV_COMPAIGNS; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail compaigns_thumb"><i class="pg-charts" class="compaigns"></i></span>
                    </li>


                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- END SIDEBAR MENU -->
        </div>
















