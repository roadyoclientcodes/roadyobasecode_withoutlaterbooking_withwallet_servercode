<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");


define("APP_NAME", "OPA");

/*
 * Navigation items
 */
define("LIST_COMPANY_MOBIFY", "Only Numbers Are Allowed");


define("SELECT_BY_DRIVER", "Select By Driver");

define("NAV_DASHBOARD", "DASHBOARD");
define("NAV_CITIES", "CITIES");
define("NAV_COMPANYS", "COMPANIES");
define("NAV_VEHICLETYPES", "VEHICLE TYPES");
define("NAV_VEHICLES", "VEHICLES");
define("NAV_DRIVERS", "DRIVERS");
define("NAV_PASSENGERS", "PASSENGERS");
define("NAV_BOOKINGS", "ALL BOOKINGS");
define("NAV_CANCLEDBOOKINGS", "CANCLED BOOKINGS");
define("NAV_DISPATCHERS", "DISPATCHERS");
define("NAV_PAYROLL", "PAYROLL");
define("NAV_ACCOUNTING", "ACCOUNTING");
define("NAV_DOCUMENT", "DOCUMENTS");
define("NAV_DRIVERREVIEW", "DRIVER REVIEWS");
define("NAV_PASSENGERRATING", "PASSENGER RATINGS");
define("NAV_DISPUTES", "DISPUTES");
define("NAV_VEHICLEMODELS", "VEHICLE MODELS");
define("NAV_FINANCE", "MARKETING");
define("NAV_DELETE", "DELETE");
define("NAV_GODSVIEW", "GODSVIEW");
define("NAV_COMPAIGNS", "CAMPAIGNS");

/*
 * headings
 */
define("HEAD_DASHBOARD", "Dashboard ");
define("HEAD_CITIES", "Cities ");
define("HEAD_COMPANYS", "Company's ");
define("HEAD_VEHICLETYPE", "Vehicle type ");
define("HEAD_VEHICLES", "Vehicles ");
define("HEAD_DRIVERS", "Drivers ");
define("HEAD_PASSENGERS", "Passengers ");
define("HEAD_BOOKINGS", "Bookings ");
define("HEAD_CANCLEDBOOKINGS", "Cancelled Bookings ");
define("HEAD_DISPATCHERS", "Dispatchers ");
define("HEAD_PAYROLL", "Payroll ");
define("HEAD_ACCOUNTING", "Accounting");

define("HEAD_DOCUMENT", "Document ");
define("HEAD_DRIVERREVIEW", "Driver Review ");
define("HEAD_PASSENGERRATING", "Passenger Rating");
define("HEAD_DISPUTES", "Disputes ");
define("HEAD_VEHICLEMODEL", "Vehicle Model ");
define("HEAD_COMPAIGNS", "Campaigns ");
define("HEAD_GODSVIEW", "GodzView ");
define("HEAD_FINANCE", "Finance ");
define("HEAD_DELETE", "Delete ");

define("SELECT_COUNTRY_ANDCITY", "Select City And Country");


/*
 * LIST ELEMENTS
 */
define("LIST_RESETPASSWORD_HEAD", "Change Password");
define("FIELD_COMPAIGNS_TITLE", "Title");


//define("VEHICLE_TYPE", "New");



define("LIST_NEW", "New");
define("LIST_ACCEPTED", "Accepted ");
define("LIST_REJECTED", "Rejected ");
define("LIST_SUSPENDED", "Suspended ");



define("LIST_UNDERREVIEW", "Under review");
define("LIST_ACCEPTEDFREE", "Accepted & Free");
define("LIST_REJECTED", "Rejected");
define("LIST_ACTIVE", "Active");
define("LIST_INACTIVE", "Inactive");



define("LIST_FREE", "Free");
define("LIST_FREEONLINE", "Online & Free");
define("LIST_OFFLINE", "Offline");
define("LIST_BOOKED", "Booked");


define("LIST_DRIVERSLICENSE", "Drivers Licence");
define("LIST_BANKPASSBOOK", "Bank Passbook");
define("LIST_CARRIAGEPERMIT", "Carriage Permit");
define("LIST_CR", "Certificate Of Registration");
define("LIST_INSURENCECERTIFICATE", "Insurance Certificate");

define("LIST_PASSENGERRATING", "Passenger Rating");

define("LIST_REPORTED", "Reported");

define("LIST_VEHICLEMAKE", "Vehicle Make");
define("LIST_VEHICLEMODELS", "Vehicle Models");

define("LIST_REFERRALS", "Referrals");
define("LIST_PROMOTIONS", "Promotions");

define("LIST_CITIES", "Cities");
define("LIST_OF_CITIES", "List of Cities");
define("LIST_ADDCITIES", "Add City");
define("LIST_ADD_COUNTRY_DETAILS", "Add Country Details");
define("LIST_ADD_CITY_DETAILS", "Add City Details");

define("LIST_COMPANY", "COMPANY");
define("LIST_ADD_COMPANY_DETAILS", "Add Company Details");
define("LIST_EDIT_COMPANY_DETAILS", "Edit Company Details");

define("LIST_VEHICLETYPE", "Vehicle Type");
define("LIST_ADD_VEHICLETYPE_DETAILS", "Add Vehicle Details");
define("LIST_EDIT_VEHICLETYPE_DETAILS", "Edit Vehicle Details");

define("LIST_VEHICLE", "Vehicle");
define("LIST_VEHICLE_ADD", "Add New");
define("LIST_VEHICLE_VEHICLESETUP", "Vehicle Setup");
define("LIST_VEHICLE_DETAILS", "Details");
define("LIST_VEHICLE_DOCUMETS", "Documents");

define("LIST_DRIVER", "DRIVER");
define("LIST_DRIVER_ADDNEW", "ADD NEW");
define("LIST_DRIVER_PESIONALDETAILS", "PERSONAL DETAILS");
define("LIST_DRIVER_LOGINDETAILS", "LOGIN DETAILS");
define("LIST_DRIVER_DRIVINGLICENCE", "DRIVING LICENSE");



define("LIST_DISPATCHERS_ADDMODEL", "Select country and city");
define("LIST_DISPATCHERS_EDITMODEL", "Select city");

define("LIST_PASSENGERRATING_EDITMODEL", "PASSENGER RATING");

define("LIST_DISPUTES_REPORTED", "REPORTED");
define("LIST_DISPUTES_RESOLVED", "RESOLVED");
define("LIST_DISPUTES_EDITDISPUTE", "Edit Dispute");


define("LIST_DISPUTES", "ADD VEHICLE TYPE");

define("LIST_COMPAIGNS_HEAD", "Add Referral");

define("VEHICLEMAKE_ADDVEHICLE", "Add Vehicle make");
define("VEHICLEMODEL_ADDVEHICLE", "Add Vehicle model");
define("SELECT_COMPANY", "select company");




/*
 *BUTTONS
 */
define("COMPAIGNS_DISPLAY", "Are you sure to deactivate");
define("VEHICLEMODEL_DELETE", "Are you sure to delete");
define("BUTTON_YES", "Yes ");
define("BUTTON_OK", "OK ");

define("SEARCH", "Search ");

define("SELECT", "Select ");

define("LIST_RESETPASSWORD_DRIVERDOCUMENTS", "Driver Documents");
define("DRIVERS_TABLE_DRIVER_DOCUMENT", "DOCUMENT TYPE");
 define("DRIVERS_TABLE_DRIVER_EXPIREDATE", "EXPIRY DATE");
define("DRIVERS_TABLE_DRIVER_VIEW", "VIEW/DOWNLOAD");



define("BUTTON_ADD", "Add ");
define("POPUP_VEHICLE_MAKE", "Are You Sure to Delete VehicleMake ");
define("BUTTON_ACTIVATE", "Activate ");
define("BUTTON_ACCEPT", "Accept ");
define("BUTTON_ACTIVE", "Active ");
define("BUTTON_REJECT", "Reject");
define("BUTTON_DEACTIVATE", "Deactivate ");
define("BUTTON_INACTIVE", "Inactivate ");
define("BUTTON_DEACTIVE", "Deactive ");
define("BUTTON_EDIT", "Edit ");
define("BUTTON_SUSPEND", "Suspend ");
define("BUTTON_DELETE", "Delete ");
define("BUTTON_CITIES", "Manage cities and countries");

define("BUTTON_RESOLVE", "Resolve");

define("BUTTON_RESETPASSWORD", "Reset password");

define("BUTTON_ADDCOUNTRY", "Add Country");
define("BUTTON_ADDCITY", "Add City");
define("BUTTON_CANCEL", "Cancel");
define("BUTTON_ADD_COMPANY", "Add Company");
define("BUTTON_EDIT_COMPANY", "Edit Company");

define("BUTTON_ADD_VEHICLETYPE", "Add Vehicle type");

define("BUTTON_EDIT_VEHICLETYPE", "Edit Vehicle Type");

define("BUTTON_NEXT", "NEXT");
define("BUTTON_PREVIOUS", "PREVIOUS");
define("BUTTON_FINISH", "FINISH");

define("BUTTON_SUBMIT", "Submit");

/*
 * Popups
 */
define("POPUP_RESETPASSWORD_ANYONE", "Select any one to reset the password ");

define("POPUP_SELECT_CITY", "Please Select the city");
define("POPUP_SELECT_COUNTRY", "Please Select the country");


define("POPUP_DRIVER_ALERTTAB", "complete Personal Details tab properly");
define("POPUP_DRIVER_ALERTLOGINTAB", "complete Login Details tab properly");
define("POPUP_DRIVER_ALERTLOGINTABDRIVING", "complete Driving Licence tab properly");

define("POPUP_SELECT_COUNTRY", "Please Select the country");


define("POPUP_CITIES_ENTER_COUNTRY_NAME", "Please Enter the Country Name ");
define("POPUP_CITIES_COUNTRY_ADDED", "Country Added Successfully ");

define("POPUP_CITIES_CITY_ENTER", "Please Enter The City Name");
define("POPUP_CITIES_CITY_ENTERALPHA", "Please Enter The City Name as Alphabets Only");
define("POPUP_CITIES_CITY_CURENCY", " Enter The Currency With 3 Characters Only");
define("POPUP_CITIES_CITY_ADDED", "City Added Successfully ");
define("POPUP_CITIES_CITY_EXIST", "City Already Exist ");
define("POPUP_LAT_LONG_UPDATED", "Your Lot Long Updated Successfully");
define("POPUP_CITYTT_UPDATED", "Your city updated successfully");
define("POPUP_LAT_LONG_ADDED", "Your City Added Successfully");
define("POPUP_LAT_LONG_DELETED", "Your City Deleted successfully");
define("POPUP_COUNTRY_ADDED", "Your Country Added successfully");
define("POPUP_CITY_ADDED", "Your city Added Successfully");
define("POPUP_LAT_LONG_UPDATE_FAILED", "Your lot long update failed");
define("POPUP_ALPHABET", "Please Enter Alphabet");


define("POPUP_ENTER", "please enter the data");
define("POPUP_COMPANY_NAME", "Please Enter The Company Name");
define("POPUP_COMPANY_NAMEVALID", "Enter The Company Name as text/number or both but not a special characters");
define("POPUP_COMPANY_PASSWORD", "Enter the password");
define("POPUP_COMPANY_EMAIL", "Please enter your email number");







define("POPUP_DRIVER_FIRSTNAME", "Please enter the driver name");
define("POPUP_DRIVER_LASTNAME", "Please enter the last name");
define("POPUP_DRIVER_MOBILE", "Please enter the mobile number");
define("POPUP_DRIVER_DRIVERPHOTO", "Please upload a driver photo");

define("POPUP_DRIVER_DRIVER_EMAIL", "Please enter the email ");
define("POPUP_DRIVER_DRIVER_YOUREMAIL", "Please enter a valid email ");
define("POPUP_DRIVER_DRIVER_ALLOCATED", "Email is already allocated !");
define("POPUP_DRIVER_DRIVER_PASSWORD", "Please enter the password ");
define("POPUP_DRIVER_DRIVER_PASSWORD_VALID", "Please enter the password with atleast one number and character");
define("POPUP_DRIVER_DRIVER_ZIPCODE", "Please enter the zipcode ");



define("POPUP_ADDCOMPANY_NAME", "Please select the company");
define("POPUP_ADDCITY__NAME", "Please select  the last city");
define("POPUP_SELECT_TYPE", "Please select the vehicle type");
define("POPUP_SELECT_VEHICLEMAKE", "Please select  the vehicle  make");
define("POPUP_SELECT_VEHICLEMODAL", "Please select  the vehicle  modal");
define("POPUP_SELECT_VEHICLEIMAGE", "Please select  vehicle image");

define("POPUP_SELECT_VEHICLEREGNO", "Please enter the vehicle reg number");
define("POPUP_SELECT_VEHICLEPLATENO", "Please enter the vehicle plate number");
define("POPUP_SELECT_VINSURENCENUMBER", "Please enter the vehicle  insurence number");
define("POPUP_SELECT_VEHICLECOLOR", "Please enter the vehicle color");
define("POPUP_SELECT_VEHICLEIMAGE", "Please select  vehicle image");


define("POPUP_SELECT_VEHICLEUPLOADREGNO", "Please upload the registration certificate");
define("POPUP_SELECT_VEHICLE_DATE", "Please select the expire date ");
define("POPUP_SELECT_VINSURENCENUMBER_INSURENCE", "Please upload  the  motor insurence certificate");
define("POPUP_SELECT_VEHICLECOLOR_CARRIAGE_PERMIT", "Please upload the carriage permit certificate");




define("POPUP_COMPANY_EMAILVALID", "Please Enter A Valid Email");
define("POPUP_COMPANY_ADDRESS", "Enter the address");
define("POPUP_COMPANY_ADDED_D", "your company added successfully");
define("POPUP_COMPANY_EXIST", " Company Already Register With This Email Id ");
define("POPUP_COMPANY_EDITED_D", "your company edited successfully");
define("POPUP_COMPANY_SELECT", "Select the city");
define("POPUP_COMPANY_VATNUMBER", "Enter the vat number");
define("POPUP_COMPANY_PPCODE", "Enter valid pincode number");
define("POPUP_COMPANY_VATNUMBERNUM", "Enter the valid vat number");
define("POPUP_COMPANY_ADDED", "Your company added successfully");


define("POPUP_COMPANY_DELETEVEHICLE", "Please select vehicle type to delete");
define("POPUP_COMPANY_DELETECOMPANY", "Please select company to delete");
define("POPUP_COMPANY_DELETECOUNTRY", "please select country to delete");
define("POPUP_COMPANY_DELETEDRIVER", "please select driver to delete");
define("POPUP_COMPANY_LOGOUTDRIVER", "please select driver to logout");

define("POPUP_COMPANY_SURELOGOUTDRIVER", "Are you sure to logout this driver");

//define("POPUP_COMPANY_VATNUMBER", "Enter the vat number");
//define("POPUP_COMPANY_VATNUMBERNUM", "Enter the valid vat number");
//define("POPUP_COMPANY_ADDED", "Your company added successfully");


define("POPUP_VEHICLE_TAB_D", "complete vehicle setup tab properly");
define("POPUP_VEHICLE_DETAILTAB_D", "complete details tab properly");




define("POPUP_COMPANY_ATLEASTONENAME", "Please select  Atleast one Company Name");
define("POPUP_ACCEPTED", "Are you sure you wish to accept this company and have validated all of its documents ,etc ? ");
define("POPUP_REJECTED", "Are you sure you wish to reject this company and have validated all of its documents ,etc ? ");
define("POPUP_SUSPENDED", "Are you sure you wish to suspend this company and have validated all of its documents ,etc ? ");
define("POPUP_DELETE", "Are you sure you wish to delete this company and have validated all of its documents ,etc ? ");
;


define("POPUP_DELETEVEHICLETYPE", "Are you sure to delete this vehicle type? ");
define("POPUP_DELETECOMPANY", "Are you sure to delete this company? ");
define("POPUP_DELETECOUNTRY", "Are you sure to delete this country ? ");
define("POPUP_DELETEDRIVER", "Are you sure to delete this driver ? ");
define("POPUP_DELETEDVEHICLEMODAL", "Are you sure to delete this vehiclemodal ? ");
define("POPUP_DELETEVEHICLEMODAL", "Please select vehicle modal to delete ? ");






define("POPUP_COMPANY_FIRST_NAME", "Please Enter The First Name");
define("POPUP_COMPANY_LAST_NAME", "Please Enter The Last Name");
define("POPUP_COMPANY_MOBILE", "Please Enter Your mobile Name");
define("POPUP_COMPANY_CTATE_NAME", "Please Enter The State Name");
define("POPUP_COMPANY_PINCODE_NAME", "Please Enter The Pincode");
define("POPUP_COMPANY_ADDED", "Your company Updated successfully");

define("POPUP_CANCEL", "Are you sure to Cancel the data ");

define("POPUP_COMPANY_MOBILE", "Please enter the mobile number ");

define("POPUP_CONFIRM", "Are you sure to delete companies");
define("POPUP_VEHICLETYPE", "Are you sure to delete Vehicle model");
define("POPUP_VEHICLES", "Are you sure to delete Vehicles");
define("POPUP_DRIVERS", "Are you sure to delete Drivers");
define("POPUP_PASSENGERS", "Are you sure to inactive passengers");
define("POPUP_DISPATCHERS", "Are you sure to inactive dispatchers");
define("POPUP_DRIVERREVIEW_INACTIVE", "Are you sure to inactive driver reviews");
define("POPUP_DRIVERREVIEW_ACTIVE", "Are you sure to active driver reviews");
define("POPUP_VEHICLEMODEL", "are you sure to delete vehicletypes");

define("POPUP_COMPANY_ATLEAST", "please select atleast one company");
define("POPUP_CITY_ATLEAST", "please select atleast one city");


define("POPUP_VEHICLETYPE_ATLEAST", "please select atleast one vehicle type");

define("POPUP_COMPANY_ACTIVATED", "Your selected company/companys activated successfully");
define("POPUP_COMPANY_DEACTIVATED", "Your selected company/companys deactivated successfully");
define("POPUP_COMPANY_SUSPENDED", "Your selected company/companys Suspended successfully");
define("POPUP_COMPANY_DELETED", "Your selected company/companys Deleted successfully");

define("POPUP_COMPANY_ANYONE", "Please select any one company");
define("POPUP_COMPANY_ONLYONE", "Please select only one company to edit");

define("POPUP_CITY_ANYONE", "Please select any one city");
define("POPUP_CITY_ONLYONE", "Please select only one city to edit");
define("POPUP_CITY_LAT", "Please enter the latitude");
define("POPUP_CITY_LATVAL", "Please enter valid data at latitude");
define("POPUP_CITY_LOT", "Please enter the longitude");
define("POPUP_CITY_LONVAL", "Please enter valid data at longitude");



define("POPUP_COMPANY_ONLYONE", "Please select only one company to edit");

define("POPUP_VEHICLETYPE_ADDED", "Your Vehicletype Added successfully");
define("POPUP_VEHICLETYPE_DELETED", "Vehicletype deleted successfully");
define("POPUP_VEHICLETYPE_UPDATED", "Your Vehicletype Updated successfully");

define("POPUP_VEHICLETYPE_ENTER", "Please enter the vehicle type");
define("POPUP_VEHICLETYPE_ENTERTEXT", "Please enter the vehicle type as text");
define("POPUP_VEHICLETYPE_NOSEATINGS", "Please enter the number of seatings");
define("POPUP_VEHICLETYPE_NUMSEATINGS", "Please enter the seating as number");
define("POPUP_VEHICLETYPE_BASEFARE", "Enter the base fare");
define("POPUP_VEHICLETYPE_BASEFARENUM", "Enter the base fare as number only");
define("POPUP_VEHICLETYPE_MINUTE", "Enter the cost per minute");
define("POPUP_VEHICLETYPE_NUMMINUTE", "Enter the price per minute as number only");
define("POPUP_VEHICLETYPE_KM", "Enter the cost per kilometer");

define("POPUP_VEHICLETYPE_ANYONE", "Please select any one vehicle type");
define("POPUP_VEHICLETYPE_ONLYONE", "Please select only one vehicle type to edit");
define("POPUP_VEHICLETYPE_ATLEASTONE", "Please select atleast one vehicle type");
define("POPUP_VEHICLETYPE_SUREDELETE", "Areyou sure you want to delete vehicletypes");



define("POPUP_VEHICLETYPE_KMNUM", "Enter the cost per kilometer as number only");
define("POPUP_VEHICLETYPE_SELECTCITY", "Select the city");

define("POPUP_VEHICLES_ATLEAST", "please mark any one option");

define("POPUP_PASSENGERS_ATLEAST", "Select atleast one passenger");
define("POPUP_DRIVERS_ATLEAST", "Select atleast one driver");
define("POPUP_PASSENGERS_DEACTIVATE", "Are you sure you want to deactivate driver reviews");
define("POPUP_PASSENGERS_ACTIVATE", "Are you sure you want to activate driver reviews");

define("POPUP_DRIVERS_ACTIVAT", "Are you sure you want to activate drivers");
define("POPUP_DRIVERS_DELETE", "Are you sure you want to delete driver/drivers");
define("POPUP_DRIVERS_DEACTIVAT", "Are you sure you want to deactivate drivers");
define("POPUP_DRIVERS_NEWPASSWORD", "your new password updated successfully");
define("POPUP_DRIVERS_ERRPASSWORD", "your password already exist.enter new password");



define("POPUP_PASSENGERS_DEACTIVAT", "Are you sure you want to deactivate Passengers");
define("POPUP_PASSENGERS_ACTIVAT", "Are you sure you want to activate Passengers");

define("POPUP_PASSENGERS_ANYONEPASS", "Please select any one to reset the password");
define("POPUP_PASSENGERS_ONLYONEPASS", "Please select only one to reset the password");

define("POPUP_PASSENGERS_PASSVALID", "Please enter the password with atleast one character and one letter");
define("POPUP_PASSENGERS_PASSCONFIRM", "Please confirm the password ");
define("POPUP_PASSENGERS_PASSENTER", "Please enter the password ");
define("POPUP_PASSENGERS_SAMEPASSCONFIRM", "Please confirm the same password ");
define("POPUP_PASSENGERS_PASSNEW", "Please enter the new password ");

define("POPUP_PASSENGERS_PASSUPDATED", "Password updated successfully ");

define("POPUP_DISPATCHERS_CITY", "Please select city");

define("POPUP_DISPATCHERS_NAME", "Please enter the name");
define("POPUP_DISPATCHERS_NAMETEXT", "Please enter the name as text");
define("POPUP_DISPATCHERS_EMAIL", "Please enter the email");
define("POPUP_DISPATCHERS_VALIDEMAIL", "Please enter valid email");
define("POPUP_DISPATCHERS_PASSWORD", "Please enter the password");

define("POPUP_DISPATCHER_DELETE", "Please select Aatleast one dispatcher");
define("POPUP_DISPATCHER_EDIT", "Please select Any one dispatcher");
define("POPUP_DRIVER_EDIT", "Please select Any one driver");
define("POPUP_DISPATCHER_ONLYEDIT", "Please select only one dispatcher to edit");
define("POPUP_DRIVER_ONLYEDIT", "Please select only one driver to edit");
define("POPUP_DISPATCHER_INACTIVE", " Select Atleast one dispatcher");
define("POPUP_DISPATCHER_SUREINACTIVE", " Are you sure you want to deactivate dispatchers");
define("POPUP_DISPATCHER_SUREACTIVE", "Are you sure you want to activate dispatchers");

define("POPUP_DRIVERREVIEW_ATLEAST", "please select atleast one driver review");

define("POPUP_VEHICLE_ATLEAST", "please select atleast one vehicle");
define("POPUP_VEHICLE_ONE", "please select any  one vehicle");
define("POPUP_VEHICLE_DELETE", "Are you sure to Delete these vehicles");
define("POPUP_VEHICLE_DEACTIVATE", "Are you sure you want to Reject these vehicle/vehicles");
define("POPUP_VEHICLE_ACTIVATE", "Are you sure you want to Activate these vehicle/vehicles");


define("POPUP_MESSAGE", "Please enter the message");

define("POPUP_DISPUTES_ANYONE", "Please select any one to resolve");
define("POPUP_DISPUTES_ONLYONE", "Please select only one to resolve");
define("POPUP_COMPAIGN_ATLEAST", "Select atleast one compaign");

define("POPUP_COMPAIGN_AREYOUSURE", "Are you sure to deactivate");
define("POPUP_COMPAIGN_REFERRAL", "referral");

define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");
//define("POPUP_MESSAGE", "Please enter the message");



define("POPUP_VEHICLEMODEL_TYPENAMES", "Please enter the type name");
define("POPUP_VEHICLEMODEL_TEXT", "Please enter the type name as text");
define("POPUP_MESSAGE_COMPLETED", "Your type name added successfully");
define("POPUP_VEHICLEMODEL_ATLEAST", "Select atleast one vehicle type");

define("POPUP_VEHICLEMODEL_TYPENAME", "Please select the vehicle type");
define("POPUP_VEHICLEMODEL_MODELNAME", "Please enter the model  name");
define("POPUP_VEHICLEMODEL_SUCCESSES", "Your  model  name added successfully");


define("POPUP_VEHICLEMODEL_CODE", "Please enter the code");
define("POPUP_VEHICLEMODEL_MODELNAME", "Please select model  name");
define("POPUP_VEHICLEMODEL_STARTDATE", "Please select the start date");
define("POPUP_VEHICLEMODEL_EXPIREDATE", "please select the expire date");

define("POPUP_COMPAIGNS_DISCOUNT", "please enter the new user discount");
define("POPUP_COMPAIGNS_TITLE", "please enter the title");
define("POPUP_COMPAIGNS_NUMBERS", "please enter the numbers only");
define("POPUP_COMPAIGNS_REFERALDISCOUNT", "please enter the referral bonus");
define("POPUP_COMPAIGNS_MESSAGE", "please enter the message");
define("POPUP_COMPAIGNS_TEXT", "please enter message as text only");

define("POPUP_COMPAIGNS_CODE", "please enter the code");
define("POPUP_COMPAIGNS_TEXTNUMBER", "please enter code as text or number");
define("POPUP_COMPAIGNS_STARTDATE", "please select  the start date");
define("POPUP_COMPAIGNS_EXPIREDATE", "please select  the expire date");









/*
 * Fields
 */

define("FIELD_ENTER_COUNTRY_NAME", "ENTER COUNTRY NAME");


define("FIELD_CITIES_COUNTRY", "SELECT COUNTRY");
define("FIELD_CITIES_CITYNAME_NAME", "CITY NAME");
define("FIELD_CITIES_CURRENCY_NAME", "ENTER CURRENCY NAME");

define("FIELD_COMPANY_COMPANYNAME", "COMPANY NAME");
define("FIELD_COMPANY_FIRSTNAME", "FIRST NAME");
define("FIELD_COMPANY_LASTNAME", "LAST NAME");
define("FIELD_COMPANY_USERNAME", "USER NAME");
define("FIELD_COMPANY_PASSWORD", "PASSWORD");
define("FIELD_COMPANY_EMAIL", "EMAIL");
define("FIELD_COMPANY_ADDRESS", "ADDRESS");
define("FIELD_COMPANY_MOBILE", "MOBILE");
define("FIELD_COMPANY_CITY", "CITY");
define("FIELD_COMPANY_STATE", "STATE");
define("FIELD_COMPANY_POSTCODE", "POST CODE");
define("FIELD_COMPANY_VATNUMBER", "VAT NUMBER");

define("FIELD_COMPANY_SELECTCOUNTRY", "SELECT COUNTRY");

define("FIELD_VEHICLETYPE_NAME", "VEHICLE TYPE NAME");
define("FIELD_VEHICLETYPE_SEATINGCAPACITY", "SEATING CAPACITY");
define("FIELD_VEHICLETYPE_MINIMUMFARE", "MINIMUM FARE");
define("FIELD_VEHICLETYPE_BASEFARE", "BASE FARE   (INR)");
define("FIELD_VEHICLETYPE_PRICEMINUTE", "PRICE PER MINUTE ($)");
define("FIELD_VEHICLETYPE_PRICEKM", "PRICE PER KM/MILE ($)");
define("FIELD_VEHICLETYPE_DESCRIPTION", "VEHICLE TYPE DESCRIPTION");
define("FIELD_VEHICLETYPE_CITY", "CITY");

define("FIELD_VEHICLETYPE_LATITUDE", "LATITUDE");
define("FIELD_VEHICLETYPE_LONGITUDE", "LONGITUDE");


define("FIELD_VEHICLE_SELECTCITY", "SELECT CITY");
define("FIELD_VEHICLE_SELECTCOMPANY", "SELECT COMPANY");

define("FIELD_VEHICLE_VEHICLETYPE", "SELECT VEHICLE TYPE");
define("FIELD_VEHICLE_VEHICLEMAKE", "SELECT VEHICLE MAKE");
define("FIELD_VEHICLE_VEHICLEMODEL", "SELECT VEHICLE MODEL");
define("FIELD_VEHICLE_IMAGE", "UPLOAD PHOTO OF THE VEHICLE");

define("FIELD_VEHICLE_REGNO", "VEHICLE REG.NO");
define("FIELD_VEHICLE_PLATENO", "LICENSE PLATE NO");
define("FIELD_VEHICLE_INSURENCE", "INSURENCE NUMBER");
define("FIELD_VEHICLE_COLOR", "VEHICLE COLOR");

define("FIELD_VEHICLE_UPLOADCR", "UPLOAD CERTIFICATE OF REGISTRATION");
define("FIELD_VEHICLE_EXPIREDATE", "EXPIRATION DATE");
define("FIELD_VEHICLE_UPLOADMOTOR", "UPLOAD MOTOR INSURENCE CERTIFICATE");
define("FIELD_VEHICLE_UPLOADCP", "UPLOAD CONTRACT CARRIAGE PERMIT");
define("FIELD_VEHICLE_CHOOSE", "CHOOSE FILE");
define("FIELD_VEHICLE_NOCHOOSE", "NO FILE CHOOSEN");

define("FIELD_DRIVERS_FIRSTNAME", "FIRST NAME");
define("FIELD_DRIVERS_LASTNAME", "LAST NAME");
define("FIELD_DRIVERS_MOBILE", "MOBILE ");
define("FIELD_DRIVERS_UPLOADPHOTO", "UPLOAD PHOTO");
define("FIELD_DRIVERS_EMAIL", "EMAIL");
define("FIELD_DRIVERS_PASSWORD", "PASSWORD");
define("FIELD_DRIVERS_ZIPCODE", "ZIP CODE");
define("FIELD_DRIVERS_UPLOADDRIVERLICENSE", "UPLOAD DRIVING LICENCE");
define("FIELD_DRIVERS_EXDATE", "EXPIRY DATE");
define("FIELD_DRIVERS_UPLOADPASSBOOK", "UPLOAD BANK PASSBOOK COPY");

define("FIELD_DISPUTES_MANAGEMENTNOTE", "MANAGEMENT NOTE");

define("FIELD_SELECTCITY", "SELECT CITY");

define("FIELD_DISPATCHERS_NAME", "NAME");
define("FIELD_DISPATCHERS_EMAIL", "EMAIL");
define("FIELD_DISPATCHERS_PASSWORD", "PASSWORD");

define("FIELD_VEHICLEMODEL_TYPENAME", "TYPE NAME");


define("FIELD_VEHICLEMODEL_SELECTTYPE", "SELECT TYPE");
define("FIELD_VEHICLEMODEL_MODAL", "MODEL");

define("FIELD_COMPAIGNS_DISCOUNTTYPE", "DISCOUNT TYPE");
define("FIELD_COMPAIGNS_PERCENTAGE", "PERCENTAGE");
define("FIELD_COMPAIGNS_FIXED", "FIXED");
define("FIELD_COMPAIGNS_DISCOUNT", "NEW USER DISCOUNT ");
define("FIELD_COMPAIGNS_REFERRALDISCOUNTTYPE", "REFERRAL DISCOUNT TYPE");
define("FIELD_COMPAIGNS_REFERRALDISCOUNT", "REFERRAL BONUS ");
define("FIELD_COMPAIGNS_MESSAGE", "MESSAGE");




define("FIELD_COMPAIGNS_CODE", "CODE");
define("FIELD_COMPAIGNS_STARTDATE", "STATRT DATE");
define("FIELD_COMPAIGNS_EXPIREDATE", "EXPIRATION DATE");
define("FIELD_COMPAIGNS_DISCOUNTTYPE", "DISCOUNT TYPE");
define("FIELD_PROMOTION_DISCOUNT", "PROMOTION DISCOUNT");

define("LIST_ADDCOMPANYS", "ADD COMPANY");









/* NEW PASSWORD && CONFIRM PASSWORD*/


define("FIELD_NEWPASSWORD", "NEW PASSWORD");
define("FIELD_CONFIRMPASWORD", "CONFIRM PASSWORD");







/*
 * Table headings
 */

define("LIST_CITY_TABLE_SLNO", "SL.NO");
define("LIST_CITY_TABLE_COUNTRY", "COUNTRY");
define("LIST_CITY_TABLE_CITY", "CITY");
define("LIST_CITY_TABLE_LATITUDE", "LATITUDE");
define("LIST_CITY_TABLE_LONGITUDE", "LONGITUDE");
define("LIST_CITY_TABLE_SELECT", "SELECT");


define("LIST_CITY_TABLE_EDITCITIESGEO", "Edit City's Geo Location");



define("COMPANY_ADDEDIT_PERSIONAL", "Personal Contact Details");
define("COMPANY_TABLE_COMPANYID", "COMPANY ID");
define("COMPANY_TABLE_COMPANYNAME", "COMPANY NAME");
define("COMPANY_TABLE_ADDRESSLINE", "ADDRESS LINE");
define("COMPANY_TABLE_CITY", "CITY");
define("COMPANY_TABLE_STATE", "STATE");
define("COMPANY_TABLE_POSTALCODE", "POSTAL CODE");
define("COMPANY_TABLE_FIRSTNAME", "FIRST NAME");
define("COMPANY_TABLE_LASTNAME", "LAST NAME");
define("COMPANY_TABLE_EMAIL", "EMAIL");
define("COMPANY_TABLE_MOBILE", "MOBILE");
define("COMPANY_TABLE_SELECT", "SELECT");

define("VTYPE_TABLE_TYPEID", "TYPE ID");
define("VTYPE_TABLE_TYPENAME", "TYPE NAME");
define("VTYPE_TABLE_MAXSIZE", "MAX SIZE");
define("VTYPE_TABLE_BASEFARE", "BASE FARE");
define("VTYPE_TABLE_MINFARE", "MIN FARE");
define("VTYPE_TABLE_PRICEMINUTE", "PRICE PER MINUTE");
define("VTYPE_TABLE_PRICRMILE", "PRICE PER MILE");
define("VTYPE_TABLE_TYPEDESCRIPTION", "TYPE DESCRIPTION");
define("VTYPE_TABLE_CITY", "CITY");
define("VTYPE_TABLE_SELECT", "SELECT");

define("VEHICLES_TABLE_VEHICLEID", "VEHICLE_ID");
define("VEHICLES_TABLE_TITLE", "TITLE");
define("VEHICLES_TABLE_VMODAL", "V MODEL");
define("VEHICLES_TABLE_VTYPE", "V TYPE");
define("VEHICLES_TABLE_VREGNO", "V REG NO");
define("VEHICLES_TABLE_LICENSEPLATNO", "LICENSE PLATE NO");
define("VEHICLES_TABLE_INSURENCENUMBER", "INSURENCE NUMBER");
define("VEHICLES_TABLE_VCOLOR", "V COLOR");
define("VEHICLES_TABLE_OPTION", "OPTION");

define("DRIVERS_TABLE_DRIVER_ID", "DRIVER_ID");
define("DRIVERS_TABLE_NAME", "NAME");
define("DRIVERS_TABLE_MOBILE", "MOBILE");
define("DRIVERS_TABLE_EMAIL", "EMAIL");
define("DRIVERS_TABLE_REGDATE", "REG DATE");
define("DRIVERS_TABLE_DEVICETYPE", "DEVICE TYPE");
define("DRIVERS_TABLE_RESETPASSWORD", "RESET PASSWORD");
define("DRIVERS_TABLE_PROFILEPIC", "PROFILE PIC");
define("DRIVERS_TABLE_OPTION", "OPTION");

define("PASSENGERS_TABLE_PASSENGERID", "PASSENGER ID");
define("PASSENGERS_TABLE_FIRSTNAME", "FIRST NAME");
define("PASSENGERS_TABLE_LASTNAME", "LAST NAME");
define("PASSENGERS_TABLE_MOBILE", "MOBILE");
define("PASSENGERS_TABLE_EMAIL", "EMAIL");
define("PASSENGERS_TABLE_REGDATE", "REG DATE");
define("PASSENGERS_TABLE_DEVICETYPE", "DEVICE TYPE");
define("PASSENGERS_TABLE_ZIPCODE", "ZIP CODE");
define("PASSENGERS_TABLE_PROFILEPIC", "PROFILE PIC");
define("PASSENGERS_TABLE_STATUS", "STATUS");
define("PASSENGERS_TABLE_SELECT", "SELECT");


define("DISPATCHERS_TABLE_DISPATCHERID", "DISPATCHER ID");
define("DISPATCHERS_TABLE_CITY", "CITY");
define("DISPATCHERS_TABLE_EMAIL", "EMAIL");
define("DISPATCHERS_TABLE_DISPATCHERNAME", "DISPATCHER NAME");
define("DISPATCHERS_TABLE_NOOFBOOKINGS", "NO OF BOOKINGS");
define("DISPATCHERS_TABLE_OPTION", "OPTION");

define("DOCUMENT_TABLE_DOCUMENTID", "DOCUMENT ID");
define("DOCUMENT_TABLE_FIRSTNAME", "FIRST NAME");
define("DOCUMENT_TABLE_LASTNAME", "LAST NAME");
define("DOCUMENT_TABLE_VIEW", "VIEW");
define("DOCUMENT_TABLE_EXPIRYDATE", "EXPIRY DATE");


define("DOCUMENT_TABLE_VEHICLEID", "VEHICLE ID");
define("DOCUMENT_TABLE_COMPANY", "COMPANY");


define("DRIVERREVIEW_TABLE_SLNO", "SL.NO");
define("DRIVERREVIEW_TABLE_DRIVERID", "BOOKING ID");
define("DRIVERREVIEW_TABLE_REVIEW", "REVIEW");
define("DRIVERREVIEW_TABLE_RATING", "RATING");
define("DRIVERREVIEW_TABLE_REVIEWDATE", "BOOKING DATE AND TIME");
define("DRIVERREVIEW_TABLE_DRIVERNAME", "DRIVER NAME");
define("DRIVERREVIEW_TABLE_PASSENGERNAME", "PASSENGER ID");
define("DRIVERREVIEW_TABLE_STATUS", "STATUS");
define("DRIVERREVIEW_TABLE_SELECT", "SELECT");

define("PASSENGRRATING_TABLE_SLNO", "SL.NO");
define("PASSENGRRATING_TABLE_PASSENGERID", "PASSENGER ID");
define("PASSENGRRATING_TABLE_PASSENGERNAME", "PASSENGER NAME");
define("PASSENGRRATING_TABLE_PASSENGEREMAIL", "PASSENGER EMAIL");
define("PASSENGRRATING_TABLE_AVGRATING", "AVG RATING");

define("DISPUTES_TABLE_DISPUTEID", "DISPUTE ID");
define("DISPUTES_TABLE_BOOKINGID", "BOOKING ID");
define("DISPUTES_TABLE_PASSENGERID", "PASSENGER ID");
define("DISPUTES_TABLE_PASSENGERNAME", "PASSENGER NAME");
define("DISPUTES_TABLE_DRIVERID", "DRIVER ID");
define("DISPUTES_TABLE_DRIVERNAME", "DRIVER NAME");
define("DISPUTES_TABLE_DISPUTEMESSAGE", "DISPUTE MESSAGE");
define("DISPUTES_TABLE_DISPUTEDATE", "DISPUTE DATE");
define("DISPUTES_TABLE_APPOINTMENTID", "APPOINTMENT ID");
define("DISPUTES_TABLE_SELECT", "SELECT");


define("VEHICLEMAKE_TABLE_ID", "ID");
define("VEHICLEMAKE_TABLE_TYPENAME", "TYPE NAME");
define("VEHICLEMAKE_TABLE_SELECT", "SELECT");

define("VEHICLEMODEL_TABLE_ID", "ID");
define("VEHICLEMODEL_TABLE_MODELID", "MODEL ID");
define("VEHICLEMODEL_TABLE_MAKE", "MAKE");

define("VEHICLEMODEL_TABLE_MODEL", "MODEL");
define("VEHICLEMODEL_TABLE_SELECT", "SELECT");

define("COMPAIGNS_TABLEL_DISCOUNT", "DISCOUNT");
define("COMPAIGNS_TABLE_REFERRALDISCOUNT", "REFERRAL DISCOUNT");
define("COMPAIGNS_TABLE_MESSAGE", "MESSAGE");
define("COMPAIGNS_TABLE_CITY", "CITY ");
define("COMPAIGNS_TABLE_SELECT", " SELECT");

define("COMPAIGNS_TABLE_CODE", "CODE");
define("COMPAIGNS_TABLE_STARTDATE", "START DATE");
define("COMPAIGNS_TABLE_ENDDATE", "END DATE");
define("COMPAIGNS_TABLE_DISCOUNT", "DISCOUNT");
define("COMPAIGNS_TABLE_MESSAGE", "MESSAGE");
define("COMPAIGNS_TABLE_PROMOT_CITY", "CITY");
define("COMPAIGNS_TABLE_PROMOT_SELECT", "SELECT");









/*
 * Generic
 */

define("GEN_DELETE", "Delete");
define("GEN_OPEN", "Open");
define("GEN_CREATE", "Create");
