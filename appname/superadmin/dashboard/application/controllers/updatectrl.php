<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Updatectrl extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("superadminmodal");
        $this->load->library('session');
//        $this->load->library('excel');
header('Access-Control-Allow-Origin: *');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
    }
    
    function addInfo()
    {
         $day = $this->input->post("dob_day");
        $month = $this->input->post("dob_month");
        $year = $this->input->post("dob_year");
        $fname = $this->input->post("dob_fname");
        $lname = $this->input->post("dob_lname");
        $type = $this->input->post("dob_type");
        $ip = $this->input->post("dob_ip");
        
          $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $collection = $db->selectCollection('updaeUserInfo');
        
        $userRecord = array('dob_day' => $day, 'dob_month' => $month, 'dob_year' => $year,
            'fname' => $fname, 'lname' => $lname,'type'=>$type,'ip'=>$ip);
        
        $collection->insert($userRecord);
    }
}