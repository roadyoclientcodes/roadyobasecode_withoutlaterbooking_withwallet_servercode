<script>
    $('.sm-table').find('.header-inner').html('<div class="brand inline" style="  width: auto;\
                     font-size: 27px;\
                     color: gray;\
                     margin-left: 100px;margin-right: 20px;margin-bottom: 12px; margin-top: 10px;">\
                    <strong>Roadyo Super Admin Console</strong>\
                </div>');
    $('document').ready(function () {
        var initStripedTable = function () {
            var table = $('.stripedTable');
            var settings = {
                "sDom": "t",
                "destroy": true,
                "paging": false,
                "scrollCollapse": true
            };
            table.dataTable(settings);
        }
        initStripedTable();
    });
</script>
<!--<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCK5bz3K1Ns_BASkAcZFLAI_oivKKo98VE" type="text/javascript"></script>-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCK5bz3K1Ns_BASkAcZFLAI_oivKKo98VE"></script>

<style>
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .rating>.rated {
        color: #10cfbd;
    }
    .social-user-profile {
        width: 83px;
    }
</style>
<?php
$i = 0;
     $routes = array();
     foreach ($data['res']['route'] as $trip_data)
     {
        $routes[$j++] = array('Geometry' => array('Latitude' => $val->latitude, 'Longitude' => $val->longitude));
        $i++;
     }

    
$start = $routes[0];

$startLat = $start[1];
$startLong = $start[2];

$end = end($routes);

$endLat = $end[1];
$endLong = $end[2];

$j = 0;
foreach ($data['trip_route'] as $trip_data) {
    $flg = true;
    foreach ($trip_data as $val) {
        if ($flg) {
            $flg = false;
        }
        $routes[$j++] = array('Geometry' => array('Latitude' => $val->latitude, 'Longitude' => $val->longitude));
        $i++;
    }
    $s_e_ship[] = $j - 1;
}
$end = end($routes);

$endLat = $end[1];
$endLong = $end[2];

$routes_google[0] = array('Geometry' => array('Longitude' => (Float)$data['appt_data']->appt_long, 'Latitude' => (Float)$data['appt_data']->appt_lat));
$routes_google[1] = array('Geometry' => array('Longitude' => (Float)$data['appt_data']->drop_long, 'Latitude' => (Float)$data['appt_data']->drop_lat));
    print_r($routes);die;
?>

<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="<?= base_url() ?>/index.php/superadmin/completed_jobs">Completed Jobs</a>
            </li>
            <li>
                <a href="#" class="active">Job Details - <?php echo $data['appt_data']->appointment_id; ?></a>
            </li>
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Job Details
                    </div>
                </div>
                <div class="panel-body no-padding">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <div class="panel">
                                <ul class="nav nav-tabs nav-tabs-simple hidden-xs" role="tablist" data-init-reponsive-tabs="collapse">
                                    <li class="active">
                                        <a href="#customer_details" data-toggle="tab" role="tab" aria-expanded="true">Customer Details</a>
                                    </li>
                                    <li class="">
                                        <a href="#driver_details" data-toggle="tab" role="tab" aria-expanded="false">Driver Details</a>
                                    </li>
                                </ul><div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                                <div class="tab-content hidden-xs">
                                    <div class="tab-pane active" id="customer_details">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking ID</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->appointment_id; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking Type</a>
                                                </p>
                                                <p class="small">
                                                    <?php if ($data['appt_data']->appt_type == 1)
                                                        echo "Now";
                                                    else
                                                        "Later";
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo date('g:i A', strtotime($data['appt_data']->created_dt)); ?>
                                                </p>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Customer</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['customer_data']->first_name; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Phone</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['customer_data']->phone; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Email</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['customer_data']->email; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Pickup Address</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->address_line1; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Pickup Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->start_dt; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Drop Address</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->drop_addr1; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Drop Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->complete_dt; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Waiting Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->waiting_mts; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->start_dt; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Distance</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
                                                        if($data['appt_data']->distance_in_mts != NULL)
                                                        {    

                                                            echo bcdiv($data['appt_data']->distance_in_mts, '1609.344', 2); 
                                                             echo " Miles";
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="driver_details">
                                        <div class="row-xs-height">
                                            <div class="social-user-profile col-xs-height text-center col-top">
                                                <div class="thumbnail-wrapper circular bordered b-white">
                                                    <img alt="<?php echo $data['driver_data']->first_name; ?>" width="55" height="55" style="cursor:pointer;"
                                                         onclick='openimg(this)'
                                                         data-src-retina="<?php echo $data['driver_data']->profile_pic ?>" 
                                                         data-src="<?php echo $data['driver_data']->profile_pic ?>"
                                                         src="<?php echo $data['driver_data']->profile_pic ?>"
                                                         onerror="this.src = '<?php echo base_url('/../../pics/user.jpg') ?>'">
                                                </div>
                                                <p class="rating">
                                                    <?php
                                                    for ($i = 1; $i <= 5; $i++) {
                                                        if ($i > $data['res']['rating'])
                                                            echo '<i class="fa fa-star"></i>';
                                                        else
                                                            echo '<i class="fa fa-star rated"></i>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-xs-height p-l-20">
                                                <h3><?php echo $data['driver_data']->first_name; ?></h3>
                                                <p class="fs-16"><?php echo $data['driver_data']->email; ?></p>
                                                <p class="no-margin fs-16"><?php echo $data['driver_data']->mobile; ?></p>
                                            </div>
                                        </div>                                        
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Distance</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
                                                        if($data['appt_data']->distance_in_mts != NULL)
                                                        {    

                                                            echo bcdiv($data['appt_data']->distance_in_mts, '1609.344', 2); 
                                                            echo " Miles";
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Duration</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo gmdate("H:i:s",($data['appt_data']->duration * 60));?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Distance Fee</a>
                                                </p>
                                                <p class="small">
                                                    <?php print_r($data['car_data']->price_per_km * bcdiv($data['appt_data']->distance_in_mts, '1609.344', 2)." $");?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Time Fee</a>
                                                </p>
                                                <p class="small">
                                                    <?php print_r($data['car_data']->price_per_min * $data['appt_data']->duration." $");?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Parking Fee</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->parking_fee." $";?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Subtotal</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo ($data['appt_data']->amount-$data['appt_data']->discount)." $"; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Discounts</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->discount." $"; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Final Fare</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $data['appt_data']->amount," $"; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Payment Method</a>
                                                </p>
                                                <p class="small">
                                                    <?php if($data['appt_data']->payment_type == 1)echo 'Card';else echo 'Cash';?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12">
                            <div id="map" style="width: 100%; height: 670px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    jQuery(function () {
        var stops = jQuery.parseJSON('<?php echo json_encode($routes); ?>');
        if (stops.length > 3)
        {
            console.log("route");
            var map = new window.google.maps.Map(document.getElementById("map"));
            var myOptions = {
                zoom: 13,
                center: new window.google.maps.LatLng(51.507937, -0.076188), // default to London
                mapTypeId: window.google.maps.MapTypeId.ROADMAP
            };
            map.setOptions(myOptions);
            var bounds = new window.google.maps.LatLngBounds();

            // extend bounds for each record
            var ico='http://maps.google.com/mapfiles/ms/icons/green-dot.png';
            var msg = "Appointment Started";
            var j=0;
            var i=0;
            var markers = [];
            var myLatlng = '';
            var inf = new google.maps.InfoWindow;;
            jQuery.each(stops, function (key, val) {
                myLatlng = new window.google.maps.LatLng(val.Geometry.Latitude, val.Geometry.Longitude);
                markers[i] = new google.maps.Marker({
                    icon:ico,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    msg: msg,
                    position: myLatlng
                });
                if(j==0){
                    j++;
                    google.maps.event.addListener(markers[i], 'click', function () {
                        inf.setContent(this.msg);
                        inf.open(map,this);
                    });
                }
                ico='http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
                bounds.extend(myLatlng);
                i++;
            });
            msg = "Appointment Completed";
            google.maps.event.addListener(markers[i-1], 'click', function () {
                inf.setContent(this.msg);
                inf.open(map,this);
            });
            markers[i-1].setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
            map.fitBounds(bounds);            
            var marker, i;
        }
        else
        {
            console.log('google');
            var directionsDisplay;
                var directionsService = new google.maps.DirectionsService();
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(<?php echo json_encode($data['appt_data']->appt_lat); ?>, <?php echo json_encode($data['appt_data']->appt_long); ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            directionsDisplay = new google.maps.DirectionsRenderer();

            directionsDisplay.setMap(map);

            var start = new google.maps.LatLng(<?php echo json_encode($data['appt_data']->appt_lat); ?>, <?php echo json_encode($data['appt_data']->appt_long); ?>);
            var end = new google.maps.LatLng(<?php echo json_encode($data['appt_data']->drop_lat); ?>, <?php echo json_encode($data['appt_data']->drop_long); ?>);

            var request = {
                origin: start,
                destination: end,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
                directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
                }             });
         }
    });

    function openimg(imgid)
    {
        $('#modal-img').modal('show');
        var src=$(imgid).attr("src");
        console.log(src);
        $("#showimg").attr("src",src);
    }
</script>
<div class="modal fade fill-in" id="modal-img" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        <i class="pg-close"></i>
    </button>
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                
            </div>
            <div class="modal-body">
                 <div class="row text-center" style="padding:20px;height:80vh;">   
                    <img src="" id="showimg" style="height:100%;">
                </div>  
            </div>
            <div class="modal-footer">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>