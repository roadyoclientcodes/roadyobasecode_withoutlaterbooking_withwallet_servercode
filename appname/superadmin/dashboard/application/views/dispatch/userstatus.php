<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

$m = new MongoClient();
$db = $m->opa;
$location = $db->selectCollection('location');
$userDet = $location->findOne(array('user' => (int) $_REQUEST['uid']));

if($userDet['status'] != 3){
    $data['status'] = 2;
}
else{
    $data['status'] = 1;
}



echo 'data:'. json_encode($data)."\n\n";
flush();
?>