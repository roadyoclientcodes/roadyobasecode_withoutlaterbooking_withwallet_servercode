<?php
session_start();
//$_SESSION['']
?>

<!--start of map script-->
<style>
    html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
    }
    #map-canvas {
        margin: 0;
        padding: 0;
        height: 600px;
        border: 1px solid #ccc;
        /*display: none;*/

    }
    .form-horizontal .form-group {
        border-bottom: 1px solid #e6e6e6;
        padding-top: 3px;
        padding-bottom: 5px;
        margin-bottom: 0;
    }
    .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
        position: relative;
        min-height: 1px;
        padding-right: 0px;
        padding-left: 9px;
    }
    #headerpart{
        display: none;
    }
    .nav-tabs-fillup li {
        margin-top: 0px !important;
    }
    .pac-container { z-index:2000 !important; }
    .datepicker{z-index:1151 !important;}
    #map_canvas {display:none;}
    /*#map img {*/
    /*max-width: none;*/
    /*}*/
    /*#myMap2,#myMap3{*/
        /*display: none;*/
    /*}*/
    #myMap1,#myMap3,#myMap2 img {
        max-width: none;
    }

    form {
        background: #f2f2f2;
        padding: 20px;
    }





</style>
<!--<script src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>-->
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>

<script src="//ubilabs.github.io/geocomplete/jquery.geocomplete.js"></script>


<script src="<?php echo serverdata_folder;?>pubnub.js"></script>
<script>


    var x = document.getElementById("demo");
    var lat, long ;


//    var geocoder;
    var map;


        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }

    function showPosition(position) {
        lat = position.coords.latitude;
        long = position.coords.longitude
    }



    function initialize() {
//        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng(lat, long)
        };
        map = new google.maps.Map(document.getElementById('myMap1'),
            mapOptions);


    }



    function calculateRoute(rootfrom, rootto) {
        // Center initialized to Naples, Italy
        var myOptions = {
            zoom: 8,
            center: new google.maps.LatLng(40.84, 14.25),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        // Draw the map

        var rendererOptions = {
            draggable: true
        };
        var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
         map = new google.maps.Map(document.getElementById("myMap3"), myOptions);

        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('directionsPanel'));




        var directionsService = new google.maps.DirectionsService();
        var directionsRequest = {
            origin: rootfrom,
            destination: rootto,
            travelMode: google.maps.DirectionsTravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC
        };
        directionsService.route(
            directionsRequest,
            function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    new google.maps.DirectionsRenderer({
                        map: map,
                        directions: response
                    });
                }
                else
                    $("#lblError").append("Unable To Find Root");
            }
        );

        google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
            computeTotalDistance(directionsDisplay.getDirections());
        });
    }


    google.maps.event.addDomListener(window, 'load', initialize);

//    google.maps.event.addListener(window, 'markercomplete', getdistance);



//$(function(){
//    alert('adsf');
//    $("#Pickup").geocomplete({
//        map: "#myMap1",
//        details: "form",
//        detailsAttribute: "data-geo",
//        types: ["geocode", "establishment"],
//        markerOptions: {
//            draggable: true
//        }
//    });
//
//    $("#Pickup").geocomplete()
//        .bind("geocode:result", function(event, result){
////                $.log("Result: " + result.formatted_address);
//            $('#location_addr1').val(result.formatted_address);
////                alert('address 1' + result.formatted_address);
////                alert('address 1' + result.lat);
//        })
//        .bind("geocode:error", function(event, status){
//            $.log("ERROR: " + status);
//        })
//        .bind("geocode:multiple", function(event, results){
//            $.log("Multiple: " + results.length + " results found");
//        })
//        .bind("geocode:dragged", function(event, latLng){
//            $("input[name=latitude]").val(latLng.lat());
//            $("input[name=longitude]").val(latLng.lng());
//        });
//});

    var pubnub = PUBNUB.init({
        publish_key: 'pub-c-6835782e-3042-4fa0-9a72-9ba3808eaba1',
        subscribe_key: 'sub-c-5be63084-ef33-11e4-9c3d-02ee2ddab7fe',
        ssl: false,
        jsonp: false
    });
$(function(){
    $('#myMap2').hide();
    $('#myMap3').hide();
    $('#headerpart').hide();



    $('.phonebooking').addClass('active');

    $("#Pickup").geocomplete({
        map: "#myMap2",
        details: "form",
        detailsAttribute: "data-geo",
        types: ["geocode", "establishment"],
        markerOptions: {
            draggable: true
        },
        mapOptions: {
            zoom: 4,
            scrollwheel:  true
        }
     });


    var mapone = $("#Pickup").geocomplete("map");


//
//    $('#Pickup').keyup(function(e){
//        if(e.keyCode == 13)
//        {
//            $('#myMap1').hide();
//            $('#myMap3').hide();
//            $('#myMap2').show();
//            google.maps.event.trigger(mapone, "resize");
//
//        }
//    });
//
//   ;

    $("#Pickup").geocomplete()
        .bind("geocode:result", function(event, result){
            $('#myMap1').hide();
            $('#myMap3').hide();
            $('#myMap2').show();

            $('#myMap2').fadeIn(); //Load the map wrapper once results arrive
            map = $("#Pickup").geocomplete("map");
            google.maps.event.trigger(map, 'resize');
            map.setZoom(14);       //set appropriate zoom
            map.setCenter(result.geometry.location);  //this is imp


            if($("input[name=drop_lat]").val() != "" || $("input[name=drop_long]").val() != "")
                getdistance();
            google.maps.event.trigger(mapone, "resize");
            $.log("Result: " + result.formatted_address);

        })
        .bind("geocode:error", function(event, status){
            $.log("ERROR: " + status);
        })
        .bind("geocode:multiple", function(event, results){
            $.log("Multiple: " + results.length + " results found");
        })
        .bind("geocode:dragged", function(event, latLng){
            $("input[name=pickup_lat]").val(latLng.lat());
            $("input[name=pickup_long]").val(latLng.lng());
//            alert("Result: " + latLng.formatted_address);
            $("#Pickup").geocomplete("find", latLng.toString());

//            $.log("Result: " + latLng.formatted_address);

        });

    $("#Drop").geocomplete({
        map: "#myMap3",
        details: "form",
        detailsAttribute: "data-geo2",
        types: ["geocode", "establishment"],
        markerOptions: {
            draggable: true
        },mapOptions: {
            zoom: 4,
            scrollwheel:  true
        }

    });
    var maptwo = $("#Drop").geocomplete("map");

//    $('#Drop').keyup(function(e){
//        if(e.keyCode == 13)
//        {
//            $('#myMap1').hide();
//            $('#myMap2').hide();
//            $('#myMap3').css('display' , 'block');
//            google.maps.event.trigger(maptwo, "resize");
//
//        }
//    })



    $("#Drop").geocomplete()
        .bind("geocode:result", function(event, result){
            getdistance();
            get_vehicle_type();
            $('#myMap1').hide();
            $('#myMap2').hide();
            $('#myMap3').css('display' , 'block');


            $('#myMap3').fadeIn(); //Load the map wrapper once results arrive
            map = $("#Drop").geocomplete("map");
            google.maps.event.trigger(map, 'resize');
            map.setZoom(14);       //set appropriate zoom
            map.setCenter(result.geometry.location);  //this is imp
            calculateRoute($('#Pickup').val(),$('#Drop').val());


            google.maps.event.trigger(maptwo, "resize");
            $.log("Result: " + result.formatted_address);
        })
        .bind("geocode:error", function(event, status){
            $.log("ERROR: " + status);
        })
        .bind("geocode:multiple", function(event, results){
            $.log("Multiple: " + results.length + " results found");
        })
        .bind("geocode:dragged", function(event, latLng){
            $("input[name=drop_lat]").val(latLng.lat());
            $("input[name=drop_long]").val(latLng.lng());
            $("#Drop").geocomplete("find", latLng.toString());
        });


    });





function get_vehicle_type(){
    $.ajax({
        type: "post",
        url: "<?php echo base_url()?>index.php/dispatch/get_vehicle_type",
        data: {pic_lat : $("input[name=pickup_lat]").val(), pic_long : $("input[name=pickup_long]").val()},
        dataType: "json",
        success: function (result) {
          var code = '';
            $.each(result,function(index,row){
              code +="<option value="+row.type_id+">"+row.type_name+" (Max Size "+ row.max_size +")</option>";

            });
            $('#vehicle_type').html(code);
        }

    });


}



function getdistance(){
    var dis = Math.ceil(distance($("input[name=pickup_lat]").val(), $("input[name=pickup_long]").val(), $("input[name=drop_lat]").val(), $("input[name=drop_long]").val(), "M"));
       $('#distance').val(dis);
       $('#appfare').val(dis * 2);
}
    function distance(lat1, lon1, lat2, lon2, unit) {
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var radlon1 = Math.PI * lon1/180
        var radlon2 = Math.PI * lon2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return dist
    }
    function get_slave_data (val){
      $.ajax({
            type: "post",
            url: "get_slave_data",
            data :{domid:val.id,domvalue : val.value},
            dataType: "json",
            success:function(result){

                if(result.fname ||result.lname ||result.phone||result.slave_id ||result.email) {
                    $('#fname').val(result.fname);
                    $("#lname").val(result.lname);
                    $("#phone").val(result.phone);
                    $('#slave_id').val(result.slave_id);
                    $('#email').val(result.email);
                }

            }


        });
     }

    $(document).keypress(
        function(event){
            if (event.which == '13') {
                event.preventDefault();
            }


        });

    $(document).ready(function(){

        $('#datepicker-component').on('changeDate', function(){
            $(this).datepicker('hide');
        });



//        $("#datepicker1").datepicker({ minDate: 0});
        var date = new Date();
        $('#datepicker-component').datepicker({
            startDate: date
        });

        $('#submitbtn').click(function(){


            if(validateForm()) {

                $.ajax({
                    type: "post",
                    url: "<?php echo base_url()?>index.php/dispatch/add_appointment",
                    data: $("#formdata").serialize(),
                    dataType: "json",
                    success: function (result) {
                        if (result.response == 1) {

                            var obj = {a: 12, bid: result.bid};
                            pubnub.publish({
                                channel: "dispatcher",
                                message: obj

                            });
                            alert('data has been sent !');
                            $('input').removeAttr('value');
                        }
                        else {
                            alert('Some error Occurred  !');
                        }


                    }


                });
            }
            else{
                alert('Insert All Data Correctly');
            }


        });


        $('#submitbtn_for_service').click(function(){

            $.ajax({
                type: "post",
                url: "<?php echo base_url() ?>../webService_dispatch.php/dispatchJob",
                data : $("#formdata" ).serialize(),
                dataType: "json",
                success:function(result){
                    if(result.response == 1){
                        alert('data has been sent !');
                        $('input').removeAttr('value');
                    }
                    else{
                        alert('Some error Occurred  !');
                    }


                }


            });


        });
    });

    function validateForm() {
        var isValid = true;
        $("#formdata").each(function() {

            if($('#Pickup').val() === "" || $("select[name='hours']" ).val() === "" || $("input[name='date']" ).val() === ""  || $("select[name='min']" ).val() === ""  || $('#Drop').val() === "" || $('#Email').val() === "")
                isValid = false;
        });
        return isValid;
    }
</script>






<div class="page-content-wrapper">
<!-- START PAGE CONTENT -->
<div class="content" style="padding-top: 2px !important;">
<!-- START JUMBOTRON -->
<div class="jumbotron bg-white" data-pages="parallax">
<div class="container-fluid container-fixed-lg " style="padding-left: 17px;padding-right: 16px;">



<div class="row">

<div class="panel panel-transparent ">


    <div class="tab-pane slide-left" id="slide2">
        <div class="row">
            <div class="col-md-12">

            <div class="col-md-4"><div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Phone Booking
                        </div>
                    </div>
                    <div class="panel-body">

                        <form  class="form-horizontal" id="formdata" role="form" autocomplete="off" novalidate="novalidate">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="fname" class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" required="required" onblur="get_slave_data(this)">
                                    </div>

                                </div>



                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="fname" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email" required="required" onblur="get_slave_data(this)">
                                    </div>

                                </div>

                            </div>
                            <div class="form-group">

                                <div class="col-sm-12">
                                    <label for="fname" class="col-sm-2 control-label">First Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="fname" name="fname" placeholder="First name" required="required">
                                    </div>

                                </div>


                            </div>
                            <div class="form-group">


                                <div class="col-sm-12">
                                    <label for="fname" class="col-sm-2 control-label">Last Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="lname" name="lname" placeholder="Last name" required="required">
                                    </div>

                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 required">
                                    <label for="fname" class="col-sm-2 control-label">Pickup</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="Pickup" name="address_line1" placeholder="Pickup" required="required">
                                    </div>
                                    <input type="hidden" name="pickup_lat"  data-geo="lat">
                                    <input type="hidden" name="pickup_long"  data-geo="lng">
                                </div>

                            </div>
                            <div class="form-group">

                                <div class="col-sm-12 required">
                                    <label for="fname" class="col-sm-2 control-label">Drop</label>
                                    <div class="col-sm-9 required">
                                        <input type="text" class="form-control" id="Drop" placeholder="Drop" name="drop_addr1" required aria-required="true">
                                    </div>
                                    <input type="text" style="display: none" name="drop_lat"   id="drop_lat" data-geo2="lat" >
                                    <input type="hidden" name="drop_long"  data-geo2="lng">

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="fname" class="col-sm-2 control-label">Date</label>
                                    <div class="col-sm-9">
                                        <div id="datepicker-component" class="input-group date ">
                                            <input type="text" class="form-control" name="date" id="datepicker1"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>



                                </div>

                            </div>
                            <div class="form-group">

                                <div class="col-sm-12">
                                    <label for="fname" class="col-sm-2 control-label">Time</label>
                                    <div class="col-sm-5">
<!--                                        <input type="time" class="form-control" id="name" placeholder="Time" required="required" name="time">-->
                                        <select size="1" name="hours" class="form-control">
                                            <option value="" selected="">Hrs</option>

                                            <option value="0">
                                                0</option>

                                            <option value="1">
                                                1</option>

                                            <option value="2">
                                                2</option>

                                            <option value="3">
                                                3</option>

                                            <option value="4">
                                                4</option>

                                            <option value="5">
                                                5</option>

                                            <option value="6">
                                                6</option>

                                            <option value="7">
                                                7</option>

                                            <option value="8">
                                                8</option>

                                            <option value="9">
                                                9</option>

                                            <option value="10">
                                                10</option>

                                            <option value="11">
                                                11</option>

                                            <option value="12">
                                                12</option>

                                            <option value="13">
                                                13</option>

                                            <option value="14">
                                                14</option>

                                            <option value="15">
                                                15</option>

                                            <option value="16">
                                                16</option>

                                            <option value="17">
                                                17</option>

                                            <option value="18">
                                                18</option>

                                            <option value="19">
                                                19</option>

                                            <option value="20">
                                                20</option>

                                            <option value="21">
                                                21</option>

                                            <option value="22">
                                                22</option>

                                            <option value="23">
                                                23</option>

                                        </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <select size="1" name="min" class="form-control">
                                            <option value="" selected="">Min</option>

                                            <option value="0">
                                                0</option>

                                            <option value="1">
                                                1</option>

                                            <option value="2">
                                                2</option>

                                            <option value="3">
                                                3</option>

                                            <option value="4">
                                                4</option>

                                            <option value="5">
                                                5</option>

                                            <option value="6">
                                                6</option>

                                            <option value="7">
                                                7</option>

                                            <option value="8">
                                                8</option>

                                            <option value="9">
                                                9</option>

                                            <option value="10">
                                                10</option>

                                            <option value="11">
                                                11</option>

                                            <option value="12">
                                                12</option>

                                            <option value="13">
                                                13</option>

                                            <option value="14">
                                                14</option>

                                            <option value="15">
                                                15</option>

                                            <option value="16">
                                                16</option>

                                            <option value="17">
                                                17</option>

                                            <option value="18">
                                                18</option>

                                            <option value="19">
                                                19</option>

                                            <option value="20">
                                                20</option>

                                            <option value="21">
                                                21</option>

                                            <option value="22">
                                                22</option>

                                            <option value="23">
                                                23</option>

                                            <option value="24">
                                                24</option>

                                            <option value="25">
                                                25</option>

                                            <option value="26">
                                                26</option>

                                            <option value="27">
                                                27</option>

                                            <option value="28">
                                                28</option>

                                            <option value="29">
                                                29</option>

                                            <option value="30">
                                                30</option>

                                            <option value="31">
                                                31</option>

                                            <option value="32">
                                                32</option>

                                            <option value="33">
                                                33</option>

                                            <option value="34">
                                                34</option>

                                            <option value="35">
                                                35</option>

                                            <option value="36">
                                                36</option>

                                            <option value="37">
                                                37</option>

                                            <option value="38">
                                                38</option>

                                            <option value="39">
                                                39</option>

                                            <option value="40">
                                                40</option>

                                            <option value="41">
                                                41</option>

                                            <option value="42">
                                                42</option>

                                            <option value="43">
                                                43</option>

                                            <option value="44">
                                                44</option>

                                            <option value="45">
                                                45</option>

                                            <option value="46">
                                                46</option>

                                            <option value="47">
                                                47</option>

                                            <option value="48">
                                                48</option>

                                            <option value="49">
                                                49</option>

                                            <option value="50">
                                                50</option>

                                            <option value="51">
                                                51</option>

                                            <option value="52">
                                                52</option>

                                            <option value="53">
                                                53</option>

                                            <option value="54">
                                                54</option>

                                            <option value="55">
                                                55</option>

                                            <option value="56">
                                                56</option>

                                            <option value="57">
                                                57</option>

                                            <option value="58">
                                                58</option>

                                            <option value="59">
                                                59</option>

                                        </select>
                                        </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="fname" class="col-sm-3 control-label">Vehicle Type</label>
                                    <div class="col-sm-9">
                                        <select name="vehicle_type" id="vehicle_type" class="form-control">


                                        </select>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="fname" class="col-sm-3 control-label">Distance &nbsp; (M)</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" id="distance" name="distance" placeholder="Distance" required="required" >
                                    </div>

                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="fname" class="col-sm-3 control-label">Fare<br/>(2 L.E./M)</label>
                                    <div class="col-sm-9">
                                        <input type="text" style="color: black;" class="form-control" id="appfare" name="appfare" placeholder="Approx Fare" required="required" >
                                    </div>

                                </div>
                            </div>
                            <input type="hidden" id="slave_id" name="slave_id">
                            <br>
                            <div class="row">
                                <div class="col-sm-9">
                                    <button class="btn btn-success" type="button" id="submitbtn">Submit</button>
                                    <button class="btn btn-default"><i class="pg-close"></i> Clear</button>
                                </div>
                            </div>

                        </form>


                    </div>


                </div></div>
            <div class="col-md-8">
                <div id="myMap1" style=" width:100%;height: 700px; right: 1px;"></div>
                <div id="myMap2" style=" width:100%;height: 700px; right: 1px;"></div>
                <div id="myMap3" style=" width:100%;height: 700px; right: 1px;"></div>

            </div>
            </div>
        </div>
    </div>





</div>





</div>








</div>




</div>









</div>

</div>

<!-- END FOOTER -->
</div>
















