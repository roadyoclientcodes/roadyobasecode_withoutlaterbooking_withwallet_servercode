<style>
    #companyid{
        display: none;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {

        refreshTableOnActualcitychagne_();
        $('#selectedcity').change(function () {

            refreshTableOnActualcitychagne_();
        });
//        TOwhomCredit

        $('#MANUALCredit').click(function () {
            $('#MessageNotification').val("");
            $('#AmountTocred').val("");
            $('.curruncyInput').html("");

            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {

                $("#display-Error").text("please select at least one Customer").fadeIn('slow');
                $("#display-Error").text("please select at least one Customer").fadeOut("slow");
            } else if (val.length > 1) {
                $("#display-Error").text("One Customer At a Time.").fadeIn('slow');
                $("#display-Error").text("One Customer At a Time.").fadeOut("slow");
            } else {
                $('#triggerCredit').attr('data', 2);
                $('#CreditModel').modal('show');
                var Name = '';
                var email = '';
                var Curruncy = '';
                $('.checkbox:checked').map(function () {
                    Name = $(this).closest('tr').find('td:eq(2)').text();
                    email = $(this).closest('tr').find('td:eq(3)').text();
                    Curruncy = $(this).closest('tr').find('td:eq(5)').text();
                }).get();

                $('.curruncyInput').html(Curruncy);
                $('#TOwhomCredit').html(Name + " ( " + email + ")");

            }
        });


        $('#Credit').click(function () {

            if ($('#selectedcity').val() == "0") {
                $("#alrtshow").text("Please Select CIty First.");
            }
            $('#AmountTocred').val("");
            $('#MessageNotification').val("");
            $('.curruncyInput').html("");
            $('#triggerCredit').attr('data', 1);
            var textval;
            if ($('#selectedcity').val() == '0') {
                textval = " All Users";
            } else {
                textval = ' All <span class="bg-success font-montserrat all-caps small m-b-5">' + $('#selectedcity').find(":selected").text() + "</span>  Users";
            }

            $.ajax({
                url: "<?php echo base_url() ?>index.php/customerwallet/getCurrencySymb",
                type: "POST",
                data: {cityid: $('#selectedcity').find(":selected").text()},
                dataType: 'JSON',
                success: function (response)
                {
                    if (response.flag == true) {

                        $('.curruncyInput').html(response.smb);

                        $('#CreditModel').modal('show');
                        
                        $('#TOwhomCredit').html(textval);
                    } else {
                        $('#AlertMsg').modal('show');
                        $('#alrtshow').modal('Please Select CityFirst.');
                    }
                }
            });


        });

        $('#triggerCredit').click(function () {

            if(getCountOfTable("big_table") > 0){
            

            if ($('#AmountTocred').val() != '') {
                var dataTosend = {};
                if ($(this).attr('data') == 1)
                    dataTosend = {amount: $('#AmountTocred').val(), 'msg': $('#MessageNotification').val()};
                else if ($(this).attr('data') == 2) {
                    var val = $('.checkbox:checked').map(function () {
                        return this.value;
                    }).get();
                    dataTosend = {amount: $('#AmountTocred').val(), 'msg': $('#MessageNotification').val(), 'user': val};
                }
                $.ajax({
                    url: "<?php echo base_url() ?>index.php/customerwallet/CriditTowallet",
                    type: "POST",
                    data: dataTosend,
                    dataType: 'JSON',
                    success: function (response)
                    {
                        if (response.flag == true) {
                            refreshTableOnActualcitychagne_();
                            $('.triggerClose').trigger('click');

                            $('#alrtshow').html(response.msg);
                            $('#AlertMsg').modal('show');
                        } else {
//                            $("#mobify").css({color: red});
                            $("#mobify").text(response.msg);

                        }
                    }
                });
                }
            }else{
                        $('#CreditModel').modal('hide');
                        $('#alrtshow').html("City doesn't have any User.");
                        $('#AlertMsg').modal('show');
                        
            }
             
        });


    });
    function isNumberKey(evt)
    {
        $("#mobify").text("");
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 41 || charCode > 57)) {
            //    alert("Only numbers are allowed");
//            $("#mobify").css({color: black});
            $("#mobify").text("Only Numbers Are Allowed");
            return false;
        }
        return true;
    }


    function refreshTableOnActualcitychagne_() {
        var date = new Date();
        $('#datepicker-component').datepicker({
            startDate: date
        });
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/customerwallet/GetRechargedata_ajax/'+$('#selectedcity').val(),
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
        table.dataTable(settings);
        // search box for table
        $('#search-table').keyup(function () {
//            alert('hi');
            table.fnFilter($(this).val());
        });
    }

    function getCountOfTable(tableId) {

        var settings1 = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, 'desc']]

        };
        var table = $('#' + tableId).dataTable(settings1);
        return table.fnGetData().length;
    }
</script>

<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content"style="padding-top: 80px">


        <ul class="breadcrumb" style="margin-left: 20px;">
            <li><a href="#" class="">Customer Wallet</a>
            </li>

        </ul>

        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">


                <div class="panel panel-transparent ">

                    <div class="tab-content">
                        <div class="container-fluid container-fixed-lg bg-white">
                            <!-- START PANEL -->
                            <div class="panel panel-transparent m-t-20">
                                <div class="panel-heading">

                                    <div class="pull-left">
                                        <span class="pull-right m-t-10 m-l-10" id="display-Error" style="color:red"></span>
                                        <button class="btn btn-primary pull-right m-t-10" id="Credit" style="margin-left:10px;margin-top: 5px"> CREDIT MULTIPLE USER </button>
                                        <button class="btn btn-primary pull-right m-t-10" id="MANUALCredit" style="margin-left:10px;margin-top: 5px"> MANUAL CREDIT </button>


                                    </div>

                                    <div class="pull-right">

                                        <input type="text" id="search-table" class="form-control pull-right" placeholder="Search "> </div>

                                </div>
                                <div class="panel-body">
                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer">
                                        <div class="table-responsive">
                                            <?php echo $this->table->generate(); ?>

                                        </div><div class="row"></div></div>
                                </div>
                            </div>
                            <!-- END PANEL -->
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>

</div>


<div id="CreditModel" class="modal fade in" role="dialog"  aria-hidden="false">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">

                <p id="txtdelete"> The Amount Will Gets Credited To <span id="TOwhomCredit"></span> </p>
            </div>
            <div class="modal-body">

                <div  class="input-group date col-sm-12">
                    <span class="input-group-addon curruncyInput"></span> <input id="AmountTocred" class="form-control" placeholder="Enter Amount" onkeypress="return isNumberKey(event)" type="text">    
                </div>

                <span id="mobify" style="color:red"></span>

                <div class="m-t-10">
                    <textarea id="MessageNotification" class="form-control" placeholder="Enter Text..." ></textarea>
                </div>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="triggerCredit">TRIGGER</button>
                <button type="button" class="btn btn-default triggerClose" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="AlertMsg" class="modal fade in" role="dialog"  aria-hidden="false">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">
                <p id="alrtshow">   </p>    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

