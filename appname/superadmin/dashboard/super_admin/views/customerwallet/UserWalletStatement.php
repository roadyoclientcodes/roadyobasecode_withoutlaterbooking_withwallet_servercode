<style>
    #selectedcity,#companyid{
        display: none;
    }
</style>
<script type="text/javascript">


    
    $(document).ready(function () {

//        var date = new Date();


        var date = new Date();
        $('#datepicker-component').datepicker({
            startDate: date
        });

        var table = $('#big_table');

        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/customerwallet/CustomerStatement_ajax/<?php echo $driverId; ?>',
                        "bJQueryUI": true,
                        "sPaginationType": "full_numbers",
                        "iDisplayStart ": 20,
                        "oLanguage": {
                            "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
                        },
                        "fnInitComplete": function () {
                            //oTable.fnAdjustColumnSizing();
                        },
                        'fnServerData': function (sSource, aoData, fnCallback)
                        {
                            $.ajax
                                    ({
                                        'dataType': 'json',
                                        'type': 'POST',
                                        'url': sSource,
                                        'data': aoData,
                                        'success': fnCallback
                                    });
                        }
                    };

                    table.dataTable(settings);

                    // search box for table
                    $('#search-table').keyup(function () {
                        table.fnFilter($(this).val());
                    });

//       

                });
</script>

<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content"style="padding-top: 80px">
        <ul class="breadcrumb" style="margin-left: 20px;">
            <li><a href="<?php echo base_url(); ?>index.php/customerwallet/userwallet" class="">CUSTOMER WALLET</a>
            </li>

            <li ><a href="#" class="active"><?php echo $driverinfo->first_name . ' ' . $driverinfo->last_name . ' (' . $driverinfo->email . ')'; ?></a>
            </li>
        </ul>
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">


                <div class="panel panel-transparent ">

                    <div class="tab-content">
                        <div class="container-fluid container-fixed-lg bg-white">
                            <!-- START PANEL -->
                            <div class="panel panel-transparent m-t-20">
                                <div class="panel-heading">



                                    <div class="col-sm-1">
                                        <div class="">
                                            <!--<button class="btn btn-primary" type="button" id="searchData">Search</button>-->
                                        </div>
                                    </div>


                                    <div class="row clearfix">



                                        <div class="">

                                            <div class="pull-right"><input type="text" id="search-table" class="form-control pull-right" placeholder="Search by id"> </div>
                                        </div>
                                    </div>




                                </div>
                                <div class="panel-body">


                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer">
                                        <div class="table-responsive">
                                            <table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                                <thead>

                                                    <tr role="row">
                                                        <th class="sorting_desc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column descending" style="width: 68px;">ID</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column" style="width: 50px;">OPENING BALANCE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column " style="width: 88px;">TRANSACTION TYPE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column" style="width: 88px;">INITIATED BY</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column " style="width: 88px;">BOOKING ID</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column " style="width: 88px;">TRANSACTION DATE</th> 
                                                        <!--<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PAYMENT METHOD</th>--> 
                                                        <!--<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PG COMMISSION</th>--> 
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column" style="width: 88px;">CREDITS BOUGHT (<?php echo currency;?>)</th> 
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column" style="width: 50px;">CLOSING BALANCE</th>



                                                    </tr>


                                                </thead>
                                                <tbody>



                                                    <?php
                                                    if (empty($CustomerStatement)) {
                                                        ?>


                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>

                                                    <?php
                                                } else {
                                                    $openingBal = 0;


                                                    foreach ($CustomerStatement as $result) {
                                                        
                                                        ?>

                                                        <tr role="row"  class="gradeA odd">
                                                            <td class="v-align-middle sorting_1"> <p><?php echo $result->id; ?></p></td>
                                                            <td class="v-align-middle"><?php
                                                                echo $openingBal;
                                                                
                                                                ?></td>
                                                            <td class="v-align-middle sorting_1"> <p><?php echo $result->transectionType; ?></p></td>
                                                            <td class="v-align-middle"><?php echo $result->creditedBy; ?></td>
                                                            <td class="v-align-middle"><?php echo ($result->txn_id == "" ? "<p>-</p>" : $result->txn_id); ?></td>
                                                            <td class="v-align-middle"><?php echo ($result->CreditedDate == "" ? "<p>-</p>" : $result->CreditedDate); ?></td>
                                                            <!--<td class="v-align-middle"><?php echo ($result->PaymentMethod == "" ? "<p>-</p>" : $result->PaymentMethod); ?></td>-->
                                                            <!--<td class="v-align-middle"><?php echo ($result->pgcommission == "" ? "<p>-</p>" : $result->pgcommission); ?></td>-->
                                                            <td class="v-align-middle"><?php echo ($result->CreditedAmount == "" ? "<p>-</p>" : $result->CreditedAmount); ?></td>
                                                            <td class="v-align-middle"><?php echo ($openingBal + $result->CreditedAmount); $openingBal += $result->CreditedAmount; ?></td>

                                                        </tr>
                                                        <?php
                                                    }
                                                }
//                                            
                                                ?>
                                                </tbody>
                                            </table></div><div class="row">

                                        </div></div>

                                </div>
                            </div>
                            <!-- END PANEL -->
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>

</div>



