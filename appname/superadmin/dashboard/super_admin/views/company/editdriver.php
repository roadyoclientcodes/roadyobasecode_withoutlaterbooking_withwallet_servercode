<?php
$this->load->database();
?>

<?php
foreach ($data['masterdoc'] as $row) {
    if ($row->doctype == 1) {
        $licencecertificate = $row->url;
        $expiredate = $row->expirydate;
    } else if ($row->doctype == 2) {
        $bankbook = $row->url;
        $passbookExp = $row->expirydate;
    }
}
?>





<script type="text/javascript" src="https://js.stripe.com/v2/"></script>



<style>
    .form-horizontal .form-group
    {
        margin-left: 13px;
    }

    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>

    function isNumberKey(evt)
    {
        $("#mobify").text("");
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 41 || charCode > 57)) {
            //    alert("Only numbers are allowed");
            $("#mobify").text(<?php echo json_encode(LIST_COMPANY_MOBIFY); ?>);
            return false;
        }
        return true;
    }


    function stripeResponseHandler(status, response) {


        if (response.error) { // Problem!
            alert(response.error.message);
        } else { // Token was created!

            // Get the token ID:
            var token = response.id;
            document.getElementById("Banktocken").value = token;
            stripetockencallback(token);
        }
    }
    function stripeResponseHandlerNew(status, response) {


        if (response.error) { // Problem!
            alert(response.error.message);
        } else { // Token was created!

            var token = response.id;
            stripetockencallbackNew(token);
        }
    }


    function stripetockencallback(token) {

        var data = {IdProoF: $('#idproof').val(),
            "dob": $('#DateOfB').val(), presnalid: $('#presnalid').val(), banktoken: token, 'account_holder_name': $('#account_holder_name').val(),
            'state': $('#state').val(), postalcode: $('#postalcode').val(), cityname: $('#cityname').val(), Address: $('#Address').val(), mas_id: '<?php echo $driverid; ?>'
        };
        $.ajax({
            url: '<?php echo base_url(); ?>index.php/superadmin/AddBankAccountInitial',
            type: 'post',
            data: data,
            dataType: "JSON",
            async: false,
            success: function (data) {
                document.getElementById("addBankAcc").innerHTML = "<i class='fa fa-check'></i> Account added.";
                $('#process').fadeOut('slow');
                if (data.flag == 0) {
                    alert(data.msg);
                    $('#Banktocken').val("");
                    location.reload();
                } else {
                    alert(data.msg);
                }

            },
            error: function (data) {
                $('#process').fadeOut('slow');
                $('#txtdelete').text(data);
            }

        });
    }

    function stripetockencallbackNew(id) {


        if (id == "") {
            $('#process').fadeOut('slow');
            alert('Please Add Bank Details first');
        } else {

            $.ajax({
                url: '<?php echo base_url(); ?>index.php/superadmin/deleteDriverBank',
                type: 'post',
                data: {'id': id, 'uid': '<?php echo $driverid; ?>', deleteaccount: $('#deleteaccount').val()},
                dataType: "JSON",
                async: false,
                success: function (data) {

                    $('#process').fadeOut('slow');
                    if (data.flag == 0) {
                        $('#addnewaccountmodel').modal('hide');
                        $('#deletebanckac').modal('show');
                        $('#txtdelete').text(data.message);
                        $('#addBankAccwhileDeleting').hide();

                        var settings1 = {
                            "sDom": "<'table-responsive't><'row'<p i>>",
                            "sPaginationType": "bootstrap",
                            "destroy": true,
                            "scrollCollapse": true,
                            "oLanguage": {
                                "sLengthMenu": "_MENU_ ",
                                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
                            },
                            "iDisplayLength": 20,
                            "order": [[0, 'desc']]

                        };
                        var table = $('#big_table').DataTable(settings1);
                        table
                                .clear()
                                .draw();

                        $.each(data.userData, function (index, row1) {


                            var rownod = table.row.add([
                                row1.bank_id,
                                row1.account_holder_name,
                                row1.bank_name,
                                row1.routing_number,
                                row1.country,
                                row1.currency,
                                '<button name="" type="button" class="btn btn-danger" onclick="deleteid(this)" value="' + row1.bank_id + '">Delete</button>'
                            ]).draw();
                        });
                    } else {
                        alert(data.message);
                    }

                },
                error: function (data) {

                    $('#txtdelete').text(data);
                }

            });
        }

    }
    function uploadimg(filein) {
        if ($(filein).val() == '')
            return;
        if (!/(\.gif|\.jpg|\.jpeg|\.png)$/i.test($(filein).val())) {
            $(filein).val('');
            alert("Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
            return;
        }
        console.log($(filein).prop('files')[0].size);
        if ($(filein).prop('files')[0].size > 5120000) {
            $(filein).val('');
            alert("Sorry, File is too Large(Max. 5MB).");
            return;
        }
        var formElement = $(filein).prop('files')[0]; //document.getElementById("files_upload_form");
        var form_data = new FormData();
        form_data.append('myfile', formElement);
        $('#process').fadeIn('slow');
        $.ajax({
            url: "<?php echo base_url() ?>index.php/superadmin/uploadimage",
            type: "POST",
            data: form_data,
            dataType: "JSON",
            mimeType: "multipart/form-data",
            //                async: false,
            cache: false,
            contentType: false,
            processData: false,
            async: false,
            success: function (result) {
                $('#process').fadeOut('slow');
                if (result.msg == '1') {
                    $('#idproof').val(result.fileName);
                } else {
                    alert("Error In Uploading File, Please Upload it Again");
                }
            }
        });
    }




    $(document).ready(function () {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/bookDetailctrl/getInfo",
            type: "POST",
            data: { id: <?php echo $driverid ;?>, stripeId : '<?= $data['StripeAccId']?>'},
            dataType: "JSON",
            success: function (result) {
                var count = Object.keys(result).length;
                if(count > 0){
                    var res = result.legal_entity;
                    var day = res.dob.day;
                    var month = res.dob.month;
                    var year = res.dob.year;
                    var date = month + '/' + day + '/'+year;

                    $('#u_dob_day').val(date);

                    $('#u_fname').val(res.first_name);
                    $('#u_lname').val(res.last_name);

                    $('#u_cityname').val(res.address.city);
                    $('#u_Address').val(res.address.line1);
                    $('#u_postalcode').val(res.address.postal_code);
                    $('#u_state').val(res.address.state);
                    $('#u_presnalid').val(res.personal_id_number);
                    $('#u_idProof').val(result.idProof);
                    if(result.transfers_enabled == true){
                        $('#tab5').append('<div class="alert alert-success" role="alert" style="margin:10px;">\
                                            <i id="tab1icon" class="fa fa-check-circle"></i> Details Successfully Verified\
                                        </div>');
                    }else{
                        $('#tab5').append('<div class="alert alert-warning" role="alert" style="margin:10px;">\
                                            <i id="tab1icon" class="fa fa-exclamation-circle"></i> Details Not Verified\
                                        </div>');                        
                    }
//                    if(result.idProof != ""){
//                        $('#u_idproofView').attr('href','http://www.deliveryplus.us/DeliveryPlus/temp/'+result.idProof);
//                        $('#u_idproofView').show();
//                    }
                }
            }
        });

        Stripe.setPublishableKey('<?php echo Stripe_Pk; ?>');
        $('#addBankAcc').click(function () {
            if ($('#DateOfB').val() == "") {
                alert('Date Of Birth Is missing.');
            } else if ($('#presnalid').val() == "") {
                alert('Presnal Id Is missing.');
            } else if ($('#state').val() == "") {
                alert('Presnal Id Is missing.');
            } else if ($('#postalcode').val() == "") {
                alert('postal code  Is missing.');
            } else if ($('#cityname').val() == "") {
                alert('cityname  Is missing.');
            } else if ($('#Address').val() == "") {
                alert('Address  Is missing.');
            } else if ($('#idproof').val() == "") {
                alert('Plz choose Id Proof.');
            } else {
                if ($('#Banktocken').val() == "") {
                    $('#process').fadeIn('slow');
                    Stripe.bankAccount.createToken({
                        country: $('#country').val(),
                        currency: $('#currency').val(),
                        routing_number: $('#routing_number').val(),
                        account_number: $('#account_number').val(),
                        account_holder_name: $('#account_holder_name').val(),
                        account_holder_type: 'individual'//$('.account_holder_type').val()
                    }, stripeResponseHandler);
                } else {
                    stripetockencallback($('#Banktocken').val());
                }
            }
        });
        $('#addBankAccBeforeDelete').click(function () {
            $('#process').fadeIn('slow');
            Stripe.bankAccount.createToken({
                country: $('#countrym').val(),
                currency: $('#currencym').val(),
                routing_number: $('#routing_numberm').val(),
                account_number: $('#account_numberm').val(),
                account_holder_name: $('#account_holder_namem').val(),
                account_holder_type: 'individual'//$('.account_holder_type').val()
            }, stripeResponseHandlerNew);
        });
        $(":file").on("change", function (e) {
            var ext = $(this).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                $(this).val('');
                alert('invalid extension..!   Please choose file type in (gif,png,jpg,jpeg)');
            }
        });
        $('.datepicker-component').on('changeDate', function () {
            $(this).datepicker('hide');
        });
//        $("#datepicker1").datepicker({ minDate: 0});
        var date = new Date();
        $('.datepicker-component').datepicker({
            startDate: date
        });
        $("#firstname").keypress(function (event) {
            var inputValue = event.which;
            //if digits or not a space then don't let keypress work.
            if ((inputValue > 64 && inputValue < 91) // uppercase
                    || (inputValue > 96 && inputValue < 123) // lowercase
                    || inputValue == 32) { // space
                return;
            }
            event.preventDefault();
        });
        $("#lastname").keypress(function (event) {
            var inputValue = event.which;
            //if digits or not a space then don't let keypress work.
            if ((inputValue > 64 && inputValue < 91) // uppercase
                    || (inputValue > 96 && inputValue < 123) // lowercase
                    || inputValue == 32) { // space
                return;
            }
            event.preventDefault();
        });
//

        $("#file_upload").change(function ()

        {
            var iSize = ($("#file_upload")[0].files[0].size / 1024);
            if (iSize / 1024 > 5)

            {
                $("#file_driver_photo").html("your file is too large");
            } else
            {
                iSize = (Math.round(iSize * 100) / 100)
                $("#file_driver_photo").html(iSize + "kb");
            }



        });
        $("#file_upload_l").change(function ()

        {

            var iSize = ($("#file_upload_l")[0].files[0].size / 1024);
            if (iSize / 1024 > 5)

            {
                $("#file_driver_license").html("your file is too large");
            } else
            {
                iSize = (Math.round(iSize * 100) / 100)
                $("#file_driver_license").html(iSize + "kb");
            }



        });
        $("#file_upload_p").change(function ()

        {

            var iSize = ($("#file_upload_p")[0].files[0].size / 1024);
            if (iSize / 1024 > 5)

            {
                $("#file_passbook").html("your file is too large");
            } else
            {
                iSize = (Math.round(iSize * 100) / 100)
                $("#file_passbook").html(iSize + "kb");
            }



        });
        $('.drivers').addClass('active');
        $('.driver_thumb').addClass("bg-success");
        $('#city_select').change(function () {
            $('#getvechiletype').load('<?php echo base_url() ?>index.php/superadmin/ajax_call_to_get_types/vtype', {city: $('#city_select').val()});
        });
        $('#title').change(function () {
            $('#vehiclemodel').load('<?php echo base_url() ?>index.php/superadmin/ajax_call_to_get_types/vmodel', {adv: $('#title').val()});
        });
        $('#addBankAccwhileDeletingd').click(function () {

            $('#deletebanckac').modal('hide');
            $('#addnewaccountmodel').modal('show');
        });
        $('#nextscreen').click(function () {

//            $('#nextscreenHtml').html('slow');
//alert($('#nextscreenHtmlNextScreen').html());
            $('.nextscreenHtml').html($('#nextscreenHtmlNextScreen').html());
//            $('#nextscreenHtml').fadeIn('slow');


        });
        $('#addBankAccwhileDeleting').click(function () {
            $('#deletebanckac').modal('hide');
            $('#addnewaccountmodel').modal('show');
        });
    });
//validations for each previous tab before proceeding to the next tab
    function managebuttonstate()

    {
        $("#prevbutton").addClass("hidden");
        $("#cancelbutton").removeClass("hidden");
    }

    function profiletab(litabtoremove, divtabtoremove)
    {
        var pstatus = true;
        $("#error-box").text("");
        $("#text_firstname").text("");
        $("#text_lastnmae").text("");
        $("#driver_mobile").text("");
        $("#file_driver_photo").text("");
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var mobile = $('#mobile').val();
//        var driverphoto = $('#file_upload').val();



        var password = /^(?=.*\d)(?=.*[a-zA-Z])(?!.*[\W_\x7B-\xFF]).{6,15}$/;
        var number = /^[0-9-+]+$/;
        var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/; //^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var text = /^[a-zA-Z ]*$/;
        var alphabit = /^[a-zA-Z]+$/;
        if (firstname == "" || firstname == null)
        {
            $("#text_firstname").text(<?php echo json_encode(POPUP_DRIVER_FIRSTNAME); ?>);
            pstatus = false;
        }


//        else if (lastname == "" || lastname == null)
//        {
//
//            $("#text_lastnmae").text(<?php echo json_encode(POPUP_DRIVER_LASTNAME); ?>);
//            pstatus = false;
//        }


        else if (mobile == "" || mobile == null)
        {
            $("#driver_mobile").text(<?php echo json_encode(POPUP_DRIVER_MOBILE); ?>);
            pstatus = false;
        }


//        else if((driverphoto == "" || driverphoto == null) && $('#viewimage_hidden').val() == '')
//        {
//            $("#file_driver_photo").text(<?php echo json_encode(POPUP_DRIVER_DRIVERPHOTO); ?>);
//            pstatus = false;
//        }


        if (pstatus === false)
        {
            setTimeout(function ()
            {
                proceed(litabtoremove, divtabtoremove, 'firstlitab', 'tab1');
            }, 300);
            $("#tab1icon").removeClass("fs-14 fa fa-check");
            return false;
        }
        $("#tab1icon").addClass("fs-14 fa fa-check");
        $("#cancelbutton").removeClass("hidden");
        $("#prevbutton").removeClass("hidden");
        $("#nextbutton").removeClass("hidden");
        $("#finishbutton").addClass("hidden");
        return true;
    }

    function addresstab(litabtoremove, divtabtoremove)
    {
        var astatus = true;
//
//                            var number = /^[0-9-+]+$/;
//
////                            var phone = /^\d{10}$/;
////                            var company = /^[-\w\s]+$/;
////                            var re = /[a-zA-Z0-9\-\_]$/;
//
//                            var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/; //^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//                            var text = /^[a-zA-Z ]*$/;
//                            var alphabit = /^[a-zA-Z]+$/;





        if (profiletab(litabtoremove, divtabtoremove))
        {
            astatus == true;
            $("#text_password").text("");
            $("#text_zip").text("");
//              $('#email').attr('disabled', true);

            var email = $("#email").val();
            var password = $("#password").val();
            var zipcode = $('#zipcode').val();
//         var driverphoto = $('#file_upload').val();



            var pass = /^(?=.*\d)(?=.*[a-zA-Z])(?!.*[\W_\x7B-\xFF]).{6,15}$/;
//            if (email == "" || email == null)
//            {
//                $("#editerrorbox").html(<?php echo json_encode(POPUP_DRIVER_DRIVER_EMAIL); ?>);
//                astatus = false;
//            }
////                            
//
//            else if (validateEmail($("#email").val()) !== 2)
//            {
//                $("#editerrorbox").html(<?php echo json_encode(POPUP_DRIVER_DRIVER_YOUREMAIL); ?>);
//                astatus = false;
//
//
//            }
//            else if ($("#email").attr('data') == 1)
//            {
//                $("#editerrorbox").html(<?php echo json_encode(POPUP_DRIVER_DRIVER_ALLOCATED); ?>);
//                astatus = false;
//
//
//            }

            if (password == "" || password == null)
            {
                $("#text_email").text("");
                $("#text_password").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_PASSWORD); ?>);
                astatus = false;
            }
//            else if ((!pass.test(password)))
//            {
//                $("#text_email").text("");
//                $("#text_password").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_PASSWORD_VALID); ?>);
//                astatus = false;
//            }


//            else if (zipcode == "" || zipcode == null)
//            {
//                $("#text_password").text("");
//                $("#text_zip").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_ZIPCODE); ?>);
//                astatus = false;
//            }

            if (astatus === false)
            {
                setTimeout(function ()
                {
                    proceed(litabtoremove, divtabtoremove, 'secondlitab', 'tab2');
                }, 100);
//                alert("complete Login Details tab properly")
                $("#tab2icon").removeClass("fs-14 fa fa-check");
                return false;
            }

            $("#tab2icon").addClass("fs-14 fa fa-check");
            $("#finishbutton").addClass("hidden");
            $("#nextbutton").removeClass("hidden");
            return astatus;
        }
    }

    function bonafidetab(litabtoremove, divtabtoremove)
    {
        var bstatus = true;
        if (addresstab(litabtoremove, divtabtoremove))
        {
            if ($("#expirationrc").val() == '')
            {
                bstatus = false;
            }

            if (bstatus === false)
            {
                setTimeout(function ()
                {
                    proceed(litabtoremove, divtabtoremove, 'thirdlitab', 'tab3');
                }, 100);
                alert("complete Driving Licence tab properly");
                $("#tab3icon").removeClass("fs-14 fa fa-check");
                return false;
            }

            $("#tab3icon").addClass("fs-14 fa fa-check");
            $("#nextbutton").removeClass("hidden");
            $("#finishbutton").addClass("hidden");
            $("#cancelbutton").removeClass("hidden");
            return bstatus;
        }
    }
    
    function bookingtab(litabtoremove, divtabtoremove)
    {
        var bstatus = true;
        if (bonafidetab(litabtoremove, divtabtoremove))
        {
//            if (isBlank(isBlank($("#expirationrc").val())))
//            {
//                bstatus = false;
//            }
//
//            if (bstatus === false)
//            {
//                setTimeout(function ()
//                {
//                    proceed(litabtoremove, divtabtoremove, 'thirdlitab', 'tab3');
//                }, 100);
//                alert("complete Driving Licence tab properly");
//                $("#tab3icon").removeClass("fs-14 fa fa-check");
//                return false;
//            }

            $("#tab4icon").addClass("fs-14 fa fa-check");
            $("#nextbutton").addClass("hidden");
            $("#finishbutton").removeClass("hidden");
            $("#cancelbutton").removeClass("hidden");
            return bstatus;
        }
    }

    function signatorytab(litabtoremove, divtabtoremove)
    {
        var bstatus = true;
        if (bonafidetab(litabtoremove, divtabtoremove))
        {
            if (isBlank($("#entitypersonname").val()) || isBlank($("#entitysignatorymobileno").val()) || isBlank($("#entitysignatoryimagefile").val()) || $("#entitydegination").val() === "null")
            {
                bstatus = false;
            }

            if (validateEmail($("#entityemail").val()) !== 2)
            {
                bstatus = false;
            }

            if (bstatus === false)
            {
                setTimeout(function ()
                {
                    proceed(litabtoremove, divtabtoremove, 'fourthlitab', 'tab4');
                }, 100);
                alert("complete Other Document tab properly");
                $("#tab4icon").removeClass("fs-14 fa fa-check");
                return false;
            }

            $("#tab4icon").addClass("fs-14 fa fa-check");
            $("#cancelbutton").removeClass("hidden");
            $("#nextbutton").addClass("hidden");
            $("#finishbutton").removeClass("hidden");
            return bstatus;
        }

    }


    function proceed(litabtoremove, divtabtoremove, litabtoadd, divtabtoadd)
    {
        $("#" + litabtoremove).removeClass("active");
        $("#" + divtabtoremove).removeClass("active");
        $("#" + litabtoadd).addClass("active");
        $("#" + divtabtoadd).addClass("active");
    }

    /*-----managing direct click on tab is over -----*/

//manage next next and finish button
    function movetonext()
    {
        var currenttabstatus = $("li.active").attr('id');
        if (currenttabstatus === "firstlitab")
        {
            profiletab('secondlitab', 'tab2');
            proceed('firstlitab', 'tab1', 'secondlitab', 'tab2');
        } else if (currenttabstatus === "secondlitab")
        {
            addresstab('thirdlitab', 'tab3');
            proceed('secondlitab', 'tab2', 'thirdlitab', 'tab3');
        } else if (currenttabstatus === "thirdlitab")
        {
            bonafidetab('fourthlitab', 'tab4');
            proceed('thirdlitab', 'tab3', 'fourthlitab', 'tab4');
            $("#finishbutton").addClass("hidden");
            $("#nextbutton").removeClass("hidden");
        } else if (currenttabstatus === "fourthlitab")
        {
            bookingtab('fifthlitab', 'tab5');
            proceed('fourthlitab', 'tab4', 'fifthlitab', 'tab5');
            $("#finishbutton").removeClass("hidden");
            $("#nextbutton").addClass("hidden");
        }
    }

    function movetoprevious()
    {
        var currenttabstatus = $("li.active").attr('id');
        if (currenttabstatus === "secondlitab")
        {
            profiletab('secondlitab', 'tab2');
            proceed('secondlitab', 'tab2', 'firstlitab', 'tab1');
            $("#prevbutton").addClass("hidden");
        } else if (currenttabstatus === "thirdlitab")
        {
            addresstab('thirdlitab', 'tab3');
            proceed('thirdlitab', 'tab3', 'secondlitab', 'tab2');
            $("#nextbutton").removeClass("hidden");
            $("#finishbutton").addClass("hidden");
            $("#prevbutton").removeClass("hidden");
        } else if (currenttabstatus === "fourthlitab")
        {            
            bonafidetab('fourthlitab', 'tab4');
            proceed('fourthlitab', 'tab4', 'thirdlitab', 'tab3');
            $("#finishbutton").addClass("hidden");
            $("#nextbutton").removeClass("hidden");
            $("#prevbutton").removeClass("hidden");
        } else if (currenttabstatus === "fifthlitab")
        {            
            bonafidetab('fourthlitab', 'tab4');
            proceed('fifthlitab', 'tab5', 'fourthlitab', 'tab4');
            $("#finishbutton").addClass("hidden");
            $("#nextbutton").removeClass("hidden");
            $("#prevbutton").removeClass("hidden");
        }
//    else if(currenttabstatus === "fourthlitab")
//    {
//        bonafidetab('fourthlitab','tab4');
//        proceed('fourthlitab','tab4','thirdlitab','tab3');
//        $("#nextbutton").removeClass("hidden");
//        $("#finishbutton").addClass("hidden");
//    }
    }

//here this function validates all the field of form while adding new subadmin you can find all related functions in RylandInsurence.js file

    function validate() {

        if (!isBlank($("#Firstname").val()))
        {
            if (!isAlphabet($("#Firstname").val()))
            {
                $("#errorbox").html("Enter only character in First name");
                return false;
            }
        } else
        {
            $("#errorbox").html("First name is blank");
            return false;
        }
    }
    function validateForm()
    {
        if (!isBlank($("#Firstname").val()))
        {
            if (!isAlphabet($("#Firstname").val()))
            {
                $("#errorbox").html("Enter only character in First name");
                return false;
            }
        } else
        {
            $("#errorbox").html("First name is blank");
            return false;
        }

        if (!isBlank($("#Lastname").val()))
        {
            if (!isAlphabet($("#Lastname").val()))
            {
                $("#errorbox").html("Enter only character in Last name");
                return false;
            }
        } else
        {
            $("#errorbox").html("Last name is blank");
            return false;
        }

        if (validateEmail($("#Email").val()) == 1)
        {

            $("#errorbox").html("Enter valid email");
            return false;
        }

        if (isBlank($("#Password").val()))
        {
            $("#errorbox").html("Password is Blank");
            return false;
        }

        if (!MatchPassword($("#Password").val(), $("#Cpassword").val()))
        {
            $("#errorbox").html("Password not matching");
            return false;
        }
        // return true;
    }

    function ValidateFromDb() {

        $.ajax({
            url: "validateEmail",
            type: "POST",
            data: {email: $('#email').val()},
            dataType: "JSON",
            success: function (result) {

                $('#email').attr('data', result.msg);
                if (result.msg == 1) {

                    $("#editerrorbox").html("Email is already allocated !");
                    $('#email').focus();
                    return false;
                } else if (result.msg == 0) {
                    $("#editerrorbox").html("");
                }
            }
        });
    }


    function submitform()
    {


        $("#file_driver_license").val('');
        $("#file_driver_exdate").val('');
        $("#file_passbook").val('');
        var uploaddrivinglicence = $("#file_upload_l").val();
        var expiredatelicence = $("#expirationrc").val();
        var expiredatePassbook = $("#expirationPassbook").val();
//        var bankpassbook = $("#file_upload_p").val();


//        if ((uploaddrivinglicence == "" || uploaddrivinglicence == null) && $('#licence_hidden').val() == '')
//        {
//           $("#file_driver_license").text(<?php echo json_encode(POPUP_DRIVER_FILE_POPUP_DRIVING); ?>);
//        }


//        else if (expiredatelicence == "" || expiredatelicence == null)
//        {
//
//             $("#file_driver_license").text("");
//            $("#file_driver_exdate").text(<?php echo json_encode(POPUP_DRIVER_FILE_POPUP_EXPERIENCE); ?>);
//
//        }
//        else if (expiredatePassbook == "" || expiredatePassbook == null)
//        {
//
//             $("#file_passbook_exp").text("");
//            $("#file_passbook_exp").text(<?php echo json_encode(POPUP_DRIVER_FILE_POPUP_EXPERIENCE_PASSBOOK); ?>);
//
//        }

//        else if ((bankpassbook == "" || bankpassbook == null) && $('#passbook_hidden').val() == '')
//        {
//           $("#file_driver_exdate").text("");
//             $("#file_passbook").text(<?php echo json_encode(POPUP_DRIVER_FILE_POPUP_BANKPASSBOOK); ?>);
//
//        }

//        else {
        $('#addentity').submit();
//        }

    }

    function cancel() {

        window.location = "<?php echo base_url(); ?>//index.php/superadmin/Drivers/my/1";
    }

    function deleteid(pointer) {

        var settings1 = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, 'desc']]

        };
        var table = $('#big_table').dataTable(settings1);
        if (table.fnGetData().length > 1) {
            deleteIndividualBankInfo(pointer);
        } else {

            $('#addBankAccwhileDeleting').show();
            var id = $(pointer).val();
            $('#deletebanckac').modal('show')
            $("#deleteaccount").val(id);
        }
    }
    function deleteIndividualBankInfo(pointer) {
        if (confirm("Are You Sure ? ")) {
            $('#process').fadeIn('slow');
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/superadmin/deleteDriverBank',
                type: 'post',
                data: {'uid': '<?php echo $driverid; ?>', deleteaccount: $(pointer).val()},
                dataType: "JSON",
                async: false,
                success: function (data) {
                    $('#process').fadeOut('slow');
                    if (data.flag == 0) {


                        var settings1 = {
                            "sDom": "<'table-responsive't><'row'<p i>>",
                            "sPaginationType": "bootstrap",
                            "destroy": true,
                            "scrollCollapse": true,
                            "oLanguage": {
                                "sLengthMenu": "_MENU_ ",
                                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
                            },
                            "iDisplayLength": 20,
                            "order": [[0, 'desc']]

                        };
                        var table = $('#big_table').DataTable(settings1);
                        table
                                .row($(pointer).parents('tr'))
                                .remove()
                                .draw();

                    } else {
                        alert(data.message);
                    }

                },
                error: function (data) {

                    $('#txtdelete').text(data);
                }

            });
        }

    }

    function updateInfo()
    {
        var str = $('#u_dob_day').val();
        var res = str.split("/");
        var month = res[0];
        var day = res[1];
        var year = res[2];
        var u_fname = $('#u_fname').val();
        var u_lname = $('#u_lname').val();
        var cityname = $('#u_cityname').val();
        var Address = $('#u_Address').val();
        var postalcode = $('#u_postalcode').val();
        var state = $('#u_state').val();
        var presnalid = $('#u_presnalid').val();
        var old_idproof = $('#u_idProof').val();
        var stripeid = $('#u_stripe_acc_id').val();

        var formElement = $($('#u_uploadautherFile')).prop('files')[0];              //document.getElementById("files_upload_form");
        var form_data = new FormData();
        form_data.append('myfile', formElement);
        form_data.append('id', <?php echo $driverid; ?>);

        form_data.append('dob_month', month);
        form_data.append('dob_day', day);
        form_data.append('dob_year', year);
        form_data.append('u_fname', u_fname);
        form_data.append('u_lname', u_lname);
        form_data.append('cityname', cityname);
        form_data.append('Address', Address);
        form_data.append('postalcode', postalcode);
        form_data.append('state', state);
        form_data.append('presnalid', presnalid);
        form_data.append('old_idproof', old_idproof);
        form_data.append('stripeid', stripeid);

        $.ajax({
            url: "<?php echo base_url(); ?>index.php/bookDetailctrl/addInfo",
            type: "POST",
            data: form_data,
            dataType: "JSON",
            mimeType: "multipart/form-data",
            //                async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                if(result.error == 0)
                    alert("Successfully Updated");
                else
                    alert("Problem in Update Info");
            }

        });
    }

</script>




<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron bg-white" data-pages="parallax">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb" style="margin-left: 20px;">
                    <li><a href="<?php echo base_url(); ?>//index.php/superadmin/Drivers/my/1" class=""><?php echo LIST_DRIVER; ?></a>
                    </li>

                    <li style="width: 100px"><a href="#" class="active"><?php echo LIST_DRIVER_EDIT; ?></a>
                    </li>
                </ul>
                <!-- END BREADCRUMB -->
            </div>



            <div class="container-fluid container-fixed-lg bg-white">

                <div id="rootwizard" class="m-t-50">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm" id="mytabs">
                        <li class="active" id="firstlitab" onclick="managebuttonstate()">
                            <a data-toggle="tab" href="#tab1" id="tb1"><i id="tab1icon" class=""></i> <span><?php echo LIST_DRIVER_PESIONALDETAILS; ?></span></a>
                        </li>
                        <li class="" id="secondlitab">
                            <a data-toggle="tab" href="#tab2" onclick="profiletab('secondlitab', 'tab2')" id="mtab2"><i id="tab2icon" class=""></i> <span><?php echo LIST_DRIVER_LOGINDETAILS; ?></span></a>
                        </li>
                        <li class="" id="thirdlitab">
                            <a data-toggle="tab" href="#tab3" onclick="addresstab('thirdlitab', 'tab3')"><i id="tab3icon" class=""></i> <span><?php echo LIST_DRIVER_DRIVINGLICENCE; ?></span></a>
                        </li>
                        <li class="" id="fourthlitab">
                            <a data-toggle="tab" href="#tab4" onclick="bonafidetab('fourthlitab', 'tab4')">

                                <i id="tab4icon" class="fa fa-credit-card tab-icon"></i> <span><?php echo BANK_ACC; ?></span></a>
                        </li>
                        <li class="" id="fifthlitab">
                            <a data-toggle="tab" href="#tab5" onclick="bookingtab('fifthlitab', 'tab5')"><i id="tab5icon" class=""></i> <span>Stripe Details</span></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <form id="addentity" class="form-horizontal" role="form" action="<?php echo base_url(); ?>index.php/superadmin/editdriverdata" method="post" enctype="multipart/form-data">
                        <div class="tab-content">
                            <?php foreach ($data['masterdata'] as $row) { ?>

                                <input type="hidden" value="<?php echo $driverid; ?>" name="driver_id"/>
                                <div class="tab-pane padding-20 slide-left active" id="tab1">
                                    <div class="row row-same-height">


                                        <div class="form-group">
                                            <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_FIRSTNAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">

                                                <input type="text"  id="firstname" name="firstname" required="required"class="form-control" value="<?php echo $row->first_name; ?>"/>

                                            </div>
                                            <div class="col-sm-3 error-box" id="text_firstname"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_LASTNAME; ?></label>
                                            <div class="col-sm-6">

                                                <input type="text" id="lastname" name="lastname" required="required"class="form-control" value="<?php echo $row->last_name; ?>">

                                            </div>
                                            <div class="col-sm-3 error-box" id="text_lastnmae"></div>
                                        </div>



                                        <div class="form-group">
                                            <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_MOBILE; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">

                                                <input type="text"  id="mobile" name="mobile" required="required"class="form-control" onkeypress="return isNumberKey(event)" value="<?php echo $row->mobile; ?>">

                                            </div>
                                            <div class="col-sm-3 error-box" id="driver_mobile"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="address" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_UPLOADPHOTO; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">
                                                <!--                <input type="text" class="form-control" name="entitydocname" id="entitydocname">-->
                                                <input type="file" class="form-control" style="height: 37px;" name="photos" id="file_upload" >

                                                <input type="hidden" value="<?php echo $row->profile_pic; ?>" id='viewimage_hidden'/>
                                                <?php
                                                if ($row->profile_pic != '') {
                                                    ?>
                                                    <a target="_blank" href="<?php echo base_url() ?>../../pics/<?php echo $row->profile_pic; ?>">view</a> 

                                                <?php }
                                                ?>




                                            </div>
                                            <div class="col-sm-3 error-box" id="file_driver_photo"></div>
                                        </div>







                                    </div>
                                </div>
                                <div class="tab-pane slide-left padding-20" id="tab2">
                                    <div class="row row-same-height">

                                        <div class="form-group">
                                            <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_EMAIL; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">

                                                <input type="email" data="1" id="email" name="email" required="required" onblur="ValidateFromDb()" class="form-control" value="<?php echo $row->email; ?>" disabled="disabled">


                                            </div>
                                            <span id="editerrorbox" class="col-sm-3 control-label" style="color: #ff0000"></span>
                                            <div class="col-sm-3 error-box" id="text_email"></div>
                                        </div>




                                        <div class="form-group">
                                            <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_PASSWORD; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">

                                                <input type="password"  id="password" name="password" required="required" class="form-control" value="<?php echo $row->password; ?>">

                                            </div>
                                            <div class="col-sm-3 error-box" id="text_password"></div>
                                        </div>

                                        <!--                                    <div class="form-group">
                                                                                <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_ZIPCODE; ?><span style="color:red;font-size: 18px">*</span></label>
                                                                                <div class="col-sm-6">
                                        
                                                                                    <input type="text"  id="zipcode" name="zipcode" required="required" class="form-control" value="<?php echo $row->zipcode; ?>" onkeypress="return isNumberKey(event)">
                                        
                                                                                </div>
                                                                                <div class="col-sm-3 error-box" id="text_zip"></div>
                                                                            </div>-->
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="tab-pane slide-left padding-20" id="tab3">
                                <div class="row row-same-height">


                                    <div class="form-group">

                                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_UPLOADDRIVERLICENSE; ?></label>

                                        <div class="col-sm-6">
                                            <input type="file" class="form-control" style="height: 37px;" name="certificate" id="file_upload_l">

                                            <input type="hidden" value="<?php echo $licencecertificate; ?>" id='licence_hidden'/>

                                            <?php if ($licencecertificate != '') { ?>
                                                <a target="_blank" href="<?php echo base_url() ?>../../pics/<?php echo $licencecertificate; ?>">view</a> 

                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="col-sm-3 error-box" id="file_driver_license"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_EXDATE; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input id="expirationrc" name="expirationrc" required="required"  type="" class="form-control datepicker-component"
                                                   value="" autocomplete="off">
                                        </div>
                                        <div class="col-sm-3 error-box" id="file_driver_exdate"></div>
                                    </div>

                                   <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_UPLOADPASSBOOK; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                           
                                            <input type="file" class="form-control" style="height: 37px;" name="passbook"  id="file_upload_p">
                                                    
                                                  
                                            <input type="hidden" value="<?php echo  $bankbook; ?>" id='passbook_hidden'/>
                                            <?php if($bankbook !=''){?>
                                             <a target="_blank" href="<?php echo base_url()?>../../pics/<?php echo $bankbook; ?>">view</a> 
                                                 <?php } ?>
                                        </div>
                                        <div class="col-sm-3 error-box" id=file_passbook></div>
                                    </div>



                                </div>
                            </div>





                            <!--// bank hidden form fileds-->

                            <input type="hidden" id="Banktocken" name="Banktocken">



                            <div class="tab-pane slide-left padding-20" id="tab4">
                                <div class="row row-same-height">

                                    <?php if (empty($data['bankData'])) { ?>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading text-center">
                                                        <h4><span id="card_type"></span> Add a bank account</h4>
                                                    </div>
                                                    <div class="panel-body">


                                                        <!--                                                    <form action="" method="POST" id="payment-form">
                                                                                                             </form>-->
                                                        <div class="errors"></div>
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <label>Country</label>
                                                                    <select class="form-control input-lg" id="country" data-stripe="country">
                                                                        <option value="US">United States</option>
                                                                        <option value="AU">Australia</option>
                                                                        <option value="AT">Austria</option>
                                                                        <option value="BE">Belgium</option>
                                                                        <option value="BR">Brazil</option>
                                                                        <option value="CA">Canada</option>
                                                                        <option value="DK">Denmark</option>
                                                                        <option value="FI">Finland</option>
                                                                        <option value="FR">France</option>
                                                                        <option value="DE">Germany</option>
                                                                        <option value="HK">Hong Kong</option>
                                                                        <option value="IE">Ireland</option>
                                                                        <option value="IT">Italy</option>
                                                                        <option value="JP">Japan</option>
                                                                        <option value="LU">Luxembourg</option>
                                                                        <option value="MX">Mexico</option>
                                                                        <option value="NZ">New Zealand</option>
                                                                        <option value="NL">Netherlands</option>
                                                                        <option value="NO">Norway</option>
                                                                        <option value="PT">Portugal</option>
                                                                        <option value="SG">Singapore</option>
                                                                        <option value="ES">Spain</option>
                                                                        <option value="SE">Sweden</option>
                                                                        <option value="CH">Switzerland</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Currency</label>
                                                                    <select class="form-control input-lg" id="currency" data-stripe="currency">
                                                                        <option value="usd">USD</option>
                                                                        <option value="aud">AUD</option>
                                                                        <option value="brl">BRL</option>
                                                                        <option value="cad">CAD</option>
                                                                        <option value="eur">EUR</option>
                                                                        <option value="gbp">GBP</option>
                                                                        <option value="hkd">HKD</option>
                                                                        <option value="jpy">JPY</option>
                                                                        <option value="mxn">MXN</option>
                                                                        <option value="nzd">NZD</option>
                                                                        <option value="sgd">SGD</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Full Legal Name</label>
                                                                    <input class="form-control input-lg account_holder_name" id="account_holder_name" type="text" data-stripe="account_holder_name" placeholder="Jane Doe" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <!--                                                            <div class="col-md-6">
                                                                                                                            <div class="form-group">
                                                                                                                                <label>Account Type</label>
                                                                                                                                <select class="form-control input-lg account_holder_type" id="account_holder_type" data-stripe="account_holder_type">
                                                                                                                                    <option value="individual">Individual</option>
                                                                                                                                    <option value="company">Company</option>
                                                                                                                                </select>
                                                                                                                            </div>
                                                                                                                        </div>-->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6" id="routing_number_div">
                                                                <div class="form-group">
                                                                    <label id="routing_number_label">Routing Number</label>
                                                                    <input class="form-control input-lg bank_account" id="routing_number" type="tel" size="12" data-stripe="routing_number" placeholder="111000025" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label id="account_number_label">Account Number</label>
                                                                    <input class="form-control input-lg bank_account" id="account_number" type="tel" size="20" data-stripe="account_number" placeholder="000123456789" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <?php if ($data['bankAccountStatus'] == 1) { ?>
                                                            <input type="hidden" id="idproof">
                                                            <div id="nextscreenHtmlNextScreen">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label> City </label>
                                                                            <input class="form-control input-lg" id="cityname"  placeholder="City name" autocomplete="off">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label id="account_number_label">Address </label>
                                                                            <input class="form-control input-lg" id="Address" placeholder="Address" autocomplete="off">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label> postal code </label>
                                                                            <input class="form-control"   id="postalcode" placeholder="postal code" autocomplete="off">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label id="account_number_label">state</label>
                                                                            <input class="form-control input-lg" placeholder="state" id="state" autocomplete="off">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label> personal id number </label>
                                                                            <input class="form-control"   id="presnalid" placeholder="personal id number" autocomplete="off">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label> DOB </label>
                                                                            <div id="datepicker-component" class="input-group date col-sm-12 ">
                                                                                <input type="text" class="form-control" id="DateOfB"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="form-group">

                                                                    <label for="fname" class="col-sm-3 control-label">ID PROOF</label>

                                                                    <div class="col-sm-6">
                                                                        <input type="file" class="form-control" style="height: 37px;" onchange="uploadimg(this)">

                                                                    </div>
                                                                    <div class="col-sm-3 error-box" id="file_driver_license"></div>
                                                                </div>


                                                            </div>


                                                        <?php } ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button class="btn btn-lg btn-block btn-success " type="button" id="addBankAcc">VERIFY ACCOUNT</button>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    <?php } else { ?>
                                        <div class="row">

                                            <?php // echo form_button(['name' => 'addnew', 'content' => 'Add New', 'class' => 'btn btn-primary pull-right', 'data-toggle' => 'modal', 'data-target' => '#addnewaccountmodel']);  ?>


                                            <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive"><table class="table table-hover demo-table-search dataTable no-footer" class="tableWithSearch1" id="big_table" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">
                                                        <thead>

                                                            <tr role="row">
                                                                <th  tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;font-size:15px">BANK ID</th>
                                                                <th  tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;font-size:15px">ACC HOLDER NAME</th>
                                                                <th  tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;font-size:15px">BANK NAME</th>
                                                                <th  tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;font-size:15px">routing number</th>
                                                                <th  tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;font-size:15px">country</th>
                                                                <th  tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;font-size:15px">currency</th>
                                                                <th  tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;font-size:15px">ACTION</th>

                                                            </tr>


                                                        </thead>
                                                        <tbody>

                                                            <?php
                                                            foreach ($data['bankData'] as $rep) {
                                                                ?>

                                                                <tr role="row"  class="gradeA odd">
                                                                    <td id="d_no" class="v-align-middle sorting_1"> <p><?php echo $rep['bank_id']; ?></p></td>
                                                                    <td id="d_no" class="v-align-middle sorting_1"> <p><?php echo $rep['account_holder_name']; ?></p></td>
                                                                    <td class="v-align-middle"><?php echo $rep['bank_name']; ?></td>
                                                                    <td class="v-align-middle"><?php echo $rep['routing_number']; ?></td>
                                                                    <td class="v-align-middle"><?php echo $rep['country']; ?></td>
                                                                    <td class="v-align-middle"><?php echo $rep['currency']; ?></td>
                                                                    <td class="v-align-middle"><?= form_button(['class' => 'bankid', 'onclick' => "deleteid(this)", 'content' => 'Delete', 'class' => 'btn btn-danger', 'value' => $rep['bank_id']]); ?></td>
                                                                <?php } ?>
                                                        </tbody>
                                                    </table></div><div class="row"></div></div>


                                        </div>

                                    <?php } ?>



                                </div>
                            </div>


                            <div class="tab-pane slide-left padding-20" id="tab5">

                                <?php
                                    if($data['StripeAccId'] == "")
                                    {
                                        echo '<div class="alert alert-danger" role="alert" style="margin:10px;">
                                            <i id="tab1icon" class="fa fa-times-circle"></i> For Adding Bank Details First create an Account
                                        </div>';
                                    }else{
                                ?>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="panel panel-default">
                                            <div class="panel-heading text-center">
                                                <!--<h4><span id="card_type"></span> Add a bank account</h4>-->
                                            </div>
                                            <div class="panel-body">
                                                <input type="hidden" id='u_stripe_acc_id' value="<?= $data['StripeAccId']?>"/>

                                                <div class="row">
                                                    <div class="col-md-6" id="routing_number_div">
                                                        <div class="form-group">
                                                            <label id="routing_number_label">First Name</label>
                                                            <input class="form-control" id="u_fname" type="tel" size="12" data-stripe="u_fname" placeholder="first name" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label id="account_number_label">Last Name</label>
                                                            <input class="form-control" id="u_lname" type="tel" size="20" data-stripe="u_lname" placeholder="last name" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>



                                                <div id="nextscreenHtmlNextScreen">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> City </label>
                                                                <input class="form-control" id="u_cityname" data-stripe="cityname" placeholder="City name" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label id="account_number_label">Address </label>
                                                                <input class="form-control " id="u_Address" data-stripe="Address" placeholder="Address" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> postal code </label>
                                                                <input class="form-control" id="u_postalcode" data-stripe="postalcode" placeholder="postal code" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label id="account_number_label">state</label>
                                                                <input class="form-control" placeholder="state" id="u_state" data-stripe="state" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> personal id number </label>
                                                                <input class="form-control" id="u_presnalid" data-stripe="presnalid" placeholder="personal id number" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> DOB </label>
                                                                <div id="datepicker-component" class="input-group date col-sm-12 ">
                                                                    <input type="text" class="form-control" id="u_dob_day"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <label for="fname" class="col-sm-3 ">ID PROOF</label>
                                                        <input type="hidden" id="u_idProof">
                                                        <div class="col-sm-6">
                                                            <input type="file" class="form-control" style="height: 37px;" id="u_uploadautherFile">
                                                            <a href="" style="display:none;" id="u_idproofView" target="_blank">view</a>

                                                        </div>
                                                        <div class="col-sm-3 error-box" id="file_driver_license"></div>
                                                    </div>


                                                </div>


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button onclick="updateInfo()" class="btn btn-lg btn-block btn-success submit" type="button" id="u_addBankAcc">VERIFY ACCOUNT</button>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    }
                                ?>

                            </div>

                            <div class="padding-20 bg-white">
                                <ul class="pager wizard">
                                    <li class="next" id="nextbutton">
                                        <button class="btn btn-primary btn-cons btn-animated from-left  pull-right" type="button" onclick="movetonext()">
                                            <span><?php echo BUTTON_NEXT; ?></span>
                                        </button>
                                    </li>
                                    <li class="hidden" id="finishbutton">
                                        <button class="btn btn-primary btn-cons btn-animated from-left fa fa-cog pull-right" type="button" onclick="submitform()">
                                            <span><?php echo BUTTON_FINISH; ?></span>
                                        </button>
                                    </li>

                                    <li class="previous hidden" id="prevbutton">
                                        <button class="btn btn-default btn-cons pull-right" type="button" onclick="movetoprevious()">
                                            <span><?php echo BUTTON_PREVIOUS; ?></span>
                                        </button>
                                    </li>
                                    <li class="" id="cancelbutton">

                                        <button  type="button" class="btn btn-default btn-cons pull-right" onclick = "cancel()" ><?php echo BUTTON_CANCEL; ?></button>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </form>



                </div>


            </div>
            <!-- END PANEL -->
        </div>

    </div>
    <!-- END JUMBOTRON -->


    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

</div>
<!-- END PAGE CONTENT -->
<!-- START FOOTER -->

<!-- END FOOTER -->

<!--Add new Account model-->
<div id="addnewaccountmodel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                <h4><span id="card_type"></span> Add a bank account Detail</h4>
                            </div>
                            <div class="panel-body nextscreenHtml">


                                <!--                                                    <form action="" method="POST" id="payment-form">
                                                                                     </form>-->
                                <div class="errors"></div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control input-lg" id="countrym" data-stripe="country">
                                                <option value="US">United States</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BR">Brazil</option>
                                                <option value="CA">Canada</option>
                                                <option value="DK">Denmark</option>
                                                <option value="FI">Finland</option>
                                                <option value="FR">France</option>
                                                <option value="DE">Germany</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IT">Italy</option>
                                                <option value="JP">Japan</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MX">Mexico</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="NO">Norway</option>
                                                <option value="PT">Portugal</option>
                                                <option value="SG">Singapore</option>
                                                <option value="ES">Spain</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Currency</label>
                                            <select class="form-control input-lg" id="currencym" data-stripe="currency">
                                                <option value="usd">USD</option>
                                                <option value="aud">AUD</option>
                                                <option value="brl">BRL</option>
                                                <option value="cad">CAD</option>
                                                <option value="eur">EUR</option>
                                                <option value="gbp">GBP</option>
                                                <option value="hkd">HKD</option>
                                                <option value="jpy">JPY</option>
                                                <option value="mxn">MXN</option>
                                                <option value="nzd">NZD</option>
                                                <option value="sgd">SGD</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Full Legal Name</label>
                                            <input class="form-control input-lg account_holder_name" id="account_holder_namem" type="text" data-stripe="account_holder_name" placeholder="Jane Doe" autocomplete="off">
                                        </div>
                                    </div>
                                    <!--                                                            <div class="col-md-6">
                                                                                                    <div class="form-group">
                                                                                                        <label>Account Type</label>
                                                                                                        <select class="form-control input-lg account_holder_type" id="account_holder_type" data-stripe="account_holder_type">
                                                                                                            <option value="individual">Individual</option>
                                                                                                            <option value="company">Company</option>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>-->
                                </div>
                                <div class="row">
                                    <div class="col-md-6" id="routing_number_div">
                                        <div class="form-group">
                                            <label id="routing_number_label">Routing Number</label>
                                            <input class="form-control input-lg bank_account" id="routing_numberm" type="tel" size="12" data-stripe="routing_number" placeholder="111000025" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label id="account_number_label">Account Number</label>
                                            <input class="form-control input-lg bank_account" id="account_numberm" type="tel" size="20" data-stripe="account_number" placeholder="000123456789" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-lg btn-block btn-success submit" type="submit" id="addBankAccBeforeDelete" data="5">Add bank account</button>
                                    </div>
                                </div>


                                <input type="hidden" id="deleteaccount">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>




    </div>
</div>

<!--delete banck account modal-->
<!-- Modal -->
<div id="deletebanckac" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <p id='txtdelete'> In Order To Delete Account You need Provide New Account.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id='addBankAccwhileDeleting'>ADD ONE</button>
                <button type="button" class="btn btn-default close_confirm" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>