<style>
    html,
    body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    .nav-tabs-fillup  bg-white{
        height: 65px;
    }
    .form-control{
        background-color: white;
    }

/*    h1, h2, h3, h4, h5, h6 {
        //color: aliceblue;

    }*/

    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    strong {
        display: none;
    }

    /*    .page-sidebar
        {
           display: none; 
        }*/

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}

    #addZoneModal .modal-dialog,
    #map_display .modal-dialog,
    #editZoneModal .modal-dialog {
        width: 90%;
    }

/*    .modal-header,
    .modal-footer {
        color: white;
        background-color: grey;
    }*/

/*    .modal-header,
    .modal-footer button {
        color: white;
        background-color: grey;
    }*/

    #addmodalmap,
    #mapPolygon,
    #editmodalmap {
        /*height: 600px;*/
        height: 80vh;
    }

    #zoneform label {
        margin-left: 12px;
    }

    #zoneform .pointscontrols {
        width: 35%;
        margin: 5px;
        margin-top: 2px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    .btncontrols {
        margin: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        color: white;
        background-color: cornflowerblue;
    }

    /*#cities,*/
    #editnow {
        border: 1px solid transparent;
        border-radius: 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        color: white;
        background-color: darkgray;
    }

    .success {
        color: springgreen;
    }

    .error {
        color: red;
    }

    .waitmsg {
        display: inline;
        margin-left: 50%;

    }

    .modal .modal-body p {
        color: aliceblue;
    }

    .displayinline {
        display: inline;
    }


    /*------------for auto complete search box---------------------*/

    .controls {
        margin: 10px 0;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        border-radius: 5px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

     #pac-input1 {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        border-radius: 5px;
    }

    #pac-input1:focus {
        border-color: #4d90fe;
    }


    .pac-container,.select2-drop {
        font-family: Roboto;
        z-index: 99999 !important;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #target {
        width: 345px;
    }
</style>
<style>
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .rating>.rated {
        color: #10cfbd;
    }
    .social-user-profile {
        width: 83px;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>timepicker/font-awesome.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>timepicker/bootstrap-timepicker.min.css" />
 <script src="<?php echo base_url(); ?>timepicker/bootstrap-timepicker.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?v=3&libraries=drawing,places&key=AIzaSyCK5bz3K1Ns_BASkAcZFLAI_oivKKo98VE"></script>
<script src="<?php echo base_url()?>/theme/ZoneMapAssets/javascript.util.min.js"></script>
<script src="<?php echo base_url()?>/theme/ZoneMapAssets/jsts.min.js"></script>
<script src="<?php echo base_url()?>/theme/ZoneMapAssets/wicket.js"></script>
<script src="<?php echo base_url()?>/theme/ZoneMapAssets/wicket-gmap3.js"></script>

<script>
    //Surge Price should be either in decimal/Number
    
    var mapedit;
    var cities = <?= json_encode($cities); ?>;
    $(document).ready(function () {
        $('.zones').addClass('active');
        $('.zones').attr('src','<?php echo base_url();?>/theme/icon/campaigns_on.png');
        $('#editzonesurge_price,#zonesurge_price').keypress('click', function (event) {

            return isNumber(event, this)

        });
        $('#s2id_city_select').removeClass('full-width');
        var table = $('#tableWithSearch').dataTable();
        $('#city_select').change(function(){
//            $('#city_selection_add').children(":selected").attr("lat").trim();
            table.fnFilter($(this).val());
        });
        
        
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) && // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    
    
    //Using For Generate Wicket from google polygon
    function UseWicketToGoFromGooglePolysToWKT(poly1, poly2) {
        var wicket = new Wkt.Wkt();

        wicket.fromObject(poly1);
        var wkt1 = wicket.write();

        wicket.fromObject(poly2);
        var wkt2 = wicket.write();

        return [wkt1, wkt2];
    }
    //Using For check Polygon is Overlapping to other or not
    function UseJstsToTestForIntersection(wkt1, wkt2) {
        // Instantiate JSTS WKTReader and get two JSTS geometry objects
        var wktReader = new jsts.io.WKTReader();
        var geom1 = wktReader.read(wkt1);
        var geom2 = wktReader.read(wkt2);

        if (geom2.intersects(geom1)) {
          return true
        } else {
          return false;
        }
    }

    function loadScript() {
//        var script = document.createElement("script");
//        script.src = "http://maps.googleapis.com/maps/api/js?libraries=drawing,places";//&callback=initMap";
//        document.body.appendChild(script);
    }
    window.onload = loadScript;
    
//-----------------------View Zones----------------------  
    var mapview;
    var datafromserver_view = null;
    var overlays_view = [];
    var infoWindow_view = [];
    var selectedCity_view = "";
    function viewzones(){
        $('#pac-input2').val('');
        selectedCity_view = "";
    /*ajax request to get all the details of polygon*/
        mapview = new google.maps.Map(document.getElementById('mapPolygon'), {
            center: {
                lat: 12.972442010578353
                , lng: 77.5909423828125
            }
            , zoom: 10
        });
        
        google.maps.event.addListenerOnce(mapview, 'idle', function () {
            google.maps.event.trigger(mapview, 'resize');
        });
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            mapview.setCenter(pos);
          }, function() {
            console.log('error');
          });
        } 
        
        google.maps.event.addListenerOnce(mapview, 'idle', function () {
            google.maps.event.trigger(mapview, 'resize');
        });
        
        $.getJSON("<?= base_url() ?>index.php/superadmin/zonemapsapi", function (data, status) {
            datafromserver_view = data;
            var uniqueCities = $.unique(data.map(function (d) {
                return d.city;
            }));

            var citiesslthtml = "";

            for (var index in uniqueCities) {
                citiesslthtml += "<option value=\"" + uniqueCities[index] + "\">" + uniqueCities[index] + "</option>";
            }
            $("#city_selection_view").html(citiesslthtml);
            $("#city_selection_view").select2();
            $("#city_selection_view").trigger('change');
        });
    }
    
    $(document).ready(function(){
        
         $('#add_surge_enabled').click(function ()
        {
            if($(this).is(':checked'))
            {
                $('#surge_enable_status').val('1');
               
            }
            else
            {
               $('#surge_enable_status').val('0');
               
             }
            
        });
         $('#edit_surge_enabled').click(function ()
        {
            if($(this).is(':checked'))
            {
                $('#edit_surge_enable_status').val('1');
               
            }
            else
            {
               $('#edit_surge_enable_status').val('0');
               
             }
            
        });
    
});
    
    $(document).ready(function () {
        
    $('#timepicker1').timepicker({defaultTime:'04:00 PM'});
    $('#timepicker2').timepicker({defaultTime:'08:00 PM'});
    $('#timepicker3').timepicker({defaultTime:'04:00 PM'});
    $('#timepicker4').timepicker({defaultTime:'08:00 PM'}); 
        $('#city_selection_view').change(function ()
        {
            if(selectedCity_view != $("#city_selection_view option:selected").val()){
                $('#pac-input2').val('');
                selectedCity_view = $("#city_selection_view option:selected").val();
//                while (overlays_view[0]) {
//                    overlays_view.pop().setMap(null);
//                }
//                while (infoWindow_view[0]) {
//                    infoWindow_view.pop().setMap(null);
//                }
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'address': selectedCity_view
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {

                        var latlng = {
                            lat: results[0].geometry.location.lat()
                            , lng: results[0].geometry.location.lng()
                        };
                        mapview.setCenter(latlng);
                    } else {
                        console.log("unable to center map");
                    }
                });
                for (i=0; i < overlays_view.length; i++) {
                    overlays_view[i].setMap(null);
                }
                for(i=0; i < infoWindow_view.length; i++){
                    infoWindow_view[i].setMap(null);
                }
                overlays_view = [];
                infoWindow_view = [];
                drawzones_view(selectedCity_view);
            }
        });
        $('#pac-input2').focus(function(){
             var input;
            input = document.getElementById('pac-input2');

            var searchBox = new google.maps.places.SearchBox(input);
            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            mapview.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
                    });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    var lat = parseFloat(place.geometry.location.lat()).toFixed(6);
                    var lng = parseFloat(place.geometry.location.lng()).toFixed(6);
                    var city = "";
                    var flg = true;
                    $.each(cities,function(ind,val){
                        if(flg){
                            var d = calculateDistance(lat,lng,val.City_Lat,val.City_Long,'K');
                            if(d<30){
                                city = val.City_Name;
                                flg=false;
                            }
                        }
                    });
                    selectedCity_view = city;
                    $('#city_selection_view option[value='+city+']').prop('selected','selected');
                    $('#city_selection_view').select2();
                    for (i=0; i < overlays_view.length; i++) {
                        overlays_view[i].setMap(null);
                    }
                    for(i=0; i < infoWindow_view.length; i++){
                        infoWindow_view[i].setMap(null);
                    }
                    overlays_view = [];
                    infoWindow_view = [];
                    drawzones_view(city);

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: mapview
                        , title: place.name
                        , position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });

                mapview.fitBounds(bounds);
                mapview.setZoom(13);
            });
            //map.setZoom(10);
        });
    });
    
    function drawzones_view(selectedCity){
        var zone = [];
        for (var iZone = 0; iZone < datafromserver_view.length; iZone++) {
            if (datafromserver_view[iZone].city == selectedCity) {

                var polgonProperties = datafromserver_view[iZone].polygonProps;
                polgonProperties.zoneIndex = iZone; //to add the array index of the polygon to its properties in the json object received from server

                zone[iZone] = new google.maps.Polygon(polgonProperties);
                zone[iZone].setMap(mapview);

                overlays_view.push(zone[iZone]);

                var latLng = {lat: datafromserver_view[iZone].polygonProps.paths[0].lat, lng: datafromserver_view[iZone].polygonProps.paths[0].lng};
                var inf = new google.maps.InfoWindow;

                var Str = '<b>' + datafromserver_view[iZone].title + '</b><br> Fare:' + datafromserver_view[iZone].surge_price + ' X';

                inf.setContent(Str);
                inf.setPosition(latLng);
                inf.open(mapview);
                infoWindow_view.push(inf);
            }
        }
    }


//----------------addnewzone-----------------------------------
    var map;
    var datafromserver = null;
    var newPolygon = null;
    var overlays_add = [];
    var infoWindow_add = [];
    var selectedCity_add = "";
    var pnt_overlay_add = 0;
    var pnt_infwin_add = 0;
    var polygonCoordinates = [];
    var polygonCoordinates1 = [];
    var rectangle;
    var marker_add = "";
    
    function addNewZone() {
        $('#pac-input').val('');
//        $('#city_selection_add option[value=""]').prop('selected','selected');
//        $('#city_selection_add').select2();
        selectedCity_add = "";
        
        /*ajax request to get all the details of polygon*/
        $.getJSON("<?= base_url() ?>index.php/superadmin/zonemapsapi", function (data, status) {
            datafromserver = data;
        });

        map = new google.maps.Map(document.getElementById('addmodalmap'), {
                center: {
                lat: 12.972442010578353
                , lng: 77.5909423828125
            }
            , zoom: 10
        });
        
        google.maps.event.addListenerOnce(map, 'idle', function () {
            google.maps.event.trigger(map, 'resize');
        });
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            map.setCenter(pos);
          }, function() {
            console.log('error');
          });
        } 

        //Map does not load in a modal window until it is resized
        google.maps.event.addListenerOnce(map, 'idle', function () {
            google.maps.event.trigger(map, 'resize');
        });

        //Properties for the new polygon to be drawn
        var polyOptions = {
            strokeWeight: 0
            , fillOpacity: 0.45
            , editable: true
            , draggable: true
        };

        //drawing manager is a tool to draw the polygon
        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON
            , drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER
                , drawingModes: [google.maps.drawing.OverlayType.POLYGON]
            }
            , polygonOptions: polyOptions
            , map: map
        });

        $("form").submit(function (event) {
            event.preventDefault();
        });

        $('#savezone').hide();
        $('#clearzone').hide();
        
        //To create and fill the points input boxes of the newly created polygon
        function fillPathsInput(polygon) {
            newPolygon = polygon;
            var len = polygon.getPath().getLength();
            var htmlStr = "";
            
            var newpoly = [];
            for (var i = 0; i < len; i++) {
                var latlngobj = {
                    "lat": polygon.getPath().getAt(i).lat()
                    , "lng": polygon.getPath().getAt(i).lng()
                };
                newpoly.push(latlngobj);
            }
            var newpolyProp = new google.maps.Polygon({
                    "paths" : newpoly,
                    strokeColor: '#000000',
                    strokeOpacity: 0.8,
                    strokeWeight: 1,
                    fillColor: '#00FF00',
                    fillOpacity: 0.35
                });
            for (var iZone = 0; iZone < datafromserver.length; iZone++) {
                if (datafromserver[iZone].city == selectedCity_add) {
                    var polgonProperties = datafromserver[iZone].polygonProps;
                    var oldpolyProp =  new google.maps.Polygon(polgonProperties);

                    var wkt = UseWicketToGoFromGooglePolysToWKT(newpolyProp,oldpolyProp);
                    if(UseJstsToTestForIntersection(wkt[0], wkt[1])){
                        $('#clearzone').trigger('click');
                        alert("Do not Overlap Existing Zone.")
                        return;
                    }                        
                }
            }
            
            for (var i = 0; i < len; i++) {
                htmlStr += "<label>Point" + (i + 1) + " </label> <input type='number' id='p" + (i + 1) + "lat' value='" + polygon.getPath().getAt(i).lat() + "' class='pointscontrols' readonly><input type='number' id='p" + (i + 1) + "lng' value='" + polygon.getPath().getAt(i).lng() + "' class='pointscontrols' readonly><br>";
            }
            $('#info').html(htmlStr);
            $('#savezone').show();
            $('#clearzone').show();
        }
    
        google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
            // Switch back to non-drawing mode after drawing a shape.
            drawingManager.setMap(null);

            drawingManager.setDrawingMode(null);
            
            fillPathsInput(polygon);

            google.maps.event.addListener(polygon.getPath(), 'set_at', function () {
                fillPathsInput(polygon);
            });

            google.maps.event.addListener(polygon.getPath(), 'insert_at', function () {
                fillPathsInput(polygon);
            });
        });
        google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
            if (event.type == google.maps.drawing.OverlayType.POLYGON) {
                if(rectangle != null)
                    rectangle.setMap(null);
                rectangle = event.overlay;
                drawingManager.setDrawingMode(null);
            }
        });

        google.maps.event.addListener(drawingManager, "drawingmode_changed", function() {
            if ((drawingManager.getDrawingMode() == google.maps.drawing.OverlayType.POLYGON) && 
                (rectangle != null))
                rectangle.setMap(null);
        });
        clearFields();
    }
    
      function Converttimeformat(time) {
        // var time = $("#starttime").val();
//        var time = document.getElementById('txttime').value;
        var hrs = Number(time.match(/^(\d+)/)[1]);
        var mnts = Number(time.match(/:(\d+)/)[1]);
        var format = time.match(/\s(.*)$/)[1];
        if (format == "PM" && hrs < 12) hrs = hrs + 12;
        if (format == "AM" && hrs == 12) hrs = hrs - 12;
        var hours = hrs.toString();
        var minutes = mnts.toString();
        if (hrs < 10) hours = "0" + hours;
        if (mnts < 10) minutes = "0" + minutes;
//        alert(hours + ":" + minutes);
        return (hours + ":" + minutes);
}
    function clearFields() {
//    alert();
        $('#zonecity').val("");
        $('#zonetitle').val("");
        $('#zonesurge_price').val("");
        $('#info').html("");
        $('#savezone').hide();
        $('#clearzone').hide();
        $('#pac-input').val('');
        $('#city_selection_add').prop('selectedIndex',0);
        $('#city_selection_add').select2();
        selectedCity_add = "";
        polygonCoordinates.splice(0, polygonCoordinates.length);
        newPolygon = null;
    }
    $(document).ready(function () {
        $('#clearzone').click(function (e){
            rectangle.setMap(null);
            drawingManager.setMap(map);
            polygonCoordinates.splice(0, polygonCoordinates.length);
            newPolygon = null;
            $('#info').html('');
            $('#savezone').hide();
            $('#clearzone').hide();
        });
        
        //To save the zone details in the database
        $('#savezone').click(function (e) {
    //            alert($('#city_selection_add').val());

            if ($('#city_selection_add').val() == '#' || $('#city_selection_add').val() == null)
            {
                alert("Select City");
            } else if ($('#zonetitle').val() == '' || $('#zonetitle').val() == null)
            {
                alert("Please provide the zone title");
            } else if ($('#zonesurge_price').val() == '' || $('#zonesurge_price').val() == null)
            {
                alert("Surge price should not be empty");
            } else if ($('#city_selection_add').val() != "" && $('#zonetitle').val() != "" && $('#zonesurge_price').val() != "") {

                var len = newPolygon.getPath().getLength();

                for (var i = 0; i < len; i++) {
                    var latlngobj = {
                        "lat": newPolygon.getPath().getAt(i).lat()
                        , "lng": newPolygon.getPath().getAt(i).lng()
                    };
                    polygonCoordinates.push(latlngobj);
                }

                //changed Done 08-05-2016
                var coordinates1 = [];
                for (var i = 0; i < len; i++) {
                    coordinates1.push([newPolygon.getPath().getAt(i).lng(),newPolygon.getPath().getAt(i).lat()]);
                }

                coordinates1.push(coordinates1[0]);
    //                alert(JSON.stringify(coordinates1));

                polygonCoordinates1.push(coordinates1);



                var cl = "#" + Math.floor(Math.random()*(9-0+1)+0) +""+Math.floor(Math.random()*(9-0+1)+0)+"" + Math.floor(Math.random()*(9-0+1)+0) +""+Math.floor(Math.random()*(9-0+1)+0) + Math.floor(Math.random()*(9-0+1)+0) +""+Math.floor(Math.random()*(9-0+1)+0);
                var polygonProperties = {
                    "paths": polygonCoordinates
                    , "strokeColor": cl
                    , "strokeOpacity": 0.2
                    , "strokeWeight": 2
                    , "fillColor": cl
                    , "fillOpacity": 0.35
                    , "draggable": false
                    , "editable": false
                }

                //changed Done 08-05-2016
                var polygonProperties1 = {
                    "type": "Polygon",
                    "coordinates": polygonCoordinates1

                }


                var data = {
                    "city": $('#city_selection_add').val(),
                    "city_id":$('#city_selection_add').children(":selected").attr("data-id"),
                    'charge_type':$('input[name=charge_type_add]:checked').val(),
                    "title": $('#zonetitle').val(),
                    "surge_price": $('#zonesurge_price').val(),
                    "startTime":Converttimeformat($('#timepicker1').val()),
                    "endTime": Converttimeformat($('#timepicker2').val()),
                    "surge_enabled": $('#surge_enable_status').val(),
                    "polygonProps": polygonProperties,
                    "polygons": polygonProperties1     //changed Done 08-05-2016
                }
                $('#addmsg').text("please wait..");

                //To update the zone details
                $.ajax({
                    url: '<?= base_url() ?>index.php/superadmin/zonemapsapi'
                    , dataType: "json"
                    , type: "POST"
                    , contentType: 'application/json; charset=utf-8'
                    , data: JSON.stringify(data)
                    , async: true
                    , success: function (data) {
                        if (data.status == 'success') {
                            $('#addZoneModal').modal('toggle');
    //                            $('#editmsg').text("zone updated successfuly").toggleClass("success");
                            var count = 1;
                            var s='';
                            $.each(polygonProperties.paths, function(ind,val){

                                s += 'P' + count + "(" + val['lat'] + "," + val['lng']+ ")";
                                s += " ";
                                if ((count % 2) == 0)
                                    s += ' <br>';
                                count++;
                            });
                             var en_dis = '<b style=color:red>Disabled</b>';
                            if($('#surge_enable_status').val() == 1)
                                en_dis = '<b style=color:green>Enabled</b>';
                            s +="<input type='hidden' id='zone_"+data.data._id.$id+"'>";
                            
                            var table = $('#tableWithSearch').DataTable();
                            table.row.add([
                                table.page.info().recordsTotal + 1,
                                $('#city_selection_add').val(),
                                $('#zonetitle').val(),
                                ($('input[name=charge_type_add]:checked').val()=='SUBCHARGE')?"Sub Charge":"Surge Charge",
                                $('#timepicker1').val(),
                                $('#timepicker2').val(),
                                $('#zonesurge_price').val(),
                                en_dis,
                                s
                            ]).draw();
                            clearFields();
                        } else if (data.status == 'error') {
                            $('#addmsg').text("unable to update please try again!").addClass("error");
                            clearFields();
                        }
                    }
                    , error: function (xhr) {
                        alert('error');
                    }
                });
            }
        });

        $('#city_selection_add').change(function ()
        {
            if(selectedCity_add != $("#city_selection_view option:selected").val()){
                $('#pac-input').val('');
                var customerlat;
                var customerlong;
                customerlat = $('#city_selection_add').children(":selected").attr("lat").trim();
                customerlong = $('#city_selection_add').children(":selected").attr("lng").trim();

                map.setCenter(new google.maps.LatLng(customerlat, customerlong));
                if(marker_add !="")
                {
                    marker_add.setMap(null);
                    marker_add = "";
                }
                marker_add = new google.maps.Marker({
                    map: map
                    , title: selectedCity_add
                    , position: new google.maps.LatLng(customerlat, customerlong)
                });
                map.setCenter(marker_add.getPosition());
                selectedCity_add = $("#city_selection_add option:selected").val();
 
                for (i=0; i < pnt_overlay_add; i++) {
                    overlays_add[i].setMap(null);
                }
                for(i=0; i < pnt_infwin_add; i++){
                    infoWindow_add[i].setMap(null);
                }
                overlays_add = [];
                infoWindow_add = [];
                pnt_overlay_add = 0;
                pnt_infwin_add = 0;
                drawZones_add(selectedCity_add);
            }
        });
        
        $('#pac-input').focus(function(){
             var input;
            input = document.getElementById('pac-input');

            var searchBox = new google.maps.places.SearchBox(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
                    });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                if(marker_add !="")
                {
                    marker_add.setMap(null);
                    marker_add = "";
                }
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    var lat = parseFloat(place.geometry.location.lat()).toFixed(6);
                    var lng = parseFloat(place.geometry.location.lng()).toFixed(6);
                    var city = "#";
                    var flg = true;
                    $.each(cities,function(ind,val){
                        if(flg){
                            var d = calculateDistance(lat,lng,val.City_Lat,val.City_Long,'K');
                            if(d<30){
                                city = val.City_Name;
                                flg=false;
                            }
                        }
                    });
                    selectedCity_add = city;
                    $('#city_selection_add option[value='+city+']').prop('selected','selected');
                    $('#city_selection_add').select2();
                    for (i=0; i < pnt_overlay_add; i++) {
                        overlays_add[i].setMap(null);
                    }
                    for(i=0; i < pnt_infwin_add; i++){
                        infoWindow_add[i].setMap(null);
                    }
                    overlays_add = [];
                    infoWindow_add = [];
                    pnt_overlay_add = 0;
                    pnt_infwin_add = 0;
                    
                    drawZones_add(city);
                    // Create a marker for each place.
                    marker_add = new google.maps.Marker({
                        map: map
                        , title: place.name
                        , position: place.geometry.location
                    });
                    map.setCenter(marker_add.getPosition());
                });
            });
        });
    });

    //plots the zones or polygons on the map based on the selected city from dropdown
    function drawZones_add(selectedCity) {
        var zone = [];
        for (var iZone = 0; iZone < datafromserver.length; iZone++) {
            if (datafromserver[iZone].city == selectedCity) {

                var polgonProperties = datafromserver[iZone].polygonProps;
                polgonProperties.zoneIndex = iZone; //to add the array index of the polygon to its properties in the json object received from server
                
                overlays_add[pnt_overlay_add] = new google.maps.Polygon(polgonProperties);
                overlays_add[pnt_overlay_add].setMap(map);

                pnt_overlay_add++;

                var latLng = {lat: datafromserver[iZone].polygonProps.paths[0].lat, lng: datafromserver[iZone].polygonProps.paths[0].lng};
                infoWindow_add[pnt_infwin_add] = new google.maps.InfoWindow;

                var Str = '<b>' + datafromserver[iZone].title + '</b><br> Fare:' + datafromserver[iZone].surge_price + ' X';

                infoWindow_add[pnt_infwin_add].setContent(Str);
                infoWindow_add[pnt_infwin_add].setPosition(latLng);
                infoWindow_add[pnt_infwin_add].open(map);
                pnt_infwin_add++;
            }
        }

    }


    //function to sort lat long
    function calculateDistance(lat1, lon1, lat2, lon2, unit) {
        var radlat1 = Math.PI * lat1 / 180
        var radlat2 = Math.PI * lat2 / 180
        var radlon1 = Math.PI * lon1 / 180
        var radlon2 = Math.PI * lon2 / 180
        var theta = lon1 - lon2
        var radtheta = Math.PI * theta / 180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180 / Math.PI
        dist = dist * 60 * 1.1515
        if (unit == "K") {
            dist = dist * 1.609344
        }
        if (unit == "N") {
            dist = dist * 0.8684
        }
        return dist
    }
    //end of sorting



//---------------edit zone details--------------------------------

    var newPolygon_edit = null;
    var datafromserver_edit = null;
    var overlays_edit = [];
    var infoWindow_edit = [];
    var pnt_overlay = 0;
    var pnt_infwin = 0;
    var selectedCity_edit = "";
    var polygonCoordinates_edit = [];
    var polygonCoordinates1_edit = [];
    var old_polygon = "";
    var marker_edit = "";
    function loadExistingZones() {

        $('#pac-input1').val('');
        selectedCity_edit = "";
        old_polygon = "";
        newPolygon_edit = null;
        //creating a map 
        mapedit = new google.maps.Map(document.getElementById('editmodalmap'), {
            center: {
                lat: 12.972442010578353
                , lng: 77.5909423828125
            }
            , zoom: 13
        });

        //Map does not load in a modal window until it is resized
        google.maps.event.addListenerOnce(mapedit, 'idle', function () {
            google.maps.event.trigger(mapedit, 'resize');
        });

        /*ajax request to get all the details of polygon*/
        $.getJSON("<?= base_url() ?>index.php/superadmin/zonemapsapi", function (data, status) {

            datafromserver_edit = data;

            var uniqueCities = $.unique(data.map(function (d) {
                return d.city;
            }));

            var citiesslthtml = "";

            for (var index in uniqueCities) {
               
                citiesslthtml += "<option value=\"" + uniqueCities[index] + "\">" + uniqueCities[index] + "</option>";
            }
            $("#cities").html(citiesslthtml);
            $("#cities").select2();
            
            $("#cities").trigger('change');

        });

        $("form").submit(function (event) {
            event.preventDefault();
        });

        $('#saveeditzone').hide();
        $('#deletezone').hide();
        clearFields_edit();
        //To refresh the page after editing the zone
//        $('#editZoneModal').on('hidden.bs.modal', function () {
//            $('.select2').select2("close");
//        });
    }
    function clearFields_edit() {
//            loadExistingZones();
//        old_polygon = "";
//        newPolygon_edit = null;
//        $('#editzonecity').val('');
        $('#editzonetitle').val('');
        $('#editzonesurge_price').val('');
        $('#pointsinfo').html('');
    }
    //plots the zones or polygons on the map based on the selected city from dropdown
    function drawZones_edit(selectedCity) {
        var zone = [];
        clearFields_edit();
        for (var iZone = 0; iZone < datafromserver_edit.length; iZone++) {
            if (datafromserver_edit[iZone].city == selectedCity) {

                var polgonProperties = datafromserver_edit[iZone].polygonProps;
                polgonProperties.zoneIndex = iZone; //to add the array index of the polygon to its properties in the json object received from server

                overlays_edit[pnt_overlay] = new google.maps.Polygon(polgonProperties);
                overlays_edit[pnt_overlay].setMap(mapedit);


                google.maps.event.addListener(overlays_edit[pnt_overlay], 'click', function (event) {
                    editZoneDetails(this);
                    
                });
                pnt_overlay++;

                var latLng = {lat: datafromserver_edit[iZone].polygonProps.paths[0].lat, lng: datafromserver_edit[iZone].polygonProps.paths[0].lng};
                infoWindow_edit[pnt_infwin] = new google.maps.InfoWindow;

                var Str = '<b>' + datafromserver_edit[iZone].title + '</b><br> Fare:' + datafromserver_edit[iZone].surge_price + ' X';

                infoWindow_edit[pnt_infwin].setContent(Str);
                infoWindow_edit[pnt_infwin].setPosition(latLng);
                infoWindow_edit[pnt_infwin].open(mapedit);
                pnt_infwin++;
            }
        }

    }



    function editZoneDetails(polygon) {
        if((old_polygon || old_polygon=="0") && old_polygon == polygon.zoneIndex){
            newPolygon_edit.setPaths(datafromserver_edit[old_polygon].polygonProps.paths);
            newPolygon_edit.setEditable(false);
            newPolygon_edit.setDraggable(false);
            clearFields_edit();
            return;
        }
        if((old_polygon || old_polygon=="0") && old_polygon != polygon.zoneIndex){
            newPolygon_edit.setPaths(datafromserver_edit[old_polygon].polygonProps.paths);
        }
        if(newPolygon_edit){
            newPolygon_edit.setEditable(false);
            newPolygon_edit.setDraggable(false);
        }
        old_polygon=polygon.zoneIndex;
        polygon.setEditable(true);
        polygon.setDraggable(true);

        $('#editzonecity').val(datafromserver_edit[polygon.zoneIndex].city);
        if(datafromserver_edit[polygon.zoneIndex].charge_type == 'SURGECHARGE')
            $('#charge_surge_edit').prop("checked",true);
        else
            $('#charge_sub_edit').prop("checked",true);
        
        if(datafromserver_edit[polygon.zoneIndex].surge_enabled == '0')
            $('#edit_surge_enabled').prop("checked",false);
        else
            $('#edit_surge_enabled').prop("checked",true);
        $('#editzonetitle').val(datafromserver_edit[polygon.zoneIndex].title);
        $('#editzonesurge_price').val(datafromserver_edit[polygon.zoneIndex].surge_price);
        fillPathsInput_edit(polygon);

        google.maps.event.addListener(polygon.getPath(), 'set_at', function () {
            fillPathsInput_edit(polygon);
        });

        google.maps.event.addListener(polygon.getPath(), 'insert_at', function () {
            fillPathsInput_edit(polygon);
        });

        google.maps.event.addListener(polygon, "mouseout", function () {
            polygon.setDraggable(true);
        });

        $('#saveeditzone').show();
        $('#deletezone').show();
    }

    //To create and fill the points input boxes of the edited polygon
    function fillPathsInput_edit(polygon) {
        newPolygon_edit = polygon;
        var len = polygon.getPath().getLength();
        var htmlStr = "";
        
        for (var i = 0; i < len; i++) {
            htmlStr += "<label>Point" + (i + 1) + " </label> <input type='number' id='p" + (i + 1) + "lat' value='" + polygon.getPath().getAt(i).lat() + "' class='pointscontrols' readonly><input type='number' id='p" + (i + 1) + "lng' value='" + polygon.getPath().getAt(i).lng() + "' class='pointscontrols' readonly><br>";
        }

        $('#pointsinfo').html(htmlStr);


    }
    $(document).ready(function ()
    {
        //To save the zone details in the database
        $('#saveeditzone').click(function () {

            if ($('#editzonetitle').val() == '' || $('#editzonetitle').val() == null)
            {
                alert("Please provide zone title");
            } else if ($('#editzonesurge_price').val() == '' || $('#editzonesurge_price').val() == null)
            {
                alert("Surge price should not be empty");
            } else if ($('#editzonecity').val() != "" && $('#editzonetitle').val() != "" && $('#editzonesurge_price').val() != "") {
                var len = newPolygon_edit.getPath().getLength();
                polygonCoordinates_edit = [];
                polygonCoordinates1_edit = [];
                for (var i = 0; i < len; i++) {
                    var latlngobj = {
                        "lat": newPolygon_edit.getPath().getAt(i).lat()
                        , "lng": newPolygon_edit.getPath().getAt(i).lng()
                    };
                    polygonCoordinates_edit.push(latlngobj);
                }

                var coordinates1 = [];
                for (var i = 0; i < len; i++) {
                    coordinates1.push([newPolygon_edit.getPath().getAt(i).lng(),newPolygon_edit.getPath().getAt(i).lat()]);
                }
                
                coordinates1.push(coordinates1[0]);

                polygonCoordinates1_edit.push(coordinates1);

                //changed Done 08-05-2016
                var polygonProperties1 = {
                    "type": "Polygon",
                    "coordinates": polygonCoordinates1_edit

                }
                var cl = "#" + Math.floor(Math.random()*(9-0+1)+0) +""+Math.floor(Math.random()*(9-0+1)+0)+"" + Math.floor(Math.random()*(9-0+1)+0) +""+Math.floor(Math.random()*(9-0+1)+0) + Math.floor(Math.random()*(9-0+1)+0) +""+Math.floor(Math.random()*(9-0+1)+0);
                
                var polygonProperties = {
                    "paths": polygonCoordinates_edit
                    , "strokeColor": cl
                    , "strokeOpacity": 0.2
                    , "strokeWeight": 2
                    , "fillColor": cl
                    , "fillOpacity": 0.35
                    , "draggable": false
                    , "editable": false
                };
                
                var newpolyProp = new google.maps.Polygon(polygonProperties);
                for (var iZone = 0; iZone < datafromserver_edit.length; iZone++) {
                    if (datafromserver_edit[iZone].city == selectedCity_edit && newPolygon_edit.zoneIndex != iZone) {
                        var oldpolyProp =  new google.maps.Polygon(datafromserver_edit[iZone].polygonProps);

                        var wkt = UseWicketToGoFromGooglePolysToWKT(newpolyProp,oldpolyProp);

                        if(UseJstsToTestForIntersection(wkt[0], wkt[1])){
                            newPolygon_edit.setPaths(datafromserver_edit[old_polygon].polygonProps.paths);
                            newPolygon_edit.setDraggable(false);
                            newPolygon_edit.setEditable(false);
                            
                            alert("Do not Overlap Existing Zone.");
                            editZoneDetails(newPolygon_edit);
                            return;
                        }                        
                    }
                }
                var zid = datafromserver_edit[newPolygon_edit.zoneIndex]._id.$id;
                var data = {
                    "id": datafromserver_edit[newPolygon_edit.zoneIndex]._id.$id
                    , "details": {
                        "city": $('#cities').val()
                        , "title": $('#editzonetitle').val()
                        , 'charge_type':$('input[name=charge_type_edit]:checked').val()
                        , "surge_price": $('#editzonesurge_price').val()
                          ,"startTime":Converttimeformat($('#timepicker3').val())
                         , "endTime": Converttimeformat($('#timepicker4').val())
                          ,"surge_enabled": $('#edit_surge_enable_status').val()
                        , "polygonProps": polygonProperties,
                        "polygons": polygonProperties1     //changed Done 08-05-2016
                    }
                }

                $('#editmsg').text("please wait..")

                //To update the zone details
                $.ajax({
                    url: '<?= base_url() ?>index.php/superadmin/zonemapsapi'
                    , dataType: "json"
                    , type: "PUT"
                    , contentType: 'application/json; charset=utf-8'
                    , data: JSON.stringify(data)
                    , async: true
                    , success: function (data) {
                        if (data.status == 'success') {
                            var count = 1;
                            var s='';
                            $.each(polygonProperties.paths, function(ind,val){

                                s += 'P' + count + "(" + val['lat'] + "," + val['lng']+ ")";
                                s += " ";
                                if ((count % 2) == 0)
                                    s += ' <br>';
                                count++;
                            });
                              var en_dis = '<b style=color:red>Disabled</b>';
                            if($('#edit_surge_enable_status').val() == 1)
                                en_dis = '<b style=color:green>Enabled</b>';
                            
                            s +="<input type='hidden' id='zone_"+String(zid)+"'>";
                            $('#zone_'+zid).closest('tr').find('td:nth-child(2)').html($('#cities').val());
                            $('#zone_'+zid).closest('tr').find('td:nth-child(3)').html($('#editzonetitle').val());
                            $('#zone_'+zid).closest('tr').find('td:nth-child(4)').html(($('input[name=charge_type_add]:checked').val()=='SUBCHARGE')?"Sub Charge":"Surge Charge");
                            $('#zone_'+zid).closest('tr').find('td:nth-child(5)').html($('#timepicker3').val());
                            $('#zone_'+zid).closest('tr').find('td:nth-child(6)').html($('#timepicker4').val());
                            $('#zone_'+zid).closest('tr').find('td:nth-child(7)').html($('#editzonesurge_price').val());
                            $('#zone_'+zid).closest('tr').find('td:nth-child(8)').html(en_dis);
                            $('#zone_'+zid).closest('tr').find('td:nth-child(9)').html(s);
                            $('#editZoneModal').modal('toggle');
                            clearFields_edit();
                        } else if (data.status == 'error') {
                            $('#editmsg').text("unable to update!").addClass("error");
                            clearFields_edit();
                        }
                    }
                    , error: function (xhr) {
                        alert('error');
                    }
                });
            }

        });

        //To delete the zone 
        $('#deletezone').click(function () {
            var confirmDelete = confirm("Are you sure you want to delete this zone?");
            if (confirmDelete == true) {
                var zid = datafromserver_edit[newPolygon_edit.zoneIndex]._id.$id;
                var deleteZoneId = {
                    "id": datafromserver_edit[newPolygon_edit.zoneIndex]._id.$id
                };

                $('#editmsg').text("please wait..")

                $.ajax({
                    url: '<?= base_url() ?>index.php/superadmin/zonemapsapi'
                    , dataType: "json"
                    , type: "DELETE"
                    , contentType: 'application/json; charset=utf-8'
                    , data: JSON.stringify(deleteZoneId)
                    , async: true
                    , success: function (data) {
                        if (data.status == 'success') {
                            $('#editZoneModal').modal('toggle');
                            var table = $('#tableWithSearch').DataTable();
                            table.row($('#zone_'+zid).closest('tr')).remove().draw();
                            clearFields_edit();
                        } else if (data.status == 'error') {
                            $('#editmsg').text("unable to delete!").addClass("error");
                            clearFields_edit();
                        }
                    }
                    , error: function (xhr) {
                        alert('error');
                    }
                });
            }

        });
        $('#cities').change(function ()
        {            
            $('#pac-input1').val('');
            if(selectedCity_edit != $("#cities option:selected").val()){
                clearFields_edit();
                selectedCity_edit = $("#cities option:selected").val();
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'address': selectedCity_edit
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {

                        var latlng = {
                            lat: results[0].geometry.location.lat()
                            , lng: results[0].geometry.location.lng()
                        };
                        mapedit.setCenter(latlng);
                        if(marker_edit !="")
                        {
                            marker_edit.setMap(null);
                            marker_edit = "";
                        }
                        marker_edit = new google.maps.Marker({
                            map: mapedit
                            , title: selectedCity_add
                            , position: new google.maps.LatLng(customerlat, customerlong)
                        });
                    } else {
                        console.log("unable to center map");
                    }
                });
                var i=0;
                for (i=0; i < pnt_overlay; i++) {
                    overlays_edit[i].setMap(null);
                }
                for(i=0; i < pnt_infwin; i++){
                    infoWindow_edit[i].setMap(null);
                }
                overlays = [];
                infoWindow_edit = [];
                pnt_overlay = 0;
                pnt_infwin = 0;
                drawZones_edit(selectedCity_edit);
            }
        });
        
        $('#pac-input1').focus(function(){
            var input;
            input = document.getElementById('pac-input1');

            var searchBox = new google.maps.places.SearchBox(input);
            
            mapedit.addListener('bounds_changed', function () {
                searchBox.setBounds(mapedit.getBounds());
                    });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }
                if(marker_edit !="")
                {
                    marker_edit.setMap(null);
                    marker_edit = "";
                }
                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    var lat = parseFloat(place.geometry.location.lat()).toFixed(6);
                    var lng = parseFloat(place.geometry.location.lng()).toFixed(6);
                    var city = "";
                    var flg = true;
                    $.each(cities,function(ind,val){
                        if(flg){
                            var d = calculateDistance(lat,lng,val.City_Lat,val.City_Long,'K');
                            if(d<30){
                                city = val.City_Name;
                                flg=false;
                            }
                        }
                    });
                    console.log(city);
                    selectedCity_edit = city;
                    $('#cities option[value='+city+']').prop('selected','selected');
                    $('#cities').select2();
                    var i=0;
                    for (i=0; i < pnt_overlay; i++) {
                        overlays_edit[i].setMap(null);
                    }
                    for(i=0; i < pnt_infwin; i++){
                        infoWindow_edit[i].setMap(null);
                    }
                    overlays = [];
                    infoWindow_edit = [];
                    pnt_overlay = 0;
                    pnt_infwin = 0;
                    
                    drawZones_edit(city);
                    
                    // Create a marker for each place.
                    marker_edit = new google.maps.Marker({
                        map: mapedit
                        , title: place.name
                        , position: place.geometry.location
                    });
                    map.setCenter(marker_edit.getPosition());
                });
            });
        });
    });
</script>  
<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;z-index: 800;margin-left:10px;">
                <h3 class="text-black">Zones</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="<?= base_url() ?>/index.php/superadmin/zones">Zones</a>
            </li>
            <li>
                <select data-init-plugin="select2" tabindex="-1"class="bg-transparent full-width select2-offscreen" id="city_select" style="border:0;">
                    <option value="">Select City</option>
                    <?php
                    foreach ($cities as $data) {
                        ?>
                        <option value="<?php echo $data->City_Name; ?>" lat="<?php echo $data->City_Lat; ?>" lng="<?php echo $data->City_Long; ?>"><?php echo $data->City_Name; ?></option>    
                        <?php
                    }
                    ?>
                </select>
            </li>
            <div class="pull-right">
                <div class="col-xs-12">
                    <button data-toggle="modal" class="btn btn-primary btn-cons" data-target="#addZoneModal" onclick="addNewZone(12.972442010578353, 77.5909423828125)"> 
                        <i class="fa fa-plus text-white"></i> Add Zone 
                    </button>
                    <button data-toggle="modal" class="btn btn-primary" data-target="#editZoneModal" onclick="loadExistingZones()"> 
                        <i class="fa fa-edit text-white"></i> Edit zone</button>
                    <button data-toggle="modal" class="btn btn-primary btn-cons" data-target="#map_display" onclick="viewzones()"> 
                        <i class="fa fa-eye text-white"></i> View On Map 
                    </button>
                </div>
            </div>
        </ul>
        
        <div class="container-md-height m-b-20">
            <div class="panel panel-default">
                
                <div class="panel-body no-padding">
                    <div class="row">
                       <div id="big_table_wrapper" class="dataTables_wrapper no-footer" style="padding: 0 2% 2% 2%;" id="all_cities">
                            <div class="table-responsive">
                                <table id="tableWithSearch" border="1"  data-filter="true"  data-input="#city_select" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" role="grid" aria-describedby="big_table_info" style="margin-top: 30px;" >
                                    <thead>
                                        <tr style="font-size:20px" role="row">
                                            <th class="sorting ui-state-default sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 68px;" aria-sort="ascending">
                                                <div class="DataTables_sort_wrapper">Sl No.<span class="DataTables_sort_icon css_right ui-icon ui-icon-triangle-1-n"></span></div>
                                            </th><th class="sorting ui-state-default" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 127px;"><div class="DataTables_sort_wrapper">CITY<span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span></div></th>
                                            <th class="sorting ui-state-default" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 127px;"><div class="DataTables_sort_wrapper">TITLE<span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span></div></th>
                                            <th class="sorting ui-state-default" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 127px;"><div class="DataTables_sort_wrapper">CHARGE TYPE<span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span></div></th>
                                             <th class="sorting ui-state-default" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 127px;"><div class="DataTables_sort_wrapper">START TIME<span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span></div></th>
                                            <th class="sorting ui-state-default" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 127px;"><div class="DataTables_sort_wrapper">END TIME<span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span></div></th>
                                            <th class="sorting ui-state-default" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 127px;"><div class="DataTables_sort_wrapper">SURGE FACTOR<span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span></div></th>
                                            <th class="sorting ui-state-default" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 127px;"><div class="DataTables_sort_wrapper">ENABLED/DISABLED<span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span></div></th>
                                           
                                            <th class="sorting ui-state-default" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Activities: activate to sort column ascending" style="width: 127px;"><div class="DataTables_sort_wrapper">POLYGON POINTS<span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span></div></th>

                                        </tr>
                                    </thead>
                                    <tbody id="table_zone">
                                        <?php
                                        $start = 1;
                                        foreach ($zones_data as $data) {
                                            ?>

                                            <tr role="row" class="odd">
                                                <td><?php echo $start++; ?></td>
                                                <td><?php echo $data['city']; ?></td>
                                                <td><?php echo $data['title']; ?></td>
                                                <td><?php if($data['charge_type']=='SUBCHARGE')echo "Sub Charge";else echo "Surge Charge";?></td>
                                                  <td><?php echo date('g:i A', strtotime($data['startTime']));?></td>
                                                <td><?php echo date('g:i A', strtotime($data['endTime']));?></td>
                                                <td><?php echo $data['surge_price']; ?></td>
                                                <td><?php if($data['surge_enabled'] == '1') echo '<b style=color:green>Enabled</b>'; else echo '<b style=color:red>Disabled</b>'?></td>
                                                <td><?php
                                                    $count = 1;
                                                    foreach ($data['polygonProps']['paths'] as $points) {

                                                        echo 'P' . $count . "(" . ucwords($points['lat']) . "," . ucwords($points['lng']) . ")";
                                                        echo " ";
                                                        if (($count % 2) == 0)
                                                            echo ' <br>';
                                                        $count++;
                                                    }
                                                    ?>
                                                    <input type="hidden" id="zone_<?= $data['_id']?>"/>
                                                </td>

                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>  
                                </table>
                            </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <div class="modal fade fill-in" id="addZoneModal" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <i class="pg-close"></i>
        </button>
        <div class="modal-dialog">
            
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="">Add new zone</h4>
                </div>
                <div class="modal-body" style="padding-top:1%;">
                    <div class="container-fluid bg-white" style="padding-top: 1%;padding-bottom: 1%;">
                        <div class="row">
                            <input id="pac-input" class="controls" type="text" placeholder="Search Location"  style="position: absolute;width: 15.5em;margin-left: 130px;z-index: 1050;">
                            <!--"onfocus="initAutocomplete(1)"-->
                            <div id="addmodalmap" class="col-md-8 col-sm-12">
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <form id="zoneform">

                                    <div class="control-group row" style="margin-top:8%;">
                                        <div class="col-sm-3"><label for="zonecity" class="control-label">Select City</label></div>
                                        <div class="col-sm-1 colon">:</div>
                                        <div class="col-sm-6" style="margin-top:-1%;"> 
                                            <!--<input type="text" id="zonecity" placeholder="Enter city name"  class="controls">-->
                                            <select data-init-plugin="select2" tabindex="-1" class="bg-white full-width select2-offscreen" id="city_selection_add">
                                                <option value="#">Select</option>
                                                <?php
                                                foreach ($cities as $data) {
                                                    ?>
                                                    <option value="<?php echo $data->City_Name; ?>" data-id="<?php echo $data->City_Id; ?>" lat="<?php echo $data->City_Lat; ?>" lng="<?php echo $data->City_Long; ?>"><?php echo $data->City_Name; ?></option>    
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="control-group row" style="margin-top:9%;">
                                        <div class="col-sm-3"><label for="zonetitle" class="control-label">Zone Title</label></div>
                                        <div class="col-sm-1 colon">:</div>
                                        <div class="col-sm-6 " style="margin-top:-4%;"> 
                                            <input type="text" id="zonetitle" placeholder="Enter zone title"  class="form-control">
                                        </div>
                                    </div>

                                    <div class="control-group row" style="margin-top:9%;">
                                        <div class="col-sm-3"><label for="charge_surge" class="control-label">Charge Type</label></div>
                                        <div class="col-sm-1">:</div>
                                        <div class="col-sm-8">
                                            <div class="radio radio-success no-margin">
                                                <input type="radio" checked="checked" value="SURGECHARGE" name="charge_type_add" id="charge_surge_add">
                                                <label for="charge_surge_add">Surge Charge</label>
                                                <input type="radio" value="SUBCHARGE" name="charge_type_add" id="charge_sub_add">
                                                <label for="charge_sub_add">Sub Charge</label>
                                            </div>
                                        </div>
                                    </div>
                                      <div class="control-group row" style="margin-top:6%;">
                                        <div class="col-sm-3"><label for="" class="control-label">Surge Time</label></div>
                                        <div class="col-sm-1">:</div>
                                        <div class="col-sm-8">
                                            
                                            <table style="margin-top:-2%">
                                                         <tbody><tr><td> Start</td><td>  </td>
                                                                 <td >  <div class="input-group bootstrap-timepicker timepicker" style="margin-left: 6%;width: 80%;">
                                                                     <input id="timepicker1" type="text" class="form-control input-small" name="addTimeslot[]">
                                                                     <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                     </div>
                                                                 </td></tr></tbody>
                                                     </table>

                                                   
                                        </div>


                                    </div>
                                       <div class="control-group row" style="margin-top:6%;">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-8">
                                             <table style="margin-top:-2%">
                                                         <tbody><tr><td> End</td><td>  </td>
                                                                 <td >  <div class="input-group bootstrap-timepicker timepicker" style="margin-left: 6%;width: 80%;">
                                                                     <input id="timepicker2" type="text" class="form-control input-small" name="addTimeslot[]">
                                                                     <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                     </div>
                                                                 </td></tr></tbody>
                                                     </table>
                                            
                                        </div>
                                    </div>
                                    
                                      <div class="control-group row" style="margin-top:6%;">
                                        <div class="col-sm-3"> <label for="">Surge Enable</label></div>
                                        <div class="col-sm-1 colon">:</div>
                                        <div class="col-sm-6" style="margin-top:-2%;"> 
                                         
                                            
                                             <div class="checkbox check-success  ">
                                                 <input type="checkbox"  value="1" id="add_surge_enabled" checked>
                                            <label for="add_surge_enabled">Enabled</label>
                                            <input type="hidden" value="1" id="surge_enable_status" name="surge_enable_status">
                                            </div>
                                           
                                        </div>
                                        
                                    </div>

                                    <div class="control-group row" style="margin-top:9%;">
                                        <div class="col-sm-3"> <label for="zonesurge_price">Surge Factor</label></div>
                                        <div class="col-sm-1 colon">:</div>
                                        <div class="col-sm-6" style="margin-top:-4%;"> 
                                            <input type="text" id="zonesurge_price" placeholder="Entet zone surge factor"  class="form-control">
                                        </div>
                                        <div style="clear:both;">
                                            <br>
                                            <span style="color: cornflowerblue;font-size: 16px;">*</span>
                                            <small>Surge factor is multiplied with fare setup for the linked vehicle types for that zone</small></div>
                                    </div>

                                    <br> <br>
                                    <div id="info"></div>
                                    <button id='savezone' style="margin-left: 16%;margin-top: 10%;" class="btncontrols">Save zone</button>
                                    <button id='clearzone' style="margin-left: 16%;margin-top: 10%;" class="btncontrols">Clear</button><br/>
                                    <p id="addmsg" class="waitmsg"></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 
    <div class="modal fade fill-in" id="editZoneModal" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <i class="pg-close"></i>
        </button>
        <div class="modal-dialog">
             <!--Modal content-->
            <div class="modal-content">
                <div class="modal-header">
<!--                                    <label>Select City</label>
                                    <select id="cities" style="width:9%;"></select>-->
                    
                </div>
                <div class="modal-body" style="padding-top:1%;">
                    <div class="container-fluid bg-white" style="padding-top: 1%;padding-bottom: 1%;">
                        <div class="row">
                            <input id="pac-input1" class="controls" type="text" placeholder="Search Location" style="position: absolute;width: 15.5em;margin-left: 130px;z-index: 1050;">
                            <!--onfocus="initAutocomplete(2)"--> 
                            <div id="editmodalmap" class="col-md-8 col-sm-12"></div>
                            <div class="col-md-4 col-sm-12">
                                <form id="zoneform">



                                    <div class="control-group row" style="margin-top:9%;">
                                        <div class="col-sm-3"><label for="editzonecity">Zone City</label></div>

                                        <div class="col-sm-1 colon">:</div>
                                        <div class="col-sm-6" style="margin-top:-2%;"> 
                                            <select data-init-plugin="select2" tabindex="-1" class="bg-white full-width select2-offscreen" id="cities" style="width:88%;"></select></div>
                                    </div>


                                    <div class="control-group row" style="margin-top:9%;">
                                        <div class="col-sm-3"><label for="editzonetitle">Zone Title</label></div>
                                        <div class="col-sm-1 colon">:</div>
                                        <div class="col-sm-6" style="margin-top:-2%;"> <input class="form-control" type="text" id="editzonetitle" placeholder="Enter zone title"  class="controls"></div>
                                    </div>
                                    
                                    <div class="control-group row" style="margin-top:9%;">
                                        <div class="col-sm-3"><label for="charge_surge" class="control-label">Charge Type</label></div>
                                        <div class="col-sm-1">:</div>
                                        <div class="col-sm-8">
                                            <div class="radio radio-success no-margin">
                                                <input type="radio" checked="checked" value="SURGECHARGE" name="charge_type_edit" id="charge_surge_edit">
                                                <label for="charge_surge_edit">Surge Charge</label>
                                                <input type="radio" value="SUBCHARGE" name="charge_type_edit" id="charge_sub_edit">
                                                <label for="charge_sub_edit">Sub Charge</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group row" style="margin-top:6%;">
                                        <div class="col-sm-3"><label for="" class="control-label">Surge Time</label></div>
                                        <div class="col-sm-1">:</div>
                                        <div class="col-sm-8">
                                            
                                             <table style="margin-top:-2%">
                                                         <tbody><tr><td> Start</td><td>  </td>
                                                                 <td >  <div class="input-group bootstrap-timepicker timepicker" style="margin-left: 6%;width: 80%;">
                                                                     <input id="timepicker3" type="text" class="form-control input-small" name="editTimeslot[]">
                                                                     <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                     </div>
                                                                 </td></tr></tbody>
                                                     </table>

                                                   
                                        </div>


                                    </div>
                                    
                                     <div class="control-group row" style="margin-top:6%;">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-8">
                                              <table style="margin-top:-2%">
                                                         <tbody><tr><td> End</td><td>  </td>
                                                                 <td >  <div class="input-group bootstrap-timepicker timepicker" style="margin-left: 6%;width: 80%;">
                                                                     <input id="timepicker4" type="text" class="form-control input-small" name="editTimeslot[]">
                                                                     <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                     </div>
                                                                 </td></tr></tbody>
                                                     </table>
                                            
                                        </div>
                                    </div>
                                    
                                        <div class="control-group row" style="margin-top:6%;">
                                        <div class="col-sm-3"> <label for="">Surge Enable</label></div>
                                        <div class="col-sm-1 colon">:</div>
                                        <div class="col-sm-6" style="margin-top:-2%;"> 
                                            <div class="checkbox check-success">
                                                 <input type="checkbox"  id="edit_surge_enabled">
                                            <label for="edit_surge_enabled">Enabled</label>
                                            <input type="hidden" value="1" id="edit_surge_enable_status" name="edit_surge_enable_status">
                                            </div>
                                           
                                        </div>
                                        
                                    </div>

                                    <div class="control-group row" style="margin-top:9%;">
                                        <div class="col-sm-3"><label for="editzonesurge_price">Surge Factor</label></div>
                                        <div class="col-sm-1 colon">:</div>
                                        <div class="col-sm-6" style="margin-top:-2%;">  <input class="form-control" type="text" id="editzonesurge_price" placeholder="Enter surge factor"  class="controls">
                                        </div>
                                        <div style="clear:both;">
                                            <br>
                                            <span style="color: cornflowerblue;font-size: 16px;">*</span>
                                            <small>Surge factor is multiplied with fare setup for the linked vehicle types for that zone</small></div>
                               
                                        
                                    </div>



                                    <br> <br>
                                    <div id="pointsinfo"></div>
                                    <button id='saveeditzone' style="margin-left: 31%;margin-top: 11%;" class="btncontrols">Save zone</button>
                                    <button id="deletezone" class="btncontrols">Delete zone</button><br/>
                                    <p id="editmsg" class="waitmsg"></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade fill-in" id="map_display" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <i class="pg-close"></i>
        </button>
        <div class="modal-dialog">
   
            <div class="modal-content">
                <div class="modal-header">
                     <h4 class="modal-title" style="">All Polygons</h4>
                </div>
                <div class="modal-body" style="padding-top:1%;">
                    <input id="pac-input2" class="controls" type="text" placeholder="Search Location" style="position: absolute;width: 15.5em;margin-left: 130px;z-index: 1050;margin-top: 21px;">
                     <!--onfocus="initAutocomplete(3)"-->
                    <select data-init-plugin="select2" tabindex="-1" class="bg-white select2-offscreen" id="city_selection_view" style="position: absolute;width: 15.5em;margin-left: 359px;z-index: 1050;margin-top: 20px;">
                        <option value="#">Select</option>
                        <?php
                        foreach ($cities as $data) {
                            ?>
                            <option value="<?php echo $data->City_Name; ?>" lat="<?php echo $data->City_Lat; ?>" lng="<?php echo $data->City_Long; ?>"><?php echo $data->City_Name; ?></option>    
                            <?php
                        }
                        ?>
                    </select>
                    <div class="container-fluid bg-white" style="padding-top: 1%;padding-bottom: 1%;">
                        <div id='viewmaploader' style="display:none;position: relative;z-index: 1500;background-color: white;height: 90vh;width: 100%;">
                            <img src="<?= base_url()?>/../../../images/loading.gif" style="position: absolute;left: 0;right: 0;top: 0;bottom: 0;margin: auto;"/>
                        </div>
                        <div id='viewmap' class="row">
                            <div id="mapPolygon"  class="col-sm-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   

</div>

