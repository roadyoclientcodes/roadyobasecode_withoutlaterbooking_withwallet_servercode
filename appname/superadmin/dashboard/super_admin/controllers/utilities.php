<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class utilities extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("utilmodal");
        $this->load->library('session');
//        $this->load->library('excel');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
    }
    
    function show_help($cat_id, $lan = 0, $scat_id = ''){
        $return['helpText'] = $this->utilmodal->get_cat_hlpText($cat_id);
        $return['lan'] = $lan;
        $this->load->view("show_help", $return);
    }
    
    function submit_help($lan = '0'){
        $data = $this->utilmodal->get_lan_hlpText($lan);
        $return['msg'] = $data['msg'];
        $this->load->view("submit_help", $return);
    }
    
    public function helpText() {
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
        $return['helpText'] = $this->utilmodal->get_cat_hlpText();
        error_reporting(0);

        $return['pagename'] = "company/helpText";
        $this->load->view("company", $return);
    }
    
    function get_subcat(){
        error_reporting(0);
        $this->load->library('mongo_db');
        $res = $this->mongo_db->get_one('hlp_txt', array('cat_id' => (int)$this->input->post('cat_id')));
        echo json_encode($res);
    }
      
    function grp_action($param = ''){
        error_reporting(0);
        if($param == 'del'){
            $this->load->library('mongo_db');
            $this->mongo_db->delete('group_hlp',array('grp_id' => (int)$this->input->post('id')));
            echo json_encode(array('msg' => '1'));die;
        }
        $this->utilmodal->grp_action();
    }
    
    function help_cat($param = '', $param2 = ''){
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
        if($param == 0){
            $return['edit_id'] = '';
            $return['cat_id'] = $param2;
        }else{
            
        }

        $return['pagename'] = "company/add_help_cat";
        $this->load->view("company", $return);
    }
    
    function help_edit($param = '', $param2 = ''){
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
//        if($param2 == ''){
        $return['edit_id'] = $param;
        $return['scat_id'] = $param2;
//        }else{
            
//        }
        $return['helpText'] = $this->utilmodal->get_cat_hlpText($param);
        $return['pagename'] = "company/edit_help_cat";
        $this->load->view("company", $return);
    }
    
    function help_cat_action($param = '', $param2 = ''){
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        if($param == 'del'){
            $this->load->library('mongo_db');
            if($param2 == ''){
                $this->mongo_db->delete('hlp_txt',array('cat_id' => (int)$this->input->post('id')));
                echo json_encode(array('msg' => '1'));die;
            }else{
                $this->mongo_db->updatewithpull('hlp_txt', array('sub_cat' => array('scat_id' => new MongoId($this->input->post('id')))), array('sub_cat.scat_id' => new MongoId($this->input->post('id'))));
                echo json_encode(array('msg' => '1'));die;
            }
        }
        $return['language'] = $this->utilmodal->help_cat_action();

        redirect(base_url() . "index.php/utilities/helpText");
    }
    
    function lan_help(){
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['pagename'] = "company/hlp_language";
        $this->load->view("company", $return);
    }
    
    
    function lan_action($param = ''){
        error_reporting(0);
//        if($param == 'del'){
//            $this->load->library('mongo_db');
//            $this->mongo_db->delete('lang_hlp',array('lan_id' => (int)$this->input->post('id')));
//            echo json_encode(array('msg' => '1'));die;
//        }
        $this->utilmodal->lan_action();
    }
    
    function cancellation(){
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['reasons'] = $this->utilmodal->get_can_reasons();
        $return['pagename'] = "company/cancell_reasons";
        $this->load->view("company", $return);
    }
    
    function cancell_act($param = ''){
        error_reporting(0);
        if($param == 'del'){
            $this->load->library('mongo_db');
            $this->mongo_db->delete('can_reason',array('res_id' => (int)$this->input->post('id')));
            echo json_encode(array('msg' => '1'));die;
        }
        $this->utilmodal->cancell_act();
    }
    
    public function supportText() {
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['suppText'] = $this->utilmodal->get_cat_support();
        error_reporting(0);

        $return['pagename'] = "company/supportText";
        $this->load->view("company", $return);
    }
    
    function support_cat($param2 = '', $param = ''){
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
        if($param2 == 0){
            $return['edit_id'] = '';
            $return['cat_id'] = $param;
        }else{
            
        }

        $return['pagename'] = "company/add_support_cat";
        $this->load->view("company", $return);
    }
    
    function support_edit($param = '', $param2 = ''){
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
//        if($param2 == ''){
        $return['edit_id'] = $param;
        $return['scat_id'] = $param2;
//        }else{
            
//        }
        $return['helpText'] = $this->utilmodal->get_cat_support($param);
        $return['pagename'] = "company/edit_support_text";
        $this->load->view("company", $return);
    }
    
    function get_subcat_support(){
        error_reporting(0);
        $this->load->library('mongo_db');
        $res = $this->mongo_db->get_one('support_txt', array('cat_id' => (int)$this->input->post('cat_id')));
        echo json_encode($res);
    }
    
    function support_action($param = '', $param2 = ''){
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        if($param == 'del'){
            $this->load->library('mongo_db');
            if($param2 == ''){
                $this->mongo_db->delete('support_txt',array('cat_id' => (int)$this->input->post('id')));
                echo json_encode(array('msg' => '1'));die;
            }else{
                $this->mongo_db->updatewithpull('support_txt', array('sub_cat' => array('scat_id' => new MongoId($this->input->post('id')))), array('sub_cat.scat_id' => new MongoId($this->input->post('id'))));
                echo json_encode(array('msg' => '1'));die;
            }
        }
        $return['language'] = $this->utilmodal->support_action();

        redirect(base_url() . "index.php/utilities/supportText");
    }
}