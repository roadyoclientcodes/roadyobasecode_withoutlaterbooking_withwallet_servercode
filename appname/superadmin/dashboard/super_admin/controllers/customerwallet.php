<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customerwallet extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("customerwalletmodal");
        $this->load->library('session');
//        $this->load->library('excel');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
    }

    public function index($loginerrormsg = NULL) {
        $data['loginerrormsg'] = $loginerrormsg;

        if ($this->session->userdata('table') == 'company_info') {
            redirect(base_url() . "index.php/superadmin/Dashboard");
        } else
            $this->load->view('company/login', $data);
    }

    function Logout() {

        $this->session->sess_destroy();
        redirect(base_url() . "index.php/superadmin");
    }

    public function userwallet() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['gat_way'] = "2";
        $data['pagename'] = "customerwallet/UserWalletList";
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SLNO', 'USER ID', 'USER NAME','EMAIL','PHONE','CURRENCY','WALLET BALANCE ('.currency.')', 'LAST CREDITED DATE','SELECT','OPERATION');
        $this->load->view("company", $data);
    }

    public function UserWalletStatement($id) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['driverId'] = $id;
        $data['driverinfo'] = $this->customerwalletmodal->GetuserDetails($id);
        $data['CustomerStatement'] = $this->customerwalletmodal->CustomerStatement_ajax($id);
        $data['pagename'] = "customerwallet/UserWalletStatement";
//        $this->load->library('Datatables');
//        $this->load->library('table');
//
//
//        $this->table->clear();
//        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
//            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
//            'heading_row_end' => '</tr>',
//            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;">',
//            'heading_cell_end' => '</th>',
//            'row_start' => '<tr>',
//            'row_end' => '</tr>',
//            'cell_start' => '<td>',
//            'cell_end' => '</td>',
//            'row_alt_start' => '<tr>',
//            'row_alt_end' => '</tr>',
//            'cell_alt_start' => '<td>',
//            'cell_alt_end' => '</td>',
//            'table_close' => '</table>'
//        );
//        $this->table->set_template($tmpl);
//        $this->table->set_heading('ID','TRANSACTION TYPE','INITIATED BY','TRANSACTION ID',
//                 'PAYMENT METHOD','PG COMMISSION','CREDITS BOUGHT','OPENING BALANCE', 'COLSING BALANCE');
        $this->load->view("company", $data);
    }

    public function CustomerStatement_ajax($param) {
        $this->customerwalletmodal->CustomerStatement_ajax($param);
    }

    public function Recharge($id) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['driverId'] = $id;
        $data['driverinfo'] = $this->customerwalletmodal->GetDriverDetils($id);
        $data['pagename'] = "company/DriverRechargeDetails";
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SLNO', 'RECHARGE AMOUNT', 'RECHARGE DATE', 'OPERATION');
        $this->load->view("company", $data);
    }

    public function DriverRechargeDetails_ajax($param) {

        $this->customerwalletmodal->DriverRechargeDetails($param);
    }

    public function RechargeOperation($for, $id, $masid = '') {

        $data = $this->customerwalletmodal->RechargeOperation($for, $id, $masid);
        if ($data == 44)
            redirect(base_url() . "index.php/superadmin/Recharge/" . $masid);
    }

    public function GetRechargedata_ajax($param = '') {

        $this->customerwalletmodal->GetRechargedata_ajax($param);
    }

    // new services

    public function CriditTowallet($args) {
     $this->customerwalletmodal->CriditTowallet();   
    }
    
    public function getCurrencySymb() {
        $this->customerwalletmodal->getCurrencySymb();   
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */